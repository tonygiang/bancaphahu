﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Enermy{
	public float speed;
}

[System.Serializable]
public class DirectorMove{
	public int frameCount;
	public int angel;
}

public class PlaneEnermyControll : MonoBehaviour
{
    public Enermy enermy;
    public int currentAngle;
    Vector2 speed;
    int framecount;
    public DirectorMove[] directors;
    [SerializeField]
    bool alone;
    Rigidbody2D rg;
    



    public void Init(int angle, DirectorMove[] directors,Vector3 initPost)
    {
        this.currentAngle = angle;
        this.directors = directors;
        alone = false;
        speed = new Vector2(enermy.speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), enermy.speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle));
        transform.localEulerAngles = new Vector3(0, 0, currentAngle);
        transform.position = initPost;
        framecount = 0;
    }

    public void InitAlone(int angle, Vector3 initPost)
    {
        this.currentAngle = angle;
        alone = true;
        // this.directors = directors;
        speed = new Vector2(enermy.speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), enermy.speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle));
        transform.localEulerAngles = new Vector3(0, 0, currentAngle);
        transform.position = initPost;
        framecount = 0;
    }

    void FixedUpdate()
    {
		transform.position +=  new Vector3(speed.x, speed.y, 0);
        framecount += 1;
		float startTime = 0;
		startTime = Time.time;
        //transform.position new Vector3(speed.x, speed.y, 0) * Time.deltaTime;
        if (!alone)
        foreach (DirectorMove director in directors)
        {
            if (framecount == director.frameCount)
            {
                StartCoroutine(ProcessChangeDirector(director.angel));
                break;
            }

        }
       	
		float endTime = Time.time;

		print ("starttime "+startTime + "endtime "+endTime);


        
        //if (framecount == 250)
        //    gameObject.SetActive(false);



    }
    IEnumerator ProcessChangeDirector(int nextAngel)
    {
        currentAngle = (int)transform.localEulerAngles.z;
       if(currentAngle<nextAngel)
        do
        {
            currentAngle += 1;
            speed = new Vector2(enermy.speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), enermy.speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle));
            transform.localEulerAngles += new Vector3(0, 0, 1);
            yield return new WaitForFixedUpdate();

        } while (currentAngle < nextAngel);
       else
            do
            {
                currentAngle -= 1;
                speed = new Vector2(enermy.speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), enermy.speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle));
                transform.localEulerAngles -= new Vector3(0, 0, 1);
                yield return new WaitForFixedUpdate();

            } while (currentAngle > nextAngel);

    }

  




   

   
  
}
