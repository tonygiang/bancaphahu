﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
using System.IO;
using System.IO.Compression;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Linq;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

///-----------------------------------------------------------------------------------------
///   Namespace:      BE
///   Class:          SceneSlotGame
///   Description:    process user input & display result
///   Usage :
///   Author:         BraveElephant inc.
///   Version: 		  v1.0 (2015-08-30)
///-----------------------------------------------------------------------------------------
namespace BE
{

	public class SceneSlotGame : MonoBehaviour
	{

		public	static SceneSlotGame instance;

		public	static int uiState = 0;
		// popup window shows or not
		public  static BENumber	Win;
		// total win number

		public 	SlotGame	Slot;
		// slot game class

		public	Text textLine;
		// user selected line info text
		public	Text textBet;
		// current betting info text
		//		public	Text		textTotalBet;	// total betting info
		//		public	Text		textTotalWin;	// total win info
		public	Text textGold;
		// user gold info
		public	Text textJackPot;
		// user jackpot info

		//	public	Text		textInfo;		// other info text

		//		public 	Button		btnBuy;			// buy gold button
		//		public 	Button		btnMenu;		// call setting dialog
		//		public 	Button		btnPayTable;	// show pay table info dialog
		//		public 	Button		btnMaxLine;		// line count to max number
		//		public 	Button		btnLines;		// change line selected
		//		public 	Button		btnBet;			// change bet
		//		public 	Button		btnDouble;		// start double game
		public 	Button btnSpin, btnBetIncrease, btnBetDecrease, btnLineIncrease, btnLineDecrease, btnFast;
		// start spin
		public 	Toggle toggleAutoSpin;
		// auto spin toggle button

		public 	GameObject	FreeSpinBackground, PickLinePanel, PickPanel, HuongDanPanel, LichsuPanel, VinhDanhPanel;
		public Transform ObjectPool;
		// background og game scene
		public GemInfo gemInfo;
		public GameObject StopAutoSpin;
		private bool isAutoSpinOn = false;
		private int CurrentIndexRoom = 0;
		public List<int> Line;
		//private int LineCount = 5;
		public DataRoll dataRoll;
		private int jackpotInGame = 0;
		private List<JackPotInfo> lJackpotInfo;
		[HideInInspector]
		public int FreeSpinWin = 0;
		private bool isGetData = false;
		public bool isRooling = false;
		public bool IsSpinFast = false;
		public int CurrentGem = 0;

		void Awake ()
		{
			instance = this;
			Line = new List<int> ();
			for (int i = 0; i < 25; i++) {
				Line.Add (i + 1);
			}
			//LoadUi ();
		}

		void Start ()
		{
			SFSManager.instance.joinSlot ();
			SFSManager.instance.joinRoomSlot ();
			InvokeRepeating ("pingToSmartFox", 0f, 10f);


			textLine.text = Line.Count.ToString ();
			InvokeRepeating ("AutoSpin", 0.5f, 1f);
			InvokeRepeating ("getJackPotValue", 0, 0.5f);
			// set range of numbers and type
			//BESetting.Gold.AddUIText(textGold);
//			Win = new BENumber(BENumber.IncType.VALUE, "#,##0.00", 0, 10000000000, 0);			
//			Win.AddUIText(textTotalWin);

			//set saved user gold count to slotgame
			//Slot.Gold = (float)BESetting.Gold.Target ();
			//set win value to zero
			//	Win.ChangeTo(0);

			//UpdateUI ();

			//double button show only user win
			//btnDouble.gameObject.SetActive (false);
			//	textInfo.text = "";
		}

		void Update ()
		{
		
			// if user press 'escape' key, show quit message window
//			if ((uiState == 0) && Input.GetKeyDown (KeyCode.Escape)) { 
//				UISGMessage.Show ("Quit", "Do you want to quit this program ?", MsgType.OkCancel, MessageQuitResult);
//			}
			
			//Win.Update ();

			// if auto spin is on or user has free spin to run, then start spin
			textLine.text = Line.Count.ToString ();

		}

		void AutoSpin ()
		{
			if (isRooling) {
				return;
			}
			if ((toggleAutoSpin.isOn || Slot.gameResult.InFreeSpin ()) && Slot.Spinable () && isGetData) {
				//Debug.Log ("Update Spin");
				ButtonEnable (false);
				RequestRollToServer ();
				if (!isAutoSpinOn) {
					toggleAutoSpin.transform.gameObject.SetActive (false);
					//toggleAutoSpin.transform.GetComponent<Button>().interactable = false;
					//StopAutoSpin.transform.GetComponent<Button>().interactable = true;
				}
				isAutoSpinOn = true;
			} 

			if (toggleAutoSpin.isOn) {
				StopAutoSpin.SetActive (true);
				toggleAutoSpin.gameObject.SetActive (false);
			} else {
				StopAutoSpin.SetActive (false);
				toggleAutoSpin.gameObject.SetActive (true);
			}
		}

		public void OnButtonStopAutoSpin ()
		{
			StopAutoSpin.transform.GetComponent<Button> ().interactable = false;
			toggleAutoSpin.isOn = false;
			isAutoSpinOn = false;
			BEAudioManager.SoundPlay (0);
		}

		public void spinFast ()
		{
			IsSpinFast = !IsSpinFast;
			if (IsSpinFast) {
				btnFast.transform.GetChild (0).transform.GetComponent<Text> ().text = "Quay nhanh (Bật)";
			} else {
				btnFast.transform.GetChild (0).transform.GetComponent<Text> ().text = "Quay nhanh (Tắt)";
			
			}
		}

		public void pingToSmartFox ()
		{
			SFSManager.instance.SlotPing ();
		}

		#region == comment section ==

		// when user pressed 'ok' button on quit message.
		//		public void MessageQuitResult (int value)
		//		{
		//			//Debug.Log ("MessageQuitResult value:"+value.ToString ());
		//			if (value == 0) {
		//				Application.Quit ();
		//			}
		//		}

		//user clicked shop button, then show shop
		//		public void OnButtonShop ()
		//		{
		//			BEAudioManager.SoundPlay (0);
		//			UISGShop.Show ();
		//		}

		// user clicked option button, then show option
		//		public void OnButtonOption ()
		//		{
		//			BEAudioManager.SoundPlay (0);
		//			UISGOption.Show ();
		//		}
		//
		//		// user clicked paytable button, then show paytable
		//		public void OnButtonPayTable ()
		//		{
		//			BEAudioManager.SoundPlay (0);
		//			UISGPayTable.Show (Slot);
		//		}
		//
		//		// user clicked Max line button, then ser slot's line count to max
		//		public void OnButtonMaxLine ()
		//		{
		//			BEAudioManager.SoundPlay (0);
		//			Slot.LineSet (Slot.Lines.Count);
		//		//	UpdateUI ();
		//		}
		//
		//		//user clicked line button, increase line count
		//		public void OnButtonLines ()
		//		{
		//			BEAudioManager.SoundPlay (0);
		//			Slot.LineSet (Slot.Line + 1);
		//			//UpdateUI ();
		//		}
		//
		//		// user clicked bet button, then increase bet number
		//		public void OnButtonBet ()
		//		{
		//			BEAudioManager.SoundPlay (0);
		//			Slot.BetSet (Slot.Bet + 1);
		//		//	UpdateUI ();
		//		}
	
		//user clicked double button, then start double game
		//		public void OnButtonDouble ()
		//		{
		//			BEAudioManager.SoundPlay (0);
		//			UIDoubleGame.Show (Slot.gameResult.GameWin);
		//		}

		#endregion

		public void RequestRollToServer ()
		{
			isRooling = true;

			if (CurrentIndexRoom == 3)
				CurrentIndexRoom--;
			else if (CurrentIndexRoom == -1) {	
				CurrentIndexRoom++;
			}
			var Arr = textGold.text.Split ('.');
			string currentTextgold = "";
			foreach (string item in Arr) {
				currentTextgold += item;
			}
			int room = gemInfo.roomInfo [CurrentIndexRoom].roomId;
			int betValue = gemInfo.roomInfo [CurrentIndexRoom].betValue;		
//			if (!SlotGame.instance.IsFreeSpinMode) {
//				textGold.text = SaveLoadData.FormatMoney (int.Parse (currentTextgold) - (betValue * Line.Count));
//			}

			ButtonEnable (false);
			SFSManager.instance.Roll (room, betValue, Line);




		}

		void GoToShopGem (MessageBoxCallBack result)
		{
			OnButtonStopAutoSpin ();
			if (result.Equals (MessageBoxCallBack.OK)) {
				Shop.Instance.OpenShop ();
			}

		}
		// user clicked spin button
		public void OnButtonSpin (ISFSObject obj)
		{
			isGetData = true;
			//Debug.Log ("OnButtonSpin");
			dataRoll = new DataRoll ();
			if (obj.GetBool ("showShop")) {
				//MessageBox.instance.Show ("THÔNG BÁO", "Bạn không đủ kim cương. Nạp thêm kim cương hoặc nhận quà hằng ngày để tiêp tục chơi game nhé!");
				MessageBox.instance.ShowGoToShop ("THÔNG BÁO", "Bạn không đủ kim cương. Nạp thêm kim cương hoặc nhận quà hằng ngày để tiêp tục chơi game nhé!", MessageBoxType.OK_CANCEL, GoToShopGem);
				OnButtonStopAutoSpin ();
				ButtonEnable (true);
				//SFSManager.instance.joi
				//LoadUi ();

			}
			string json = obj.GetUtfString ("roll_result");
			if (string.IsNullOrEmpty (json)) {
				ButtonEnable (true);
				OnButtonStopAutoSpin ();
				isRooling = false;
				return;
			}
			dataRoll = JsonConvert.DeserializeObject<DataRoll> (json);
			BEAudioManager.SoundPlay (0);

			// start spin

			// if spin succeed
			if (obj.GetBool ("Success")) {				
				textGold.text = SaveLoadData.FormatMoney (dataRoll.gemStart);
				Slot.Spin ();
			}
//			} else {
//				// if spin fails
//				// show Error Message
//				MessageBox.instance.Show ("THÔNG BÁO", "Đã có lỗi xảy ra: " + dataRoll.Message);
//				OnButtonStopAutoSpin ();
//				ButtonEnable (true);
//			}
		}

		// if user clicked auto spin
		public void AutoSpinToggled (bool value)
		{
			
			if (value && !isGetData) {
				
				ButtonEnable (false);
				RequestRollToServer ();
			}
			BEAudioManager.SoundPlay (0);
		}

		// update ui text & win number
		//		public void UpdateUI ()
		//		{
		//			textLine.text = Slot.Line.ToString ();
		//			textBet.text = SaveLoadData.FormatMoney ((int)(Slot.RealBet));
		//			//textTotalBet.text = Slot.TotalBet.ToString ("#0.00");
		//		//	Win.ChangeTo (Slot.gameResult.GameWin);
		//		}

		//		public void LoadUi ()
		//		{
		//			textJackPot.text = SaveLoadData.FormatMoney (UIMenu.Instance.SlotRoom1JackpotValue);
		//			SlotNetworkManager.Instance.GetGemInfo ();
		//			//	SlotNetworkManager.Instance.JackPotData ();
		//
		//		}

		public void GetGemInfo (ISFSObject obj)
		{
			
			gemInfo = new GemInfo ();
			string json = obj.GetUtfString ("slot_info");
			gemInfo = JsonConvert.DeserializeObject<GemInfo> (json);
			gemInfo.roomInfo = gemInfo.roomInfo.OrderBy (m => m.roomId).ToList ();
			textBet.text = SaveLoadData.FormatMoney (gemInfo.roomInfo [CurrentIndexRoom].betValue * Line.Count);
			textGold.text = SaveLoadData.FormatMoney (gemInfo.gem);
			CurrentGem = gemInfo.gem;
			LoadLevel.Instance.HideLoadingPanel ();

			if (gemInfo.freeSpin > 0) {
				if (gemInfo.freeSpinBet == 100) {
					CurrentIndexRoom = 0;
				} else if (gemInfo.freeSpinBet == 1000) {
					CurrentIndexRoom = 1;
				} else if (gemInfo.freeSpinBet == 10000) {
					CurrentIndexRoom = 2;
				}
				Line = gemInfo.freeSpinLine.ToList ();
				SlotGame.instance.IsReturnToFreeSpin = true;
				SlotGame.instance.totalFreeSpinTime = gemInfo.freeSpin;

				StartCoroutine (SlotGame.instance.ShowFreeSpin ());

				SlotGame.instance.IsReturnToFreeSpin = true;
			}

		}



		public void IncreaseBet ()
		{
			CurrentIndexRoom++;
			if (CurrentIndexRoom > 2) {
				CurrentIndexRoom = 0;
			}
			textBet.text = SaveLoadData.FormatMoney (gemInfo.roomInfo [CurrentIndexRoom].betValue * Line.Count);
			StartCoroutine (updateJackPot (0, (lJackpotInfo [CurrentIndexRoom].jackPot)));

		}

		public void DecreaseBet ()
		{
			
			CurrentIndexRoom--;
			if (CurrentIndexRoom < 0) {
				CurrentIndexRoom = 2;
			}
			textBet.text = SaveLoadData.FormatMoney (gemInfo.roomInfo [CurrentIndexRoom].betValue * Line.Count);
			StartCoroutine (updateJackPot (0, (lJackpotInfo [CurrentIndexRoom].jackPot)));

		}


		// enable or disable button inputs
		public void ButtonEnable (bool bEnable)
		{			
			btnSpin.interactable = bEnable;
			btnBetDecrease.interactable = bEnable;
			btnBetIncrease.interactable = bEnable;
			btnLineDecrease.interactable = bEnable;
			btnLineIncrease.interactable = bEnable;
			toggleAutoSpin.interactable = bEnable;
			btnFast.interactable = bEnable;

		}

		//------------------------------------------
		//callback functions
		// when double game ends
		//		public void OnDoubleGameEnd (float delta)
		//		{
		//			//Debug.Log("OnDoubleGameEnd:"+delta.ToString ());
		//
		//			//change user gold by delta (change gold value)
		//			Slot.Gold += delta;
		//			BESetting.Gold.ChangeTo (Slot.Gold);
		//			BESetting.Save ();
		//			Slot.gameResult.GameWin += delta;
		//		//	Win.ChangeTo (Slot.gameResult.GameWin);
		////			btnDouble.gameObject.SetActive (false);
		//		}

		// when reel stoped
		public void OnReelStop (int value)
		{
			//Debug.Log ("OnReelStop:"+value.ToString());
			BEAudioManager.SoundPlay (2);
		}

		// when spin completed
		public void OnSpinEnd ()
		{
			//Debug.Log("OnSpinEnd");

			// if user has win
			//	if (Slot.gameResult.Wins.Count != 0)
			//	textInfo.text = "Win "+Slot.gameResult.Wins.Count.ToString ()+" Lines ";

			//	UpdateUI ();
			// increase user gold
			//	BESetting.Gold.ChangeTo (Slot.Gold);
			//	BESetting.Save ();
		}

		//when splash window shows
		public void OnSplashShow (int value)
		{
			//Debug.Log ("OnSplashShow:"+value.ToString());
			BEAudioManager.SoundPlay (3);
			UISGSplash.Show (value);

			// change background image if free spin
			if (value == (int)SplashType.FreeSpin) {
				FreeSpinBackground.SetActive (true);
			} else if (value == (int)SplashType.FreeSpinEnd) {
				FreeSpinBackground.SetActive (false);
			} else {
			}
		}

		// when splash hide
		public void OnSplashHide (int value)
		{
			//Debug.Log ("OnSplashHide:"+value.ToString());
			StartCoroutine (SlotSplashHide (value, 0.5f));
		}

		// when all splash works end
		public void OnSplashEnd ()
		{
			//Debug.Log ("OnSplashEnd");
			isRooling = false;
			//		textInfo.text = "";
			if (!toggleAutoSpin.isOn && SlotGame.instance.IsFreeSpinMode == false) {
				ButtonEnable (true);
			}
			if (SceneSlotGame.instance.dataRoll.freeSpin != 0) {
				StopAutoSpin.transform.GetComponent<Button> ().interactable = false;
			} else {
				StopAutoSpin.transform.GetComponent<Button> ().interactable = true;
			}
			

		}

		// splash idx change
		public IEnumerator SlotSplashHide (int value, float fDelay)
		{
			
			if (fDelay > 0.01f)
				yield return new WaitForSeconds (fDelay);
			
			Slot.SplashCount [value] = 0;
			Slot.SplashActive++;
			Slot.InSplashShow = false;
		}

		void getJackPotValue ()
		{
			SFSManager.instance.GetSlotJackPot ();
		}

		public void GetJackPot (ISFSObject obj)
		{
			if (CurrentIndexRoom == 3)
				CurrentIndexRoom = 2;
			else if (CurrentIndexRoom == -1) {	
				CurrentIndexRoom = 0;
			}
			var json = obj.GetUtfString ("slotjp");
			lJackpotInfo = JsonConvert.DeserializeObject<List<JackPotInfo>> (json);
			JackPotInfo currentJackpot = lJackpotInfo.FirstOrDefault (m => m.roomId == CurrentIndexRoom + 1);
			if (currentJackpot == null) {
				return;
			}
			StartCoroutine (updateJackPot (jackpotInGame, (currentJackpot.jackPot)));
		}

		IEnumerator updateJackPot (int firstValue, int secondValue)
		{
			float elapsedTime = 0;
			float seconds = 0.4f;
			do {
				textJackPot.text = SaveLoadData.FormatMoney ((int)Mathf.Floor (Mathf.Lerp (firstValue, secondValue, (elapsedTime / seconds))));
				elapsedTime += Time.deltaTime;
				yield return null;
			} while(elapsedTime < seconds);
			textJackPot.text = SaveLoadData.FormatMoney (secondValue);
			jackpotInGame = secondValue;
			SaveLoadData.Instance.jackpotNumberGem = secondValue;
		}

		public void OutRoom ()
		{
			SoundManager.Instance.ButtonSound ();
			MessageBox.instance.Show ("Thoát", "Bạn có chắc chắn thoát không?", MessageBoxType.OK_CANCEL, BackConfirm);
		}

		private void BackConfirm (MessageBoxCallBack result)
		{
			if (result.Equals (MessageBoxCallBack.OK)) {
				SlotGame.instance.totalFreeSpinTime = 0;
				SlotGame.instance.IsFreeSpinMode = false;
				SlotGame.instance.IsDoneFreeSpin = false;
				SlotGame.instance.IsReturnToFreeSpin = false;
				SFSManager.instance.OutSlot ();
				LoadLevel.Instance.ShowLoadingPanel ("Menu");

				//SFSManager.instance.USER_OUT_ROOM ();

			}
		}

		public void PickLine (string lineNumber)
		{
			
			foreach (Transform item in PickLinePanel.transform) {
				item.GetChild (0).gameObject.SetActive (false);
			} 
			foreach (Transform item in SlotGame.instance.LinePanel.transform) {
				foreach (Transform line in item.transform) {
					line.gameObject.SetActive (false);		
				}
			}

			if (lineNumber == "le") {
				Line = new List<int> ();
				for (int i = 0; i < 25; i++) {
					if ((i + 1) % 2 != 0) {
						Line.Add (i + 1);
					}
				}

			} else if (lineNumber == "chan") {
				Line = new List<int> ();
				for (int i = 0; i < 25; i++) {
					if ((i + 1) % 2 == 0) {
						Line.Add (i + 1);
					}
				}

			} else if (lineNumber == "tatCa") {
				Line = new List<int> ();
				for (int i = 0; i < 25; i++) {
					Line.Add (i + 1);
				}

			} else if (lineNumber == "boChon") {
				Line = new List<int> ();
				Line.Add (1);

			} else {
				if (Line.Contains (Int32.Parse (lineNumber))) {
					if (Line.Count > 1) {
						Line.Remove (Int32.Parse (lineNumber));
						foreach (Transform item in PickLinePanel.transform) {
							foreach (int line in Line) {
								if (item.gameObject.name == "line (" + lineNumber + ")") {
									item.GetChild (0).gameObject.SetActive (false);
								}
							} 
						}
						foreach (Transform item in SlotGame.instance.LinePanel.transform) {
							foreach (Transform line in item.transform) {
								foreach (int data in Line) {
									if (line.gameObject.name == "Line (" + lineNumber + ")") {
										line.gameObject.SetActive (false);
									}
								}		
							}
						}
					}

				} else {
					Line.Add (Int32.Parse (lineNumber));
				}
			}

			foreach (Transform item in PickLinePanel.transform) {
				foreach (int line in Line) {
					if (item.gameObject.name == "line (" + line + ")") {
						item.GetChild (0).gameObject.SetActive (true);
					}
				} 
			}
			foreach (Transform item in SlotGame.instance.LinePanel.transform) {
				foreach (Transform line in item.transform) {
					foreach (int data in Line) {
						if (line.gameObject.name == "Line (" + data + ")") {
							line.gameObject.SetActive (true);
						}
					}		
				}
			}
			textLine.text = Line.Count.ToString ();
			textBet.text = SaveLoadData.FormatMoney (gemInfo.roomInfo [CurrentIndexRoom].betValue * Line.Count);
		}

		public void openAndclosePickLinePanel (bool status)
		{
			
			PickPanel.SetActive (status);
			FreeSpinBackground.SetActive (status);
		}

		public void openAndcloseLichSuPanel (bool status)
		{
			if (status) {
				SFSManager.instance.GetHistorySlot ();
				//SFSManager.instance.GetHistoryTaiXiu()
			}
			LichsuPanel.SetActive (status);
			FreeSpinBackground.SetActive (status);
		}

		public void SetDataLichSu (ISFSObject obj)
		{
			List<LichSu> lichSu = new List<LichSu> ();
			string json = obj.GetUtfString ("roll_history");
			if (string.IsNullOrEmpty (json)) {
				return;
			}
			lichSu = JsonConvert.DeserializeObject<List<LichSu>> (json);
			var content = LichsuPanel.transform.GetChild (1).transform.GetChild (0).transform.GetChild (0).gameObject;
			Transform viewPort = LichsuPanel.transform.GetChild (1).transform.GetChild (0).transform;
			Rect rect = viewPort.GetComponent<RectTransform> ().rect;
			rect.height = 50 * lichSu.Count ();
			viewPort.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, rect.height);


			foreach (Transform item in content.transform) {
				if (item.gameObject.name.Contains ("Clone")) {
					Destroy (item.gameObject);
				}
			}
		
			foreach (var item in lichSu) {
				var Data = Instantiate (content.transform.GetChild (0).transform.gameObject);
				Data.gameObject.SetActive (true);
				Data.transform.SetParent (content.transform);
				Data.transform.localScale = new Vector3 (1f, 1f, 1f);
				Data.transform.localPosition = new Vector3 (1f, 1f, 1f);
				Data.transform.GetChild (0).gameObject.GetComponent<Text> ().text = item.timelog;
				if (item.type == 1) {
					Data.transform.GetChild (1).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (item.totalBet);
				} else {
					Data.transform.GetChild (1).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (0);
				}
				Data.transform.GetChild (2).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (item.profit + (item.betValue * item.betline));
				if (item.profit >= 0) {
					Data.transform.GetChild (3).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (item.profit);
				} else {
					Data.transform.GetChild (3).gameObject.GetComponent<Text> ().text = "-" + SaveLoadData.FormatMoney (Math.Abs (item.profit));
				}

			}
		}

		public void openAndcloseVinhDanhPanel (bool status)
		{
			if (status) {
				SFSManager.instance.GetTopSlot ();
			}
			VinhDanhPanel.SetActive (status);
			FreeSpinBackground.SetActive (status);

		}

		public void SetDataVinhDanh (ISFSObject obj)
		{
			
			List<VinhDanh> vinhDanh = new List<VinhDanh> ();
			string json = obj.GetUtfString ("vinh_danh");
			vinhDanh = JsonConvert.DeserializeObject<List<VinhDanh> > (json);
			var content = VinhDanhPanel.transform.GetChild (1).transform.GetChild (0).transform.GetChild (0).gameObject;
			foreach (Transform item in content.transform) {
				if (item.gameObject.name.Contains ("Clone")) {
					Destroy (item.gameObject);
				}
			}

			Transform viewPort = VinhDanhPanel.transform.GetChild (1).transform.GetChild (0).transform;
			Rect rect = viewPort.GetComponent<RectTransform> ().rect;
			rect.height = 50 * vinhDanh.Count ();
			viewPort.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, rect.height);

			foreach (var item in vinhDanh) {
				var Data = Instantiate (content.transform.GetChild (0).transform.gameObject);
				Data.gameObject.SetActive (true);
				Data.transform.SetParent (content.transform);
				Data.transform.localScale = new Vector3 (1f, 1f, 1f);
				Data.transform.localPosition = new Vector3 (1f, 1f, 1f);
				Data.transform.GetChild (0).gameObject.GetComponent<Text> ().text = item.time;
				Data.transform.GetChild (1).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (item.phong);
				Data.transform.GetChild (2).gameObject.GetComponent<Text> ().text = item.username;
				Data.transform.GetChild (3).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (item.amount);

			}
		}

		public void openAndcloseHuongDanPanel (bool status)
		{
			HuongDanPanel.SetActive (status);
			FreeSpinBackground.SetActive (status);
		}

		public void OpenShop ()
		{
			Shop.Instance.OpenShop ();
		}
	}

}
