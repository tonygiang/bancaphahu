﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
using System.IO;
using System.IO.Compression;
using System.Collections;
using System.Collections.Generic;
using MersenneTwister;
using System.Linq;

///-----------------------------------------------------------------------------------------
///   Namespace:      BE
///   Class:          SlotGame
///   Description:    All about slot game logic
///   Usage :
///   Author:         BraveElephant inc.
///   Version: 		  v1.0 (2015-08-30)
///-----------------------------------------------------------------------------------------
namespace BE
{

	// spin result code
	public enum SlotReturnCode
	{
		Success = 0,
		// no problem
		InSpin = 1,
		// is in spin now
		NoGold = 2,
		// not enough gold
	};

	// random number generation type
	public enum RNGType
	{
		UnityRandom = 0,
		DotNetRandom = 1,
		MersenneTwister	= 2,
	};

	// symbol types
	public enum SymbolType
	{
		Normal = 0,
		Wild = 1,
		Wild2X = 2,
		Wild3X = 3,
		Scatter = 4,
		Bonus = 5,
		WildFC = 6,
		// added
	};

	public enum SymbolRank
	{
		A0 = 0,
		B0 = 1,
		C0 = 2,
		C1 = 3,
		D0 = 4,
		D1 = 5,
		E0 = 6,
		E1 = 7,
		E2 = 8,
		F0 = 9,
		F1 = 10,
		F2 = 11,
		F3 = 12,
		G0 = 13,
		G1 = 14,
		G2 = 15,
		G3 = 16,
		G4 = 17,
		JACKPOT0 = 18,
		WILD0 = 19,
		SCATTER0 = 20,
		BONUS0 = 21,

	} ;

	// define a symbol
	[System.Serializable]
	public class Symbol
	{
		public  GameObject prfab;
		public 	SymbolType	type;
		public 	SymbolRank rank;
		// symbol types
		public  bool frequencyPerReel;
		// set different frequency to each reel
		public  int[] frequency = new int[10];
		// showing probability (permillage)
		public  int[] reward = new int[10];
		// reqard gold or freespin count
		
		public Symbol (string _path, SymbolType _type, string _frequency, string _reward, SymbolRank _rank)
		{
			BaseSet (_path, _type, _reward, _rank);

			frequencyPerReel = true;
			string[] frequencySub = _frequency.Split (',');
			for (int i = 0; i < 10; ++i)
				frequency [i] = int.Parse (frequencySub [i]);
		}

		public Symbol (string _path, SymbolType _type, int _frequency, string _reward, SymbolRank _rank)
		{
			BaseSet (_path, _type, _reward, _rank);

			frequencyPerReel = false;
			for (int i = 0; i < 10; ++i)
				frequency [i] = _frequency;
		}

		private void BaseSet (string _path, SymbolType _type, string _reward, SymbolRank _rank)
		{
			type = _type;
			rank = _rank;
			SetPrefab ((GameObject)Resources.Load (_path, typeof(GameObject)));

			string[] rewardSub = _reward.Split (',');
			for (int i = 0; i < rewardSub.Length; ++i)
				reward [i] = int.Parse (rewardSub [i]);
		}

		// is thos symbol is wild card ?
		public bool IsWild ()
		{ 
			return ((type == SymbolType.Wild) || (type == SymbolType.Wild2X) || (type == SymbolType.Wild3X)) ? true : false; 
		}

		public void SetPrefab (GameObject go)
		{
			prfab = go;
			if (go != null)
				BEObjectPool.AddPoolItem (go, 20);
		}

	}
	
	// define a line
	[System.Serializable]
	public class Line
	{
		
		public int[] Slots = new int [10];
		public Color color;

		public Line (int s0, int s1, int s2, int s3, int s4, string strColor)
		{
			Slots [0] = s0;
			Slots [1] = s1;
			Slots [2] = s2;
			Slots [3] = s3;
			Slots [4] = s4;
			color = HexToColor (strColor);
		}

		public Color HexToColor (string hex)
		{
			byte r = byte.Parse (hex.Substring (0, 2), System.Globalization.NumberStyles.HexNumber);
			byte g = byte.Parse (hex.Substring (2, 2), System.Globalization.NumberStyles.HexNumber);
			byte b = byte.Parse (hex.Substring (4, 2), System.Globalization.NumberStyles.HexNumber);
			return new Color32 (r, g, b, 255);
		}
	}

	// define marker - indicate line number
	[System.Serializable]
	public class Marker
	{
		public 	GameObject go;
		public 	int index;
		public 	Image image;
		public 	Text text;

		public Marker (int _index, Line ld, GameObject _go)
		{
			index = _index;
			go = _go;
			image = go.GetComponent<Image> ();
			text = go.transform.Find ("Text").GetComponent<Text> ();

			image.color = ld.color;
			text.text = (index + 1).ToString ();
		}
	}

	// splash window types
	public enum SplashType
	{
		None = -1,
		FiveInRow	= 0,
		// same symbols are five in row
		BigWin = 1,
		Line = 2,
		FreeSpin = 3,
		// free spin start
		FreeSpinEnd = 4,
		// free spin end(result)
		Max = 5,
	};

	// win class - which line is matched
	[System.Serializable]
	public class WinItem
	{
		public  int Line;
		// line id
		public  int SymbolIdx;
		// symbol id
		public  int Matches;
		// match count
		public  float	WinGold;
		// win reward
		
		public WinItem (int _Line, int _SymbolIdx, int _Matches, float _WinGold)
		{
			Line = _Line;
			SymbolIdx	= _SymbolIdx;
			Matches = _Matches;
			WinGold = _WinGold;
		}
	}

	public class TestItem
	{
		public  int ID;
		public  int Hit;
		public  float	Bet;
		public  float	WinGold;

		public TestItem (int _ID)
		{
			ID = _ID;
			Hit = 0;
			Bet = 0.0f;
			WinGold = 0.0f;
		}
	}

	// Core of Slot game - divided from SlotGame to simulate return rate
	public class GameResult
	{
		
		public 	float GameWin = 0.0f;
		public 	float SpinWin = 0.0f;
		public	List<WinItem> Wins = new List<WinItem> ();
		public	int NewFreeSpinCount = 0;
		public	int FreeSpinCount = 0;
		public	int FreeSpinTotalCount = 0;
		public	float FreeSpinTotalWins = 0.0f;
		public	int FreeSpinAccumCount = 0;
		public	float FreeSpinAccumWins = 0.0f;

		public	int Line = 0;
		public	float RealBet = 0.0f;
		public	float TotalBet = 0.0f;

		public	List<TestItem> TestSymbols = new List<TestItem> ();
		public	List<TestItem> TestLines = new List<TestItem> ();

		//reset all game related variables
		public void ResetGame ()
		{
			GameWin = 0.0f;
			ResetSpin ();
			FreeSpinCount = 0;
			FreeSpinTotalCount = 0;
			FreeSpinTotalWins = 0.0f;
			FreeSpinAccumCount = 0;
			FreeSpinAccumWins = 0.0f;
		}

		//reset single spin related variables
		public void ResetSpin ()
		{
			SpinWin = 0.0f;
			Wins.Clear ();
			NewFreeSpinCount = 0;
		}

		public void Spin ()
		{

			// if not freespin, reset freespin related variables
			if (FreeSpinCount > FreeSpinTotalCount) {
				FreeSpinCount = 0;
				FreeSpinTotalCount = 0;
				FreeSpinTotalWins = 0.0f;
			}
		}

		public bool InFreeSpin ()
		{
			
			return SlotGame.instance.IsFreeSpinMode;
		}
		
	}


	[System.Serializable]
	public class IntEvent : UnityEvent<int>
	{

	}

	[System.Serializable]
	public class SlotGame : MonoBehaviour
	{

		public	static SlotGame instance;
		
		public 	GameObject prefMarker;
		// prefab of marker
		public 	UILineRenderer line;
		// line render (show matched line (
		public 	Image imgSelection;
		//	private Transform tr;

		public	List<Symbol> Symbols = new List<Symbol> ();
		public	List<Line> Lines = new List<Line> ();
		//	private	List<Marker> 	Markers=new List<Marker>();

		public	int LineMin;
		public	int Line;
		public	List<float> BetTable = new List<float> ();
		public	int Bet;
		public	float RealBet;
		public	float TotalBet;
		[HideInInspector]
		public	float Gold = 2000.0f;

		public 	int SymbolSize = 160;
		public 	int MarginSize = 25;
		public	float SymbolSizePlusMargin;
		public	float DampingHeight;

		public 	RNGType rngType = RNGType.UnityRandom;
		private	System.Random randomDotNet = new System.Random ();
		private	MT19937 randomMersenneTwister = new MT19937 ();
		public GameObject[] LinesResult;
		public GameObject freeSpinPanel, BigWinPanel, WinResult, JackPot, FreeSpinBG, FreeSpinResultText, LinePanel;
		public Text FreeSpinText;
		public	List<UISGReel>	Reels = new List<UISGReel> ();
		public	int RowCount = 3;
		private	bool InSpin = false;
		private float SpinAge = 0.0f;
		private int ActiveReel = -1;
		public 	float BoundSpeed = 30.0f;
		public 	float Acceleration = 3000.0f;
		public 	float SpeedMax = 3000.0f;
		public 	float MinimumRotateDistance = 640.0f;
		public 	float MinimumRotateDistancePredict = 4000.0f;

		private bool InResult = false;
		public	int[] SplashCount = new int[(int)SplashType.Max];
		public	int SplashActive = -1;
		public	bool InSplashShow = false;

		//		private int WinOffset = 0;
		//		private float WinShowAge = 0.0f;
		//		private int WinShowBlinkCount = 2;
		//		private float WinShowPeriod = 1.2f;

		public	IntEvent ReelStopCallback = null;
		public	UnityEvent SpinEndCallback = null;
		public	IntEvent SplashShowCallback = null;
		public	UnityEvent SplashEndCallback = null;

		private BEChoice[] choice = null;
		private List<int> Deck = new List<int> ();
		public 	GameResult gameResult = new GameResult ();
		public 	GameResult resultTest = null;

		public 	int SimulationCount = 10;
		//private bool isServerResponed = false;
		public int totalFreeSpin = 0;
		public int totalFreeSpinTime = 0;
		public bool freeSpinBigWin = false;
		public bool IsDoneFreeSpin = false;
		public bool IsFreeSpinMode = false;
		public bool IsReturnToFreeSpin = false;

		private int freeSpincount = 0;

		public void SetReelCount (int value)
		{
			// set reel count range 3 ~ 10
			value = Mathf.Clamp (value, 3, 10);
			//Debug.Log ("SetReelCount "+value.ToString ());
			for (int x = 0; x < Reels.Count; ++x) {
				if ((Reels [x] == null) || (Reels [x].gameObject == null))
					continue;

				for (int y = 0; y < Reels [x].Symbols.Length; ++y) {
					if (Reels [x].Symbols [y] != null)
						BEObjectPool.Unspawn (Reels [x].Symbols [y]);
				}
				DestroyImmediate (Reels [x].gameObject);
			}
			Reels.Clear ();
			
			for (int x = 0; x < value; ++x) {
				GameObject go = new GameObject ("Reel" + x.ToString ());
				go.AddComponent<RectTransform> ();
				go.transform.SetParent (transform);
				go.transform.localPosition = Vector3.zero;
				go.transform.localScale = Vector3.one;

				UISGReel reel = go.AddComponent<UISGReel> ();
				reel.Init (this, x);
				Reels.Add (reel);

			}
			RepositionSymbols ();
		}

		public void SetRowCount (int value)
		{
			// set row count range 3 ~ 7
			value = Mathf.Clamp (value, 3, 7);
			//Debug.Log ("SetRowCount "+value.ToString ());
			RowCount = value;
			
			for (int x = 0; x < Reels.Count; ++x) {
				if ((Reels [x] == null) || (Reels [x].gameObject == null))
					continue;
				
				for (int y = 0; y < Reels [x].Symbols.Length; ++y) {
					if (Reels [x].Symbols [y] != null)
						BEObjectPool.Unspawn (Reels [x].Symbols [y]);
				}

				Reels [x].Init (this, x);
			}
			RepositionSymbols ();
		}

		public void SetSymbolSize (int value)
		{
			SymbolSize = value;
			RepositionSymbols ();
		}

		public void SetMarginSize (int value)
		{
			MarginSize = value;
			RepositionSymbols ();
		}

		public void RepositionSymbols ()
		{
			//Debug.Log ("RepositionSymbols ");
			float fStart = -(float)(Reels.Count - 1) * (float)(SymbolSize + MarginSize) / 2.0f;
			for (int x = 0; x < Reels.Count; ++x) {
				if ((Reels [x] == null) || (Reels [x].gameObject == null))
					continue;
				Reels [x].gameObject.transform.localPosition = new Vector3 (fStart, 0, 0);
				fStart += (float)(SymbolSize + MarginSize);
				
				float fYStart = -(float)(RowCount - 1) * (float)(SymbolSize + MarginSize) / 2.0f;
				for (int y = 0; y < Reels [x].Symbols.Length; ++y) {
					Reels [x].SymbolPos [y] = new Vector3 (0, fYStart, 0);
					Reels [x].Symbols [y].transform.localPosition = Reels [x].SymbolPos [y];
					fYStart += (float)(SymbolSize + MarginSize);
				}
			}
		}

		public void SymbolInsert (int ID)
		{
			if (Application.isPlaying)
				return;
			Symbols.Insert (ID, new Symbol ("Noname", SymbolType.Normal, 0, "0,0,0,0,0", SymbolRank.A0));
		}

		public int  SymbolRemove (int ID)
		{
			if (Application.isPlaying)
				return 0;
			Symbols.RemoveAt (ID);
			return 1;
		}

		public void SymbolAdd ()
		{
			SymbolInsert (Symbols.Count);
		}

		public void SymbolRemove ()
		{
			SymbolRemove (Symbols.Count - 1);
		}

		public void LineInsert (int ID)
		{
			if (Application.isPlaying)
				return;
			Lines.Insert (ID, new Line (0, 0, 0, 0, 0, "000000"));
		}

		public int  LineRemove (int ID)
		{
			if (Application.isPlaying)
				return 0;
			Lines.RemoveAt (ID);
			return 1;
		}

		public void LineAdd ()
		{
			LineInsert (Lines.Count);
		}

		public void LineRemove ()
		{
			LineRemove (Lines.Count - 1);
		}

		public void BetInsert (int ID)
		{
			if (Application.isPlaying)
				return;
			BetTable.Insert (ID, 1.0f);
		}

		public int  BetRemove (int ID)
		{
			if (Application.isPlaying)
				return 0;
			BetTable.RemoveAt (ID);
			return 1;
		}

		public void BetAdd ()
		{
			BetInsert (BetTable.Count);
		}

		public void BetRemove ()
		{
			BetRemove (BetTable.Count - 1);
		}


		void Awake ()
		{
			instance = this;
			//	tr = transform;
			Array.Clear (SplashCount, 0, SplashCount.Length);
		}

		void Start ()
		{
			#region == resize camera ==
//			float targetaspect = 16.0f / 9.0f;
//
//			// determine the game window's current aspect ratio
//			float windowaspect = (float)Screen.width / (float)Screen.height;
//
//			// current viewport height should be scaled by this amount
//			float scaleheight = windowaspect / targetaspect;
//
//			// obtain camera component so we can modify its viewport
//			Camera camera = GetComponent<Camera>();
//
//			// if scaled height is less than current height, add letterbox
//			if (scaleheight < 1.0f)
//			{  
//				Rect rect = camera.rect;
//
//				rect.width = 1.0f;
//				rect.height = scaleheight;
//				rect.x = 0;
//				rect.y = (1.0f - scaleheight) / 2.0f;
//
//				camera.rect = rect;
//			}
//			else // add pillarbox
//			{
//				float scalewidth = 1.0f / scaleheight;
//
//				Rect rect = camera.rect;
//
//				rect.width = scalewidth;
//				rect.height = 1.0f;
//				rect.x = (1.0f - scalewidth) / 2.0f;
//				rect.y = 0;
//
//				camera.rect = rect;
//			}
			#endregion

			CreateChoice ();

			SymbolSizePlusMargin = (float)(SymbolSize + MarginSize);
			DampingHeight = (float)SymbolSize * 0.4f;

			MarkerCreate ();
			LineSet (LineMin);
			BetSet (0);
			SetRandomSymbolToReel ();
//			MatchLineHide ();

			//	line.Points = new Vector2[Reels.Count + 2];

			gameResult.ResetGame ();
		}

		void Update ()
		{
			#region ===cmt section===
			//Display Match Lines
			//	if (InSplashShow) {

				
//				WinItem wi = gameResult.Wins [WinOffset];
//				Line ld = GetLine (wi.Line);
//				
//				float fValue = (float)WinShowBlinkCount * 2.0f / WinShowPeriod;
//				float fScale = Mathf.PingPong (WinShowAge * fValue, 1.0f) * 0.2f + 1.0f;
//				
//				imgSelection.color = ld.color;
//				//imgSelection.transform.localPosition = Markers[wi.Line].go.transform.localPosition;
//				
			//line.color = ld.color;
//				for (int x = 0; x < Reels.Count; ++x) {
//					Vector3 vPos = transform.InverseTransformPoint (Reels [x].Symbols [ld.Slots [x]].transform.position);
//					line.Points [x + 1] = new Vector2 (vPos.x, vPos.y);
//
//					int SymbolIdx = Deck [RowCount * x + ld.Slots [x]];
//					Symbol sd = GetSymbol (Deck [RowCount * x + ld.Slots [x]]);
////					if ((SymbolIdx == wi.SymbolIdx) || sd.IsWild ()) {
////
////						if (Reels [x].SymbolFC != null) {
////							Reels [x].SymbolFC.transform.localScale = new Vector3 (fScale, fScale, 1);
////						} else {
////							Reels [x].Symbols [ld.Slots [x]].transform.localScale = new Vector3 (fScale, fScale, 1);
////						}
////					}
//				}
				
//				if (imgSelection.transform.localPosition.x < 0.0f) {
//					Vector3 vPos = transform.InverseTransformPoint (imgSelection.transform.position);
//					//vPos.x += imgSelection.rectTransform.sizeDelta.x*0.5f;
//					line.Points [0] = new Vector2 (vPos.x, vPos.y);
//					line.Points [Reels.Count + 1] = line.Points [Reels.Count];
//				} else {
//					Vector3 vPos = transform.InverseTransformPoint (imgSelection.transform.position);
//					//vPos.x -= imgSelection.rectTransform.sizeDelta.x*0.5f;
//					line.Points [0] = line.Points [1];
//					line.Points [Reels.Count + 1] = new Vector2 (vPos.x, vPos.y);
//				}
				
//				WinShowAge += Time.deltaTime;
//				if (WinShowAge > WinShowPeriod) {
//					WinShowAge -= WinShowPeriod;
//					
//					WinOffset++;
//					if (WinOffset >= gameResult.Wins.Count) {
//						WinOffset = 0;
//
//						SplashCount [SplashActive] = 0;
//						InSplashShow = false;
//					}
//				}		
			//		}
			#endregion
			// display splash info
			if (!InSpin && InResult && !InSplashShow) {
				//Debug.Log ("SplashActive : "+SplashActive.ToString () + " SplashCount[SplashActive]:"+SplashCount[SplashActive].ToString () + " InSplashShow:"+InSplashShow.ToString ());
			
				#region == comment section ==
//				if (SplashActive >= (int)SplashType.Max) {
//
//					InResult = false;
//
//					if (SplashEndCallback != null) {
//						SplashEndCallback.Invoke ();
//						MatchLineHide ();
//					}
//				} else {
//					
//					// if no display info go to next
//					if (SplashCount [SplashActive] == 0) {
//						SplashActive++;
//					} else {
//						InSplashShow = true;
////						if (SplashShowCallback != null) {
////							//Debug.Log ("SplashShow : "+((SplashType)SplashActive).ToString ());
////
////
////							if (SplashActive != (int)SplashType.Line)
////								SplashShowCallback.Invoke (SplashActive);
////						} else {
////							SplashCount [SplashActive] = 0;
////							SplashActive++;
////						}
//					}
//				}
				#endregion
				return;
			}

			if (!InSpin)
				return;
			
			if (ActiveReel == -1) {
				SpinAge += Time.deltaTime;

				//after 0.5 sec latter stop first reel
				if (SpinAge > 0.5f) {
					ActiveReel = 0;

					//before stop. you must set result !!!
					//if you get the result value from server,
					//change if(SpinAge > 0.5f) to if(ServerResponse) to reels keep rolling.
					ApplyResult ();
					Reels [ActiveReel].Stop ();
				}
			} else {

				//if activeReel is stopped
				if (Reels [ActiveReel].Completed ()) {
					if (ReelStopCallback != null) {
						ReelStopCallback.Invoke (ActiveReel);
					}

					//check next reel
					ActiveReel++;
					
					//if all Reels stopped.
					if (ActiveReel >= Reels.Count) {
						InSpin = false;

						//check result
						//CheckWin ();
						InResult = true;
						showLine ();
						if (SpinEndCallback != null)
							SpinEndCallback.Invoke ();
					} else {
						Reels [ActiveReel].Stop ();
					}
				}
			}

		}

		public void showLine ()
		{
//			if (SceneSlotGame.instance.dataRoll.paylines.Count > 0) {
			StartCoroutine (showResult ());
//			} else {
//				
//				if (SplashEndCallback != null && !IsDoneFreeSpin) {
//					SplashEndCallback.Invoke ();
//
//				}
//				InResult = false;
//			}
				
		}

		IEnumerator ShowBigWin ()
		{
			BigWinPanel.SetActive (true);
			float time = 0.02f;
			for (int i = 0; i < 40; i++) {
				yield return new WaitForSeconds (time);
			}
			BanCa.Session.Gem.Value = SceneSlotGame.instance.dataRoll.gem;
			BigWinPanel.SetActive (false);
			if (SplashEndCallback != null) {
				SplashEndCallback.Invoke ();

			}
			InResult = false;

		}

		public IEnumerator ShowFreeSpin ()
		{
			freeSpinPanel.SetActive (true);
			freeSpinPanel.transform.GetChild (1).GetComponent<Text> ().text = (totalFreeSpinTime).ToString ();
			float time = 0.02f;
			for (int i = 0; i < 50; i++) {
				yield return new WaitForSeconds (time);
			}

			freeSpinPanel.SetActive (false);
			if (IsReturnToFreeSpin) {
				IsFreeSpinMode = true;
				FreeSpinBG.SetActive (true);
				FreeSpinText.gameObject.SetActive (true);
				SceneSlotGame.instance.RequestRollToServer ();
				IsReturnToFreeSpin = false;

			} else
				BanCa.Session.Gem.Value = SceneSlotGame.instance.dataRoll.gem;

		}

		IEnumerator ShowJackPot ()
		{
			JackPot.SetActive (true);
			yield return new WaitForSeconds (3);
			BanCa.Session.Gem.Value = SceneSlotGame.instance.dataRoll.gem;
			JackPot.SetActive (false);
			if (SplashEndCallback != null) {
				SplashEndCallback.Invoke ();

			}
			InResult = false;
			IsDoneFreeSpin = false;
		}

		IEnumerator ShowResultFreeSpin ()
		{
			foreach (var item in SceneSlotGame.instance.dataRoll.paylines) {
				LinesResult [item.paylineNumber - 1].SetActive (false);
			}
			foreach (Transform item in LinePanel.transform) {
				foreach (Transform line in item) {
					foreach (var data in SceneSlotGame.instance.dataRoll.paylines) {
						if (line.gameObject.name == "line (" + data.paylineNumber + ")") {
							line.gameObject.SetActive (false);
						}
					}
				
				}
			}
			FreeSpinResultText.SetActive (true);
			float time = 0.2f;
			var flag = true;
			for (int i = 0; i < 20; i++) {
				if (flag) {
					FreeSpinResultText.transform.GetChild (0).transform.localScale = new Vector3 (1.5f, 1.5f, 1.5f);
					flag = !flag;
				} else {
					FreeSpinResultText.transform.GetChild (0).transform.localScale = new Vector3 (1.3f, 1.3f, 1.3f);
					flag = !flag;
				}
				yield return new WaitForSeconds (time);
			}
			FreeSpinResultText.SetActive (false);
			if (SplashEndCallback != null) {
				SplashEndCallback.Invoke ();

			}
			//yield return null;

		}

	
		public IEnumerator showResult ()
		{
			float time = 0.02f;

			bool bigWin = false;
			//callBack ("BigWin", false);
			var paylines = SceneSlotGame.instance.dataRoll.paylines;
			var EffectLine = new List<Line> ();
			foreach (var item in paylines) {
				LinesResult [item.paylineNumber - 1].SetActive (true);
				EffectLine.Add (Lines [item.paylineNumber - 1]);
			}
			for (int x = 0; x < Reels.Count; ++x) {
				if ((Reels [x] == null) || (Reels [x].gameObject == null))
					continue;

				for (int y = 0; y < Reels [x].Symbols.Length; ++y) {
					foreach (var item in EffectLine) {
						var indexSymbol = 1;
						if (item.Slots [x] == 0) {
							indexSymbol = 2;
						} else if (item.Slots [x] == 2) {
							indexSymbol = 0;
						}
						if (Reels [x].Symbols [indexSymbol] != null) {
							Reels [x].Symbols [indexSymbol].gameObject.transform.GetChild (1).gameObject.SetActive (false);
							Reels [x].Symbols [indexSymbol].gameObject.transform.GetChild (0).gameObject.SetActive (true);
						}
					}

				}
			}
			if (paylines.Count > 0) {
				for (int i = 0; i < 40; i++) {
					yield return new WaitForSeconds (time);
				}
			}
			Debug.Log ("SceneSlotGame.instance.dataRoll.freeSpin:" + SceneSlotGame.instance.dataRoll.freeSpin + " totalFreeSpinTime: "+ totalFreeSpinTime);
			foreach (var item in paylines) {
				LinesResult [item.paylineNumber - 1].SetActive (false);
			}
			for (int x = 0; x < Reels.Count; ++x) {
				if ((Reels [x] == null) || (Reels [x].gameObject == null))
					continue;

				for (int y = 0; y < Reels [x].Symbols.Length; ++y) {
					foreach (var item in EffectLine) {
						var indexSymbol = 1;
						if (item.Slots [x] == 0) {
							indexSymbol = 2;
						} else if (item.Slots [x] == 2) {
							indexSymbol = 0;
						}
						if (Reels [x].Symbols [indexSymbol] != null) {
							Reels [x].Symbols [indexSymbol].gameObject.transform.GetChild (1).gameObject.SetActive (true);
							Reels [x].Symbols [indexSymbol].gameObject.transform.GetChild (0).gameObject.SetActive (false);
						}
					}

				}
			}
			//lay thong tin tien an dc
			int winValue = SceneSlotGame.instance.dataRoll.retValue + int.Parse (SceneSlotGame.instance.dataRoll.betValue) * SceneSlotGame.instance.dataRoll.nLine.Count ();
			if (winValue / (int.Parse (SceneSlotGame.instance.dataRoll.betValue) * SceneSlotGame.instance.dataRoll.nLine.Count ()) >= 4f) {
				bigWin = true;
			}  
			//kiem tra co phai big win hay ko de hien thi so tien bt
			if (!bigWin && !SceneSlotGame.instance.dataRoll.isJackPot) {


				if (winValue > 0) {
					WinResult.SetActive (true);
					StartCoroutine (ShowMoneyWin (0, winValue));
					//WinResult.transform.GetChild (0).transform.GetComponent<Text> ().text = "+" + SaveLoadData.FormatMoney (winValue);
					if (paylines.Count > 0) {
						for (int i = 0; i < 20; i++) {
							yield return new WaitForSeconds (time);
						}
					}
					WinResult.SetActive (false);
				}
			}


			if (SceneSlotGame.instance.dataRoll.getFreeSpin) {
				if (!IsFreeSpinMode) {
					totalFreeSpinTime = 0;
					freeSpincount = 0;
				}
				Debug.Log ("SceneSlotGame.instance.dataRoll.freeSpin:" + SceneSlotGame.instance.dataRoll.freeSpin + " totalFreeSpinTime: "+ totalFreeSpinTime);
				totalFreeSpinTime = totalFreeSpinTime + SceneSlotGame.instance.dataRoll.freeSpin;
				FreeSpinBG.SetActive (true);
				FreeSpinText.gameObject.SetActive (true);
				IsFreeSpinMode = true;
				StartCoroutine (ShowFreeSpin ());
			}
			if (IsFreeSpinMode && (totalFreeSpinTime == 0 || SceneSlotGame.instance.dataRoll.freeSpin == 0) ) {
				IsDoneFreeSpin = true;
				freeSpinBigWin = true;

				IsFreeSpinMode = false;

				freeSpinBigWin = false;
				IsDoneFreeSpin = false;
				FreeSpinBG.SetActive (false);
				FreeSpinText.gameObject.SetActive (false);
				totalFreeSpin = 0;
				totalFreeSpinTime = 0;

			}  
			if (IsFreeSpinMode) {
				Debug.Log ("SceneSlotGame.instance.dataRoll.basicInfo.freeSpin: " + SceneSlotGame.instance.dataRoll.freeSpin);
				Debug.Log ("totalFreeSpinTime: " + totalFreeSpinTime);
				//totalFreeSpin += winValue;

				FreeSpinText.text = "Quay miễn phí lần: "+(freeSpincount+1);
				totalFreeSpinTime = totalFreeSpinTime - 1;
				freeSpincount++;
				SceneSlotGame.instance.isRooling = false;
				bigWin = false;
			}
			// gan so tien hien tai o thong tin nguoi dung
			if (!SceneSlotGame.instance.dataRoll.isJackPot && !bigWin)
				BanCa.Session.Gem.Value = SceneSlotGame.instance.dataRoll.gem;
			if (bigWin) {
				StartCoroutine (ShowBigWin ());
//				if (freeSpinBigWin && totalFreeSpin > 0) {	
//					StartCoroutine (updateBigWin (0, totalFreeSpin));
//				} else 
//					if(bigWin) {
				StartCoroutine (updateBigWin (0, winValue));
				//	}
			
			}

      if(SceneSlotGame.instance.dataRoll.isJackPot)
      {
        StartCoroutine(ShowJackPot());
        StartCoroutine(updateJackPot(0,
          SceneSlotGame.instance.dataRoll.jackPotValueGet));
      }

//			foreach (Transform item in LinePanel.transform) {
//				foreach (Transform line in item) {
//					foreach (var data in paylines) {
//						if (line.gameObject.name == "Line (" + data.paylineNumber + ")") {
//							line.gameObject.SetActive (false);
//						}
//					}
//
//				}
//			}
			if (!IsFreeSpinMode && !IsDoneFreeSpin && !bigWin && !SceneSlotGame.instance.dataRoll.isJackPot) {

				if (SplashEndCallback != null) {
					SplashEndCallback.Invoke ();
				}
				InResult = false;	
			} else if (IsFreeSpinMode) {
				InResult = false;
			}
			foreach(Transform trans in SceneSlotGame.instance.ObjectPool){
				Destroy (trans.gameObject);
			}
		}

		IEnumerator ShowMoneyWin (int firstValue, int secondValue)
		{
			float elapsedTime = 0;
			float seconds = 0.2f;

			do {
				WinResult.transform.GetChild (0).transform.GetComponent<Text> ().text = "+" + SaveLoadData.FormatMoney ((int)Mathf.Floor (Mathf.Lerp (firstValue, secondValue, (elapsedTime / seconds))));

				//WinResult.transform.GetChild (1).transform.localScale = new Vector3(elapsedTime,elapsedTime,elapsedTime);

				elapsedTime += Time.deltaTime;
				yield return null;
			} while(elapsedTime < seconds);
			WinResult.transform.GetChild (0).transform.GetComponent<Text> ().text = "+" + SaveLoadData.FormatMoney (secondValue);
		}


		IEnumerator ShowMoneyIsDoneFreeSpin (int firstValue, int secondValue)
		{
			float elapsedTime = 0;
			float seconds = 1f;

			do {
				FreeSpinResultText.transform.GetChild (0).GetComponent<Text> ().text = "+" + SaveLoadData.FormatMoney ((int)Mathf.Floor (Mathf.Lerp (firstValue, secondValue, (elapsedTime / seconds))));

				//WinResult.transform.GetChild (1).transform.localScale = new Vector3(elapsedTime,elapsedTime,elapsedTime);

				elapsedTime += Time.deltaTime;
				yield return null;
			} while(elapsedTime < seconds);
			FreeSpinResultText.transform.GetChild (0).GetComponent<Text> ().text = "+" + SaveLoadData.FormatMoney (secondValue);
		}

		IEnumerator updateJackPot (int firstValue, int secondValue)
		{
			float elapsedTime = 0;
			float seconds = 2f;
			float vector = 0.5f;
			do {
				JackPot.transform.GetChild (1).GetComponent<Text> ().text = SaveLoadData.FormatMoney ((int)Mathf.Floor (Mathf.Lerp (firstValue, secondValue, (elapsedTime / seconds))));
			
				JackPot.transform.GetChild (1).transform.localScale = new Vector3 (elapsedTime, elapsedTime, elapsedTime);
			
				elapsedTime += Time.deltaTime;
				yield return null;
			} while(elapsedTime < seconds);
			JackPot.transform.GetChild (1).GetComponent<Text> ().text = SaveLoadData.FormatMoney (secondValue);

		}

		IEnumerator updateBigWin (int firstValue, int secondValue)
		{
			float elapsedTime = 0;
			float seconds = 0.8f;
			do {
				BigWinPanel.transform.GetChild (1).transform.GetComponent<Text> ().text = SaveLoadData.FormatMoney ((int)Mathf.Floor (Mathf.Lerp (firstValue, secondValue, (elapsedTime / seconds))));
				elapsedTime += Time.deltaTime;
				yield return null;
			} while(elapsedTime < seconds);
			BigWinPanel.transform.GetChild (1).transform.GetComponent<Text> ().text = SaveLoadData.FormatMoney (secondValue);

		}

		public void CreateChoice ()
		{
			//BEChoice class is a utility class to choose a symbol from many symbols with various frequency.  
			choice = new BEChoice[Reels.Count];
			for (int x = 0; x < Reels.Count; ++x) {
				
				choice [x] = new BEChoice ();
				choice [x].Clear ();
				
				for (int i = 0; i < Symbols.Count; ++i) {
					Symbol sd = GetSymbol (i);
					
					// if symbol's frequency of this reel is not 0 
					// then add frequency to choice object
					if (sd.frequency [x] != 0)
						choice [x].Add (i, sd.frequency [x]);
				}
			}	
		}

		public void CheckWin ()
		{

			gameResult = CheckSpinWin (Deck, gameResult);

			Gold += gameResult.SpinWin;

			InResult = true;
			SplashActive = 0;
			GC.Collect ();
		}

		public void MarkerCreate ()
		{

		}

		public void LineSet (int _Line)
		{

		}

		public void BetSet (int _Bet)
		{
			Bet = _Bet;
			if (Bet >= BetTable.Count)
				Bet = 0;
			if (Bet < 0)
				Bet = BetTable.Count - 1;
			RealBet = BetTable [Bet];
			TotalBet = (float)Line * RealBet;
		}

		public void SetRandomSymbolToReel ()
		{
			for (int x = 0; x < Reels.Count; ++x)
				Reels [x].SetSymbolRandom ();
		}

		public int GetResultSymbolCount (List<int> iDeck, SymbolType eSymbol)
		{
			int Count = 0;
			for (int x = 0; x < Reels.Count; ++x) {
				for (int y = 0; y < RowCount; ++y) {
					if (GetSymbol (iDeck [RowCount * x + y]).type == eSymbol)
						Count++;
				}
			}
			
			return Count;
		}

		public bool Spinable ()
		{
			return (!InSpin && !InResult) ? true : false;
		}

		public SlotReturnCode Spin ()
		{

			GC.Collect ();

			if (InSpin)
				return SlotReturnCode.InSpin;

			gameResult.Line = Line;
			gameResult.RealBet = BetTable [Bet];
			gameResult.Spin ();

			if (gameResult.FreeSpinCount == 0) {
				gameResult.GameWin = 0.0f;
				Gold -= TotalBet;
			}

			Array.Clear (SplashCount, 0, SplashCount.Length);
//			MatchLineHide ();

			//Start Reels spin
			for (int x = 0; x < Reels.Count; ++x) {
				Reels [x].SymbolScaleReset ();
				Reels [x].Spin ();
			}

			SpinAge = 0.0f;
			ActiveReel = -1;
			InSpin = true;
		
			return SlotReturnCode.Success;
		}

		// make result value by frequency of each symbols
		public List<int> GetDeck ()
		{
			
			List<int> result = new List<int> ();


			//BEChoice class is a utility class to choose a symbol from many symbols with various frequency.  
			for (int x = 0; x < Reels.Count; ++x) {
				for (int y = 0; y < RowCount; ++y) {

					//get random value from RNG 
					int RandomValue = -1;
					if (rngType == RNGType.UnityRandom)
						RandomValue = UnityEngine.Random.Range (0, choice [x].ValueTotal);
					else if (rngType == RNGType.DotNetRandom)
						RandomValue = randomDotNet.Next (0, choice [x].ValueTotal);
					else if (rngType == RNGType.MersenneTwister)
						RandomValue = randomMersenneTwister.RandomRange (0, choice [x].ValueTotal - 1);
					else {
					}

					//get selected symbol and add to result list
					int SymbolIdx = choice [x].Choice (RandomValue);
					result.Add (SymbolIdx);
				}
			}

			//Debug.Log ("GetDeck");
			//Debug.Log (GetSymbol(result[2]).prfab.name+","+GetSymbol(result[5]).prfab.name+","+GetSymbol(result[8]).prfab.name+","+GetSymbol(result[11]).prfab.name+","+GetSymbol(result[14]).prfab.name);
			//Debug.Log (GetSymbol(result[1]).prfab.name+","+GetSymbol(result[4]).prfab.name+","+GetSymbol(result[7]).prfab.name+","+GetSymbol(result[10]).prfab.name+","+GetSymbol(result[13]).prfab.name);
			//Debug.Log (GetSymbol(result[0]).prfab.name+","+GetSymbol(result[3]).prfab.name+","+GetSymbol(result[6]).prfab.name+","+GetSymbol(result[ 9]).prfab.name+","+GetSymbol(result[12]).prfab.name);

			return result;
		}

		public void ApplyResult ()
		{

			//Create Result Symbols
			Deck = GetDeck ();
			var data = SceneSlotGame.instance.dataRoll;

			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 3; j++) {
					if (i == 0) {
						foreach (var item in Symbols) {	
							if (data.matrix.col1 [j].symbol.ToLower () == item.rank.ToString ().ToLower ()) {
								data.matrix.col1 [j].index = Symbols.IndexOf (item);

							}
						}
					} else if (i == 1) {
						
						foreach (var item in Symbols) {	
							if (data.matrix.col2 [j].symbol.ToLower () == item.rank.ToString ().ToLower ()) {
								data.matrix.col2 [j].index = Symbols.IndexOf (item);
							
							}
							
						}
					} else if (i == 2) {
						
						foreach (var item in Symbols) {
							if (data.matrix.col3 [j].symbol.ToLower () == item.rank.ToString ().ToLower ()) {
								data.matrix.col3 [j].index = Symbols.IndexOf (item);
							}
							

						}
					} else if (i == 3) {
						
						foreach (var item in Symbols) {
							if (data.matrix.col4 [j].symbol.ToLower () == item.rank.ToString ().ToLower ()) {
								data.matrix.col4 [j].index = Symbols.IndexOf (item);
								
							}
						}
					} else if (i == 4) {
						
						foreach (var item in Symbols) {
							if (data.matrix.col5 [j].symbol.ToLower () == item.rank.ToString ().ToLower ()) {
								data.matrix.col5 [j].index = Symbols.IndexOf (item);
							}
							
						}
					}

				}
			}
			//Start Reels spin
			for (int x = 0; x < Reels.Count; ++x) {

				//make array of final value for each reel
				var col = new List<Col1> ();
				if (x == 0) {
					foreach (var item in data.matrix.col1) {
						var itemData = new Col1 ();
						itemData.index = item.index;
						col.Add (itemData);
					}
				} else if (x == 1) {
					foreach (var item in data.matrix.col2) {
						var itemData = new Col1 ();
						itemData.index = item.index;
						col.Add (itemData);
					}
				} else if (x == 2) {
					foreach (var item in data.matrix.col3) {
						var itemData = new Col1 ();
						itemData.index = item.index;
						col.Add (itemData);
					}
				} else if (x == 3) {
					foreach (var item in data.matrix.col4) {
						var itemData = new Col1 ();
						itemData.index = item.index;
						col.Add (itemData);
					}
				} else if (x == 4) {
					foreach (var item in data.matrix.col5) {
						var itemData = new Col1 ();
						itemData.index = item.index;
						col.Add (itemData);
					}
				} 
				int[] value = new int[RowCount];
				for (int y = 0; y < 3; ++y) {
					value [y] = col [y].index;
				}
				Array.Reverse (value);
				//IN KET QUA TAI DAY
				// set value to each reel
				Reels [x].ApplyResult (value);
			}
			
			// Bonus, Scatter Spin Effect
			{
				int iScatterSum = 0;
				int iBonusSum = 0;
				int iWildSum = 0;
				int iJackpot = 0;
				bool StartSet = false;
				for (int x = 0; x < Reels.Count; ++x) {
					
					// if there are scatter or Bonus Symbols more than 2
					// Reels Spin longer than normal before stop
					if (Math.Max (Math.Max (Math.Max (iScatterSum, iBonusSum), iWildSum), iJackpot) >= 2) {
						
						Reels [x].MinimumRotateDistance = MinimumRotateDistancePredict;
						
						if (!StartSet) {
							Reels [x].bSlotIndicateFirst = true;
							StartSet = true;
							//Debug.Log ("Indicator on : "+x.ToString ());
						}
					}
					
//					for(int y=0 ; y < RowCount ; ++y) {
//						SymbolType eType = GetSymbol(Deck[RowCount*x+y]).type;
//						if(eType == SymbolType.Scatter) iScatterSum++;
//						if(eType == SymbolType.Bonus)   iBonusSum++;
//					}
				}
			}
		}

		public GameResult CheckSpinWin (List<int> iDeck, GameResult result)
		{

//			for (int x = 0; x < Reels.Count; ++x) {
//				bool bWildFCExist = false;
//				for (int y = 0; y < RowCount; ++y) {
//					int SymbolIdx = iDeck [RowCount * x + y];
//					if (GetSymbol (SymbolIdx).type == SymbolType.WildFC) {
//						bWildFCExist = true;
//						break;
//					}				
//				}
//
//				if (bWildFCExist) {
//					int SymbolWildIdx = GetSymbolIdxByType (SymbolType.Wild);
//					for (int y = 0; y < RowCount; ++y) {
//						iDeck [RowCount * x + y] = SymbolWildIdx;
//					}
//				}
//
//			}

			//resset spin of result
			result.ResetSpin ();
			int[] iResult = new int [Reels.Count];

			for (int i = 0; i < result.Line; ++i) {
				Line ld = GetLine (i);

				for (int x = 0; x < Reels.Count; ++x) {
					iResult [x] = iDeck [RowCount * x + ld.Slots [x]];
				}

				// check
				int MatchCount = 0;
				bool bFirstSymbol = false;
				int SymbolIdx = -1;
				for (int x = 0; x < Reels.Count; ++x) {
					if (!bFirstSymbol) {
						if (!GetSymbol (iResult [x]).IsWild ()) {
							SymbolIdx = iResult [x];
							bFirstSymbol = true;
						}
						MatchCount++;
					} else {
						if ((SymbolIdx == iResult [x]) || GetSymbol (iResult [x]).IsWild ()) {
							MatchCount++;
						} else {
							break;
						}
					}
				}
				
				if (SymbolIdx == -1)
					continue;
				
				Symbol sd = GetSymbol (SymbolIdx);
				if ((sd.type == SymbolType.Normal) && (MatchCount != 0) && (sd.reward [MatchCount - 1] != 0)) {
					
					float LineWin = (float)(sd.reward [MatchCount - 1]) * result.RealBet;
					
					//Check whether Wild2X & Wild3X symbol is included
					int Wild2XSymbolCount = 0;
					int Wild3XSymbolCount = 0;
					for (int x = 0; x < Reels.Count; ++x) {
						if (GetSymbol (iResult [x]).type == SymbolType.Wild2X)
							Wild2XSymbolCount++;
						if (GetSymbol (iResult [x]).type == SymbolType.Wild3X)
							Wild3XSymbolCount++;
					}
					
					// Change Reward Gold by Wild Symbol Counts
					if (Wild3XSymbolCount > 0)
						LineWin *= 3.0f;
					else if (Wild2XSymbolCount >= 2)
						LineWin *= 4.0f;
					else if (Wild2XSymbolCount >= 1)
						LineWin *= 2.0f;
					else {
					}
					
					//Debug.Log ("Win "+result.Wins.Count.ToString ()+" Line:"+i.ToString ()+" Symbol:"+SymbolIdx.ToString ()+" MatchCount:"+MatchCount.ToString ()+" Reward:"+LineWin.ToString ());
					result.Wins.Add (new WinItem (i, SymbolIdx, MatchCount, LineWin));
					result.SpinWin += LineWin;

					// 
					if ((result.TestSymbols.Count != 0) && (result.TestLines.Count != 0)) {
						result.TestSymbols [SymbolIdx].Hit++;
						result.TestSymbols [SymbolIdx].WinGold += LineWin;
						result.TestLines [i].Hit++;
						result.TestLines [i].WinGold += LineWin;
					}
				}
			}

			result.GameWin += result.SpinWin;

			if ((result.FreeSpinTotalCount != 0) && (result.FreeSpinCount <= result.FreeSpinTotalCount)) {
				result.FreeSpinTotalWins += result.SpinWin;
				result.FreeSpinAccumWins += result.SpinWin;
			}

			{
				int ScaterSymbolCount = GetResultSymbolCount (iDeck, SymbolType.Scatter);
				if ((3 <= ScaterSymbolCount) && (ScaterSymbolCount <= Reels.Count)) {
					Symbol sd = GetSymbolByType (SymbolType.Scatter);
					result.NewFreeSpinCount = sd.reward [ScaterSymbolCount - 1];
					result.FreeSpinAccumCount += result.NewFreeSpinCount;
				}
				
				result.FreeSpinTotalCount += result.NewFreeSpinCount;
				if (result.FreeSpinTotalCount > 0) {
					result.FreeSpinCount++;
				}
			}

			return result;
		}

		public void Simulation ()
		{

			CreateChoice ();

			resultTest = new GameResult ();
			GameResult rt = resultTest;
			rt.ResetGame ();
			rt.Line = Lines.Count;
			rt.RealBet = BetTable [0];

			// 
			rt.TestSymbols.Clear ();
			for (int i = 0; i < Symbols.Count; ++i) {
				rt.TestSymbols.Add (new TestItem (i));
			}
			rt.TestLines.Clear ();
			for (int i = 0; i < Lines.Count; ++i) {
				rt.TestLines.Add (new TestItem (i));
			}

			for (int c = 0; c < SimulationCount; ++c) {

				rt.Spin ();

				// get result
				List<int> iDeck = GetDeck ();
				rt = CheckSpinWin (iDeck, rt);
				//Debug.Log ("result"+c.ToString ("00000")+" line:"+result.Wins.Count.ToString ()+" GameWin:"+result.GameWin);
			}

/*			float TotalBet = BetTable[0]*(float)SimulationCount;
			float Ratio = rt.GameWin/TotalBet * 100.0f;
			Debug.Log ("SlotGame::Simulation count:"+SimulationCount+" Bet:"+TotalBet+" Win:"+rt.GameWin+" Ratio:"+Ratio.ToString ("000.00"));
			Debug.Log ("FreeSpinAccumCount:"+rt.FreeSpinAccumCount+" FreeSpinAccumWins:"+rt.FreeSpinAccumWins);

			for(int i=0 ; i < Symbols.Count ; ++i) {
				Debug.Log ("Symbol:"+GetSymbol(i).prfab.name+" Hit:"+rt.TestSymbols[i].Hit+" Win:"+rt.TestSymbols[i].WinGold);
			}
			
			for(int i=0 ; i < Lines.Count ; ++i) {
				Debug.Log ("Line:"+i+" Hit:"+rt.TestLines[i].Hit+" Win:"+rt.TestLines[i].WinGold);
			}
*/
		}

		public int GetSymbolIdxByType (SymbolType type)
		{
			for (int i = 0; i < Symbols.Count; ++i) {
				if (Symbols [i].type != type)
					continue;
				
				return i;
			}
			
			return -1;
		}

		public Symbol GetSymbolByType (SymbolType type)
		{
			int idx = GetSymbolIdxByType (type);
			return (idx == -1) ? null : Symbols [idx];
		}

		public Symbol GetSymbol (int idx)
		{ 
			return Symbols [idx]; 
		}

		public Line GetLine (int idx)
		{ 
			return Lines [idx]; 
		}
		

	}
	
}
