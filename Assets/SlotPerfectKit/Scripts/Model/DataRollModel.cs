﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using System;
public class Payline
{
	public string symbol { get; set; }
	public string symbolRank { get; set; }
	public int chain { get; set; }
	public int paylineNumber { get; set; }
}

public class Col1
{
	public string symbol { get; set; }
	public string symbolRank { get; set; }
	public int index { get; set;}
}

public class Col2
{
	public string symbol { get; set; }
	public string symbolRank { get; set; }
	public int index { get; set;}
}

public class Col3
{
	public string symbol { get; set; }
	public string symbolRank { get; set; }
	public int index { get; set;}
}

public class Col4
{
	public string symbol { get; set; }
	public string symbolRank { get; set; }
	public int index { get; set;}
}

public class Col5
{
	public string symbol { get; set; }
	public string symbolRank { get; set; }
	public int index { get; set;}
}

public class Matrix
{
	public List<Col1> col1 { get; set; }
	public List<Col2> col2 { get; set; }
	public List<Col3> col3 { get; set; }
	public List<Col4> col4 { get; set; }
	public List<Col5> col5 { get; set; }
}

public class BasicInfo
{
	public string _id { get; set; }
	public string username { get; set; }
	public string password { get; set; }
	public string mail { get; set; }
	public int gold { get; set; }
	public int gem { get; set; }
	public DateTime activeDate { get; set; }
	public string faceid { get; set; }
	public string displayname { get; set; }
	public int vip { get; set; }
	public int dailyreward { get; set; }
	public DateTime lastcheckin { get; set; }
	public DateTime lastlogin { get; set; }
	public string refrencecode { get; set; }
	public string token { get; set; }
	public string client { get; set; }
	public int block { get; set; }
	public int gemday { get; set; }
	public int gemweek { get; set; }
	public int gemmonth { get; set; }
	public int goldday { get; set; }
	public int goldweek { get; set; }
	public int goldmonth { get; set; }
	public int jackpotday { get; set; }
	public int jackpotweek { get; set; }
	public int jackpotmonth { get; set; }
	public int arenaday { get; set; }
	public int arenaweek { get; set; }
	public int arenamonth { get; set; }
	public string deviceid { get; set; }
	public int minibank { get; set; }
	public int freeSpin { get; set; }
	public int freeSpinBet { get; set; }
}
public class DataRoll
{
	public bool Success { get; set; }
	public int gem { get; set; }
	public int minibank { get; set; }
	public int retValue { get; set; }
	public List<Payline> paylines { get; set; }
	public Matrix matrix { get; set; }
	public int winrate { get; set; }
	public int bonusrate { get; set; }
	public List<int> nLine { get; set; }
	public string betValue { get; set; }
	public bool getFreeSpin { get; set; }
	public int freeSpinGet { get; set; }
	public BasicInfo basicInfo { get; set; }
	public List<JackPotInfo> jackPotInfo { get; set; }
	public bool isJackPot { get; set; }
  public int jackPotValueGet;
	public string Message { get; set;}
	public int freeSpin { get; set; }
	public int gemStart { get; set; }
}
