﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
[System.Serializable]
public class RoomInfo
{
	public int roomId { get; set; }
	public int betValue { get; set; }
}
[System.Serializable]
public class GemInfo
{
	public int gem { get; set; }
	public int freeSpin { get; set; }
	public int freeSpinBet { get; set; }
	public List<int> freeSpinLine { get; set; }
	public List<RoomInfo> roomInfo { get; set; }
}
public class JackPotInfo
{
	public int roomId { get; set; }
	public int jackPot { get; set; }
	public int tempJackPot { get; set; }
}

