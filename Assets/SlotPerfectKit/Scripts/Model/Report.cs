﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
//[System.Serializable]
//public class History
//{
//	public string _id { get; set; }
//	public string time { get; set; }
//	public string username { get; set; }
//	public int amount { get; set; }
//	public int balance { get; set; }
//	public string type { get; set; }
//	public int? roomId { get; set; }
//	public int? phong { get; set; }
//}

public class VinhDanh
{
	public string username { get; set; }
	public string time { get; set; }
	public int amount { get; set; }
	public int roomId { get; set; }
	public int phong { get; set; }
}
public class LichSu
{
	public string timelog { get; set; }
	public int totalBet { get; set; }
	public int profit { get; set; }
	public int betline { get; set; }
	public int type { get; set; }
	public int betValue { get; set; }
}

