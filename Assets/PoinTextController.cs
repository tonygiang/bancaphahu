﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PoinTextController : MonoBehaviour {
    [SerializeField] Transform tranform;
    int int_RoomValue=100;
    string sceneName;
    [SerializeField] Text txtContent;
    float scaleLimit = 0.45f;
    // Use this for initialization

     void Awake()
    {
        tranform.localScale = new Vector3(0.3f, 0.3f, 1);

    }

     void Start()
    {
//		if (SceneManager.GetActiveScene ().name != "Arena" ) {
//			string roomName = NetworkManager.instance.lobbyRoom.Name;
//			int_RoomValue = int.Parse (roomName.Split (new char[] { '_' }) [1]);
//			scaleLimit = 0.45f;
//		}


    }


	public void SetTextContent(string content,string _roomType)
    {
        int contentValue = int.Parse(content);
		int_RoomValue = int.Parse(_roomType);

		if (contentValue > (int_RoomValue * 25))
        {
            scaleLimit = 1.3f;

        }
        else if(contentValue>(int_RoomValue * 10))
        {
            scaleLimit = 1f;
        }else if(contentValue > (int_RoomValue * 5))
        {
            scaleLimit = 0.7f;
        } else
        {
            scaleLimit = 0.45f;
        }
        txtContent.text = "+" + content;
        StartCoroutine("ScaleOut");

    }

    //private void OnEnable()
    //{
    //    StartCoroutine("ScaleOut");
    //}

    IEnumerator ScaleOut()
    {
        Vector3 speed = new Vector3(0.05f, 0.05f, 0)* scaleLimit;
        while (tranform.localScale.x < scaleLimit)
        {
            yield return new WaitForEndOfFrame();
            tranform.localScale += speed;
        }
    }

    private void OnDisable()
    {
        tranform.localScale = new Vector3(0.3f, 0.3f, 1);
    }


}
