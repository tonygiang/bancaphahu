﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Utils;
using UnityEngine.Networking;
using System.Linq;

public class UserManager
{
  public static string UserName;
  public static string DisplayName;
  public static string AccessToken;
  public static string AvatarLink;
  public static string RefreshToken;
  public static bool isQuickLogin = true;
  public static string avatarName = "caduoi";
  public static string faceId = "none";

  public static int jackpot;

  public static Sprite avatarSpr;


  public static void SetInfo(ISFSObject obj)
  {
    if (obj.ContainsKey("deviceid"))
    {
      string _id = obj.GetUtfString("deviceid");
      PlayerPrefs.SetString("deviceid", _id);
      PlayerPrefs.Save();
    }
    DisplayName = obj.GetUtfString("displayname");
    faceId = obj.GetUtfString("faceid");
    CommonLogin(obj);
    if (faceId != "none") UIMenu.Instance.OFF_CONNECT_FB();
    else UIMenu.Instance.ON_CONNECT_FB();

    isQuickLogin = true;
  }

  public static void SetInfoFB(ISFSObject obj)
  {
    CommonLogin(obj);

    isQuickLogin = false;
    UIMenu.Instance.OFF_CONNECT_FB();

  }

  public static void SET_INFO_LOGIN_REGISTER(ISFSObject obj)
  {
    DisplayName = obj.GetUtfString("displayname");
    CommonLogin(obj);

    isQuickLogin = true;
    UIMenu.Instance.OFF_CONNECT_FB();

  }

  static void CommonLogin(ISFSObject obj)
  {
    UserName = obj.GetUtfString("username");
    AccessToken = obj.GetUtfString("access_token");
    BanCa.Session.TreasureKeys.Value = obj.GetInt("khobau_key");

    if (faceId != "none")
    {
      UIMenu.Instance.OFF_CONNECT_FB();
      AvatarLink = "https" + "://graph.facebook.com/" + faceId + "/picture?type=square&height=100&width=100";
      //			avatarName = AvatarLink;
      Network.Get(AvatarLink).Then(handler =>
      {
        Texture2D texture = new Texture2D(100, 100);
        texture.LoadImage(handler.data);
        avatarSpr = Sprite.Create(texture,
          new Rect(0, 0, texture.width, texture.height),
          new Vector2(0.5f, 0.5f));
        ChangeAvatar.Instance.SetAvatarFB(avatarSpr);
        Shop.Instance.SetUserAvatar(avatarSpr);
      });
    }
    else
    {
      avatarName = "caduoi";
      AvatarLink = avatarName;
      avatarSpr = Resources.Load<Sprite>("avatar/" + avatarName);
      ChangeAvatar.Instance.SetAvatarFB(avatarSpr);
      Shop.Instance.SetUserAvatar(avatarSpr);
    }

    UIMenu.Instance.SetUserName();
    Network.PostToServer("Coin", ("method", "getcoin"))
      .Then(HandleGetCoinResponse);
    Network.PostToServer("BehindLogin", ("method", "getmailbox"))
      .Then(handler =>
      {
        ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
          .PassTo(SFSObject.NewFromJsonData);
        if (smartfoxResponse.GetInt("message_code") != 1)
          MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
        else Inbox.Instance.GetMail(smartfoxResponse.GetSFSArray("list"));
      });
    Network.PostToServer("BehindLogin", ("method", "getuserquest"))
      .Then(handler => handler.text.PassTo(Crypto.Decrypt)
        .PassTo(SFSObject.NewFromJsonData)
        .PassTo(MissionManager.Instance.LoadUserMission));

    Shop.Instance.SetInfo();
    UIMenu.Instance.ActiveAvatarVip();
  }

  public static void ResetUser()
  {

    SFSManager.instance.isLogInSfs = false;
    AccessToken = "";
    RefreshToken = "";

    UserName = "";
    DisplayName = "";
    AvatarLink = "";
    faceId = "none";

    BanCa.Session.VIPLevels = Enumerable.Empty<BanCa.VIPLevel>();
    BanCa.Session.Gem.Value = 0;
    FreeManager.Instance.ResetCheckIn();
  }

  #region VIP

  public static void LoadVipBuyGem(int _gem)
  {
    if (UIMenu.Instance != null)
    {
      UIMenu.Instance.ActiveAvatarVip();
    }
  }

  #endregion

  public static void SET_DISPLAY_NAME(string _displayname)
  {
    DisplayName = _displayname;
    UIMenu.Instance.FINISH_CHANGE_NAME(_displayname);
    Shop.Instance.SetInfo();
  }

  public static void SET_AVATAR_BACKGAME()
  {
    ChangeAvatar.Instance.SetAvatarFB(avatarSpr);
    Shop.Instance.SetUserAvatar(avatarSpr);
    if (faceId != "none")
    {
      UIMenu.Instance.OFF_CONNECT_FB();
    }
    else
    {
      UIMenu.Instance.ON_CONNECT_FB();
    }
  }

  static void HandleGetCoinResponse(DownloadHandler handler)
  {
    ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
      .PassTo(SFSObject.NewFromJsonData);
    if (smartfoxResponse.GetInt("message_code") != 1)
      MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
    else BanCa.Session.Gem.Value = smartfoxResponse.GetInt("gem");
  }
}
