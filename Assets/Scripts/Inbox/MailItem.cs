﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using UnityEngine.UI;
using Utils;

public class MailItem : MonoBehaviour
{

  public string id;
  public string title;
  public string date;
  public string content;
  public int state;

  public Sprite closeMail, openMail;
  public Image mailImg;

  public void Init(ISFSObject obj)
  {
    id = obj.GetUtfString("mailid");
    date = obj.GetUtfString("datecreate");
    content = obj.GetUtfString("content");
    state = obj.GetInt("state");
    title = obj.GetUtfString("title");
    if (state == 0)
    {
      mailImg.sprite = closeMail;
    }
    else
    {
      mailImg.sprite = openMail;
    }
    mailImg.SetNativeSize();
    transform.GetChild(1).GetComponent<Text>().text = title;
    transform.GetChild(2).GetComponent<Text>().text = date;
    GetComponent<Button>().onClick.AddListener(() =>
    {
      if (state == 0)
      {
        Network.PostToServer("BehindLogin",
          ("method", "readmail"),
          ("mailid", id))
          .Then(handler =>
          {
            ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
              .PassTo(SFSObject.NewFromJsonData);
            if (smartfoxResponse.GetInt("message_code") != 1)
              MessageBox.instance.Show(
                smartfoxResponse.GetUtfString("message"));
            else
            {
              Inbox.Instance.RefreshInbox();
            }
          });
        state = 1;
        mailImg.sprite = openMail;
        mailImg.SetNativeSize();
      }
      Inbox.Instance.PickMail(id, content);
    });
    gameObject.name = id;
  }


}
