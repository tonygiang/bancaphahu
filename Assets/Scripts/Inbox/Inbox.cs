﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using UnityEngine.UI;
using Utils;

public class Inbox : MonoBehaviour
{

  public static Inbox Instance;

  public GameObject mailItem, mailContainer, mailPanel, firstMenu;
  public Text contentText, readMailNum;
  public GameObject notiMail, NotiMailNumber;
  [SerializeField]
  Sprite clickOn, clickOff;

  void Awake()
  {
    Instance = this;

  }


  public void ShowContent(string _content)
  {

    contentText.text = _content;
  }

  public void GetMail(ISFSArray arr)
  {

    contentText.text = "";
    foreach (Transform chil in mailContainer.transform)
    {
      if (chil.gameObject != null)
        Destroy(chil.gameObject);
    }
    int mailNum = 0;
    for (int i = 0; i < arr.Size(); i++)
    {
      ISFSObject obj = arr.GetSFSObject(i);
      GameObject ga = Instantiate(mailItem);
      ga.transform.SetParent(mailContainer.transform);
      ga.transform.localScale = Vector3.one;
      ga.GetComponent<MailItem>().Init(obj);
      if (obj.GetInt("state") == 0)
      {
        mailNum++;
      }
    }
    if (mailNum > 0)
    {
      notiMail.SetActive(true);
      notiMail.transform.GetChild(0).GetComponent<Text>().text = mailNum.ToString();
      NotiMailNumber.SetActive(true);
      NotiMailNumber.transform.GetChild(0).GetComponent<Text>().text = mailNum.ToString();
    }
    else
    {
      notiMail.SetActive(false);
      NotiMailNumber.SetActive(false);
    }
    readMailNum.text = string.Format("CHƯA ĐỌC: {0}", mailNum);
  }

  public void OpenMail()
  {
    firstMenu.SetActive(false);
    mailPanel.SetActive(true);
    Network.PostToServer("BehindLogin", ("method", "getmailbox"))
      .Then(handler =>
      {
        ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
          .PassTo(SFSObject.NewFromJsonData);
        if (smartfoxResponse.GetInt("message_code") != 1)
          MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
        else GetMail(smartfoxResponse.GetSFSArray("list"));
      });
  }

  public void CloseMail()
  {
    mailPanel.SetActive(false);
    firstMenu.SetActive(true);
  }

  public void RefreshInbox()
  {
    int mailNum = 0;
    foreach (Transform tran in mailContainer.transform)
    {
      if (tran.GetComponent<MailItem>().state == 0)
      {
        mailNum++;
      }
    }
    notiMail.transform.GetChild(0).GetComponent<Text>().text = mailNum.ToString();
    readMailNum.text = string.Format("CHƯA ĐỌC: {0}", mailNum);
    if (mailNum > 0)
    {
      notiMail.SetActive(true);
      NotiMailNumber.SetActive(true);
    }
    else
    {
      notiMail.SetActive(false);
      NotiMailNumber.SetActive(false);
    }

  }

  public void PickMail(string _nameMail, string _content)
  {
    foreach (Transform tran in mailContainer.transform)
    {
      if (tran.name == _nameMail)
      {
        tran.GetComponent<Image>().sprite = clickOn;
      }
      else
      {
        tran.GetComponent<Image>().sprite = clickOff;
      }
    }
    contentText.text = _content;
  }
}
