﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionComplete : MonoBehaviour {

	public static  MissionComplete Instance;

	public GameObject misComPanel;
	bool isButtonMoving=false;

	void Awake(){
		if(Instance != null)
		{
			Destroy(this);
			return;
		}
		DontDestroyOnLoad(this);
		Instance = this;
	}

	public void ShowMissionComplete(string _content){
		Vector2 fro = new Vector2 (0, 90.5f);
		Vector2 to = new Vector2 (0, -90.5f);

		if (!isButtonMoving) {
			isButtonMoving = true;
			misComPanel.SetActive (true);
			misComPanel.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = _content;
			misComPanel.transform.GetChild (0).GetChild (1).gameObject.SetActive (false);
			StartCoroutine (aniCom (misComPanel,fro,to));
		}
	}

	IEnumerator aniCom(GameObject ga, Vector3 fro, Vector3 to)
	{
		float time = 0.5f;

		float currentTime = 0;
		do{
			ga.GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(fro, to, currentTime/time);

			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		ga.GetComponent<RectTransform>().anchoredPosition = to;
		yield return new WaitForSeconds (0.1f);
		misComPanel.transform.GetChild (0).GetChild (1).gameObject.SetActive (true);
		currentTime = 0;
		do{
			misComPanel.transform.GetChild (0).GetChild (1).transform.localScale=Vector3.Lerp(Vector3.zero, new Vector3(1f,1f,1f), currentTime/0.3f);
			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= 0.3f);
		misComPanel.transform.GetChild (0).GetChild (1).transform.localScale = new Vector3 (1f, 1f, 1f);	
		yield return new WaitForSeconds (0.5f);
		currentTime = 0;
		do{
			ga.GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(to, fro, currentTime/time);

			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		ga.GetComponent<RectTransform>().anchoredPosition = fro;
		misComPanel.SetActive (false);
		isButtonMoving = false;

	}
}
