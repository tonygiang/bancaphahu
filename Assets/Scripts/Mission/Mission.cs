﻿using UnityEngine;
using System.Collections;
using Sfs2X.Entities.Data;
using System.Text.RegularExpressions;
using UnityEngine.UI;


public class Mission: MonoBehaviour  {
	public string id;
	public string content;
	public string requiment;
	public string reqType;
	public int reqValue;

	public int reward;
	public string rewardType;

	public int progress;
	public int target;
	public int status;

	public Sprite goldSprite, gemSprite,getSprite,completeSprite;

	public Mission(){}

	public void LoadData(Mission _mission){
		id = _mission.id;
		content = _mission.content;
		requiment = _mission.requiment;
		reqType = _mission.reqType;
		reqValue = _mission.reqValue;

		reward = _mission.reward;
		rewardType = _mission.rewardType;

		progress = _mission.progress;
		target = _mission.target;
		status = _mission.status;
		Init ();
	}

	public void Add(ISFSObject data)
	{
		id = data.GetUtfString("id");
		content = data.GetUtfString("content");
		requiment = Regex.Unescape(data.GetUtfString("requiment"));
		string[] splitReq = requiment.Split (new char[]{'_'});
		reqType = splitReq [0];
		reqValue = int.Parse (splitReq [1]);

		reward = data.GetInt("reward");
		rewardType = data.GetUtfString("reward_type");

		//transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text = string.Format ("0/{1}", reqValue);
		transform.GetChild (1).GetComponent<Text> ().text = content;
		transform.GetChild (3).GetChild (0).GetComponent<Text> ().text = reward.ToString ();
	}

	public Mission(ISFSObject data)
	{
		id = data.GetUtfString("id");
		content = data.GetUtfString("content");
		requiment = Regex.Unescape(data.GetUtfString("requiment"));
		string[] splitReq = requiment.Split (new char[]{'_'});
		reqType = splitReq [0];
		reqValue = int.Parse (splitReq [1]);

		reward = data.GetInt("reward");
		rewardType = data.GetUtfString("reward_type");

	}


	public void Init(){
		transform.GetChild (0).GetChild (0).GetComponent<Image> ().fillAmount = (float)progress / (float)target;
		transform.GetChild (0).GetChild (0).GetChild (0).GetComponent<Text> ().text = string.Format ("{0}/{1}", progress, target);
		transform.GetChild (1).GetComponent<Text> ().text = content;
		if (progress == target) {
			if (status == 0) {
				transform.GetChild (2).GetComponent<Image> ().sprite = getSprite;
				transform.GetChild (2).GetComponent<Button> ().onClick.AddListener (() => {
					SFSManager.instance.SentUserFinishQuest (id);
				});
			} else {
				transform.GetChild (2).GetComponent<Image> ().sprite = completeSprite;
				transform.GetChild (2).GetComponent<Button> ().enabled = false;
			}
			transform.GetChild (2).gameObject.SetActive (true);
			transform.GetChild (2).GetComponent<Image> ().SetNativeSize ();
		} else {
			transform.GetChild (2).gameObject.SetActive (false);
		}
		transform.GetChild (3).GetChild (0).GetComponent<Text> ().text = SaveLoadData.FormatMoney (reward);
		SwitchRewardType (rewardType);
	}

	public void SwitchRequimentType(string _reqType,int _pro,int _tar){
		switch (_reqType) {
		case "none":
			break;
		case "arena":
			break;
		case "taixiu":
			break;
		case "ads":
			break;
		case "quest":
			break;
		}
	}

	public void SwitchRewardType(string _rewardType){
		switch (_rewardType) {
		case "gold":
			transform.GetChild (3).GetComponent<Image> ().sprite = goldSprite;
			transform.GetChild (3).GetComponent<Image> ().SetNativeSize ();
			break;
		case "gem":
			transform.GetChild (3).GetComponent<Image> ().sprite = gemSprite;
			transform.GetChild (3).GetComponent<Image> ().SetNativeSize ();
			break;
		}
	}

	public void CompleteMission(){
		transform.GetChild (2).GetComponent<Image> ().sprite = completeSprite;
		transform.GetChild (2).GetComponent<Image> ().SetNativeSize ();
		transform.GetChild (2).GetComponent<Button> ().enabled = false;
		status = 1;
		MessageBox.instance.Show (string.Format ("Bạn đã nhận được {0} {1}", reward, SwitchRewardTypeText(rewardType)));
	}


	string  SwitchRewardTypeText(string _type){
		if (_type == "gem") {
			return "kim cương";
		}
		return "vàng";
	}
}
