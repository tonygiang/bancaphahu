﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

public class MissionManager : MonoBehaviour
{

	public static MissionManager Instance;

	public GameObject missionContainer, missionItem;

	//public Dictionary<string,Mission> listMission = new Dictionary<string, Mission> ();

	void Awake ()
	{
		Instance = this;
	}


	public void LoadMission (ISFSObject _obj)
	{
		ISFSArray arrMis = _obj.GetSFSArray ("list");
		foreach (ISFSObject mi in arrMis) {
			Mission _mis = new Mission (mi);
			SaveLoadData.Instance.listMission.Add (_mis.id, _mis);

		}
	}

	public void LoadUserMission (ISFSObject _obj)
	{
		ISFSArray arrMis = _obj.GetSFSArray ("list");
		//SaveLoadData.Instance.listMission = new Dictionary<string, Mission> ();
		foreach (ISFSObject mi in arrMis) {
			string _questId = mi.GetUtfString ("questid");
			int _progress = mi.GetInt ("progress");
			int _target = mi.GetInt ("target");
			int _status = mi.GetInt ("status");
			SaveLoadData.Instance.LoadUserMission (_questId, _progress, _target, _status);
		}
	}

	public  void OpenMission ()
	{
//		int dem = 0;
//		foreach(Transform tran in missionContainer.transform)
//		{
//			dem++;
//		}
//		if (dem == 0) {

		foreach (Transform tran in missionContainer.transform) {
			Destroy (tran.gameObject);
		}
		var listmis = new List<string> (SaveLoadData.Instance.listMission.Keys);

		foreach (var key in listmis) {
			Mission mi = SaveLoadData.Instance.listMission [key];
			GameObject ga = Instantiate (missionItem) as GameObject;
			ga.transform.SetParent (missionContainer.transform);
			ga.transform.localScale = Vector3.one;
			ga.GetComponent<Mission> ().LoadData (mi);
		}
//		}
	}

	public void RefresfMission (ISFSObject _obj)
	{
		string _questid = _obj.GetUtfString ("questid");
		foreach (Transform tran in missionContainer.transform) {
			if (tran.GetComponent<Mission> ().id == _questid) {
				tran.GetComponent<Mission> ().CompleteMission ();
				SaveLoadData.Instance.listMission [_questid].status = 1;
			}
		}
		if (_questid != "quest") {
			SaveLoadData.Instance.CountOne_Mission("quest");

		}
	}

	public void ResetMission ()
	{
		foreach (Transform tran in missionContainer.transform) {
			Destroy (tran.gameObject);
		}
	}
}
