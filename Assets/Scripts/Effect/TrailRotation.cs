﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailRotation : MonoBehaviour {

	public float Mult;
	public float TimeMult;

    [SerializeField]
	Transform defaultPos;

	// Use this for initialization
	void Start ()
	{
		// Store initial position
		//defaultPos = transform.position;
	}

	// Update is called once per frame
	void Update ()
	{
		// Used in the example scene
		// Moves the trail by circular trajectory 
		transform.position = defaultPos.position + new Vector3(Mathf.Sin(Time.time * TimeMult) * Mult,  Mathf.Cos(Time.time * TimeMult) * Mult,0f );    
	}
}
