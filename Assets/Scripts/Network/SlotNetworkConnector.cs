﻿
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Sfs2X.Entities.Data;


public class SlotNetworkConnector : MonoBehaviour 
{
	public static SlotNetworkConnector Instance;

	private void Awake()
	{
		DontDestroyOnLoad(this);
		Instance = this;

	}
	public void PostToServer(string server, Dictionary<string, object> parameter, string key, string tokenkey = null)
	{
		string url = server;
		WWWForm form = new WWWForm();
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/x-www-form-urlencoded");

		foreach(var item in parameter)
		{
			form.AddField(item.Key,item.Value.ToString());
			if(item.Key == "nLine")
			{
				Debug.Log(item.Value.ToString());
			}
		}

		//     form.AddField(key, value);       
		if (tokenkey != null)
		{
			headers.Add("token", tokenkey);
		}
		WWW request = new WWW(url, form.data, headers);

		StartCoroutine(WaitForRequest(request, key, SlotNetworkManager.Instance.PostCallBack));



	}


	private IEnumerator WaitForRequest(WWW request,string key, Action<WWW, string> callBack)
	{

		yield return request;
		// check for errors
		if (request.error == null)
		{
			//DataJson Data = new DataJson();
			//SlotNetwork.Instance.dataJsons.json = request.text;
			//SlotNetwork.Instance.dataJsons.IsSuccess = true;
			//SlotNetwork.Instance.dataJsons.message = "Thành Công";
			Debug.Log(string.Format(key+" :{0}", request.text));
			callBack(request,key);
		}
		else
		{
			//MessageBox.instance.Show("THÔNG BÁO", "Tính Năng Bảo Trì");
			Debug.Log("WWW Error: " + request.error);
			Debug.Log(string.Format(key + " :{0}", request.text));
		}

	}
}


