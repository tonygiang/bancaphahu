﻿using System;


public class Sfsproxy
{
	public string android { get; set; }

	public string ios { get; set; }

	public string webgl { get; set; }
}

public class Apiproxy
{
	public string android { get; set; }

	public string ios { get; set; }

	public string webgl { get; set; }
}

public class NetworkModel
{
	public string message { get; set; }

	public int message_code { get; set; }

	public Sfsproxy sfsproxy { get; set; }

	public Apiproxy apiproxy { get; set; }

	public string slot { get; set; }

	public string facebook { get; set; }
	public string onesignal_android { get; set; }
	public string onesignal_ios { get; set; }
	public string iapkey { get; set; }


}


