﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[DisallowMultipleComponent]
public class NetworkInstance : MonoBehaviour
{
  public UnityEvent OnPostError = new UnityEvent();
  public UnityEvent OnReset = new UnityEvent();
  public UnityEvent OnAfterGetServerInfo = new UnityEvent();

  void Awake() => Network.OnPostError.AddListener(OnPostError.Invoke);

  void OnDestroy() => Network.OnPostError.RemoveListener(OnPostError.Invoke);
}
