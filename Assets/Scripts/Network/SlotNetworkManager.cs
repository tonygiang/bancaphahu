﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using Reign;
using Sfs2X;
using Sfs2X.Entities.Data;
using Sfs2X.Entities.Variables;
using Sfs2X.Entities;
using UnityEngine.Networking;
using BE;
public class SlotNetworkManager : MonoBehaviour
{
	public static SlotNetworkManager Instance;

	[HideInInspector]
	string uri = "";
	void Awake ()
	{
		DontDestroyOnLoad (this);
		Instance = this;
		//uri = NetworkManager.instance.netWork.slot;
	}

	public void GetGemInfo ()
	{


		string url = uri + "/slotapi/getgameinfo";
		Dictionary<string, object> parameter = new Dictionary<string, object> ();
		parameter.Add ("userName", UserManager.UserName); // mỗi 1 param là 1 dòng add
		SlotNetworkConnector.Instance.PostToServer (url, parameter, "GetGamgeInfo", UserManager.AccessToken);
		// key phải trùng với call back
		// token truyền vào khi nào cần

	}

	public void JackPotData ()
	{
		string url = uri + "/slotapi/getJP";
		Dictionary<string, object> parameter = new Dictionary<string, object> ();
		parameter.Add ("uid", UserManager.UserName);
		SlotNetworkConnector.Instance.PostToServer (url, parameter, "LoadJackPotData");
	}

	public void Roll (int roomId, int betValue, List<int> nLine)
	{
		string url = uri + "/slotapi/roll";
		Dictionary<string, object> parameter = new Dictionary<string, object> ();
		parameter.Add ("userName", UserManager.UserName);
		parameter.Add ("roomId", roomId);
		parameter.Add ("betValue", betValue);
		string lines = "";
		foreach (var item in nLine) {
			lines += item + ",";
		}
		lines = lines.Substring (0, lines.Length - 1);
		lines = "[" + lines + "]";
		parameter.Add ("nLine", lines);

		SlotNetworkConnector.Instance.PostToServer (url, parameter, "Roll", UserManager.AccessToken);
	}
	public void GetvinhDanh(){
		string url = uri + "/slotapi/getListVinhDanh";
		Dictionary<string, object> parameter = new Dictionary<string, object> ();
		parameter.Add ("username", UserManager.UserName);
		parameter.Add ("page", 1);
		SlotNetworkConnector.Instance.PostToServer (url, parameter, "GetvinhDanh");
	}
	public void GetLichSu(){
		string url = uri + "/slotapi/getRollHistory";
		Dictionary<string, object> parameter = new Dictionary<string, object> ();
		parameter.Add ("username", UserManager.UserName);
		parameter.Add ("page", 1);
		SlotNetworkConnector.Instance.PostToServer (url, parameter, "GetLichSu");
	}
	public void PostCallBack (WWW request, string key)
	{
		Debug.Log (request.text);
		if (key == "GetGamgeInfo") { // key phải trùng key truyền vào hàm SlotLoad.Instance.PostToServer
//			SceneSlotGame.instance.GetGemInfo (request.text);
		}
		if (key == "Roll") {
		//	SceneSlotGame.instance.OnButtonSpin (request.text);
		}
		if (key == "GetvinhDanh") {
//			SceneSlotGame.instance.SetDataVinhDanh (request.text);
		}
		if (key == "GetLichSu") {
			//SceneSlotGame.instance.SetDataLichSu (request.text);
		}
	}
}


