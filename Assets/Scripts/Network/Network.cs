﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sfs2X.Entities;
using Utils;
using System.Linq;
using System;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Network
{
  public static NetworkModel Info = null;
  static readonly Dictionary<Platform, Func<string>> _servers =
    new Dictionary<Platform, Func<string>>
    {
      {Platform.ANDROID, () => Info.apiproxy.android},
      {Platform.IOS, () => Info.apiproxy.ios},
      {Platform.WEBGL, () => Info.apiproxy.webgl},
      {Platform.DEFAULT, () => Info.apiproxy.android}
    };
  public static string Server => _servers[AppInfo.CurrentPlatform]();
  static readonly Dictionary<Platform, Func<string>> _smartfoxProxies =
    new Dictionary<Platform, Func<string>>
    {
      {Platform.ANDROID, () => Info.sfsproxy.android},
      {Platform.IOS, () => Info.sfsproxy.ios},
      {Platform.WEBGL, () => Info.sfsproxy.webgl},
      {Platform.DEFAULT, () => Info.sfsproxy.android}
    };
  public static string SmartfoxProxy =>
    _smartfoxProxies[AppInfo.CurrentPlatform]();
  public static string SmartfoxProxyIP => SmartfoxProxy.Split(':')[0];
  public static int SmartfoxProxyPort => SmartfoxProxy.Split(':')[1].ToInt();
  public static string FacebookID => Info.facebook;

  public static UnityEvent OnBeginPost = new UnityEvent();
  public static UnityEvent OnPostError = new UnityEvent();
  public static UnityEvent OnPostResponse = new UnityEvent();

  static readonly (string, string)[] _telemetryInfo = new[] {
    ("deviceid", SystemInfo.deviceUniqueIdentifier),
    ("client", AppInfo.PlatformString),
    ("versioncode", BanCa.Config.Brand),
    ("branch", BanCa.Config.Brand),
    ("code", BanCa.Config.CodeVersion.ToString())
  };

  static readonly (string, int)[] _telemetryInfoInt = new[] {
    ("date", System.DateTime.Now.DayOfWeek.CastTo<int>() + 1)
  };

  public static PromiseBehaviour<string, DownloadHandler> PostToServer(
    string serverSuffix,
    params (string, string)[] parameterPairs)
  {
    WWWForm form = new WWWForm();
    form.AddField("Content-Type", "application/x-www-form-urlencoded");
    form.AddField("data", parameterPairs
      .Concat(_telemetryInfo)
      .Append(("token", UserManager.AccessToken))
      .ToDictionary(pair => pair.Item1, pair => pair.Item2)
      .PassTo(JObject.FromObject)
      .PassTo(source => source.Merge(_telemetryInfoInt
        .ToDictionary(pair => pair.Item1, pair => pair.Item2)
        .PassTo(JObject.FromObject)))
      .ToString(Formatting.None)
      .PassTo(Crypto.Encrypt));

    OnBeginPost.Invoke();
    return UnityWebRequest.Post(Server + serverSuffix, form)
      .Resolve()
      .IfError(error => OnPostError.Invoke())
      .Then(handler => OnPostResponse.Invoke());
  }

  public static PromiseBehaviour<string, DownloadHandler> Get(string url) =>
    UnityWebRequest.Get(url).Resolve();
}
