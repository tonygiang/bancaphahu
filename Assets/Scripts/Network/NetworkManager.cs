﻿using UnityEngine;

using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using Sfs2X.Entities.Data;
using Sfs2X.Entities;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Utils;

// Only keeping this class around because some parts are not refactored yet
public class NetworkManager : MonoBehaviour
{
  public static NetworkManager instance;
  public Room LobbyRoom, TaixiuRoom, ArenaRoom;

  void Awake()
  {
    if (instance != null)
    {
      Destroy(this);
      return;
    }
    DontDestroyOnLoad(this);
    instance = this;
  }


  //===========POST=======================
  public void PostCallBack(WWW request, string methodName)
  {
    if (request.error != null)
    {
    }
    else
    {
      ISFSObject data = request.text.PassTo(Crypto.Decrypt)
        .PassTo(SFSObject.NewFromJsonData);

      if (data != null && data.ContainsKey("code"))
      {
        ResponseCode(methodName, data);
        return;
      }
    }
  }


  #region  ------------  GET AVATAR  ------------

  string _listAvaDefault = "caduoi,denlong,nemo,rua,sua";

  public void GetImage(string url, Image image)
  {
    if (!_listAvaDefault.Contains(url))
    {
      StartCoroutine(Image(url, image));
    }
    else
    {
      image.sprite = Resources.Load<Sprite>("avatar/" + url);
      image.SetNativeSize();

    }
  }

  IEnumerator Image(string url, Image image)
  {
    WWW request = new WWW(url);

    yield return request;

    if (request.error == null)
    {
      Texture2D texture = request.texture;
      Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
      image.sprite = sprite;
    }
    else
    {
      //			Debug.Log(request.error);
    }
  }

  #endregion

  void ResponseCode(string cmd, ISFSObject isfs)
  {
    string code = isfs.GetText("code");
    Dictionary<string, object> value = null;
    SaveLoadData.listResponseCode.TryGetValue(code, out value);
    if (value == null)
    {
      return;
    }
    object objMessage;
    value.TryGetValue("message", out objMessage);
    string message = objMessage.ToString();

    int gold = 0;
    if (value.ContainsKey("gold"))
    {
      gold = isfs.GetInt("gold");
      message = message.Replace("gold", gold.ToString());
    }

    int gem = 0;
    if (value.ContainsKey("gem"))
    {
      gem = isfs.GetInt("gem");
      message = message.Replace("gem", gem.ToString());

    }

    int bet = 0;
    if (value.ContainsKey("bet"))
    {
      bet = isfs.GetInt("bet");
      message = message.Replace("bet", bet.ToString());
    }

    string displayname = "";
    if (value.ContainsKey("displayname"))
    {

      displayname = isfs.GetText("displayname");
      message = message.Replace("displayname", displayname);
    }

    string roomName = "";
    if (value.ContainsKey("room"))
    {
      roomName = displayname = isfs.GetText("room");
      message = message.Replace("room", roomName);

    }
  }
}