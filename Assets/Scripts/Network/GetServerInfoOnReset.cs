﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Utils;
using System.Text.RegularExpressions;

[DisallowMultipleComponent]
[RequireComponent(typeof(NetworkInstance))]
public class GetServerInfoOnReset : ExtensionBehaviour<NetworkInstance>
{
  // live URL: https://xsapi-ef757.firebaseapp.com/readme_bancaphahu.txt
  // test URL 1: https://xsapi-ef757.firebaseapp.com/readme_ca_test.txt
  // test URL khobau: https://xsapi-ef757.firebaseapp.com/readme_chungca_______0.txt
  // test URL 2: https://xsapi-ef757.firebaseapp.com/readme_ca_ducanh.txt
  [SerializeField] [TextArea] string ServerInfoAddress = string.Empty;

  void Start() => BaseComp.OnReset.AddListener(() => StartCoroutine(Fetch()));

  IEnumerator Fetch()
  {
    UnityWebRequest www = UnityWebRequest.Get(ServerInfoAddress);
    www.SetRequestHeader("Cache-Control", "max-age=0, no-cache, no-store");
    www.SetRequestHeader("Pragma", "no-cache");

    yield return www.SendWebRequest();
    if (www.isNetworkError)
    {
      Debug.Log(www.error);
      StartCoroutine(Fetch());
      yield break;
    }
    else
    {
      Network.Info = www.downloadHandler.text
        .PassTo(new AES().Decrypt)
        .PassTo(Regex.Unescape)
        .PassTo(JsonConvert.DeserializeObject<NetworkModel>);
      BaseComp.OnAfterGetServerInfo.Invoke();
      yield return null;
    }
  }
}