﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(NetworkInstance))]
public class ResetNetworkOnStart : ExtensionBehaviour<NetworkInstance>
{
  void Start() => BaseComp.OnReset.Invoke();
}
