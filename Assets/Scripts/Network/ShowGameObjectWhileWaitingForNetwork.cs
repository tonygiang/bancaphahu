﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

public class ShowGameObjectWhileWaitingForNetwork : MonoBehaviour
{
  [SerializeField] GameObject PrefabToCreateObject = null;
  GameObject _createdObject = null;

  void OnEnable()
  {
    Network.OnBeginPost.AddListener(CreateObject);
    Network.OnPostResponse.AddListener(DestroyObject);
    Network.OnPostError.AddListener(DestroyObject);
  }

  void OnDisable()
  {
    DestroyObject();
    Network.OnBeginPost.RemoveListener(CreateObject);
    Network.OnPostResponse.RemoveListener(DestroyObject);
    Network.OnPostError.RemoveListener(DestroyObject);
  }

  void CreateObject()
  {
    if (_createdObject == null)
      _createdObject = Instantiate(PrefabToCreateObject);
  }

  void DestroyObject()
  {
    if (_createdObject != null) Destroy(_createdObject);
    _createdObject = null;
  }
}