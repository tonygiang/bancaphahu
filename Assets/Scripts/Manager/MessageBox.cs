﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;

public class MessageBox : MonoBehaviour {
	public static MessageBox instance;

	[SerializeField]
	GameObject messageBox, btnOk, btnCancel,smatMessageBox;

	[SerializeField]
	public Text title, content,smartContent,SmartAnswer;
	[SerializeField]
	InputField SmartAnswerInput;

	[SerializeField]
	Button ok, cancel,smartOK,SmartCancel;

	[SerializeField]
	GameObject inviteArenaPanel;

	Vector2 scaleMax =new Vector2(1.1f,1.1f);
	Vector2 scaleMin =new Vector2(0.9f,0.9f);
	Vector2 scaleMax2 =new Vector2(1.4f,1.4f);
	float time = 0.3f;
	float currentTime = 0;

	void Awake()
	{
		if(instance != null)
		{
			Destroy(gameObject);
			return;
		}
		instance = this;
		DontDestroyOnLoad(gameObject);
	}

	public void ShowLoading()
	{
		ResetValue();
		ShowHideButton(MessageBoxType.NONE);
		messageBox.SetActive(true);
		StartCoroutine(loading());
	}

	public void HideLoading()
	{
		StopAllCoroutines();
		messageBox.SetActive(false);
		ResetValue();
	}

	public void Close()
	{
		StartCoroutine("ShowAnim", true);
	}

	IEnumerator ShowAnim(bool isClose)
	{
		if (isClose) {
			time = 0.1f;
			currentTime = 0;
			do
			{
				messageBox.transform.GetChild(0).localScale = Vector2.Lerp(Vector2.one, Vector2.zero, currentTime / time);

				currentTime += Time.deltaTime;
				yield return null;
			} while(currentTime <= time);
			messageBox.transform.GetChild(0).localScale = scaleMax;
			messageBox.SetActive(false);
			//smatMessageBox.SetActive (false);
			ResetValue ();
		} else {

			time = 0.15f;
			currentTime = 0;

			do
			{
				messageBox.transform.GetChild(0).localScale = Vector2.Lerp(Vector2.zero, scaleMax, currentTime / time);
				currentTime += Time.deltaTime;
				yield return null;
			} while(currentTime <= time);
			time = 0.05f;
			currentTime = 0;
			do
			{
				messageBox.transform.GetChild(0).localScale = Vector2.Lerp(scaleMax,scaleMin, currentTime / time);
				currentTime += Time.deltaTime;
				yield return null;
			} while(currentTime <= time);
			messageBox.transform.GetChild(0).localScale = Vector2.one;
		}
	}

	public void Show(string content)
	{
		//titlePopup
		if (string.IsNullOrEmpty (content)) {
			return;
		}
		this.title.text = "THÔNG BÁO";
		this.content.text = content;
		ShowHideButton(MessageBoxType.OK);

		messageBox.SetActive(true);
		StartCoroutine("ShowAnim", false);

		ok.onClick.RemoveAllListeners();
		ok.onClick.AddListener( () => { ButtonSound(); Close(); });
	}

	public void Show(string title, string content)
	{
		if (string.IsNullOrEmpty (content)) {
			return;
		}
		this.title.text = title;
		this.content.text = content;

		ShowHideButton(MessageBoxType.OK);

		messageBox.SetActive(true);
		StartCoroutine("ShowAnim", false);

		ok.onClick.RemoveAllListeners();
		ok.onClick.AddListener( () => { ButtonSound(); Close(); });
	}
	public void ShowQuit(string _title, string _content, MessageBoxType type, Action<MessageBoxCallBack> callBack){
		if (!messageBox.activeInHierarchy) {
			this.title.text = _title;
			this.content.text = _content;
					
			ShowHideButton(type);

			messageBox.SetActive(true);
			StartCoroutine("ShowAnim", false);

			ok.onClick.RemoveAllListeners();
			cancel.onClick.RemoveAllListeners();
			ok.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.OK); Close(); });
			cancel.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.CANCEL); Close(); });
		}
	}

	public void ShowHetDanArena(string title, string content)
	{
		if (!messageBox.activeInHierarchy) {
			this.title.text = title;
			this.content.text = content;
			ShowHideButton (MessageBoxType.OK);

			messageBox.SetActive (true);
			StartCoroutine ("ShowAnim", false);

			ok.onClick.RemoveAllListeners ();
			ok.onClick.AddListener (() => {
				ButtonSound ();
				Close ();
			});
		}
	}

	public void ShowGoToShop(string title, string content, MessageBoxType type, Action<MessageBoxCallBack> callBack){
		if (!messageBox.activeInHierarchy) {
			this.title.text = title;
			this.content.text = content;

			ShowHideButton(type);

			messageBox.SetActive(true);
			StartCoroutine("ShowAnim", false);

			ok.onClick.RemoveAllListeners();
			cancel.onClick.RemoveAllListeners();
			ok.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.OK); Close(); });
			cancel.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.CANCEL); Close(); });
		}
	}

	public void Show(string title, string content, MessageBoxType type, Action<MessageBoxCallBack> callBack)
	{
		this.title.text = title;
		this.content.text = content;
		ShowHideButton(type);

		messageBox.SetActive(true);
		StartCoroutine("ShowAnim", false);

		ok.onClick.RemoveAllListeners();
		cancel.onClick.RemoveAllListeners();
		ok.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.OK); Close(); });
		cancel.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.CANCEL); Close(); });
	}

	public void Show(string title, string content, MessageBoxType type, Action<MessageBoxCallBack> callBack, string btnOKText)
	{
		this.title.text = title;
		this.content.text = content;
		ShowHideButton(type);

		messageBox.SetActive(true);
		StartCoroutine("ShowAnim", false);

		ok.onClick.RemoveAllListeners();
		cancel.onClick.RemoveAllListeners();
		ok.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.OK); Close();});
		cancel.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.CANCEL); Close(); });

	}

	public void Show(string title, string content, MessageBoxType type, Action<MessageBoxCallBack> callBack, string btnOKText, string btnCancelText)
	{
		this.title.text = title;
		this.content.text = content;

		ShowHideButton(type);

		messageBox.SetActive(true);
		StartCoroutine("ShowAnim", false);

		ok.onClick.RemoveAllListeners();
		cancel.onClick.RemoveAllListeners();
		ok.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.OK); Close();});
		cancel.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.CANCEL); Close();});

	}
	public void showSmartIQ(string content, MessageBoxType type, Action<MessageBoxCallBack> callBack){
		this.smartContent.text = content;
		ShowHideButton(type);
		smatMessageBox.SetActive(true);
		smatMessageBox.transform.GetChild (0).transform.GetChild (2).transform.GetChild (1).GetComponent<Text> ().text = "";
		StartCoroutine("ShowAnim", false);
		smartOK.onClick.RemoveAllListeners();
		SmartCancel.onClick.RemoveAllListeners();
		smartOK.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.OK); closeSmartIQ(); });
		SmartCancel.onClick.AddListener( () => { ButtonSound(); callBack(MessageBoxCallBack.CANCEL); closeSmartIQ(); });
	}
	void closeSmartIQ(){
		smatMessageBox.SetActive(false);
		smartContent.text = "";
		SmartAnswer.text = "";
		SmartAnswerInput.Select ();
		SmartAnswerInput.text = "";
		SmartAnswerInput.placeholder.GetComponent<Text>().text = "Đáp án";
	}
	void ResetValue()
	{
		title.text = "";
		content.text = "";
		//btnOk.transform.GetChild(0).GetComponent<Text>().text = "Đồng ý";
		//btnCancel.transform.GetChild(0).GetComponent<Text>().text = "Từ chối";

		ShowHideButton(MessageBoxType.OK_CANCEL);
	}

	void ShowHideButton(MessageBoxType type)
	{
		switch(type)
		{
		case MessageBoxType.OK:
			btnOk.SetActive(true);
			btnCancel.SetActive(false);

			break;

		case MessageBoxType.OK_CANCEL:
			btnOk.SetActive(true);
			btnCancel.SetActive(true);

			break;

		case MessageBoxType.NONE:
			btnOk.SetActive(false);
			btnCancel.SetActive(false);

			break;
		}
	}

	void ButtonSound()
	{
		SoundManager.Instance.ButtonSound();
	}

	IEnumerator loading()
	{
		while(true)
		{
			content.text = "Loading";
			yield return new WaitForSeconds(0.5f);

			content.text = "Loading.";
			yield return new WaitForSeconds(0.5f);

			content.text = "Loading..";
			yield return new WaitForSeconds(0.5f);

			content.text = "Loading...";
			yield return new WaitForSeconds(0.7f);
		}
	}

	// arena
	public void OpenInvite(string message, string roomName, int bet){
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name == "Menu" && UIMenu.Instance.logined) {
			inviteArenaPanel.transform.GetChild (0).GetChild (0).GetComponent<Text> ().text = message;
			inviteArenaPanel.transform.GetChild (0).GetChild (1).GetComponent<Button> ().onClick.AddListener (() => {
				//SFSManager.instance.sfs.Send(new JoinRoomRequest(_roomName));
				if (SFSManager.instance.isLobby) {
					SFSManager.instance.VALID_REQUEST_ARENA(roomName,bet);
					inviteArenaPanel.SetActive (false);
				} else {
					MessageBox.instance.Show ("Phòng chơi không tồn tại ");
				}

			});
			inviteArenaPanel.transform.GetChild (0).GetChild (2).GetComponent<Button> ().onClick.AddListener (() => {
				inviteArenaPanel.SetActive (false);
			});
			inviteArenaPanel.SetActive (true);
			Text _textTime = inviteArenaPanel.transform.GetChild (0).GetChild (4).GetComponent<Text> ();
			StartCoroutine (CountdownHide (_textTime));
		}
	}

	IEnumerator CountdownHide(Text _textTime){
		int _timeStart = 4;
		do {
			_textTime.text = _timeStart.ToString ();
			yield return new WaitForSeconds (1f);
			_timeStart--;
		} while(_timeStart > 0);
		inviteArenaPanel.SetActive (false);
	}




}

public enum MessageBoxType
{
	OK_CANCEL, OK, NONE
}

public enum MessageBoxCallBack
{
	OK, CANCEL
}