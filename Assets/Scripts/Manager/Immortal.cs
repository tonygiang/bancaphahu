﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using BE;
public class Immortal : MonoBehaviour {
	public static Immortal instance;



	[SerializeField]
	AudioMixer audioMixer;

	float mute = -80, unmute = 0;
	string musicVol = "musicVol", soundVol = "soundVol";

	void Awake()
	{
		if(instance != null)
		{
			Destroy(gameObject);
			return;
		}
		instance = this;
		DontDestroyOnLoad(this.gameObject);
        Application.targetFrameRate = 60;
        

	}

  

	public void SetAudio()
	{
		audioMixer.SetFloat(musicVol, PlayerPrefs.GetFloat(musicVol, unmute));
		audioMixer.SetFloat(soundVol, PlayerPrefs.GetFloat(soundVol, unmute));
	}


	//====================================================================================================================================
	//====================================================================================================================================
	// Sound

	public bool SoundButtonState()
	{
		if(PlayerPrefs.GetFloat(soundVol, unmute) == unmute)
		{
			BESetting.SoundVolume = 100;
			return true;
		}
		BESetting.SoundVolume = 0;
		return false;
	}

	public bool MusicButtonState()
	{
		if(PlayerPrefs.GetFloat(musicVol, unmute) == unmute)
		{
			return true;
		}

		return false;
	}

	public bool SoundButton()
	{
		if(PlayerPrefs.GetFloat(soundVol, unmute) == unmute)
		{
			audioMixer.SetFloat(soundVol, mute);
			PlayerPrefs.SetFloat(soundVol, mute);
			BESetting.SoundVolume = 0;
			return false;
		}
		BESetting.SoundVolume = 100;
		audioMixer.SetFloat(soundVol, unmute);
		PlayerPrefs.SetFloat(soundVol, unmute);
		return true;
	}

	public bool MusicButton()
	{
		if(PlayerPrefs.GetFloat(musicVol, unmute) == unmute)
		{
			audioMixer.SetFloat(musicVol, mute);
			PlayerPrefs.SetFloat(musicVol, mute);

			return false;
		}

		audioMixer.SetFloat(musicVol, unmute);
		PlayerPrefs.SetFloat(musicVol, unmute);
		return true;
	}


    //====================================================================================================================================
    //====================================================================================================================================
    // Google Analytics
#if UNITY_ANDROID
	string client = "Android";
#elif UNITY_IOS
	string client = "iOS";
#elif UNITY_WEBGL
    string client = "web";

#endif

   

		
}
