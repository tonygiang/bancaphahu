﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
	public static SoundManager Instance;

	[SerializeField]
	AudioClip buttonClick, missionComplete, goldChestFull, winChest, yay, kaching, waveSound, slotspin, slotstop, btnslotspin, slotwinnorlam, slotjackpot,chicken;

	[SerializeField]
	AudioClip[] shootSounds;

	AudioSource source;

	[Header("Bầu Cua")]
	[SerializeField]
	AudioClip xocDia,thang,thua,moBat,chipMove,clock,sapBatDau;

	// arena
	[SerializeField]
	AudioClip win,lose;

	[SerializeField]
	AudioClip caTo,caMap;

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
		Instance = this;

		source = GetComponent<AudioSource>();
	}

	public void ButtonSound()
	{
		source.PlayOneShot(buttonClick);
	}

	public void ShootSound(int  gunIndex)
	{
		source.PlayOneShot(shootSounds[gunIndex - 1]);
	}



	public void MissionComplete()
	{
		source.PlayOneShot(missionComplete);
	}

	public void GoldChestFull()
	{
		source.PlayOneShot(goldChestFull);
	}

	public void WinChest()
	{
		source.PlayOneShot(winChest);
	}

	public void YaySound()
	{
		source.PlayOneShot(yay);
	}

	public void Kaching()
	{
		source.PlayOneShot(kaching);
	}

	public void WaveSound()
	{
		source.PlayOneShot (waveSound);
	}

	// Slot
	public void SpinSound()
    {
        source.PlayOneShot(slotspin);
    }
    public void SpinStopSound()
    {
        source.PlayOneShot(slotstop);
    }
    public void ClickSpinSound()
    {
        source.PlayOneShot(btnslotspin);

    }
    public void SlotWinNormalSound()
    {
        source.PlayOneShot(slotwinnorlam);

    }
    public void SlotWinJackpotSound()
    {
        source.PlayOneShot(slotjackpot);

    }
	
	// BauCua
	public void Thang(){
		source.PlayOneShot (thang);
	}
	public void Thua(){
		source.PlayOneShot (thua);
	}
	public void XocDia(){
		source.PlayOneShot (xocDia);
	}
	public void ChipMove(){
		source.PlayOneShot (chipMove);
	}
	public void MoBat(){
		source.PlayOneShot (moBat);
	}
	public void Clock(){
		source.PlayOneShot (clock);
	}

	public void SapBatDau(){
		source.PlayOneShot (sapBatDau);
	}


	// arena
	public void ARENA_WIN(){
		source.PlayOneShot (win);
	}
	public void ARENA_LOSE(){
		source.PlayOneShot (lose);
	}

	public void AN_Ca_To(){
		source.PlayOneShot (caTo);
	}
	public void CaMapKeu(){
		source.PlayOneShot (caMap);
	}
	public void ChickenDock(){
		source.PlayOneShot (chicken);
	}
}
