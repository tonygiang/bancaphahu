﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Facebook.MiniJSON;
using System.IO;
using System.Text;
using System;
using Newtonsoft.Json;
using System.Linq;
using Utils;
using UnityEngine.Networking;

public class SaveLoadData : MonoBehaviour
{
  public static SaveLoadData Instance;
  public Dictionary<string, FishInfo> listFishInfo = new Dictionary<string, FishInfo>();
  public Dictionary<string, BulletInfo> listBulletInfo = new Dictionary<string, BulletInfo>();
  public static Dictionary<string, Dictionary<string, object>> listResponseCode = new Dictionary<string, Dictionary<string, object>>();

  public Dictionary<string, FishPath> ListFishPath = new Dictionary<string, FishPath>();

  //  SỐ NGƯỜI CHƠI TRONG PHÒNG   
  [HideInInspector]
  public int jackpotNumberGold, jackpotNumberGem;

  //  NHIỆM VỤ HẰNG NGÀY 
  public Dictionary<string, Mission> listMission = new Dictionary<string, Mission>();
  public static int CheckInValue;
  [HideInInspector] public bool isKick = false;
  [HideInInspector] public string reasonKick = "Mất kết nối tới máy chủ. Vui lòng đăng nhập lại để tiếp tục chơi";

  // checkversion 
  int _versionFish, _versionBullet, _versionCheckIn, _versionQuest, _versionCard, _versionSms, _versionRulerEvent, _versionPath, _versionIap, _version_responsecode;

  [HideInInspector] public int _betArena;
  [HideInInspector] public string faceappid, faceapplink, homepage;
  // bau cua 
  public ISFSObject AllUserBauCua;

  void Awake()
  {
    if (Instance != null)
    {
      Destroy(this);
      return;
    }
    DontDestroyOnLoad(this);
    Instance = this;
  }

  public void GetFishInfo(ISFSArray arr, string _method)
  {
    for (int i = 0; i < arr.Size(); i++)
    {
      FishInfo f = new FishInfo(arr.GetSFSObject(i));
      listFishInfo.Add(f.Type, f);
    }
    string _content = arr.ToJson();
    PlayerPrefs.SetInt("version_fish", _versionFish);

    // SaveData(_method, _content);
    //		GuideManager.Instance.LoadBirdInfo ();
  }
  public void GetBulletInfo(ISFSArray arr, string _method)
  {
    for (int i = 0; i < arr.Size(); i++)
    {
      BulletInfo b = new BulletInfo(arr.GetSFSObject(i));
      listBulletInfo.Add(b.Type, b);
    }
    PlayerPrefs.SetInt("version_bullet", _versionBullet);
  }

  public void GetFishPathInfo(ISFSArray arr, string _method)
  {
    for (int i = 0; i < arr.Size(); i++)
    {
      FishPath b = new FishPath(arr.GetSFSObject(i));
      if (ListFishPath.ContainsKey(b.id))
      {
        //                Debug.Log("same key " + b.id);
      }
      else
      {
        ListFishPath.Add(b.id, b);
      }
    }
    PlayerPrefs.SetInt("version_path", _versionPath);
  }

  public void SetGoldNumber(ISFSObject obj)
  {
    ISFSObject asyn = SFSObject.NewFromJsonData(obj.GetUtfString("asyn"));

    jackpotNumberGold = asyn.GetInt("jackpot");
    if (UIMenu.Instance != null)
    {
      UIMenu.Instance.SetNumberPlayer("gold", jackpotNumberGold);
    }
  }

  public void SetGemNumber(ISFSObject obj)
  {
    ISFSObject asyn = SFSObject.NewFromJsonData(obj.GetUtfString("asyn"));

    jackpotNumberGem = asyn.GetInt("jackpot");
    if (UIMenu.Instance != null)
    {
      UIMenu.Instance.SetNumberPlayer("gem", jackpotNumberGem);
    }
  }

  //  NHIỆM VỤ HẰNG NGÀY 
  public void CountOne_Mission(string missionID) => listMission.Values
    .Where(m => m.reqType == missionID)
    .Where(m => m.progress < m.target)
    .ForEach(m => ++m.progress)
    .Where(m => m.reqType != "quest")
    .Where(m => m.progress == m.target)
    .ForEach(m => MissionComplete.Instance.ShowMissionComplete(m.content));

  public void LoadUserMission(string _questId, int _pro, int _tar, int _sta)
  {
    listMission[_questId].progress = _pro;
    listMission[_questId].target = _tar;
    listMission[_questId].status = _sta;
  }

  public void SendUpdateMission() => listMission.Values
    .Where(m => m.progress != 0)
    .Where(m => m.status == 0)
    .ForEach(m => Network.PostToServer("BehindLogin",
      ("method", "updatequest"),
      ("questid", m.id),
      ("progress", m.progress.ToString())));

  void OnApplicationQuit()
  {
    PlayerPrefs.Save();
    if (SFSManager.instance.isLogInSfs)
    {
      //			Method.UpdateInfo ();
      SendUpdateMission();
    }
  }

  void OnApplicationPause(bool pauseStatus)
  {
    if (pauseStatus)
    {
      //			Method.UpdateInfo ();
      SendUpdateMission();

    }
  }

  public void GetQuestInfo(ISFSArray arr, string _method)
  {
    int _countQuest = listMission.Count;
    if (_countQuest == 0)
    {

    }
    else
    {
      listMission.Clear();
    }
    foreach (ISFSObject mi in arr)
    {
      Mission _mis = new Mission(mi);
      listMission.Add(_mis.id, _mis);
    }
  }
  public void GetSmsInfo(ISFSObject _obj)
  {
    Shop.Instance.LoadSMS(_obj);
    string _content = _obj.ToJson();
  }

  public static string FormatMoney(int coin) =>
    coin.ToString("N0", BanCa.Config.MoneyFormat);

  //"version_bird":1,"version_bullet":1,"version_checkin":null,"version_quest":null,"version_card":null,"version_sms":null,"
  public void SaveCheckVersion(string _funtion, ISFSObject _data)
  {
    _versionFish = _data.GetInt("version_fish");
    _versionBullet = _data.GetInt("version_bullet");
    _versionCheckIn = _data.GetInt("version_checkin");
    _versionQuest = _data.GetInt("version_quest");
    _versionCard = _data.GetInt("version_card");
    _versionSms = _data.GetInt("version_sms");
    _versionRulerEvent = _data.GetInt("version_ruler_event");
    _versionPath = _data.GetInt("version_path");
    _versionIap = _data.GetInt("version_iap");
    _versionIap = _data.GetInt("version_iap");
    _version_responsecode = _data.GetInt("version_responsecode");
    faceappid = _data.GetUtfString("faceappid");
    faceapplink = _data.GetUtfString("faceapplink");
    homepage = _data.GetUtfString("homepage");

    // version fish
    if (!PlayerPrefs.HasKey("version_fish"))
    {

      Network.PostToServer("CheckVersion", ("method", "getfish"))
        .Then(HandleGetFishResponse);
    }
    else
    {
      if (PlayerPrefs.GetInt("version_fish") != _versionFish)
      {
        Network.PostToServer("CheckVersion", ("method", "getfish"))
          .Then(HandleGetFishResponse);
        return;
      }
      else
      {
        string fish = PlayerPrefs.GetString("fish");
        fish = Crypto.Decrypt(fish);
        //                print(fish);
        ISFSObject sfsFish = SFSObject.NewFromJsonData(fish);
        ISFSArray _arr = sfsFish.GetSFSArray("list");
        for (int i = 0; i < _arr.Size(); i++)
        {
          FishInfo f = new FishInfo(_arr.GetSFSObject(i));
          listFishInfo.Add(f.Type, f);
        }
      }

    }
    //		Method.GetBullet ();
    // version bullet
    if (!PlayerPrefs.HasKey("version_bullet"))
    {
      Network.PostToServer("CheckVersion", ("method", "getbullet"))
        .Then(HandleGetBulletResponse);
    }
    else
    {
      if (PlayerPrefs.GetInt("version_bullet") != _versionBullet)
      {
        Network.PostToServer("CheckVersion", ("method", "getbullet"))
          .Then(HandleGetBulletResponse);
        return;
      }
      else
      {
        string bullet = PlayerPrefs.GetString("bullet");
        bullet = Crypto.Decrypt(bullet);
        ISFSObject jsonBullet = SFSObject.NewFromJsonData(bullet);
        ISFSArray _arr = jsonBullet.GetSFSArray("list");
        for (int i = 0; i < _arr.Size(); i++)
        {
          BulletInfo b = new BulletInfo(_arr.GetSFSObject(i));
          listBulletInfo.Add(b.Type, b);
        }
      }

    }


    if (!PlayerPrefs.HasKey("version_responsecode"))
      Network.PostToServer("CheckVersion", ("method", "getresponsecode"))
        .Then(HandleGetResponseCode);
    else
    {
      int localVersion = PlayerPrefs.GetInt("version_responsecode");
      int serverVersion = _version_responsecode;
      if (localVersion != serverVersion)
        Network.PostToServer("CheckVersion", ("method", "getresponsecode"))
          .Then(HandleGetResponseCode);
      else PlayerPrefs.GetString("responsecode")
        .PassTo(Crypto.Decrypt)
        .PassTo(SaveResponseCode);
    }
    if (!PlayerPrefs.HasKey("version_path"))
      Network.PostToServer("CheckVersion", ("method", "getpath"))
        .Then(HandleGetPathResponse);
    else
    {
      int _versionPath = PlayerPrefs.GetInt("version_path");
      int serverVersion = this._versionPath;
      if (_versionPath != serverVersion) Network.PostToServer("CheckVersion",
        ("method", "getpath"))
        .Then(HandleGetPathResponse);
      else
      {
        string path = PlayerPrefs.GetString("path");
        path = Crypto.Decrypt(path);
        //                print("ơpath: " + path);
        ISFSObject sfs = SFSObject.NewFromJsonData(path);
        ISFSArray sfsArr = sfs.GetSFSArray("paths");
        int size = sfsArr.Size();
        for (int i = 0; i < size; i++)
        {
          ISFSObject obj = sfsArr.GetSFSObject(i);
          FishPath b = new FishPath(obj);
          if (ListFishPath.ContainsKey(b.id))
          {
            Debug.Log("same key " + b.id);
          }
          else
          {
            ListFishPath.Add(b.id, b);
          }
        }
      }
    }

    //version_quest
    Network.PostToServer("CheckVersion", ("method", "getquest"))
      .Then(handler => handler.text.PassTo(Crypto.Decrypt)
        .PassTo(SFSObject.NewFromJsonData)
        .GetSFSArray("list")
        .MakeTuple("version_quest")
        .PassTo(GetQuestInfo));
    Network.PostToServer("BehindLogin", ("method", "getreward7day"))
      .Then(HandleGetDailyRewardResponse);
    if (BanCa.Session.Functions.Contains("sms"))
      Network.PostToServer("CheckVersion", ("method", "getsmsformat"))
        .Then(handler =>
        {
          ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          if (smartfoxResponse.GetInt("message_code") == 0)
            MessageBox.instance.Show("THÔNG BÁO!",
              smartfoxResponse.GetUtfString("message"));
          else GetSmsInfo(smartfoxResponse);
        });
    if (BanCa.Session.Functions.Contains("card"))
      Network.PostToServer("CheckVersion", ("method", "getcardformat"))
        .Then(handler => handler.text.PassTo(Crypto.Decrypt)
          .PassTo(SFSObject.NewFromJsonData)
          .PassTo(Shop.Instance.LoadCardFormat));



    // version path 
    if (!PlayerPrefs.HasKey("version_path"))
      Network.PostToServer("CheckVersion", ("method", "getpath"))
        .Then(HandleGetPathResponse);
    else
    {
      if (PlayerPrefs.GetInt("version_path") != _versionPath)
      {
        Network.PostToServer("CheckVersion", ("method", "getpath"))
          .Then(HandleGetPathResponse);
        return;
      }
      else
      {
        string path = PlayerPrefs.GetString("path");
        path = Crypto.Decrypt(path);

        ISFSObject sfs = SFSObject.NewFromJsonData(path);
      }
    }
  }

  public void DropUser(ISFSObject _obj)
  {
    if (_obj.ContainsKey("reason"))
    {
      reasonKick = _obj.GetUtfString("reason");
    }
  }

  public void SaveResponseCode(string text)
  {

    ISFSObject sfs = SFSObject.NewFromJsonData(text);
    ISFSArray sfsArr = sfs.GetSFSArray("codes");
    int size = sfsArr.Size();
    for (int i = 0; i < size; i++)
    {
      ISFSObject obj = sfsArr.GetSFSObject(i);
      string code = obj.GetText("code");
      string[] keys = obj.GetKeys();
      Dictionary<string, object> dict_content = new Dictionary<string, object>();
      foreach (string k in keys)
      {
        object value = obj.GetText(k);
        dict_content.Add(k, value);
      }
      listResponseCode.Add(code, dict_content);
    }
    PlayerPrefs.SetInt("version_responsecode", _version_responsecode);
  }

  void HandleGetFishResponse(DownloadHandler handler)
  {
    PlayerPrefs.SetString("fish", handler.text);
    PlayerPrefs.Save();
    handler.text.PassTo(Crypto.Decrypt)
      .PassTo(SFSObject.NewFromJsonData)
      .GetSFSArray("list")
      .MakeTuple("version_fish")
      .PassTo(GetFishInfo);
  }

  void HandleGetBulletResponse(DownloadHandler handler)
  {
    PlayerPrefs.SetString("bullet", handler.text);
    PlayerPrefs.Save();
    handler.text.PassTo(Crypto.Decrypt)
      .PassTo(SFSObject.NewFromJsonData)
      .GetSFSArray("list")
      .MakeTuple("version_bullet")
      .PassTo(GetBulletInfo);
  }

  void HandleGetPathResponse(DownloadHandler handler)
  {
    PlayerPrefs.SetString("path", handler.text);
    PlayerPrefs.Save();
    handler.text.PassTo(Crypto.Decrypt)
      .PassTo(SFSObject.NewFromJsonData)
      .GetSFSArray("paths")
      .MakeTuple("version_path")
      .PassTo(GetFishPathInfo);
  }

  void HandleGetDailyRewardResponse(DownloadHandler handler) =>
    BanCa.Gifts.LoginRewards = handler.text.PassTo(Crypto.Decrypt)
      .PassTo(SFSArray.NewFromJsonData)
      .Cast<ISFSObject>()
      .Select(sfs => new BanCa.LoginReward
      {
        ID = sfs.GetInt("id"),
        Gem = sfs.GetInt("reward"),
        Key = sfs.GetInt("keyKhoBau"),
        Skills = sfs.GetSFSArray("skillData").Cast<ISFSObject>()
          .Select(skillSFS => new
          {
            Key = skillSFS.GetUtfString("Id"),
            Value = skillSFS.GetInt("Quantity")
          })
          .ToDictionary(p => p.Key, p => p.Value)
      })
      .OrderBy(r => r.ID);

  void HandleGetResponseCode(DownloadHandler handler)
  {
    PlayerPrefs.SetString("responsecode", handler.text);
    PlayerPrefs.Save();
    SaveResponseCode(handler.text.PassTo(Crypto.Decrypt));
  }
}
