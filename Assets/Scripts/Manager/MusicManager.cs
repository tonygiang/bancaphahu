using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour  {
	public static MusicManager Instance;

	[SerializeField]
	AudioClip doubleTimeMusic, bgmSlot;

	[SerializeField]
	AudioClip[] menu, inGame;

	[SerializeField]
	AudioClip inGameBauCua;

	AudioSource source;

	void Awake()
	{
		if(Instance != null)
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
		Instance = this;

		source = GetComponent<AudioSource>();
	}

	public void PlayMenuMusic()
	{
		source.clip = menu[Random.Range(0, menu.Length)];
		source.Play();
	}

	public void Play2XTimeMusic()
	{
		source.clip = doubleTimeMusic;
		source.Play();
	}

	public void PlayInGameMusic()
	{
		source.clip = inGame[Random.Range(0, inGame.Length)];
		source.Play();
	}
	
	public void PlayInGameBauCua(){
		source.clip = inGameBauCua;
		source.Play();
	}
	
	public IEnumerator DecreaseVolume()
    {
        do
        {
            source.volume -= 0.02f;
            yield return new WaitForEndOfFrame();
        } while (source.volume > 0.3f);
        source.volume = 0.3f;
    }
	
    public IEnumerator IncreaseVolume()
    {
        do
        {
            source.volume += 0.02f;
            yield return new WaitForEndOfFrame();
        } while (source.volume < 1f);
        source.volume = 1;
    }
    
    public void PlaySlotMachine()
    {
        source.clip = bgmSlot;
        source.Play();
    }
}
