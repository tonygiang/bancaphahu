﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoBat : MonoBehaviour {
	public static MoBat Instance;

	void Awake()
	{

		Instance = this;
	}
	private void OnTriggerExit2D(Collider2D col){
		if (col.CompareTag ("Bowl") && !TaiXiu.Instance.isOpenBowl) {
			StartCoroutine (TaiXiu.Instance.RunAniResult ());
			TaiXiu.Instance.isOpenBowl = true;

		}
	}
}
