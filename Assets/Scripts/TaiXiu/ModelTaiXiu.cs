﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;




[System.Serializable]
public class BxhTaiXiu
{
	public string user;
	public int reward;
}
[System.Serializable]
public class HistoryTaiXiu{
	public string phien;
	public string time;
	public string cua;
	public string kq;
	public int dat;
	public int tra;
	public int nhan;
}
[System.Serializable]
public class ThongKeTaiXiu{
	public string phien;
	public string number;
	public int tong;
	public string result;
}
[System.Serializable]
public class RootTaiXiu
{
	public BxhTaiXiu[] BxhTaiXius;
	public HistoryTaiXiu[] HistoryTaiXius; 
	public ThongKeTaiXiu[] ThongKeTaiXius;
}


