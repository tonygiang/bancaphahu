﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using CodeMonkey.Utils;
using BE;
using Utils;

public class TaiXiu : MonoBehaviour
{
  public static TaiXiu Instance;

  public GameObject aniXucXac, xucXacResult, betPanel, TutPanel, TopPanel, HistoryPanel, WinTai, WinXiu, CountDownBelow10s, NanPanel, btnNan, ThongKePanel;
  public Transform UserTAI;
  public Text TaiBetText = null;
  public Transform UserXIU;
  public Text XiuBetText = null;
  public Sprite[] xucxacSprite;
  public string betType = "";
  public int betValue = 0;

  public Text textPhase, textTime, textThongBao, textTimeCuoc;
  public Image bgTextPhase;

  public GameObject historyContainer, historyItem, bgTime;
  public Sprite hisTai, hisXiu;
  public GameObject[] TopList, HistoryList;
  public Sprite btnDef, btnAct;

  public Sprite txON, txOFF;
  [HideInInspector]
  private bool IsSpin = false;
  public Image[] listBtnTaiXiu;
  Vector3 rota360 = new Vector3(0, 0, 360f);
  bool isBet = false;

  static string OCuoc = "";
  static int SoTienCuoc = 0;
  [HideInInspector]
  public bool isNan = false;
  [HideInInspector]
  public bool isOpenBowl = false;
  Vector3 endScale = new Vector3(0.5f, 0.5f, 1f);
  Vector3 endPos = new Vector3(0, 100f, 1f);
  GameObject _objResultTaiXiu;
  GameObject _objEffectTaiXiu;
  string _valueTaiXiu;
  bool isAniResultdone = false;
  //bieu do
  public RectTransform graphContainer, LabelTemplateX, LabelTemplateY, DashTemplateX, DashTemplateY;
  public RectTransform graphXucXacContainer, LabelXucXacTemplateX, LabelXucXacTemplateY, DashXucXacTemplateX, DashXucXacTemplateY;
  public Sprite WhitecircleSprite, BlackcircleSprite, Line, XucXac1, XucXac2, XucXac3;
  public Font FontThongKe;
  public GameObject checkBoxTong, CheckBoxXucxac1, CheckBoxXucxac2, CheckBoxXucxac3, Phien, KetQua;
  private bool CheckedTong = true;
  private bool CheckedXx1 = true;
  private bool CheckedXx2 = true;
  private bool CheckedXx3 = true;
  [SerializeField] Text PhaseText = null;
  [SerializeField] Text ResultText = null;
  [SerializeField] GameObject LoseAnimation = null;
  [SerializeField] GameObject WinAnimation = null;

  void Awake()
  {
    Instance = this;
    string formattedBetMoney =
      $"{SaveLoadData.FormatMoney(SoTienCuoc / 1000)}K";
    if (OCuoc == "tai") TaiBetText.text = formattedBetMoney;
    if (OCuoc == "xiu") XiuBetText.text = formattedBetMoney;
  }

  void Update()
  {
    if (IsSpin)
    {
      float rotAmmount = 360 * Time.deltaTime;
      float curRot = CountDownBelow10s.transform.localRotation.eulerAngles.z;
      CountDownBelow10s.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, curRot + rotAmmount));

    }
  }

  public GameObject TaiXiuPanel;

  public void TAIXIU_open()
  {
    SoundManager.Instance.ButtonSound();
    if (!TaiXiuPanel.activeInHierarchy)
    {
      Network.PostToServer("BehindLogin", ("method", "gethistorytaixiu"))
        .Then(handler =>
        {
          ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          if (smartfoxResponse.GetInt("message_code") != 1)
            MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
          else ShowHistory(smartfoxResponse);
        });
    }
    TaiXiuPanel.SetActive(!TaiXiuPanel.activeInHierarchy);
  }

  public void TaiXiu_Nan_Open()
  {
    SoundManager.Instance.ButtonSound();
    if (!isNan)
    {
      NanPanel.SetActive(true);
      btnNan.transform.GetComponent<Image>().sprite = btnAct;
      isNan = true;
    }
    else
    {
      isNan = false;
      NanPanel.SetActive(false);
      btnNan.transform.GetComponent<Image>().sprite = btnDef;
    }
  }

  public void TAIXIU_chat_open()
  {
    MessageController.instance.OpenChatBox();
    SoundManager.Instance.ButtonSound();
  }

  public void TAIXIU_close()
  {
    TaiXiuPanel.SetActive(false);
    _objEffectTaiXiu.SetActive(false);
    SoundManager.Instance.ButtonSound();
  }

  public void Tut_Open()
  {
    TutPanel.SetActive(true);
    SoundManager.Instance.ButtonSound();
  }

  public void Tut_Close()
  {
    TutPanel.SetActive(false);

    SoundManager.Instance.ButtonSound();
  }

  public IEnumerator RunAniResult()
  {

    bool flag = true;
    isOpenBowl = true;
    float time = 1f;
    float currentTime = 0;
    // thong bao ben an

    for (int x = 0; x < 100; x++)
    {
      _objEffectTaiXiu.SetActive(true);
      _objEffectTaiXiu.transform.eulerAngles = Vector3.Lerp(Vector3.zero, rota360, currentTime / time);
      currentTime += Time.deltaTime;
      if (flag)
      {
        //_objResult.SetActive (false);
        _objResultTaiXiu.transform.localScale = new Vector3(0.85f, 0.85f, 1);
        flag = false;
      }
      else
      {
        //_objResult.SetActive (true);

        _objResultTaiXiu.transform.localScale = new Vector3(1, 1, 1);
        flag = true;

      }
      yield return new WaitForSeconds(0.1f);
    }
    _objEffectTaiXiu.SetActive(false);

    ShowLastHistory(_valueTaiXiu);


  }

  IEnumerator RunAniXucXac(GameObject _objResult, GameObject _objEffect, string _value)
  {
    xucXacResult.SetActive(false);
    aniXucXac.SetActive(true);

    for (int i = 0; i < xucxacSprite.Length; i++)
    {
      aniXucXac.GetComponent<Image>().sprite = xucxacSprite[i];
      aniXucXac.GetComponent<Image>().SetNativeSize();
      yield return new WaitForSeconds(0.05f);
    }
    aniXucXac.SetActive(false);
    //RunAniResult ();
    if (isNan)
    {
      NanPanel.transform.GetChild(0).gameObject.SetActive(true);
      NanPanel.transform.GetChild(0).transform.localPosition = new Vector3(-16, -15, 0);
    }
    xucXacResult.SetActive(true);

    if (!isNan)
    {
      StartCoroutine(RunAniResult());
    }
  }

  public void ClearBet()
  {
    UserTAI.GetChild(1).GetComponent<Text>().text = "( 0 )";
    UserTAI.GetChild(2).GetComponent<Text>().text = "0";
    TaiBetText.text = "0";

    UserXIU.GetChild(1).GetComponent<Text>().text = "( 0 )";
    UserXIU.GetChild(2).GetComponent<Text>().text = "0";
    XiuBetText.text = "0";
    SoTienCuoc = 0;
    OCuoc = "";
    xucXacResult.SetActive(false);
    betPanel.SetActive(false);
  }



  public void Bet(int _betValue)
  {
    betValue = _betValue;
    OK();
  }

  public void OK()
  {
    if (betValue == 0 && string.IsNullOrWhiteSpace(betType)) return;
    if (OCuoc == "" || betType == OCuoc)
      SFSManager.instance.SentBet(betValue, betType);
    else MessageBox.instance.Show("Bạn chỉ có thể đặt một cửa");
  }

  public void Cancel()
  {
    betPanel.SetActive(false);
  }

  public void Phase(ISFSObject _obj)
  {
    PhaseText.text = $"#{_obj.GetUtfString("phien")}";
    int _index = _obj.GetInt("index");
    int _time = _obj.GetInt("time");
    textTime.text = _time > 9 ? "00:" + _time : "00:0" + _time;
    switch (_index)
    {
      // Tạo màn chơi  mới 
      case 0:
        bgTime.gameObject.SetActive(true);
        textTimeCuoc.gameObject.SetActive(false);
        xucXacResult.SetActive(false);
        ClearBet();
        if (_time == 3)
        {
          StartCoroutine(ShowTimeAndPhase("Tạo màn chơi mới"));
        }

        break;
      // Đặt cửa 
      case 1:
        isBet = true;
        bgTime.gameObject.SetActive(false);
        textTimeCuoc.gameObject.SetActive(true);
        xucXacResult.SetActive(false);
        textTimeCuoc.text = _time > 9 ? "00:" + _time : "00:0" + _time;
        if (_time == 30)
        {
          StartCoroutine(ShowTimeAndPhase("Bắt đầu đặt cược"));
        }
        if (_time < 10)
        {
          IsSpin = true;
          CountDownBelow10s.SetActive(true);
        }
        aniXucXac.SetActive(false);
        break;

      // Cân kèo
      case 2:
        isBet = false;
        IsSpin = false;
        CountDownBelow10s.SetActive(false);
        bgTime.gameObject.SetActive(true);
        textTimeCuoc.gameObject.SetActive(false);
        if (_time == 3)
        {
          StartCoroutine(ShowTimeAndPhase("Cân kèo"));
        }

        break;

      // Tung xúc xắc 
      case 3:
        bgTime.gameObject.SetActive(false);
        textTimeCuoc.gameObject.SetActive(false);
        betPanel.SetActive(false);


        break;
      //nan
      case 4:
        bgTime.gameObject.SetActive(true);

        if (isAniResultdone && isOpenBowl)
        {

          StartCoroutine(RunAniResult());
        }
        if (_time == 5 && !isOpenBowl && isNan)
        {
          StartCoroutine(RunAniMoBat());

        }
        if (_time == 1 && isNan)
        {
          NanPanel.transform.GetChild(0).gameObject.SetActive(false);
          isOpenBowl = false;
        }

        break;
      // Trà tiền thắng
      case 5:
        StartCoroutine(ShowTimeAndPhase("Trả thưởng"));
        bgTime.gameObject.SetActive(true);
        textTimeCuoc.gameObject.SetActive(false);
        break;
    }


  }

  private IEnumerator RunAniMoBat()
  {
    for (int i = 0; i < 255; i++)
    {
      NanPanel.transform.GetChild(0).transform.localPosition = new Vector3(-16f, (-15 - i), 0);
      yield return new WaitForSeconds(0.001f);
    }
    StartCoroutine(RunAniResult());
  }

  public IEnumerator ShowTimeAndPhase(string _curPhase)
  {

    bgTextPhase.gameObject.SetActive(true);
    textPhase.text = _curPhase;
    while (bgTextPhase.color.a < 1)
    {
      textPhase.color += new Color(0, 0, 0, 0.1f);
      bgTextPhase.color += new Color(0, 0, 0, 0.1f);
      yield return null;
    }
    yield return new WaitForSeconds(1f);

    while (bgTextPhase.color.a > 0)
    {
      textPhase.color -= new Color(0, 0, 0, 0.1f);
      bgTextPhase.color -= new Color(0, 0, 0, 0.1f);
      yield return null;
    }
    bgTextPhase.gameObject.SetActive(false);

  }

  IEnumerator ShowResultAnimation(int deltaMoney)
  {
    ResultText.text = $"{(deltaMoney > 0 ? "+" : string.Empty)}"
      + $"{deltaMoney.ToString("N0", BanCa.Config.MoneyFormat)}";
    ResultText.gameObject.SetActive(true);
    var animationToShow = deltaMoney >= 0 ? WinAnimation : LoseAnimation;
    animationToShow.SetActive(true);
    yield return new WaitForSeconds(3f);
    ResultText.gameObject.SetActive(false);
    animationToShow.SetActive(false);
  }

  public IEnumerator ShowThongBao(string _curPhase)
  {
    textThongBao.text = _curPhase;
    textThongBao.gameObject.SetActive(true);
    float timeStart = 0;
    while (timeStart < 1f)
    {
      textThongBao.transform.localScale = Vector3.Lerp(Vector3.zero, endScale, timeStart / 1f);
      textThongBao.GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(Vector3.zero, endPos, timeStart / 1f);
      timeStart += Time.deltaTime;
      yield return null;
    }
    yield return new WaitForSeconds(0.5f);
    textThongBao.gameObject.SetActive(false);
  }

  public void ShowHistory(ISFSObject _obj)
  {
    string[] listHistory = _obj.GetUtfStringArray("List");
    foreach (Transform tran in historyContainer.transform)
    {
      Destroy(tran.gameObject);
    }

    for (int i = listHistory.Length - 1; i >= 0; --i)
    {

      GameObject ga = Instantiate(historyItem) as GameObject;
      ga.SetActive(true);
      ga.transform.SetParent(historyContainer.transform);
      ga.transform.localScale = new Vector3(1f, 1f, 1f);
      ga.transform.localPosition = new Vector3(ga.transform.localPosition.x, ga.transform.localPosition.y, 1f);
      if (listHistory[i] == "tai")
      {
        ga.GetComponent<Image>().sprite = hisTai;
      }
      else
      {
        ga.GetComponent<Image>().sprite = hisXiu;
      }
      ga.GetComponent<Image>().SetNativeSize();
      if (i == 0)
      {
        ga.transform.GetChild(0).gameObject.SetActive(true);
      }
    }

  }

  public void ShowLastHistory(string _value)
  {
    if (historyContainer.transform.childCount > 0)
    {
      historyContainer.transform.GetChild(historyContainer.transform.childCount - 1).GetChild(0).gameObject.SetActive(false);
      Destroy(historyContainer.transform.GetChild(0).gameObject);
      GameObject ga = Instantiate(historyItem) as GameObject;
      ga.SetActive(true);
      ga.transform.SetParent(historyContainer.transform);
      ga.transform.localScale = new Vector3(1f, 1f, 1f);
      ga.transform.localPosition = new Vector3(ga.transform.localPosition.x, ga.transform.localPosition.y, 1f);
      switch (_value)
      {
        case "tai":
          ga.GetComponent<Image>().sprite = hisTai;
          break;
        case "xiu":
          ga.GetComponent<Image>().sprite = hisXiu;
          break;

      }
      ga.GetComponent<Image>().SetNativeSize();
      ga.transform.GetChild(0).gameObject.SetActive(true);
    }
  }

  // Res Bet User TaiXiu
  public void ResBetUserTaiXiu(ISFSObject _obj)
  {
    int _messageCode = _obj.GetInt("message_code");
    if (_messageCode == 0)
    {
      MessageBox.instance.Show("Bạn không đủ kim cương để đặt cược");
      return;
    }

    if (_messageCode != 1) return;

    int _money = _obj.GetInt("money");
    string _door = _obj.GetUtfString("door");
    int _gem = _obj.GetInt("gem");
    BanCa.Session.Gem.Value = _gem;
    switch (_door)
    {
      case "tai":
        TaiBetText.text =
          SaveLoadData.FormatMoney(_money / 1000).ToString() + "K";
        break;
      case "xiu":
        XiuBetText.text =
          SaveLoadData.FormatMoney(_money / 1000).ToString() + "K";
        break;
    }
    OCuoc = _door;
    SoTienCuoc = _money;
  }
  // Back Money
  public void BackMoney(ISFSObject _obj)
  {
    int _money = _obj.GetInt("money");
    int gem = _obj.GetInt("gem");

    BanCa.Session.Gem.Value = gem;
    string _door = _obj.GetUtfString("door");
    int moneyBack = _obj.GetInt("doortotal");
    StartCoroutine(
      ShowThongBao($"Hoàn tiền {SaveLoadData.FormatMoney(_money)}"));

    switch (_door)
    {
      case "tai":
        TaiBetText.text = SaveLoadData.FormatMoney(moneyBack / 1000).ToString() + "K";
        break;
      case "xiu":
        XiuBetText.text = SaveLoadData.FormatMoney(moneyBack / 1000).ToString() + "K";
        break;
    }

    //
    //		switch (OCuoc) {
    //		case "tai":
    //			UserTAI.GetChild (3).GetComponent<Text> ().text = SaveLoadData.FormatMoney (SoTienCuoc / 1000).ToString () + "K";
    //			break;
    //		case "xiu":
    //			UserXIU.GetChild (3).GetComponent<Text> ().text = SaveLoadData.FormatMoney (SoTienCuoc / 1000).ToString () + "K";
    //			break;
    //		}
  }
  // Result Tai Xiu
  public void ResultTaiXiu(ISFSObject _obj)
  {
    int[] _result = _obj.GetIntArray("result");
    int sum = 0;
    for (int i = 0; i < _result.Length; i++)
    {
      xucXacResult.transform.GetChild(i).GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/TaiXiu/xucxac/" + _result[i]);
      sum += _result[i];
    }
    GameObject _objResult, _objEffect;
    string _value = "";
    if (sum >= 11)
    {
      _objResult = UserTAI.GetChild(4).gameObject;
      _value = "tai";
      _objEffect = WinTai;
    }
    else
    {
      _objResult = UserXIU.GetChild(4).gameObject;
      _value = "xiu";
      _objEffect = WinXiu;
    }
    _objResultTaiXiu = _objResult;
    _objEffectTaiXiu = _objEffect;
    _valueTaiXiu = _value;
    StartCoroutine(RunAniXucXac(_objResult, _objEffect, _value));

  }
  // Update TaiXiu
  public void UpdateTaiXiu(ISFSObject _obj)
  {
    int _userTai = _obj.GetInt("user_tai");
    int _userXiu = _obj.GetInt("user_xiu");
    int _moneyTai = _obj.GetInt("money_tai");
    int _moneyXiu = _obj.GetInt("money_xiu");

    UserTAI.GetChild(1).GetComponent<Text>().text = _userTai.ToString();
    UserTAI.GetChild(2).GetComponent<Text>().text = SaveLoadData.FormatMoney(_moneyTai / 1000).ToString() + "K";

    UserXIU.GetChild(1).GetComponent<Text>().text = _userXiu.ToString();
    UserXIU.GetChild(2).GetComponent<Text>().text = SaveLoadData.FormatMoney(_moneyXiu / 1000).ToString() + "K";
  }
  // Choose TAI XIU
  public void ChooseType(string _betType)
  {
    if (isBet)
    {
      betType = _betType;
      betPanel.SetActive(true);
      for (int i = 0; i < listBtnTaiXiu.Length; i++)
      {
        if (listBtnTaiXiu[i].name == _betType)
        {
          listBtnTaiXiu[i].sprite = txON;
        }
        else
        {
          listBtnTaiXiu[i].sprite = txOFF;
        }
      }

    }
    else
    {
      MessageBox.instance.Show("Hiện tại không thể đặt cược ");
    }

  }

  // Reward TaiXiu
  public void RewardTaiXiu(ISFSObject _obj)
  {
    int _money = _obj.GetInt("money");
    int gem = _obj.GetInt("gem");

    StartCoroutine(ShowResultAnimation(_money));

    BanCa.Session.Gem.Value = gem;
    if (_money >= 0)
    {
      StartCoroutine(ShowThongBao(string.Format("{0}", SaveLoadData.FormatMoney(_money))));
    }
    else
    {
      //StartCoroutine (ShowThongBao (string.Format ("{0}",  SaveLoadData.FormatMoney(_money * (-1)))));
    }

  }
  public void CanKeoTaiXiu(ISFSObject _obj)
  {
    int _money = _obj.GetInt("money");
    int gem = _obj.GetInt("gem");

    BanCa.Session.Gem.Value = gem;

    if (_money > 0)
    {
      StartCoroutine(ShowThongBao(string.Format("{0}", _money)));
    }

  }
  //thongke_taixiu, res_thongke_taixiu, key data: thongke
  public void Top_Open()
  {
    GetTop10TaiXiu();
    TopPanel.SetActive(true);
    betPanel.SetActive(false);
    SoundManager.Instance.ButtonSound();
  }

  public void Top_Close()
  {

    TopPanel.SetActive(false);
    SoundManager.Instance.ButtonSound();
  }

  public void GetTop10TaiXiu()
  {
    SFSManager.instance.GetBXHTaiXiu();
  }

  public void setBxhTaixiu(ISFSObject obj)
  {
    string data = obj.GetUtfString("thongke_bxh");

    RootTaiXiu returnedData = new RootTaiXiu();
    returnedData = JsonUtility.FromJson<RootTaiXiu>("{\"BxhTaiXius\":" + data + "}");
    //	Debug.Log (returnedData);
    int dataLenght = returnedData.BxhTaiXius.Length;
    for (int i = 0; i < TopList.Length; i++)
    {
      TopList[i].transform.transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString();
      if (i < dataLenght)
      {
        TopList[i].transform.GetChild(1).GetComponent<Text>().text = returnedData.BxhTaiXius[i].user;
        TopList[i].transform.GetChild(2).GetComponent<Text>().text = SaveLoadData.FormatMoney(returnedData.BxhTaiXius[i].reward);
      }
      else
      {
        TopList[i].transform.GetChild(1).GetComponent<Text>().text = "---";
        TopList[i].transform.GetChild(2).GetComponent<Text>().text = "---";
      }
    }
  }

  public void Info_Open()
  {
    HistoryPanel.SetActive(true);
    getHistoryTaiXiu();
    betPanel.SetActive(false);
    SoundManager.Instance.ButtonSound();
  }

  public void Info_Close()
  {
    HistoryPanel.SetActive(false);
    SoundManager.Instance.ButtonSound();
  }

  public void getHistoryTaiXiu()
  {
    SFSManager.instance.GetHistoryTaiXiu();
  }

  public void setHistoryTaiXiu(ISFSObject obj)
  {
    string data = obj.GetUtfString("thongke_history");

    RootTaiXiu returnedData = new RootTaiXiu();
    returnedData = JsonUtility.FromJson<RootTaiXiu>("{\"HistoryTaiXius\":" + data + "}");
    int dataLenght = returnedData.HistoryTaiXius.Length;
    for (int i = 0; i < HistoryList.Length; i++)
    {
      if (i < dataLenght)
      {
        HistoryList[i].transform.GetChild(0).GetComponent<Text>().text = returnedData.HistoryTaiXius[i].phien;
        HistoryList[i].transform.GetChild(1).GetComponent<Text>().text = returnedData.HistoryTaiXius[i].time;
        HistoryList[i].transform.GetChild(2).GetComponent<Text>().text = returnedData.HistoryTaiXius[i].cua == "xiu" ? "Xỉu" : "Tài";
        HistoryList[i].transform.GetChild(3).GetComponent<Text>().text = returnedData.HistoryTaiXius[i].kq == "xiu" ? "Xỉu" : "Tài";
        HistoryList[i].transform.GetChild(4).GetComponent<Text>().text = returnedData.HistoryTaiXius[i].dat.ToString();
        HistoryList[i].transform.GetChild(5).GetComponent<Text>().text = returnedData.HistoryTaiXius[i].tra.ToString();
        HistoryList[i].transform.GetChild(6).GetComponent<Text>().text = returnedData.HistoryTaiXius[i].nhan.ToString();
      }
      else
      {
        HistoryList[i].transform.GetChild(0).GetComponent<Text>().text = "---";
        HistoryList[i].transform.GetChild(1).GetComponent<Text>().text = "---";
        HistoryList[i].transform.GetChild(2).GetComponent<Text>().text = "---";
        HistoryList[i].transform.GetChild(3).GetComponent<Text>().text = "---";
        HistoryList[i].transform.GetChild(4).GetComponent<Text>().text = "---";
        HistoryList[i].transform.GetChild(5).GetComponent<Text>().text = "---";
        HistoryList[i].transform.GetChild(6).GetComponent<Text>().text = "---";
      }
    }
  }

  public void TAIXIU_ThongKe_open()
  {
    ThongKePanel.SetActive(true);
    SFSManager.instance.GetThongKeTaiXiu();
    SoundManager.Instance.ButtonSound();
  }

  public void TAIXIU_ThongKe_Close()
  {
    ThongKePanel.SetActive(false);
    SoundManager.Instance.ButtonSound();
  }

  public void ThongKe(int type)
  {
    if (type == 0)
    {
      foreach (Transform child in graphContainer.transform)
      {
        if (child.name == "circle" || child.name == "dotConnection")
        {
          child.gameObject.SetActive(!CheckedTong);
        }
      }
      checkBoxTong.SetActive(!CheckedTong);
      CheckedTong = !CheckedTong;

    }
    else
    {
      foreach (Transform child in graphXucXacContainer.transform)
      {
        if (child.name == "circle" + type || child.name == "dotConnection" + type)
        {
          if (type == 1)
          {
            child.gameObject.SetActive(!CheckedXx1);
          }
          if (type == 2)
          {
            child.gameObject.SetActive(!CheckedXx2);
          }
          if (type == 3)
          {
            child.gameObject.SetActive(!CheckedXx3);
          }
        }
      }
      if (type == 1)
      {
        CheckBoxXucxac1.SetActive(!CheckedXx1);
        CheckedXx1 = !CheckedXx1;
      }
      if (type == 2)
      {
        CheckBoxXucxac2.SetActive(!CheckedXx2);
        CheckedXx2 = !CheckedXx2;
      }
      if (type == 3)
      {
        CheckBoxXucxac3.SetActive(!CheckedXx3);
        CheckedXx3 = !CheckedXx3;
      }
    }


  }

  public void SetThongKeTaiXiu(ISFSObject obj)
  {
    string data = obj.GetUtfString("thongke");
    RootTaiXiu returnedData = new RootTaiXiu();

    returnedData = JsonUtility.FromJson<RootTaiXiu>("{\"ThongKeTaiXius\":" + data + "}");
    int MaxPointLoop = Mathf.Min(returnedData.ThongKeTaiXius.Length - 1, 20);

    //set phien gan nhat
    string strPhien = "#(" + returnedData.ThongKeTaiXius[MaxPointLoop].phien + ")";
    string IsTai = returnedData.ThongKeTaiXius[MaxPointLoop].result == "tai" ? "Tài" : "Xỉu";
    string strKetqua = IsTai + " " + returnedData.ThongKeTaiXius[MaxPointLoop].tong + " (" + returnedData.ThongKeTaiXius[MaxPointLoop].number.Replace(',', '-') + ")";
    KetQua.GetComponent<Text>().text = strKetqua;
    Phien.GetComponent<Text>().text = strPhien;
    foreach (Transform child in graphContainer.transform)
    {
      if (child.name == "circle" || child.name == "dotConnection" || child.name == "DashTemplateX(Clone)" || child.name == "LabelTemplateX(Clone)" || child.name == "DashTemplateY(Clone)" || child.name == "LabelTemplateY(Clone)")
      {
        GameObject.Destroy(child.gameObject);
      }
    }
    foreach (Transform child in graphXucXacContainer.transform)
    {
      if (child.name == "circle1" || child.name == "circle2" || child.name == "circle3" || child.name == "dotConnection1" || child.name == "dotConnection2" || child.name == "dotConnection3" || child.name == "DashTemplateX(Clone)" || child.name == "LabelTemplateX(Clone)" || child.name == "DashTemplateY(Clone)" || child.name == "LabelTemplateY(Clone)")
      {
        GameObject.Destroy(child.gameObject);
      }
    }
    List<int> tongValueList = new List<int>();
    List<int> XucXac1 = new List<int>();
    List<int> XucXac2 = new List<int>();
    List<int> XucXac3 = new List<int>();
    for (int i = 0; i < MaxPointLoop; i++)
    {
      tongValueList.Add(returnedData.ThongKeTaiXius[i].tong);
      int num1 = 1;
      int num2 = 1;
      int num3 = 1;
      if (returnedData.ThongKeTaiXius[i].number.Split(',').Length > 1)
      {
        if (!string.IsNullOrEmpty(returnedData.ThongKeTaiXius[i].number.Split(',')[0]))
        {
          num1 = int.Parse(returnedData.ThongKeTaiXius[i].number.Split(',')[0]);
        }
        if (!string.IsNullOrEmpty(returnedData.ThongKeTaiXius[i].number.Split(',')[0]))
        {
          num2 = int.Parse(returnedData.ThongKeTaiXius[i].number.Split(',')[1]);
        }
        if (!string.IsNullOrEmpty(returnedData.ThongKeTaiXius[i].number.Split(',')[0]))
        {
          num3 = int.Parse(returnedData.ThongKeTaiXius[i].number.Split(',')[2]);
        }
      }
      XucXac1.Add(num1);
      XucXac2.Add(num2);
      XucXac3.Add(num3);
    }
    XucXac1.Reverse();
    XucXac2.Reverse();
    XucXac3.Reverse();
    tongValueList.Reverse();
    showGraph(tongValueList);
    showGraphXucXac(XucXac1, XucXac2, XucXac3);
  }

  #region ============ BieudoTong ============

  private GameObject CreateCircle(Vector3 anchoredPosition, bool IsTai, int value)
  {
    //dot
    GameObject gameObject = new GameObject("circle", typeof(Image));
    gameObject.transform.SetParent(graphContainer, false);
    RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
    rectTransform.anchoredPosition3D = anchoredPosition;
    rectTransform.sizeDelta = new Vector3(30, 30, 1f);
    rectTransform.anchorMin = new Vector3(0, 0, 1f);
    rectTransform.anchorMax = new Vector3(0, 0, 1f);
    //inside text
    GameObject textObject = new GameObject("Text", typeof(Text));
    textObject.transform.SetParent(gameObject.transform);
    textObject.GetComponent<Text>().text = value.ToString();
    textObject.GetComponent<Text>().font = FontThongKe;
    textObject.GetComponent<Text>().fontSize = 15;
    textObject.GetComponent<Text>().fontStyle = FontStyle.Bold;
    textObject.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
    RectTransform rectTransform2 = textObject.GetComponent<RectTransform>();
    rectTransform2.anchoredPosition3D = new Vector3(15f, 15f, 1f);
    rectTransform2.sizeDelta = new Vector3(30, 30, 1f);
    rectTransform2.anchorMin = new Vector3(0, 0, 1f);
    rectTransform2.anchorMax = new Vector3(0, 0, 1f);

    textObject.transform.localScale = new Vector3(1f, 1f, 1f);

    if (!IsTai)
    {
      gameObject.GetComponent<Image>().sprite = WhitecircleSprite;
      textObject.GetComponent<Text>().color = Color.black;
    }
    else
    {
      gameObject.GetComponent<Image>().sprite = BlackcircleSprite;
      textObject.GetComponent<Text>().color = Color.white;
    }

    return gameObject;
  }

  private void showGraph(List<int> valueList)
  {
    float graphHeight = graphContainer.sizeDelta.y * 9f;
    Debug.Log("graphHeight: " + graphHeight);
    float yMaximum = 180f;
    float Xsize = 50f;
    GameObject lastCircleGameObject = null;
    for (int i = 0; i < valueList.Count; i++)
    {
      bool flag = false;
      float Xposition = 20 + i * Xsize;
      float Yposition = (valueList[i] / yMaximum) * graphHeight;

      if (valueList[i] >= 11)
      {
        flag = true;
      }
      GameObject circleGameObject = CreateCircle(new Vector3(Xposition, Yposition, 1f), flag, valueList[i]);
      if (lastCircleGameObject != null)
      {

        CreateDotconnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition3D, circleGameObject.GetComponent<RectTransform>().anchoredPosition3D);

      }
      lastCircleGameObject = circleGameObject;

      RectTransform labelX = Instantiate(LabelTemplateX);
      labelX.SetParent(graphContainer);
      labelX.gameObject.SetActive(true);
      labelX.anchoredPosition3D = new Vector3(Xposition, 0, 1f);
      //labelX.GetComponent<Text> ().text = (i+1).ToString ();
      labelX.GetComponent<Text>().font = FontThongKe;
      labelX.GetComponent<Text>().fontSize = 15;
      labelX.GetComponent<Text>().fontStyle = FontStyle.Bold;
      labelX.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
      RectTransform rectTransformlabelX = labelX.GetComponent<RectTransform>();
      rectTransformlabelX.sizeDelta = new Vector3(30, 30, 1f);
      rectTransformlabelX.anchorMin = new Vector3(0, 0, 1f);
      rectTransformlabelX.anchorMax = new Vector3(0, 0, 1f);
      rectTransformlabelX.localScale = new Vector3(1f, 1f, 1f);
      //tao duong ke doc
      RectTransform DashY = Instantiate(DashTemplateY);
      DashY.SetParent(graphContainer);
      DashY.gameObject.SetActive(true);
      DashY.anchoredPosition3D = new Vector3(Xposition, -83, 1f);
      RectTransform rectTransformDashY = DashY.GetComponent<RectTransform>();

      rectTransformDashY.localScale = new Vector3(0.1f, 1f, 1f);
      rectTransformDashY.SetAsFirstSibling();
    }
    //tao duong ke ngang2
    int separatorCount = 6;
    for (int i = 0; i <= separatorCount; i++)
    {
      RectTransform labelY = Instantiate(LabelTemplateY);
      labelY.SetParent(graphContainer);
      labelY.gameObject.SetActive(true);
      float normalizedValue = i * 0.1f / separatorCount;
      labelY.anchoredPosition3D = new Vector3(-7f, normalizedValue * graphHeight, 1f);
      labelY.GetComponent<Text>().text = Mathf.RoundToInt(normalizedValue * yMaximum).ToString();
      labelY.GetComponent<Text>().font = FontThongKe;
      labelY.GetComponent<Text>().fontSize = 15;
      labelY.GetComponent<Text>().fontStyle = FontStyle.Bold;
      labelY.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
      RectTransform rectTransformlabelX = labelY.GetComponent<RectTransform>();
      rectTransformlabelX.sizeDelta = new Vector3(30, 30, 1f);
      rectTransformlabelX.anchorMin = new Vector3(0, 0, 1f);
      rectTransformlabelX.anchorMax = new Vector3(0, 0, 1f);
      rectTransformlabelX.localScale = new Vector3(1f, 1f, 1f);

      RectTransform DashX = Instantiate(DashTemplateX);
      DashX.SetParent(graphContainer);
      DashX.gameObject.SetActive(true);
      DashX.anchoredPosition3D = new Vector3(-7f, normalizedValue * graphHeight - 83, 1f);
      RectTransform rectTransformDashX = DashX.GetComponent<RectTransform>();

      rectTransformDashX.localScale = new Vector3(0.1f, 1f, 1f);
      rectTransformDashX.SetAsFirstSibling();

    }
  }

  private void CreateDotconnection(Vector3 dotPositionA, Vector3 dotPositionB, int? xucXac = null)
  {
    GameObject gameObject = new GameObject("dotConnection" + xucXac, typeof(Image));
    gameObject.GetComponent<Image>().sprite = Line;
    gameObject.GetComponent<Image>().color = new Color(0, 184, 29, .5f);
    if (xucXac != null && xucXac.Value == 1)
    {
      gameObject.GetComponent<Image>().color = new Color(129, 255, 184, .5f);
    }
    else if (xucXac != null && xucXac.Value == 2)
    {
      gameObject.GetComponent<Image>().color = new Color(45, 255, 188, .5f);
    }
    else if (xucXac != null && xucXac.Value == 3)
    {
      gameObject.GetComponent<Image>().color = new Color(347, 255, 184, .5f);
    }
    //gameObject.transform.localScale = new Vector3 (1f, 1f, 1f);
    if (xucXac != null)
    {
      gameObject.transform.SetParent(graphXucXacContainer.transform);
      gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 1f);
    }
    else
    {
      gameObject.transform.SetParent(graphContainer.transform);
      gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, 1f);
    }

    RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
    Vector3 dir = (dotPositionB - dotPositionA).normalized;
    float distance = Vector3.Distance(dotPositionA, dotPositionB);
    rectTransform.localScale = new Vector3(1f, 1f, 1f);
    rectTransform.anchorMin = new Vector3(0, 0, 1f);
    rectTransform.anchorMax = new Vector3(0, 0, 1f);
    rectTransform.sizeDelta = new Vector3(distance, 3f, 1f);
    rectTransform.SetAsFirstSibling();
    rectTransform.anchoredPosition = dotPositionA + dir * distance * .4f;
    rectTransform.localEulerAngles = new Vector3(0, 0, UtilsClass.GetAngleFromVectorFloat(dir));
    //yield return null;
  }

  #endregion

  #region ============ BieuDoxucXac ============

  private GameObject CreateCircleXucXac(Vector3 anchoredPosition, int value)
  {
    //dot
    GameObject gameObject = new GameObject("circle" + value, typeof(Image));
    gameObject.transform.SetParent(graphXucXacContainer, false);
    RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
    rectTransform.anchoredPosition = anchoredPosition;
    rectTransform.sizeDelta = new Vector3(30, 30, 1f);
    rectTransform.anchorMin = new Vector3(0, 0, 1f);
    rectTransform.anchorMax = new Vector3(0, 0, 1f);
    if (value == 1)
    {
      gameObject.GetComponent<Image>().sprite = XucXac1;
    }
    else if (value == 2)
    {
      gameObject.GetComponent<Image>().sprite = XucXac2;
    }
    else if (value == 3)
    {
      gameObject.GetComponent<Image>().sprite = XucXac3;
    }
    return gameObject;

  }

  private void showGraphXucXac(List<int> listXucXac1, List<int> listXucXac2, List<int> listXucXac3)
  {
    float graphHeight = graphXucXacContainer.sizeDelta.y * 28f;
    Debug.Log("graphHeight: " + graphXucXacContainer);
    float yMaximum = 180f;
    float Xsize = 50f;
    GameObject lastCircleGameObject1 = null;
    GameObject lastCircleGameObject2 = null;
    GameObject lastCircleGameObject3 = null;

    for (int i = 0; i < listXucXac1.Count; i++)
    {

      float Xposition = 20 + i * Xsize;
      float Yposition1 = (listXucXac1[i] / yMaximum) * graphHeight;
      float Yposition2 = (listXucXac2[i] / yMaximum) * graphHeight;
      float Yposition3 = (listXucXac3[i] / yMaximum) * graphHeight;

      GameObject circleGameObject1 = CreateCircleXucXac(new Vector3(Xposition, Yposition1, 1f), 1);
      GameObject circleGameObject2 = CreateCircleXucXac(new Vector3(Xposition, Yposition2, 1f), 2);
      GameObject circleGameObject3 = CreateCircleXucXac(new Vector3(Xposition, Yposition3, 1f), 3);

      if (lastCircleGameObject1 != null)
      {

        CreateDotconnection(lastCircleGameObject1.GetComponent<RectTransform>().anchoredPosition, circleGameObject1.GetComponent<RectTransform>().anchoredPosition, 1);
        CreateDotconnection(lastCircleGameObject2.GetComponent<RectTransform>().anchoredPosition, circleGameObject2.GetComponent<RectTransform>().anchoredPosition, 2);
        CreateDotconnection(lastCircleGameObject3.GetComponent<RectTransform>().anchoredPosition, circleGameObject3.GetComponent<RectTransform>().anchoredPosition, 3);

      }
      lastCircleGameObject1 = circleGameObject1;
      lastCircleGameObject2 = circleGameObject2;
      lastCircleGameObject3 = circleGameObject3;

      RectTransform labelX = Instantiate(LabelXucXacTemplateX);
      labelX.SetParent(graphXucXacContainer);
      labelX.gameObject.SetActive(true);
      labelX.anchoredPosition3D = new Vector3(Xposition, 0, 1f);
      //labelX.GetComponent<Text> ().text = (i+1).ToString ();
      labelX.GetComponent<Text>().font = FontThongKe;
      labelX.GetComponent<Text>().fontSize = 15;
      labelX.GetComponent<Text>().fontStyle = FontStyle.Bold;
      labelX.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
      RectTransform rectTransformlabelX = labelX.GetComponent<RectTransform>();
      rectTransformlabelX.sizeDelta = new Vector3(30, 30, 1f);
      rectTransformlabelX.anchorMin = new Vector3(0, 0, 1f);
      rectTransformlabelX.anchorMax = new Vector3(0, 0, 1f);
      rectTransformlabelX.localScale = new Vector3(1f, 1f, 1f);
      //tao duong ke doc
      RectTransform DashY = Instantiate(DashXucXacTemplateY);
      DashY.SetParent(graphXucXacContainer);
      DashY.gameObject.SetActive(true);
      DashY.anchoredPosition3D = new Vector3(Xposition, -83, 1f);
      RectTransform rectTransformDashY = DashY.GetComponent<RectTransform>();

      rectTransformDashY.localScale = new Vector3(0.1f, 1f, 1f);
      rectTransformDashY.SetAsFirstSibling();
    }
    //tao duong ke ngang2
    int separatorCount = 6;
    for (int i = 0; i <= separatorCount; i++)
    {
      RectTransform labelY = Instantiate(LabelXucXacTemplateY);
      labelY.SetParent(graphXucXacContainer);
      labelY.gameObject.SetActive(true);
      float normalizedValue = i * 0.034f / separatorCount;
      labelY.anchoredPosition3D = new Vector3(-7f, normalizedValue * graphHeight, 1f);
      labelY.GetComponent<Text>().text = Mathf.RoundToInt(normalizedValue * yMaximum).ToString();
      labelY.GetComponent<Text>().font = FontThongKe;
      labelY.GetComponent<Text>().fontSize = 15;
      labelY.GetComponent<Text>().fontStyle = FontStyle.Bold;
      labelY.GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
      RectTransform rectTransformlabelX = labelY.GetComponent<RectTransform>();
      rectTransformlabelX.sizeDelta = new Vector3(30, 30, 1f);
      rectTransformlabelX.anchorMin = new Vector3(0, 0, 1f);
      rectTransformlabelX.anchorMax = new Vector3(0, 0, 1f);
      rectTransformlabelX.localScale = new Vector3(1f, 1f, 1f);

      RectTransform DashX = Instantiate(DashXucXacTemplateX);
      DashX.SetParent(graphXucXacContainer);
      DashX.gameObject.SetActive(true);
      DashX.anchoredPosition3D = new Vector3(-7f, normalizedValue * graphHeight - 83, 1f);
      RectTransform rectTransformDashX = DashX.GetComponent<RectTransform>();

      rectTransformDashX.localScale = new Vector3(0.1f, 1f, 1f);
      rectTransformDashX.SetAsFirstSibling();

    }
  }

  #endregion
}
