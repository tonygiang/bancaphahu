﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppInfo
{
#if UNITY_ANDROID
  public const Platform CurrentPlatform = Platform.ANDROID;
#elif UNITY_IOS
    public const Platform CurrentPlatform = Platform.IOS;
#elif UNITY_WEBGL
    public const Platform CurrentPlatform = Platform.WEBGL;
#else
  public const Platform CurrentPlatform = Platform.DEFAULT;
#endif
  static readonly Dictionary<Platform, string> _platformStrings =
    new Dictionary<Platform, string>
    {
      {Platform.ANDROID, "android"},
      {Platform.IOS, "ios"},
      {Platform.WEBGL, "webgl"},
      {Platform.DEFAULT, "webgl"}
    };
  public static string PlatformString => _platformStrings[CurrentPlatform];
  public static Scene ActiveScene => SceneManager.GetActiveScene();
  public static string LastSceneName = string.Empty;
}
