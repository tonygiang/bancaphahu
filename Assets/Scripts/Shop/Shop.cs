﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Reign;
using Sfs2X.Entities.Data;
using Newtonsoft.Json;
using Utils;

public class Shop : MonoBehaviour
{
  public static Shop Instance;

  public GameObject shopPanel;
  public Text IQAnser;
  public GameObject[] listPanel;
  public Image[] listTabBtn;
  public Text gemText;
  public Transform playerinfo;

  [Header("--- CARD ---")]
  public Image[] listImgOpe;
  public Sprite[] spriteOpeDefault, spriteOpeActive;
  public InputField cardSerial, cardNumber;
  //    private string[] listStringOpe = { "VTT", "VNP", "VMS" };
  //    private string operatorName = "";
  private int[] listCardType = { 1, 3, 2 };
  private int cardtype;
  private int cardvalue = 0;
  private string IQResult;
  // IAP
  //	private const string item1 = "banca_2usd";
  //	private const string item2 = "banca_5usd_2";
  //	private const string item3 = "banca_10usd";
  //	private string[] items = { item1, item2, item3 };

  private string[] restoreInAppStatusText;

  private string lastkey = "";


  // SMS
  public Image[] listImgOpeSMS;
  public List<SmsItem> ListSmsItem = new List<SmsItem>();
  public List<SmsItem> ListSmsViettel = new List<SmsItem>();
  public List<SmsItem> ListSmsVina = new List<SmsItem>();
  public List<SmsItem> ListSmsMobi = new List<SmsItem>();
  [SerializeField]
  GameObject smsContainer, smsItem;

  //public bool isTexting = false;

  // EXCHANGE GEM
  [SerializeField]
  InputField inputGem;

  [SerializeField]
  Text goldOut, progressText;

  // CARD

  public int priceactive = 0;
  // ẨN - HIỆN PHẦN CHỌN MỆNH GIÁ
  [SerializeField]
  GameObject cardFormatItem, pickCardPanel;

  [SerializeField]
  Transform cardFormatContainer;

  [SerializeField]
  Text displayCardValue;


  public List<CardFormat> cardFormatVettel;
  public List<CardFormat> cardFormatVina;
  public List<CardFormat> cardFormatMobi;

  [SerializeField]
  GameObject quyenloiDialog;
  int vipCurrent = 0;
  Vector2 scaleMax = new Vector2(1.1f, 1.1f);
  Vector2 scaleMin = new Vector2(0.9f, 0.9f);
  Vector2 scaleMax2 = new Vector2(1.4f, 1.4f);
  float timeST = 0.3f;
  float currentTimeST = 0;
  public GameObject[] listVip;

  // SMS
  [SerializeField]
  Text textChonMucNap;
  [Header("--- SMS PC ---")]
  public Text cuphapText;
  public Text dausoText;
  public GameObject smsContainerPC;
  private ISFSArray dataIAPAndroid;
  bool waiting = false;
  string cpName = string.Empty;

  void Awake()
  {
    if (Instance != null)
    {
      Destroy(gameObject);
      return;
    }
    Instance = this;
    DontDestroyOnLoad(this);
  }

  void Start()
  {
    // InApp-Purchases - NOTE: you can set different "In App IDs" for each platform.

    Network.PostToServer("CheckVersion", ("method", "getiapformat"))
      .Then(handler => handler.text.PassTo(Crypto.Decrypt)
        .PassTo(SFSObject.NewFromJsonData)
        .PassTo(GetIAPItems));

    var json = "{\"user\":1}";
    var data = JsonConvert.DeserializeObject<sample>(json);


  }
  class sample
  {
    public int user;
  }
  #region	iap

  private void createdCallback(bool succeeded)
  {
    InAppPurchaseManager.MainInAppAPI.AwardInterruptedPurchases(awardInterruptedPurchases);
  }

  private void awardInterruptedPurchases(string inAppID, bool succeeded)
  {
    int appIndex = InAppPurchaseManager.MainInAppAPI.GetAppIndexForAppID(inAppID);
    if (appIndex != -1)
    {
      restoreInAppStatusText[appIndex] = "Restore Status: " + inAppID + ": " + succeeded + " Index: " + appIndex;
    }
  }

  public void BuyItem(int index)
  {
    //		Method.IAP("banca_10usd", "1.99USD", "orderid", "cpName","receipt", "googleSign");
#if UNITY_ANDROID || UNITY_IOS
    if (!waiting)
    {
      waiting = true;
      // NOTE: You can pass in a "InAppID string value" or an "index" value.
      InAppPurchaseManager.MainInAppAPI.Buy(index, buyAppCallback);
    }
#endif
  }

  string GetGUIKey()
  {
    string orderid;
    do
    {
      System.Guid guid = System.Guid.NewGuid();
      orderid = guid.ToString().Replace("-", "");
      if (orderid != lastkey)
      {
        lastkey = orderid;
        break;
      }
    } while (true);
    return orderid;
  }


#if UNITY_ANDROID


  void buyAppCallback(string inAppID, string receipt, bool succeeded)
  {
    waiting = false;
    int appIndex = InAppPurchaseManager.MainInAppAPI.GetAppIndexForAppID(inAppID);

    Debug.Log("Success: " + succeeded + "\nReceipt: " + receipt + "\nGoogle sign: " + inAppID);

    if (succeeded)
    {
      string orderid = GetGUIKey();
      int _gold = 0;
      float _prize = 0;
      foreach (SFSObject data in dataIAPAndroid)
      {
        if (data.GetUtfString("key").Equals(inAppID))
        {
          SFSManager.instance.IAP(inAppID, data.GetUtfString("proid").Substring(0, data.GetUtfString("proid").Length - 1) + "USD", orderid, cpName, receipt);
        }
      }

    }
    else
    {
      Debug.Log("Buy fail");
    }
  }
#elif UNITY_IOS
	void buyAppCallback(string inAppID, string receipt, bool succeeded)
	{
		waiting = false;
		int appIndex = InAppPurchaseManager.MainInAppAPI.GetAppIndexForAppID(inAppID);

		if(succeeded)
		{
			string orderid = GetGUIKey();

			//	Debug.Log("Receipt: " + receipt);
			foreach (SFSObject data in dataIAPAndroid) {
				if (data.GetUtfString ("key").Equals (inAppID)) {
					SFSManager.instance.IAP (inAppID, data.GetUtfString ("proid").Substring (0, data.GetUtfString ("proid").Length - 1) + "USD", orderid, cpName, receipt);
				}
			}


		}
		else
		{
		//	Debug.Log("Buy fail");
		}
	}
#endif

  #endregion

  #region action panel

  public void OpenShopAddButton(int _tabValue)
  {
    shopPanel.SetActive(true);
    PickTab(_tabValue);
  }

  public void OpenShop()
  {
    shopPanel.SetActive(true);
    PickTab(2);
  }

  public void CloseShop()
  {
    if (shopPanel.activeInHierarchy)
    {
      shopPanel.SetActive(false);
      PRESS_CuaHang();
    }
  }

  public void PickTab(int value)
  {
    for (int i = 0; i < listTabBtn.Length; i++)
    {
      if (i == value)
      {
        listTabBtn[i].GetComponent<Button>().enabled = false;
        listPanel[i].SetActive(true);
        //listTabBtn [i].sprite = spriteTabActive [i];
      }
      else
      {
        listTabBtn[i].GetComponent<Button>().enabled = true;
        listPanel[i].SetActive(false);
        //listTabBtn [i].sprite = spriteTabDefault [i];
      }
    }
  }
  public void checkIQ()
  {
    if (PlayerPrefs.HasKey("verified_on_this_device")) { PickTab(1); return; }
    SFSManager.instance.CHECK_SMART();

  }
  public void LoadSmartUi(ISFSObject obj)
  {
    string json = obj.GetUtfString("who_is_smartman");
    var Data = JsonConvert.DeserializeObject<SmartIQ>(json);
    string opera = "";
    if (Data.operate)
    {
      opera = " cộng ";
    }
    else
    {
      opera = " trừ ";
    }
    IQResult = Data.okman;
    var content = Data.number1 + opera + Data.number2 + " bằng bao nhiêu?";
    MessageBox.instance.showSmartIQ(content, MessageBoxType.OK_CANCEL, checkData);
  }
  void checkData(MessageBoxCallBack result)
  {

    if (result.Equals(MessageBoxCallBack.OK))
    {
      if (IQResult == IQAnser.text)
      {
        PlayerPrefs.SetInt("verified_on_this_device", 1);
        PickTab(1);
        IQAnser.text = "";
        IQResult = "";
      }
      else
      {
        MessageBox.instance.Show("THÔNG BÁO", "Sai đáp án!");
      }

    }
    IQAnser.text = string.Empty;
  }

  public void SetGem(int _gem)
  {
    gemText.text = SaveLoadData.FormatMoney(_gem);
  }

  public void SetInfo()
  {
    playerinfo.GetChild(0).GetChild(0).GetComponent<Text>().text = UserManager.DisplayName;
  }

  public void SetUserAvatar(Sprite _spr)
  {
    playerinfo.GetChild(1).GetChild(0).GetChild(0).GetComponent<Image>().sprite = _spr;
  }

  public void ActiveMask(bool _isactive)
  {
    if (_isactive)
    {
      playerinfo.GetChild(1).GetChild(0).GetComponent<Mask>().enabled = false;
    }
    else
    {
      playerinfo.GetChild(1).GetChild(0).GetComponent<Mask>().enabled = true;
    }
  }

  public void SetDialogVip(Sprite _spr)
  {
    playerinfo.GetChild(0).GetComponent<Image>().sprite = _spr;
    //playerinfo.GetChild (0).GetComponent<Image> ().SetNativeSize ();
    playerinfo.GetChild(1).GetChild(0).GetChild(1).gameObject.SetActive(true);
  }

  public void SetDialogNor(Sprite _spr)
  {
    playerinfo.GetChild(0).GetComponent<Image>().sprite = _spr;
    //playerinfo.GetChild (0).GetComponent<Image> ().SetNativeSize ();
    playerinfo.GetChild(1).GetChild(0).GetChild(1).gameObject.SetActive(false);
  }

  public void ResetUser()
  {
    ListSmsItem.Clear();
    foreach (Transform tran in smsContainer.transform)
    {
      Destroy(tran.gameObject);
    }
  }

  #endregion

  #region ------------ THẺ CÀO ------------

  // Thẻ cào

  public void LoadCardFormat(ISFSObject _obj)
  {
    if (_obj.ContainsKey("priceactive"))
    {
      priceactive = _obj.GetInt("priceactive");
      if (priceactive == 0)
      {
        return;
      }
    }
    ISFSArray _listCardFormat = _obj.GetSFSArray("list");
    foreach (ISFSObject _operator in _listCardFormat)
    {
      if (_operator.ContainsKey("Viettel"))
      {
        ISFSArray cardArr = _operator.GetSFSArray("Viettel");
        foreach (ISFSObject _card in cardArr)
        {
          CardFormat _cardFor = new CardFormat(_card);
          cardFormatVettel.Add(_cardFor);
        }
      }
      if (_operator.ContainsKey("Mobifone"))
      {
        ISFSArray cardArr = _operator.GetSFSArray("Mobifone");
        foreach (ISFSObject _card in cardArr)
        {
          CardFormat _cardFor = new CardFormat(_card);
          cardFormatMobi.Add(_cardFor);
        }
      }
      if (_operator.ContainsKey("Vinaphone"))
      {
        ISFSArray cardArr = _operator.GetSFSArray("Vinaphone");
        foreach (ISFSObject _card in cardArr)
        {
          CardFormat _cardFor = new CardFormat(_card);
          cardFormatVina.Add(_cardFor);
        }
      }
    }
    TAT_BAT_NHAMANG(_obj);
  }

  public void CARD_ChooseOpera(int value)
  {
    int _countTypeCard = listImgOpe.Length;
    for (int i = 0; i < _countTypeCard; i++)
    {
      listImgOpe[i].sprite = spriteOpeDefault[i];
    }
    listImgOpe[value].sprite = spriteOpeActive[value];
    displayCardValue.transform.localPosition = listImgOpe[value].transform.localPosition;

    if (priceactive == 1)
    {
      if (cardtype != listCardType[value] || cardtype == 0)
      {
        InitOperaterCardFormat(value);
      }
      else
      {
        cardtype = listCardType[value];
        StartCoroutine(ShowAnimPopUp(false, pickCardPanel));
      }
    }
    cardtype = listCardType[value];
  }

  public void InitOperaterCardFormat(int _operaterValue)
  {
    switch (_operaterValue)
    {
      case 0:
        InitListCard(cardFormatVettel);
        break;
      case 1:
        InitListCard(cardFormatVina);
        break;
      case 2:
        InitListCard(cardFormatMobi);
        break;
    }
  }

  void InitListCard(List<CardFormat> _typeCard)
  {
    StartCoroutine(ShowAnimPopUp(false, pickCardPanel));
    int _countList = _typeCard.Count;
    foreach (Transform tran in cardFormatContainer)
    {
      Destroy(tran.gameObject);
    }
    for (int index = 0; index < _countList; index++)
    {
      int _active = _typeCard[index].active;
      int _price = _typeCard[index].price;
      int _sale = _typeCard[index].promotion;
      if (_active == 1)
      {
        GameObject ga = Instantiate(cardFormatItem) as GameObject;
        ga.transform.SetParent(cardFormatContainer);
        ga.transform.localScale = Vector3.one;
        ga.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/Shop/CardFormat/" + _price);

        if (_sale == 0)
        {
          ga.transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
          ga.transform.GetChild(1).gameObject.SetActive(true);
          ga.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = string.Format("+{0}%", _sale);
        }
        ga.transform.GetChild(2).GetComponent<Text>().text = Global.CoinToString(_typeCard[index].gem);
        ga.name = _price.ToString();
        ga.GetComponent<Button>().onClick.AddListener(() =>
        {
          cardvalue = _price;
          //					pickCardPanel.SetActive(false);
          StartCoroutine(ShowAnimPopUp(true, pickCardPanel));
          displayCardValue.text = string.Format("Thẻ {0} VNĐ", Global.CoinToString(cardvalue));
          foreach (Transform tran in cardFormatContainer)
          {
            if (ga.name == tran.name)
            {
              tran.GetChild(4).gameObject.SetActive(true);
            }
            else
            {
              tran.GetChild(4).gameObject.SetActive(false);
            }
          }
        });
        ga.SetActive(true);
      }
    }
  }

  public void ClosePickCardPanel()
  {
    StartCoroutine(ShowAnimPopUp(true, pickCardPanel));
  }

  public void CARD_NapTheButton()
  {
    if (cardtype == 0)
    {
      MessageBox.instance.Show("Bạn chưa chọn nhà mạng");
      return;
    }
    if (cardvalue == 0)
    {
      MessageBox.instance.Show("Bạn chưa chọn mệnh giá thẻ cào");
      return;
    }
    if (cardSerial.text.Length < 9)
    {
      MessageBox.instance.Show("Thiếu số seri!\n Số serial có độ dài từ 9 đến 15 kí tự\nVui lòng kiểm tra lại");
      return;
    }
    if (cardNumber.text.Length < 12)
    {
      MessageBox.instance.Show("Thiếu mã thẻ!\n Mã thẻ có độ dài từ 12 đến 15 kí tự\nVui lòng kiểm tra lại");
      return;
    }
    Network.PostToServer("Payment",
      ("method", "paycard"),
      ("pin", cardNumber.text),
      ("serial", cardSerial.text),
      ("cardtype", cardtype.ToString()),
      ("cardvalue", cardvalue.ToString()))
      .Then(handler =>
      {
        ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
          .PassTo(SFSObject.NewFromJsonData);
        MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
        ResetCard();
      });
  }

  public void ResetCard()
  {
    cardSerial.text = "";
    cardNumber.text = "";
  }

  public void TAT_BAT_NHAMANG(ISFSObject _obj)
  {
    if (_obj.ContainsKey("vtt"))
    {
      int _acVTT = _obj.GetInt("vtt");
      if (_acVTT == 0)
      {
        listImgOpe[0].gameObject.SetActive(false);
      }
      else
      {
        listImgOpe[0].gameObject.SetActive(true);
      }
    }
    if (_obj.ContainsKey("vnp"))
    {
      int _acVTT = _obj.GetInt("vnp");
      if (_acVTT == 0)
      {
        listImgOpe[1].gameObject.SetActive(false);
      }
      else
      {
        listImgOpe[1].gameObject.SetActive(true);
      }
    }
    if (_obj.ContainsKey("vms"))
    {
      int _acVTT = _obj.GetInt("vms");
      if (_acVTT == 0)
      {
        listImgOpe[2].gameObject.SetActive(false);
      }
      else
      {
        listImgOpe[2].gameObject.SetActive(true);
      }
    }
  }

  #endregion

  #region SMS

  public void SMS_ChooseOperator(int value)
  {
    smsContainer.SetActive(false);
    int _countTypeCard = listImgOpeSMS.Length;
    for (int i = 0; i < _countTypeCard; i++)
    {
      if (i == value)
      {
        //				listImgOpeSMS[i].GetComponent<Button>().enabled = false;
        listImgOpeSMS[i].sprite = spriteOpeActive[i];
        InitOperaterSMSFormat(i);
      }
      else
      {
        //				listImgOpeSMS[i].GetComponent<Button>().enabled = true;
        listImgOpeSMS[i].sprite = spriteOpeDefault[i];
      }
    }
    textChonMucNap.text = "2. CHỌN MỨC NẠP";
    textChonMucNap.gameObject.SetActive(true);
  }

  public void LoadSMS(ISFSObject _obj)
  {
    ISFSArray _smsViettel = _obj.GetSFSArray("viettel");
    foreach (ISFSObject _card in _smsViettel)
    {
      SmsItem _cardFor = new SmsItem(_card);
      ListSmsViettel.Add(_cardFor);
    }

    ISFSArray _smsMobi = _obj.GetSFSArray("mobifone");
    foreach (ISFSObject _card in _smsMobi)
    {
      SmsItem _cardFor = new SmsItem(_card);
      ListSmsVina.Add(_cardFor);
    }

    ISFSArray _smsVina = _obj.GetSFSArray("vinaphone");
    foreach (ISFSObject _card in _smsVina)
    {
      SmsItem _cardFor = new SmsItem(_card);
      ListSmsMobi.Add(_cardFor);
    }


  }

  public void InitOperaterSMSFormat(int _operaterValue)
  {
    switch (_operaterValue)
    {
      case 0:
        InitListSMS(ListSmsViettel, "VIETTEL");
        break;
      case 1:
        InitListSMS(ListSmsVina, "VINAPHONE");
        break;
      case 2:
        InitListSMS(ListSmsMobi, "MOBIPHONE");
        break;
    }
  }

  void InitListSMS(List<SmsItem> _typeCard, string nhaMang)
  {
    smsContainerPC.SetActive(false);
    smsContainer.SetActive(true);
    int _countList = _typeCard.Count;
    foreach (Transform tran in smsContainer.transform)
    {
      Destroy(tran.gameObject);
    }
    for (int i = 0; i < _countList; ++i)
    {
      SmsItem item = _typeCard[i];
      GameObject ga = Instantiate(smsItem);
      ga.SetActive(true);
      ga.transform.SetParent(smsContainer.transform);
      ga.transform.localScale = new Vector3(0.9f, 0.9f, 1f);
      ga.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = string.Format("{0}\nVNĐ", SaveLoadData.FormatMoney(item.MenhGia));
      ga.transform.GetChild(2).GetComponent<Text>().text = SaveLoadData.FormatMoney(item.Vang);
      ga.transform.GetComponent<Button>().onClick.AddListener(() =>
      {
        ClickSMS(item.DauSo, item.CuPhap, item.MenhGia, nhaMang);
      });
    }
  }


  public void ClickSMS(int dauso, string cuphap, int menhgia, string nhamang)
  {
#if UNITY_ANDROID
    string smsText = string.Format("sms:{0}?body={1}{2}", dauso, cuphap, UserManager.UserName);
    Application.OpenURL(smsText);
#elif UNITY_IOS
		IOSBridge.SendMess(dauso.ToString(), cuphap+UserManager.UserName);
#else
		cuphapText.text = string.Format ("{0}{1}", cuphap, UserManager.UserName);
		dausoText.text = dauso.ToString ();
		smsContainer.SetActive (false);
		smsContainerPC.SetActive(true);
		textChonMucNap.text = string.Format("NẠP {0} VNĐ {1} BẠN SOẠN CÚ PHÁP SAU",Global.CoinToString(menhgia),nhamang);
#endif



  }


  #endregion

  #region Exchange Gem

  public void ExchangeGemToGold()
  {
    string str = inputGem.text;
    if (str.Length > 0)
    {
      int _gemEx = int.Parse(inputGem.text);
      SFSManager.instance.SentExchangeGem(_gemEx);
    }
    else
    {
      MessageBox.instance.Show("Bạn chưa nhập kim cương");
    }
  }

  public void OnValueChange()
  {
    string str = inputGem.text;
    if (str.Length > 0)
    {
      goldOut.text = (int.Parse(inputGem.text) * 10).ToString();
    }
  }

  public void ResponseEx(int _coin, int _gem)
  {
    inputGem.text = "";
    goldOut.text = "";
  }

  #endregion

  #region VIP

  public void PRESS_QuyenLoi()
  {
    StartCoroutine(ShowAnimPopUp(false, quyenloiDialog));
    SoundManager.Instance.ButtonSound();

  }

  public void PRESS_CuaHang()
  {
    StartCoroutine(ShowAnimPopUp(true, quyenloiDialog));
    SoundManager.Instance.ButtonSound();
  }


  public void VIP_btnNext()
  {
    vipCurrent++;
    if (vipCurrent > listVip.Length - 1)
    {
      vipCurrent = 0;
    }
    for (int i = 0; i < listVip.Length; i++)
    {
      if (i == vipCurrent)
      {
        listVip[i].SetActive(true);
      }
      else
      {
        listVip[i].SetActive(false);
      }
    }
  }

  public void VIP_btnPre()
  {
    vipCurrent--;
    if (vipCurrent < 0)
    {
      vipCurrent = listVip.Length - 1;
    }
    for (int i = 0; i < listVip.Length; i++)
    {
      if (i == vipCurrent)
      {
        listVip[i].SetActive(true);
      }
      else
      {
        listVip[i].SetActive(false);
      }
    }
  }

  IEnumerator ShowAnimPopUp(bool isClose, GameObject _panel)
  {
    Transform _dialog = _panel.transform.GetChild(0);
    if (isClose)
    {
      timeST = 0.15f;
      currentTimeST = 0;
      do
      {
        _dialog.localScale = Vector2.Lerp(Vector2.one, Vector2.zero, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);
      _dialog.localScale = scaleMax;
      _panel.SetActive(false);
    }
    else
    {
      _panel.SetActive(true);
      timeST = 0.15f;
      currentTimeST = 0;

      do
      {
        _dialog.localScale = Vector2.Lerp(Vector2.zero, scaleMax, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);
      timeST = 0.05f;
      currentTimeST = 0;
      do
      {
        _dialog.localScale = Vector2.Lerp(scaleMax, scaleMin, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);


      _dialog.localScale = Vector2.one;
    }

  }

  #endregion

  #region IAP

  public void GetIAPItems(ISFSObject obj)
  {
    ISFSArray data = obj.GetSFSArray("items");
    dataIAPAndroid = data;
    var inAppIDs = new InAppPurchaseID[data.Count];
    GameObject IAPpanel = listPanel[2].transform.GetChild(0).transform.GetChild(0).gameObject;
    var index = 0;
    foreach (ISFSObject item in data)
    {
      IAPpanel.transform.GetChild(index).GetChild(1).GetComponent<Text>().text = SaveLoadData.FormatMoney(item.GetInt("gem"));
      inAppIDs[index] = new InAppPurchaseID(item.GetUtfString("key"), decimal.Parse(item.GetUtfString("proid").Substring(0, item.GetUtfString("proid").Length - 1)), "$", InAppPurchaseTypes.Consumable);
      index++;

    }
    var desc = new InAppPurchaseDesc();

    // Global
    desc.Testing = false;

#if UNITY_EDITOR
    // Editor
    //iapBtn.SetActive (false);
    desc.Editor_InAppIDs = inAppIDs;
#elif UNITY_IOS
		// iOS
		desc.iOS_InAppPurchaseAPI = InAppPurchaseAPIs.AppleStore;
		desc.iOS_AppleStore_InAppIDs = inAppIDs;
		desc.iOS_AppleStore_SharedSecretKey = "f8f6e5fa862b414989cf5be3de555f8c";// NOTE: Must set SharedSecretKey, even for Testing!

#elif UNITY_ANDROID
		// Android
		// Choose for either GooglePlay or Amazon.
		// NOTE: Use "player settings" to define compiler directives.

		desc.Android_InAppPurchaseAPI = InAppPurchaseAPIs.GooglePlay;

		desc.Android_GooglePlay_InAppIDs = inAppIDs;
		//desc.Android_GooglePlay_Base64Key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjjqydqU7uN/Hwn9pgR/ontWF7XseRdqGLczlW08wpTrfyeu55dyzUZRRdvkbAfZY69dP2zyg/Rw3LtPqqPh9v6mirJSGUdBYlhyd0nFKHD0t6a1CzmR7kZoh3fICART7LLjXmvfTAhJvoctlZmF1loDy0NjkVPWsnKjts4bh/U2w9kks8Sm9bsKmvOywyp9w51M0LKiexDPLN8AuejBZjYB/bPndK8x6CBK0WFiF/JHXWChq2E6IPte6AkQEwKy2o59FhF8d7A8+5vx799edoa8mkEHXdRnkryvmYQr9mITXSaAbVFZno3NQaqm04ek+zgsTz1tEkAHJKrIo6H0SXwIDAQAB";
		desc.Android_GooglePlay_Base64Key = Network.Info.iapkey;
#endif
    // init

    InAppPurchaseManager.Init(desc, createdCallback);

  }

  public void GetIapData(ISFSObject _obj)
  {
    string message = _obj.GetUtfString("message");
    int messageCode = _obj.GetInt("message_code");
    if (messageCode == 0)
    {
      MessageBox.instance.Show("THÔNG BÁO!", message);
    }
    else
    {
      // khong lam gi ca 
    }
    Loading.instance.Hide();

  }

  #endregion
}
