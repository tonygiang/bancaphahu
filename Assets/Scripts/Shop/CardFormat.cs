﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;


[System.Serializable]
public class CardFormat {

	public int price;
	public int gem;
	public int active;
	public int promotion;


	public CardFormat(ISFSObject _obj){
		price = _obj.GetInt ("price");
		gem = _obj.GetInt ("gem");
		active = _obj.GetInt ("active");
		promotion = _obj.GetInt ("promotion");
	}

	public CardFormat(){
	}


}
