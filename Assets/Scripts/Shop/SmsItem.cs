﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

[System.Serializable]
public class SmsItem {

	public int MenhGia;
	public string CuPhap;
	public int Vang;
	public int DauSo;



	public SmsItem(){
	}
	public SmsItem(ISFSObject _obj){
		MenhGia = _obj.GetInt ("menhgia");
		CuPhap = _obj.GetUtfString ("cuphap");
		Vang = _obj.GetInt ("vang");
		DauSo = _obj.GetInt ("dauso");
	}
}
