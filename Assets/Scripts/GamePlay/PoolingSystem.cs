﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PoolingSystem : MonoBehaviour
{
  public static PoolingSystem instance;

  public Transform fishContainer, bulletContainer;

  public Transform coinCanvas;
  Vector3 coinScale = new Vector3(0.3f, 0.3f, 1);

  void Awake()
  {
    instance = this;
  }

  public GameObject SpawnFish(string fishName)
  {
    GameObject pa = transform.Find(fishName).gameObject;
    GameObject ga = null;
    if (pa.transform.childCount > 0)
    {
      ga = pa.transform.GetChild(0).gameObject;

    }
    else
    {
      ga = Instantiate(Resources.Load<GameObject>("Prefabs/Fishes/" + fishName));
      ga.name = ga.name.Replace("(Clone)", "");
    }
    ga.transform.SetParent(fishContainer);
    ga.SetActive(true);

    return ga;
  }

  public void RemoveFish(GameObject fish, string fishName)
  {
    GameObject pa = transform.Find(fishName).gameObject;
    if (pa.transform.childCount <= 50)
    {
      fish.transform.SetParent(pa.transform);
      fish.name = pa.name;
      fish.transform.localPosition = new Vector3(0, 0, 0);

      fish.SetActive(false);
    }
    else
    {
      Destroy(fish);
    }
  }

  public GameObject SpawnBullet(string bulletName)
  {
    string _bName = SearchBullt(bulletName);
    GameObject pa = transform.Find(_bName).gameObject;
    GameObject ga = null;

    if (pa.transform.childCount > 0)
    {
      ga = pa.transform.GetChild(0).gameObject;
    }
    else
    {
      ga = Instantiate(Resources.Load<GameObject>("Prefabs/Bullets/" + _bName)) as GameObject;
      ga.name = ga.name.Replace("(Clone)", "");
    }
    ga.transform.SetParent(bulletContainer);
    ga.SetActive(true);
    return ga;
  }

  public string SearchBullt(string _bName)
  {
    //room gold 100 - 300
    if (_bName == "bullet100")
    {
      return "bulletGun1";
    }
    if (_bName == "bullet200" || _bName == "bullet500")
    {
      return "bulletGun2";
    }

    //room gem 100 - 300
    if (_bName == "bullet1000")
    {
      return "bulletGun7";
    }
    if (_bName == "bullet2000" || _bName == "bullet3000")
    {
      return "bulletGun8";
    }

    return "bulletGun1";
  }

  public void RemoveBullet(GameObject _bullet, string _bulletName)
  {
    GameObject pa = transform.Find(_bulletName).gameObject;
    if (pa.transform.childCount <= 50)
    {
      _bullet.transform.SetParent(pa.transform);
      _bullet.name = pa.name;
      _bullet.transform.localPosition = new Vector3(0, 0, 0);

      _bullet.SetActive(false);
    }
    else
    {
      Destroy(_bullet);
      Debug.Log("Destroy " + _bulletName);
    }
  }

  public GameObject SpawnSnare(string snareName, Vector3 position)
  {
    GameObject pa = transform.Find("Luoi" + snareName).gameObject;
    GameObject ga = null;

    if (pa.transform.childCount > 0)
    {
      ga = pa.transform.GetChild(0).gameObject;
      ga.SetActive(true);
      ga.transform.SetParent(null);
      ga.transform.position = position;
    }
    else
    {
      ga = Instantiate(Resources.Load<GameObject>("Prefabs/Luoi/Luoi" + snareName), position, new Quaternion(0, 0, 0, 0)) as GameObject;
      ga.name = ga.name.Replace("(Clone)", "");
    }

    return ga;
  }

  public GameObject SpawnEffect(string snareName, Vector3 position)
  {
    GameObject pa = transform.Find(snareName).gameObject;
    GameObject ga = null;

    if (pa.transform.childCount > 0)
    {
      ga = pa.transform.GetChild(0).gameObject;
      ga.SetActive(true);
      ga.transform.SetParent(null);
      ga.transform.position = position;
    }
    else
    {
      ga = Instantiate(Resources.Load<GameObject>("Prefabs/EffectFishDie/" + snareName), position, new Quaternion(0, 0, 0, 0)) as GameObject;
      ga.name = ga.name.Replace("(Clone)", "");
    }

    return ga;
  }

  public GameObject SpawnCoin(Vector3 position, string text, string roomType)
  {
    GameObject pa = transform.Find("PointText").gameObject;
    GameObject ga = null;

    if (pa.transform.childCount > 0)
    {
      ga = pa.transform.GetChild(0).gameObject;
      ga.SetActive(true);
      ga.transform.SetParent(null);
    }
    else
    {
      ga = Instantiate(Resources.Load<GameObject>("Prefabs/Others/" + "PointText"), position, new Quaternion(0, 0, 0, 0)) as GameObject;
      ga.name = ga.name.Replace("(Clone)", "");
    }
    ga.transform.position = position;
    ga.GetComponent<PoinTextController>().SetTextContent(text, roomType);

    return ga;
  }

  public GameObject SpawnCoinJackpot()
  {
    GameObject pa = transform.Find("PointTextJackpot").gameObject;
    GameObject ga = null;

    if (pa.transform.childCount > 0)
    {
      ga = pa.transform.GetChild(0).gameObject;

      ga.transform.SetParent(null);
    }
    else
    {
      ga = Instantiate(Resources.Load<GameObject>("Prefabs/Others/" + "PointTextJackpot")) as GameObject;
      ga.name = ga.name.Replace("(Clone)", "");
    }
    ga.transform.SetParent(coinCanvas.transform);
    ga.transform.localScale = Vector3.one;
    ga.transform.position = Vector3.zero;
    ga.SetActive(true);

    return ga;
  }

  public GameObject SpawnObject(Vector3 position, string name)
  {
    GameObject pa = transform.Find(name).gameObject;
    GameObject ga = null;

    if (pa.transform.childCount > 0)
    {
      ga = pa.transform.GetChild(0).gameObject;
      ga.SetActive(true);
      ga.transform.parent = null;
    }
    else
    {
      ga = Instantiate(Resources.Load<GameObject>("Prefabs/Others/" + name), position, new Quaternion(0, 0, 0, 0)) as GameObject;
      ga.name = ga.name.Replace("(Clone)", "");
    }
    ga.transform.position = position;

    return ga;
  }

  public void RemoveObject(GameObject ga, float waitTime, int childCount)
  {
    StartCoroutine(CouRemoveObject(ga, waitTime, childCount));
  }

  private IEnumerator CouRemoveObject(GameObject ga, float waitTime, int childCount)
  {
    yield return new WaitForSeconds(waitTime);
    if (ga != null)
      if (transform.Find(ga.name) != null)
      {
        GameObject pa = transform.Find(ga.name).gameObject;
        if (pa.transform.childCount <= childCount)
        {
          ga.transform.SetParent(pa.transform);
          ga.transform.position = Vector3.zero;
          ga.gameObject.name = pa.gameObject.name;
          ga.SetActive(false);
        }
        else
        {
          Destroy(ga);
          //				Debug.Log("Destroy " + ga.name);
        }
      }
  }


  public void INIT_COIN(Vector3 _posFish, string _coinFish, string roomType)
  {
    GameObject _coin = SpawnCoin(_posFish, _coinFish, roomType);
    _coin.transform.SetParent(coinCanvas.transform);
    _coin.transform.localScale = coinScale;
    RemoveObject(_coin, 1.2f, 10);
  }
}
