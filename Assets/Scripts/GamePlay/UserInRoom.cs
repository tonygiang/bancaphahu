﻿using System.Collections;
using UnityEngine;
using System.Xml.Linq;
using Sfs2X.Entities.Data;
using Utils;

[System.Serializable]
public class UserInRoom
{

  public string UserName;
  public Observable<string> DisplayName = null;
  public Observable<int> Coin = null;
  public int Position;
  public int BulletType;
  public string AvatarLink;

  public int gunType;
  public int gunVipCurrent;
  public int vip;

  public UserInRoom(ISFSObject obj)
  { 
    UserName = obj.GetUtfString("username");
    DisplayName = new Observable<string>(obj.GetUtfString("displayname"));
    Coin = new Observable<int>(obj.GetData("coin")
      .Data.ToString()
      .PassTo(int.Parse));
    Position = obj.GetInt("position");
    vip = obj.GetInt("vip");
    BulletType = obj.GetInt("bullettype");
    AvatarLink = obj.GetUtfString("avatar");
  }
  public UserInRoom()
  {
  }
}
