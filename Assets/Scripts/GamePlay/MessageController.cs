﻿using UnityEngine;
using System.Collections;
using Sfs2X.Entities;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using System.Linq;
using Utils;

public class MessageController : MonoBehaviour
{
  public static MessageController instance;

  [SerializeField]
  GameObject chatBox, emoEntry, emoContainer, emoPanel, quickChatEntry, quickChatContainer, quickChatPanel;

  [SerializeField]
  GameObject textArea, emoText, plainText;

  [SerializeField]
  InputField inputField;

  string[] colorCodes = { "#3399FF", "#FFFF33", "#FF3366", "#54FF9F" };

  string[] quickChats = {
    "Hế lô mấy chế"
    , "Mày hả bưởi"
    , "Hế lô mấy cưng, hé hé hé"
    , "Đậu xanh rau má"
    , "Mấy cưng còn non và xanh lắm"
  };

  float[] chatTimes = { 2, 2, 2, 2 };

  void Awake()
  {
    instance = this;
  }

  void Start()
  {
    SetEmotions();
    SetQuickChat();
  }

  public void OpenChatBox()
  {
    chatBox.SetActive(true);
  }

  public void showLobbyMessage(string _uname, string message, string isEmo)
  {
    if (TaiXiu.Instance.TaiXiuPanel.activeInHierarchy)
    {

      string _displayname = UserManager.DisplayName;
      if (isEmo == "true")
      {
        GameObject ga = Instantiate(emoText) as GameObject;
        ga.transform.SetParent(textArea.transform);
        ga.transform.localScale = new Vector3(1f, 1f, 1f);
        ga.transform.localPosition = new Vector3(ga.transform.localPosition.x, ga.transform.localPosition.y, 1f);
        ga.GetComponent<TextMeshProUGUI>().text = string.Format("<size=40><color={0}>{1}:</color><size=100>{2}</size></size>", colorCodes[0], _uname, message);
        //ga.GetComponent<TextMeshProUGUI>().text = string.Format("", colorCodes[slotIndex], _displayname, message);

      }
      else
      {
        GameObject ga = Instantiate(plainText) as GameObject;
        ga.transform.SetParent(textArea.transform);
        ga.transform.localScale = new Vector3(1f, 1f, 1f);
        ga.transform.localPosition = new Vector3(ga.transform.localPosition.x, ga.transform.localPosition.y, 1f);
        ga.GetComponent<TextMeshProUGUI>().text = string.Format("<size=40><color={0}>{1}:</color>{2}</size>", colorCodes[0], _uname, message);
      }
    }
  }

  IEnumerator ActiveInactive(GameObject ga, int slotIndex)
  {
    ga.SetActive(false);
    ga.SetActive(true);
    do
    {
      chatTimes[slotIndex] -= Time.deltaTime;

      yield return null;
    } while (chatTimes[slotIndex] >= 0);

    ga.SetActive(false);
  }

  //  ==================================================================================================== //

  void SetEmotions() => Resources.LoadAll<Sprite>("Fonts & Materials/staremo")
  .ForEach(sprite =>
  {
    var ga = Instantiate(emoEntry, emoContainer.transform);
    ga.transform.localScale = Vector3.one;
    ga.transform.localPosition = new Vector3(ga.transform.localPosition.x,
      ga.transform.localPosition.y,
      1f);
    ga.GetComponent<Image>().sprite = sprite;

    ga.GetComponent<Button>().onClick.AddListener(() =>
    {
      emoPanel.SetActive(false);
      ChooseQuickChat($@"<sprite name=""{sprite.name}"">", "true");
    });
  });

  void SetQuickChat()
  {
    foreach (string str in quickChats)
    {
      GameObject ga = Instantiate(quickChatEntry) as GameObject;
      ga.transform.SetParent(quickChatContainer.transform);
      ga.transform.localScale = Vector3.one;
      ga.transform.GetChild(0).GetComponent<Text>().text = str;
      //ga.transform.localPosition (ga.transform.localPosition.x, ga.transform.localPosition.y, 1f);
      string quickchat = str;
      ga.GetComponent<Button>().onClick.AddListener(() =>
      {
        quickChatPanel.SetActive(false);
        ChooseQuickChat(quickchat, "false");
      });
    }
  }



  //  ==================================================================================================== //

  public void CloseButton()
  {
    chatBox.SetActive(false);
  }

  public void SendButton()
  {
    if (inputField.text != "")
    {
      string mess = inputField.text;
      SFSManager.instance.SendPublicChat(mess, "false");
      inputField.text = "";
    }
  }

  public void QuickChatButton()
  {
    emoPanel.SetActive(false);
    quickChatPanel.SetActive(!quickChatPanel.activeInHierarchy);
  }

  public void EmoButton()
  {
    emoPanel.SetActive(!emoPanel.activeInHierarchy);
    quickChatPanel.SetActive(false);
  }

  void ChooseQuickChat(string mess, string isEmo)
  {

    SFSManager.instance.SendPublicChat(mess, isEmo);
  }
}
