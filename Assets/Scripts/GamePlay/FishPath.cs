﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;

public class FishPath  {

	public string id;
	public string initPosition;
	public float initPosX;
	public float initPosY;
	public int initAngel;
	public ISFSArray Frame;

	public FishPath(){

	}

	public FishPath(ISFSObject _obj){
		id = _obj.GetUtfString ("id");
		initPosition = _obj.GetUtfString ("initPosition");
		string[] posXY = initPosition.Split (new char[]{'_'});
		initPosX = float.Parse (posXY [0]);
		initPosY = float.Parse (posXY [1]);
		initAngel = int.Parse (_obj.GetUtfString ("initAngel"));
		Frame = _obj.GetSFSArray ("Frame");
	}

	public int GetX(int _index){
		string _frameNumber = Frame.GetSFSObject(_index).GetUtfString (_index.ToString ());
		string[] posXY = _frameNumber.Split (new char[]{'_'});
		float frameLife = float.Parse (posXY [0]);
		return (int)frameLife;
	}

	public int GetY(int _index){
		string _frameNumber = Frame.GetSFSObject(_index).GetUtfString (_index.ToString ());
		string[] posXY = _frameNumber.Split (new char[]{'_'});
		float nextAngle = float.Parse (posXY [1]);
		return (int)nextAngle;
	}
}
