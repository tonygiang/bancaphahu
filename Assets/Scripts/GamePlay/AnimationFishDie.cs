﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationFishDie : MonoBehaviour {

	public static AnimationFishDie Instance;

	Vector3 rota360 = new Vector3(0,0,360f);

	bool isButtonMoving = false;
	Vector2 fro = new Vector2 (36, 0);
	Vector2 to = new Vector2 (136, 0);

	void Awake()
	{
		Instance = this;
	}


	public void DIE_ROTATE(FishMovement _fish, GameObject _shooter){
		StartCoroutine (FishRotate (_fish, _shooter));
	}
	IEnumerator FishRotate(FishMovement fish, GameObject shooter)
	{
		float time = 1f;
		float currentTime = 0;
		do{
			fish.transform.eulerAngles=Vector3.Lerp(Vector3.zero,rota360,currentTime/time);
			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		fish.FishDie ();
		Vector3 currentPos = fish.transform.position;
		Vector3 destination = shooter.transform.position;
		destination.z = 0;
		Vector3 currentScale = fish.transform.localScale;

		time = 0.5f;
		currentTime = 0;
		do{
			fish.transform.position = Vector3.Lerp(currentPos, destination, currentTime / time);
			fish.transform.localScale = Vector3.Lerp(currentScale, Vector3.zero, currentTime / time);
			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		PoolingSystem.instance.RemoveFish(fish.gameObject, fish.gameObject.name);
	}



	public void DIE_SPEED(FishMovement _fish, GameObject _shooter){
		StartCoroutine (FishSpeed (_fish, _shooter));
	}
	IEnumerator FishSpeed(FishMovement fish, GameObject shooter)
	{
		fish.ChangeTimeScale (6f);
		yield return new WaitForSeconds (1.8f);
		fish.ChangeTimeScale (1f);
		fish.FishDie ();
		Vector3 currentPos = fish.transform.position;
		Vector3 destination = shooter.transform.position;
		destination.z = 0;
		Vector3 currentScale = fish.transform.localScale;

		float time = 0.5f;
		float currentTime = 0;

		do{
			fish.transform.position = Vector3.Lerp(currentPos, destination, currentTime / time);
			fish.transform.localScale = Vector3.Lerp(currentScale,Vector3.zero, currentTime / time);
			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		PoolingSystem.instance.RemoveFish(fish.gameObject, fish.gameObject.name);
	}

	public void DIE_SMALL(FishMovement _fish, GameObject _shooter){
		StartCoroutine (FishSmall (_fish, _shooter));
	}

	IEnumerator FishSmall(FishMovement fish, GameObject shooter)
	{
		fish.FishDie ();
		float bTime = 0.2f;
		float bCurrentTime = 0;
		Vector3 _fro = new Vector3 (0.3f, 0.3f, 0.3f);
		Vector3 _to = new Vector3 (0.5f, 0.5f, 0.5f);
		do{
			fish.transform.GetChild(1).localScale = Vector3.Lerp(_fro,_to , bCurrentTime / bTime);
			bCurrentTime += Time.deltaTime;
			yield return null;
		}while(bCurrentTime <= bTime);
		bCurrentTime = 0;
		do{
			fish.transform.GetChild(1).localScale = Vector3.Lerp(_to,_fro , bCurrentTime / bTime);
			bCurrentTime += Time.deltaTime;
			yield return null;
		}while(bCurrentTime <= bTime);

		Vector3 currentPos = fish.transform.position;
		Vector3 destination = shooter.transform.position;
		destination.z = 0;
		Vector3 currentScale = fish.transform.localScale;

		float time = 0.5f;
		float currentTime = 0;

		do{
			fish.transform.position = Vector3.Lerp(currentPos, destination, currentTime / time);
			fish.transform.localScale = Vector3.Lerp(currentScale, new Vector3(0.1f, 0.1f, 1), currentTime / time);

			currentTime += Time.deltaTime;
			yield return null;

		}while(currentTime <= time);
		PoolingSystem.instance.RemoveFish(fish.gameObject, fish.gameObject.name);
	}


	// SPAWN COIN GEM ROTATING 
	public void CoinToPlayer(Vector2 playerPosition, Vector2 spawnPosition,int _mutiplie)
	{
		switch (_mutiplie) {
		case 5:
			Vector2[] _listPos3 = {
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1f)
			};
			Init1RowCoin (_listPos3, playerPosition);
			break;
		case 6:
			Vector2[] _listPos4 = {
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1f),
			};
			Init1RowCoin (_listPos4, playerPosition);
			break;
		case 8:
			Vector2[] _listPos5 = {
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1f),
			};
			Init1RowCoin (_listPos5, playerPosition);
			break;
		case 10:
			Vector2[] _listPos6 = {
				new Vector2 (spawnPosition.x - 1.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.25f, spawnPosition.y + 1f),
			};
			Init1RowCoin (_listPos6, playerPosition);
			break;
		case 12:

			Vector2[] _listPos61 = {
				new Vector2 (spawnPosition.x - 1.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.25f, spawnPosition.y + 1f),
			};
			Init1RowCoin (_listPos61, playerPosition);
			break;
		case 15:
			Vector2[] _listPos7 = {
				new Vector2 (spawnPosition.x - 1.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.75f, spawnPosition.y + 1f),
			};

			Init1RowCoin (_listPos7, playerPosition);
			break;
		case 18:
			Vector2[] _listPos81 = {
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1f),
			};
			Vector2[] _listPos82 = {
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1.75f)
			};
			Init2RowCoin (_listPos81,_listPos82, playerPosition);
			break;
		case 20:
			Vector2[] _listPos101 = {
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1f),
			};
			Vector2[] _listPos102 = {
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1.75f)
			};
			Init2RowCoin (_listPos101,_listPos102, playerPosition);
			break;
		case 25:
			Vector2[] _listPos121 = {
				new Vector2 (spawnPosition.x - 1.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.25f, spawnPosition.y + 1f),
			};
			Vector2[] _listPos122 = {
				new Vector2 (spawnPosition.x - 1.25f, spawnPosition.y + 1.750f),
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1.25f, spawnPosition.y + 1.75f)
			};
			Init2RowCoin (_listPos121,_listPos122, playerPosition);
			break;
		case 30:
			Vector2[] _listPos141 = {
				new Vector2 (spawnPosition.x - 1.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.5f, spawnPosition.y + 1f)
			};
			Vector2[] _listPos142 = {
				new Vector2 (spawnPosition.x - 1.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1.5f, spawnPosition.y + 1.75f)
			};
			Init2RowCoin (_listPos141,_listPos142, playerPosition);
			break;
		case 35:
			Vector2[] _listPos351 = {
				new Vector2 (spawnPosition.x - 1.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.5f, spawnPosition.y + 1f)
			};
			Vector2[] _listPos352 = {
				new Vector2 (spawnPosition.x - 1.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 1f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 0.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.5f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1.5f, spawnPosition.y + 1.75f)
			};
			Init2RowCoin (_listPos351,_listPos352, playerPosition);
			break;
		case 40:
			Vector2[] _listPos161 = {
				new Vector2 (spawnPosition.x - 1.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 1.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.25f, spawnPosition.y + 1f),
				new Vector2 (spawnPosition.x + 1.75f, spawnPosition.y + 1f)

			};
			Vector2[] _listPos162 = {
				new Vector2 (spawnPosition.x - 1.75f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 1.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 0.75f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x - 0.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 0.75f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1.25f, spawnPosition.y + 1.75f),
				new Vector2 (spawnPosition.x + 1.75f, spawnPosition.y + 1.75f)

			};
			Init2RowCoin (_listPos161,_listPos162, playerPosition);
			break;


		}
	}
	void Init1RowCoin(Vector2[] _listPos, Vector2 playerPosition){
		float _timeSpawn=0;
		for (int i = 0; i < _listPos.Length; i++) {
			StartCoroutine (MoveTheCoin (playerPosition, _listPos [i], _timeSpawn,_listPos.Length));
			_timeSpawn += 0.1f;
		}
	}
	void Init2RowCoin(Vector2[] _listPos1,Vector2[] _listPos2, Vector2 playerPosition){
		float _timeSpawn=0;
		for (int i = 0; i < _listPos1.Length; i++) {
			StartCoroutine (MoveTheCoin (playerPosition, _listPos1 [i], _timeSpawn,_listPos1.Length));
			StartCoroutine (MoveTheCoin (playerPosition, _listPos2 [i], _timeSpawn,_listPos2.Length));
			_timeSpawn += 0.1f;
		}
	}

	//	IEnumerator MoveTheCoin(Vector2 playerPosition, Vector2 spawnPosition)
	//	{
	//		GameObject coin = PoolingSystem.instance.SpawnObject(spawnPosition, "RotatingCoin");
	//		coin.transform.SetParent(coinContainer.transform);
	//		coin.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
	//
	//		coin.transform.localPosition = spawnPosition;
	//
	//		Vector2 direction = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;
	//
	//		float speed = Random.Range(4.0f, 5.0f);
	//		do{
	//			Vector2 pos = coin.transform.position;
	//			pos += direction * speed * Time.deltaTime;
	//			coin.transform.position = pos;
	//
	//			speed -= 8 * Time.deltaTime;
	//			yield return null;
	//		}while(speed > 1.75f);
	//
	//		direction = new Vector2(playerPosition.x - coin.transform.position.x, playerPosition.y - coin.transform.position.y).normalized;
	//		float distanceToPlayer = (direction.x * direction.x + direction.y * direction.y);
	//		speed = 1.75f;
	//
	//		float currentTime = 0;
	//		float time = distanceToPlayer / speed;
	//
	//		Vector2 fro = coin.transform.position;
	//
	//		do
	//        {
	//			coin.transform.position = Vector2.Lerp(fro, playerPosition, currentTime / time);
	//
	//			currentTime += Time.deltaTime;
	//			yield return null;
	//		}while(currentTime <= time);
	//		PoolingSystem.instance.RemoveObject(coin, 0, 21);
	//	}
	IEnumerator MoveTheCoin(Vector2 playerPosition, Vector2 _listPos,float _timeSpawn,int _count)
	{
		float _currentTime = 0;
		float _time = 0.3f;
		GameObject coin = PoolingSystem.instance.SpawnObject (_listPos, "RotatingCoin");
//		coin.transform.SetParent (coinContainer.transform);
		coin.transform.localScale = Vector3.zero;
		Vector3 _maxScale = new Vector3 (0.6f, 0.6f, 1f);
		Vector2 _to = new Vector2 (_listPos .x, _listPos .y + 1f);
		Vector2 _to2 = new Vector2 (_listPos .x, _listPos .y + 0.1f);
		coin.transform.localPosition = _listPos;
		yield return new WaitForSeconds (_timeSpawn);
		//		coin.transform.localScale = new Vector3 (0.6f, 0.6f, 1f);
		do {
			coin.transform.localPosition = Vector3.Lerp (_listPos , _to, _currentTime / _time);
			coin.transform.localScale=Vector3.Lerp(Vector3.zero,_maxScale,_currentTime/_time);
			_currentTime += Time.deltaTime;
			yield return null;
		} while(_currentTime < _time);
		coin.transform.localPosition = _to;
		_currentTime = 0;
		do {
			coin.transform.localPosition = Vector3.Lerp (_to, _listPos , _currentTime / _time);
			_currentTime += Time.deltaTime;
			yield return null;
		} while(_currentTime < _time);
		coin.transform.localPosition = _listPos;
		_currentTime = 0;
		_time = 0.1f;
		do {
			coin.transform.localPosition = Vector3.Lerp (_listPos , _to2, _currentTime / _time);
			_currentTime += Time.deltaTime;
			yield return null;
		} while(_currentTime < _time);
		coin.transform.localPosition = _to2;
		_currentTime = 0;
		do {
			coin.transform.localPosition = Vector3.Lerp (_to2, _listPos , _currentTime / _time);
			_currentTime += Time.deltaTime;
			yield return null;
		} while(_currentTime < _time);
		coin.transform.localPosition = _listPos;

		//		yield return new WaitForSeconds (0.7f - _timeSpawn);
		yield return new WaitForSeconds (0.2f);

		Vector2 direction = new Vector2(playerPosition.x - coin.transform.position.x, playerPosition.y - coin.transform.position.y).normalized;
		float distanceToPlayer = (direction.x * direction.x + direction.y * direction.y);
		float speed = 2f;

		float currentTime = 0;
		float time = distanceToPlayer / speed;

		Vector2 fro = coin.transform.position;

		do
		{
			coin.transform.position = Vector2.Lerp(fro, playerPosition, currentTime / time);

			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		PoolingSystem.instance.RemoveObject(coin, 0, 21);
	}



	public void UPDATE_SCORE(int fishValue,Text fishText,int coinValue,Text coinText){
		StartCoroutine (UpdateScoresAmount (fishValue, fishText, coinValue, coinText));
	}
	IEnumerator UpdateScoresAmount (int fishValue,Text fishText,int coinValue,Text coinText)
	{
		const float seconds = 0.5f;
		float elapsedTime = 0;
		while (elapsedTime < seconds) {
			elapsedTime += Time.deltaTime;
			fishText.text = SaveLoadData.FormatMoney(Mathf.FloorToInt(Mathf.Lerp (0, fishValue, (elapsedTime / seconds))));
			coinText.text = SaveLoadData.FormatMoney(Mathf.FloorToInt(Mathf.Lerp (0, coinValue, (elapsedTime / seconds))));
			yield return null;
		}
	}




	public void Shaking()
	{
		StartCoroutine(Shake());
	}

	IEnumerator Shake() {
		//		#if UNITY_ANDROID || UNITY_IOS
		//		Handheld.Vibrate();
		//		#endif

		float elapsed = 0.0f;

		float duration = 1, magnitude = 0.15f;

		Vector3 originalCamPos = Camera.main.transform.position;

		while (elapsed < duration) {

			elapsed += Time.deltaTime;          

			float percentComplete = elapsed / duration;         
			float damper = 1.0f - Mathf.Clamp(4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			x *= magnitude * damper;
			y *= magnitude * damper;

			Camera.main.transform.position = new Vector3(x, y, originalCamPos.z);

			yield return null;
		}

		Camera.main.transform.position = originalCamPos;
	}


	public void ButtonToPosition(GameObject optionButtons ){
		if (!isButtonMoving) {
			StartCoroutine (FroPosition (optionButtons, fro, to));
		} else {
			StartCoroutine (ToPosition (optionButtons, to, fro));
		}
	}

	IEnumerator FroPosition(GameObject ga, Vector3 fro, Vector3 to)
	{
		float time = Mathf.Abs(to.x - fro.x) / 1400f;

		float currentTime = 0;
		do{
			ga.GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(fro, to, currentTime/time);

			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		ga.GetComponent<RectTransform>().anchoredPosition = to;

		isButtonMoving = true;
		ga.transform.GetChild (1).transform.localScale = new Vector3 (-1, 1, 1);
	}
	IEnumerator ToPosition(GameObject ga, Vector3 fro, Vector3 to)
	{
		float time = Mathf.Abs(to.x - fro.x) / 1400f;

		float currentTime = 0;
		do{
			ga.GetComponent<RectTransform>().anchoredPosition = Vector3.Lerp(fro, to, currentTime/time);

			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		ga.GetComponent<RectTransform>().anchoredPosition = to;
		isButtonMoving = false;
		ga.transform.GetChild (1).transform.localScale = new Vector3 (1, 1, 1);
	}

}
