﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Entities.Data;

public class BGManager : MonoBehaviour {
	public static BGManager instance;

	[SerializeField]
	Sprite[] bgSprites;

	[SerializeField]
	Transform bgContainer;

    [SerializeField]
    Image black;

	int bgIndex;
	float timeStart=0;

	[SerializeField]
	RectTransform wave;

	Vector2 fro = new Vector2 (640, 0);
	Vector2 to = new Vector2 (-1400, 0);

	Vector2 posStart  = new Vector2(700, 0);

	void Awake(){
		instance = this;
	}

	void Start () {
		bgIndex = Random.Range (0, bgSprites.Length);
		bgContainer.GetChild (1).GetComponent<Image> ().sprite = bgSprites [bgIndex];
//		StartCoroutine (StartChangeBG ());
	}

//	void Update(){
//		
//		if (Input.GetKeyDown (KeyCode.Space)) {
//			ChangeBackground ();
//		}
//		if (Input.GetKeyDown (KeyCode.A)) {
//			GoBlack ();
//		}
//		if (Input.GetKeyDown (KeyCode.S)) {
//			GoBlack ();
//		}
//
//	}



	public void ChangeBackground()
	{
		StartCoroutine (ChangeBG (bgContainer.GetChild (1).GetComponent<Image> ()));


	}

	IEnumerator ChangeBG(Image upperBG)
	{
		float time = 4f;
		float currentTime = 0f;
	


		float distance = fro.x - to.x;
		float speed = distance / time;

		float timeToReachEdge = (500 - (-1250)) / speed;

		float imageSpeed = 1f / timeToReachEdge;
		wave.gameObject.SetActive (true);
		SoundManager.Instance.WaveSound ();
		do{
			wave.anchoredPosition = Vector2.Lerp(fro, to, currentTime /time);

			upperBG.fillAmount -= imageSpeed * Time.deltaTime;

			currentTime += Time.deltaTime;
			yield return null;
		}while(currentTime <= time);
		upperBG.transform.SetAsFirstSibling ();
		upperBG.fillAmount = 1;
		yield return new WaitForSeconds (1);
		wave.anchoredPosition = posStart;
		++bgIndex;
		if (bgIndex >= bgSprites.Length) {
			bgIndex = 0;
		}
		bgContainer.GetChild (0).GetComponent<Image> ().sprite = bgSprites [bgIndex];
		wave.gameObject.SetActive (false);

//		bgContainer.GetChild (1).GetComponent<Image> ().transform.SetAsFirstSibling ();
//		bgContainer.GetChild (1).GetComponent<Image> ().fillAmount = 1;
	}

    public void GoBlack()
    {
        StartCoroutine(GoBlackS());
    }

    IEnumerator GoBlackS()
    {
        
            if (black.color.a <= 0.8f)
            {
                do
                {
                    Color c = black.color;
                    c.a += 1f * Time.deltaTime;
                    black.color = c;

                    yield return null;
                } while(black.color.a < 0.8f);
            }
            
		yield return new WaitForSeconds (9f);
            if (black.color.a > 0)
            {
                do
                {
                    Color c = black.color;
                    c.a -= 1f * Time.deltaTime;
                    black.color = c;

                    yield return null;
                } while(black.color.a > 0f);
            }
        
    }

	public void ChangeBackGround(ISFSObject obj)
	{
//		bgIndex=obj.GetInt("map");
		bgIndex = Random.Range (0, bgSprites.Length);
		ChangeBackground ();
	}
	public void Weather(string _weather){

		switch (_weather) {
		case "day":
			WeatherDay ();
			break;
		case "night":
			WeatherNight ();
			break;
		default:
			WeatherDay ();
			break;

		}
	}

	public void WeatherFix(string _weather){
		Color c = black.color;
		switch (_weather) {
		case "day":
			c.a = 0;
			break;
		case "night":
			c.a = 0.8f;
			break;
		default:
			c.a = 0;
			break;

		}
		black.color = c;
	}

	public void SetMapWeather(ISFSObject _obj){
		string _weather = _obj.GetUtfString ("weather");
		bgIndex = _obj.GetInt ("map");
		WeatherFix (_weather);
		bgContainer.GetChild (1).GetComponent<Image> ().sprite = bgSprites[bgIndex];
	}
	public void WeatherDay()
	{
		SoundManager.Instance.ChickenDock ();
		StartCoroutine(Day());
	}
	public void WeatherNight()
	{
		StartCoroutine(Night());
	}

	IEnumerator Night()
	{
		if (black.color.a <= 0.8f)
		{
			do
			{
				Color c = black.color;
				c.a += 0.2f * Time.deltaTime;
				black.color = c;

				yield return null;
			} while(black.color.a < 0.8f);
		}
	}

	IEnumerator Day()
	{
		if (black.color.a > 0)
		{
			do
			{
				Color c = black.color;
				c.a -= 0.2f * Time.deltaTime;
				black.color = c;

				yield return null;
			} while(black.color.a > 0f);
		}

	}

}
