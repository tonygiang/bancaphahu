﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

public class AllFish {

	public string id;
	public string type;
	public string path;
	public int frame;
	public string init;
	public float posX;
	public float posY;
	public float speedbonus;
	public float speed;


	public AllFish(){

	}

	public AllFish(ISFSObject _obj){
		id = _obj.GetUtfString ("id");
		type = _obj.GetUtfString ("type");
		path = _obj.GetUtfString ("path");
		frame = _obj.GetInt ("frame");
		init = _obj.GetUtfString ("init");
		string[] posXY = init.Split (new char[]{'_'});
		posX = float.Parse (posXY [0]);
		posY = float.Parse (posXY [1]);
		speedbonus=float.Parse(_obj.GetData ("speedbonus").Data.ToString());
		speed = float.Parse(_obj.GetData ("speed").Data.ToString());
	}
}
