﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using UnityEngine.UI;
using Spine.Unity;
using Utils;
using System.Linq;

public class PlayGemManager : MonoBehaviour
{
  public static PlayGemManager Instance;

  public Dictionary<string, BulletMovement> bullets = new Dictionary<string, BulletMovement>();


  public GunGemManager gunGemManager;
  public GameObject resultPanel;
  public Transform coinCanvas;
  int movingCount = 0;

  [SerializeField]
  GameObject optionButtons;

  [SerializeField]
  GameObject soundButton, musicButton;
  [SerializeField]
  Sprite[] sound, music;
  private Transform jp1;
  private int countJackpot = 0;
  void Awake()
  {
    Instance = this;
  }
  void Start()
  {
    SFSManager.instance.GetAllFish();
    SFSManager.instance.GetAllUserRoomGem();
    MusicManager.Instance.PlayInGameMusic();
    LoadAudio();
  }
  void FixedUpdate()
  {
    var bulletInRoom = new List<string>(bullets.Keys);

    foreach (var key in bulletInRoom)
    {
      BulletMovement bullet = bullets[key];
      if (Mathf.Abs(bullet.transform.position.y) > 6 || Mathf.Abs(bullet.transform.position.x) > 10)
      {
        RemoveBullet(key, bullet);
      }
      RenderBullet(key, bullet);
    }
    var fishInRomm = new List<string>(BanCa.Game.Shooting.Fishes.Keys);

    foreach (var key in fishInRomm)
    {
      FishMovement f = BanCa.Game.Shooting.Fishes[key];
      if (Mathf.Abs(f.transform.position.y) > 7 || Mathf.Abs(f.transform.position.x) > 12)
      {
        RemoveFish(key, f);
      }
      RenderFish(key, f);
      var bf = new List<string>(bullets.Keys);
      foreach (var Bkey in bf)
      {
        BulletMovement bullet = bullets[Bkey];
        bool result = f.CheckHit(bullet);
        if (result)
        {
          BulletRemove(Bkey);
          GameObject ga = PoolingSystem.instance.SpawnSnare(bullet.gameObject.name, bullet.transform.position);
          ga.SetActive(true);
          PoolingSystem.instance.RemoveObject(ga, 0.6f, 30);
          RemoveBullet(Bkey, bullet);

        }
      }
    }

  }


  void RenderFish(string id, FishMovement fish)
  {
    fish.FishMove();
  }

  void RemoveFish(string id, FishMovement fish)
  {
    PoolingSystem.instance.RemoveFish(fish.gameObject, fish.gameObject.name);
    BanCa.Game.Shooting.Fishes.Remove(id);
  }
  void RemoveBullet(string id, BulletMovement bu)
  {
    //bu.gameObject.SetActive (false);
    PoolingSystem.instance.RemoveBullet(bu.gameObject, bu.gameObject.name);
    bullets.Remove(id);
  }

  public void AddFish(AllFish _fish)
  {
    string _fishType = _fish.type;
    try
    {
      if (!BanCa.Game.Shooting.Fishes.ContainsKey(_fish.id))
      {
        GameObject ga = PoolingSystem.instance.SpawnFish(_fishType);
        ga.GetComponent<FishMovement>().All_Bird(_fish);
        BanCa.Game.Shooting.Fishes.Add(_fish.id, ga.GetComponent<FishMovement>());
      }
    }
    catch (KeyNotFoundException e)
    {

    }
  }
  public void AddFishSingle(ISFSObject obj)
  {
    string type = obj.GetUtfString("type");
    string color = obj.GetUtfString("color");
    string path = obj.GetUtfString("path");
    float speedbonus = obj.GetFloat("speedbonus");
    float speed = obj.GetFloat("speed");
    string[] _arrID = obj.GetUtfStringArray("id");
    string[] _init = obj.GetUtfStringArray("init");
    int _sizeArr = _arrID.Length;
    for (int i = 0; i < _sizeArr; i++)
    {
      GameObject ga = PoolingSystem.instance.SpawnFish(type);
      string _idBird = _arrID[i];
      string _initBird = _init[i];
      ga.GetComponent<FishMovement>().Add_Bird(_idBird, type, color, path, _initBird, speedbonus, speed);
      if (string.IsNullOrEmpty(_idBird) || string.IsNullOrEmpty(type) || string.IsNullOrEmpty(path) || string.IsNullOrEmpty(_initBird) || speed <= 0)
      {
        continue;
      }
      BanCa.Game.Shooting.Fishes.Replace(_idBird,
        ga.GetComponent<FishMovement>(),
        fm => Destroy(fm.gameObject));
    }

  }

  void RenderBullet(string id, BulletMovement bullet)
  {
    bullet.BulletMove();

  }

  public void AddBullet(ISFSObject obj, int clientServerLag)
  {
    string _user = obj.GetUtfString("user");
    if (BanCa.Game.Shooting.Users.ContainsKey(_user))
    {
      float _angle = obj.GetFloat("angle");
      int _coin = obj.GetInt("coin");
      string typeB = obj.GetUtfString("bullettype");
      string position = obj.GetUtfString("initposition");
      GameObject ga = PoolingSystem.instance.SpawnBullet(typeB);
      string idB = obj.GetUtfString("bulletid");
      BulletMovement _bu = ga.GetComponent<BulletMovement>();
      _bu.id = idB;
      _bu.type = typeB;
      _bu.uname = _user;
      _bu.currentAngle = _angle;
      string[] posXY = position.Split(new char[] { '_' });
      _bu.posX = float.Parse(posXY[0]);
      _bu.posY = float.Parse(posXY[1]);
      _bu.Init(clientServerLag, BanCa.Game.Shooting.Users[_user].Position);
      bullets.Add(_bu.id, _bu);
      gunGemManager.Shoot(_user, _coin, _angle);
    }
  }

  // Tải dữ liệu cá trong màn chơi 
  public void LoadFishInRoom(ISFSObject obj)
  {
    //fishes.Clear ();
    var fishInRomm = new List<string>(BanCa.Game.Shooting.Fishes.Keys);

    foreach (var key in fishInRomm)
    {
      FishMovement f = BanCa.Game.Shooting.Fishes[key];
      f.gameObject.SetActive(false);
      BanCa.Game.Shooting.Fishes.Remove(key);
    }
    BanCa.Game.Shooting.Fishes.Clear();
    JSONObject js = new JSONObject(obj.GetUtfString("fishs"));
    ISFSArray fishArr = SFSArray.NewFromJsonData(js.GetField("fishs").ToString());
    foreach (ISFSObject ob in fishArr)
    {
      AllFish f = new AllFish(ob);
      AddFish(f);
    }
  }

  // Tải dữ liệu tổng số người đang chơi trong phòng 


  public void FishRemove(ISFSObject obj)
  {
    int bCost = obj.GetInt("bulletcost");
    string fid = obj.GetUtfString("id");
    string userID = obj.GetUtfString("user");
    if (!BanCa.Game.Shooting.Users.ContainsKey(userID))
      return;
    if (BanCa.Game.Shooting.Fishes.ContainsKey(fid))
    {
      FishMovement fishD = BanCa.Game.Shooting.Fishes[fid];
      BanCa.Game.Shooting.Fishes.Remove(fid);
      if (fishD.type == "jackpot")
      {
        BanCa.Game.Shooting.Events.OnJackpot.Invoke();
        int _money = obj.GetInt("money");
        SoundManager.Instance.YaySound();
        GameObject _effe = PoolingSystem.instance.SpawnEffect("EffectJackpot", fishD.transform.position);
        PoolingSystem.instance.RemoveObject(_effe, 2f, 10);
        AnimationFishDie.Instance.Shaking();
        gunGemManager.FakeCoinFishDie(userID, _money, fishD.type);
        StartCoroutine(JackpotToGun(fishD, _money, userID));
        AnimationFishDie.Instance.DIE_SMALL(fishD, gunGemManager.GunUserPosition(userID));
        if (UserManager.UserName == userID)
        {
          UserManager.jackpot = _money;
        }
        return;
      }
      else
      {
        int _coinReveive = fishD.mutiplie * bCost;
        PoolingSystem.instance.INIT_COIN(fishD.transform.position, _coinReveive.ToString(), "1000");
        if (fishD.type == "tienca")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject ga = PoolingSystem.instance.SpawnEffect("KillTienCa", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(ga, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunGemManager.GunUserPosition(userID));
          return;
        }
        else if (fishD.type == "cakiem")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("EffectCaChep", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "bachtuoc")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("Effect8", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "casau")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("EffectCaMap", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cachinh" || fishD.type == "rua")
        {
          Effect(fishD);

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cavoi")
        {
          SoundManager.Instance.AN_Ca_To();
          EffectKillJackpot(fishD);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cua")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillTom", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_ROTATE(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cahe" || fishD.type == "casutu" || fishD.type == "cadenlong" || fishD.type == "ech")
        {
          Effect(fishD);

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_ROTATE(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "casutuaura")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillCaSuTuAura", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_ROTATE(fishD, gunGemManager.GunUserPosition(userID));
          CoinToPlayer(gunGemManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cuavang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe10k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          StartCoroutine(FishCardToGun(fishD, gunGemManager.GunUserPosition(userID), userID, "Card10"));
          return;
        }
        else if (fishD.type == "bachtuocvang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe20k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          StartCoroutine(FishCardToGun(fishD, gunGemManager.GunUserPosition(userID), userID, "Card20"));
          return;
        }
        else if (fishD.type == "casauvang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe50k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          StartCoroutine(FishCardToGun(fishD, gunGemManager.GunUserPosition(userID), userID, "Card50"));
          return;
        }
        else if (fishD.type == "cavoivang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe100k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          StartCoroutine(FishCardToGun(fishD, gunGemManager.GunUserPosition(userID), userID, "Card100"));
          return;
        }
        else
        {
          Effect(fishD);

        }

        gunGemManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
        AnimationFishDie.Instance.DIE_SMALL(fishD, gunGemManager.GunUserPosition(userID));
      }
    }
  }
  public void BulletRemove(string key)
  {
    bullets.Remove(key);
  }

  public void OutRoom()
  {
    SoundManager.Instance.ButtonSound();
    MessageBox.instance.Show("Thoát", "Bạn có chắc muốn thoát?", MessageBoxType.OK_CANCEL, BackConfirm);
  }
  private void BackConfirm(MessageBoxCallBack result)
  {
    if (result.Equals(MessageBoxCallBack.OK))
    {
      SFSManager.instance.USER_OUT_ROOM();
    }
  }

  public void OpenResult(ISFSObject obj)
  {
    string _username = obj.GetUtfString("username");
    int _fishValue = obj.GetInt("fish");
    int _coinValue = obj.GetInt("coins");
    if (BanCa.Game.Shooting.Users.ContainsKey(_username))
    {

      if (_username == UserManager.UserName)
      {
        resultPanel.SetActive(true);
        Loading.instance.Hide();
        Text _fishText = resultPanel.transform.GetChild(0).GetChild(5).GetComponent<Text>();
        Text _coinText = resultPanel.transform.GetChild(0).GetChild(6).GetComponent<Text>();

        resultPanel.transform.GetChild(0).GetChild(7).GetComponent<Button>().onClick.AddListener(() => LoadLevel.Instance.ShowLoadingPanel("Menu"));
        AnimationFishDie.Instance.UPDATE_SCORE(_fishValue, _fishText, _coinValue, _coinText);
        return;
      }
      else
      {
        gunGemManager.GunGoOut(_username);
        BanCa.Game.Shooting.Users.Remove(_username);
      }
    }

  }
  void EffectKillJackpot(FishMovement fish)
  {
    GameObject ga = PoolingSystem.instance.SpawnEffect("KillJackpot", fish.transform.position);
    PoolingSystem.instance.RemoveObject(ga, 1.5f, 10);
  }

  void Effect(FishMovement fish)
  {
    string _listFishBan = "canho,cavangnho,cachuon,canoc";
    if (!_listFishBan.Contains(fish.type))
    {
      //AnimationFishDie.Instance.Shaking2 ();
      string[] listEffect = { "Effect5", "Effect6", "Effect7", "Effect8" };
      GameObject ga = PoolingSystem.instance.SpawnEffect(listEffect[Random.Range(0, listEffect.Length)], fish.transform.position);
      PoolingSystem.instance.RemoveObject(ga, 1.5f, 10);
    }
    else
    {
      string[] listEffect = { "Effect1", "Effect2", "Effect3", "Effect4" };
      GameObject ga = PoolingSystem.instance.SpawnEffect(listEffect[Random.Range(0, listEffect.Length)], fish.transform.position);
      PoolingSystem.instance.RemoveObject(ga, 1.5f, 10);
    }
    //		}
  }

  public void CoinToPlayer(Vector2 playerPosition, Vector2 spawnPosition, int _mutiplie)
  {
    switch (_mutiplie)
    {
      case 5:
        Vector2[] _listPos3 = {
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1f)
      };
        Init1RowCoin(_listPos3, playerPosition);
        break;
      case 6:
        Vector2[] _listPos4 = {
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1f),
      };
        Init1RowCoin(_listPos4, playerPosition);
        break;
      case 8:
        Vector2[] _listPos5 = {
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1f),
      };
        Init1RowCoin(_listPos5, playerPosition);
        break;
      case 10:
        Vector2[] _listPos6 = {
        new Vector2 (spawnPosition.x - 2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 2f, spawnPosition.y + 1f),
      };
        Init1RowCoin(_listPos6, playerPosition);
        break;
      case 12:

        Vector2[] _listPos61 = {
        new Vector2 (spawnPosition.x - 2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 2f, spawnPosition.y + 1f),
      };
        Init1RowCoin(_listPos61, playerPosition);
        break;
      case 15:
        Vector2[] _listPos7 = {
        new Vector2 (spawnPosition.x - 2.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 2.2f, spawnPosition.y + 1f),
      };

        Init1RowCoin(_listPos7, playerPosition);
        break;
      case 18:
        Vector2[] _listPos81 = {
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1f),
      };
        Vector2[] _listPos82 = {
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1.7f)
      };
        Init2RowCoin(_listPos81, _listPos82, playerPosition);
        break;
      case 20:
        Vector2[] _listPos101 = {
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1f),
      };
        Vector2[] _listPos102 = {
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1.7f)
      };
        Init2RowCoin(_listPos101, _listPos102, playerPosition);
        break;
      case 25:
        Vector2[] _listPos121 = {
        new Vector2 (spawnPosition.x - 2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 2f, spawnPosition.y + 1f),
      };
        Vector2[] _listPos122 = {
        new Vector2 (spawnPosition.x - 2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 2f, spawnPosition.y + 1.7f)
      };
        Init2RowCoin(_listPos121, _listPos122, playerPosition);
        break;
      case 30:
        Vector2[] _listPos141 = {
        new Vector2 (spawnPosition.x - 2.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 2.2f, spawnPosition.y + 1f)
      };
        Vector2[] _listPos142 = {
        new Vector2 (spawnPosition.x - 2.2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 2.2f, spawnPosition.y + 1.7f)
      };
        Init2RowCoin(_listPos141, _listPos142, playerPosition);
        break;
      case 35:
        Vector2[] _listPos351 = {
        new Vector2 (spawnPosition.x - 2.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 2.2f, spawnPosition.y + 1f)
      };
        Vector2[] _listPos352 = {
        new Vector2 (spawnPosition.x - 2.2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 1.6f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 0.8f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 0.8f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 1.6f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 2.2f, spawnPosition.y + 1.7f)
      };
        Init2RowCoin(_listPos351, _listPos352, playerPosition);
        break;
      case 40:
        Vector2[] _listPos161 = {
        new Vector2 (spawnPosition.x - 2.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 1.4f, spawnPosition.y + 1f),
        new Vector2 (spawnPosition.x + 2.2f, spawnPosition.y + 1f)

      };
        Vector2[] _listPos162 = {
        new Vector2 (spawnPosition.x - 2.8f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 1.2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x - 0.4f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 0.4f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 1.2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 2f, spawnPosition.y + 1.7f),
        new Vector2 (spawnPosition.x + 2.8f, spawnPosition.y + 1.7f)

      };
        Init2RowCoin(_listPos161, _listPos162, playerPosition);
        break;


    }
  }
  void Init1RowCoin(Vector2[] _listPos, Vector2 playerPosition)
  {
    float _timeSpawn = 0;
    for (int i = 0; i < _listPos.Length; i++)
    {
      StartCoroutine(MoveTheCoin(playerPosition, _listPos[i], _timeSpawn, _listPos.Length));
      _timeSpawn += 0.1f;
    }
  }
  void Init2RowCoin(Vector2[] _listPos1, Vector2[] _listPos2, Vector2 playerPosition)
  {
    float _timeSpawn = 0;
    for (int i = 0; i < _listPos1.Length; i++)
    {
      StartCoroutine(MoveTheCoin(playerPosition, _listPos1[i], _timeSpawn, _listPos1.Length));
      StartCoroutine(MoveTheCoin(playerPosition, _listPos2[i], _timeSpawn, _listPos2.Length));
      _timeSpawn += 0.1f;
    }
  }


  //	public void CoinToPlayer(int moneyCount, Vector2 playerPosition, Vector2 spawnPosition)
  //	{
  //		int coinCount = 3;
  //		if(moneyCount >=21 && moneyCount <= 50)
  //		{
  //			coinCount = 5;
  //		}
  //		else if(moneyCount >=51 && moneyCount <= 200)
  //		{
  //			coinCount = 10;
  //		}
  //		else if(moneyCount >=201 && moneyCount <= 999)
  //		{
  //			coinCount = 15;
  //		}
  //		else if(moneyCount >= 1000)
  //		{
  //			coinCount = 20;
  //		}
  //
  //		for(int i = 0; i < coinCount; ++i)
  //		{
  //			StartCoroutine(MoveTheCoin(playerPosition, spawnPosition));
  //		}
  //
  //	}

  //	IEnumerator MoveTheCoin(Vector2 playerPosition, Vector2 spawnPosition)
  //	{
  //		GameObject coin = PoolingSystem.instance.SpawnObject(spawnPosition, "RotatingGem");
  //		coin.transform.SetParent(coinContainer.transform);
  //		coin.transform.localScale = new Vector3(0.7f, 0.7f, 1f);
  //
  //		coin.transform.localPosition = spawnPosition;
  //
  //		Vector2 direction = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized;
  //
  //		float speed = Random.Range(4.0f, 5.0f);
  //		do{
  //			Vector2 pos = coin.transform.position;
  //			pos += direction * speed * Time.deltaTime;
  //			coin.transform.position = pos;
  //
  //			speed -= 8 * Time.deltaTime;
  //			yield return null;
  //		}while(speed > 1.5f);
  //
  //		direction = new Vector2(playerPosition.x - coin.transform.position.x, playerPosition.y - coin.transform.position.y).normalized;
  //		float distanceToPlayer = (direction.x * direction.x + direction.y * direction.y);
  //		speed = 1.5f;
  //
  //		float currentTime = 0;
  //		float time = distanceToPlayer / speed;
  //
  //		Vector2 fro = coin.transform.position;
  //		yield return new WaitForSeconds (1f);
  //		do{
  //			coin.transform.position = Vector2.Lerp(fro, playerPosition, currentTime / time);
  //
  //			currentTime += Time.deltaTime;
  //			yield return null;
  //		}while(currentTime <= time);
  //
  //		PoolingSystem.instance.RemoveObject(coin, 0, 21);
  //	}
  IEnumerator MoveTheCoin(Vector2 playerPosition, Vector2 _listPos, float _timeSpawn, int _count)
  {
    float _currentTime = 0;
    float _time = 0.3f;
    GameObject coin = PoolingSystem.instance.SpawnObject(_listPos, "RotatingGem");
    coin.transform.localScale = Vector3.zero;

    Vector2 _to = new Vector2(_listPos.x, _listPos.y + 1f);
    Vector2 _to2 = new Vector2(_listPos.x, _listPos.y + 0.1f);
    coin.transform.localPosition = _listPos;
    yield return new WaitForSeconds(_timeSpawn);
    coin.transform.localScale = new Vector3(0.6f, 0.6f, 1f);
    do
    {
      coin.transform.localPosition = Vector3.Lerp(_listPos, _to, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    coin.transform.localPosition = _to;
    _currentTime = 0;
    do
    {
      coin.transform.localPosition = Vector3.Lerp(_to, _listPos, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    coin.transform.localPosition = _listPos;
    _currentTime = 0;
    _time = 0.1f;
    do
    {
      coin.transform.localPosition = Vector3.Lerp(_listPos, _to2, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    coin.transform.localPosition = _to2;
    _currentTime = 0;
    do
    {
      coin.transform.localPosition = Vector3.Lerp(_to2, _listPos, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    coin.transform.localPosition = _listPos;

    //		yield return new WaitForSeconds (0.7f - _timeSpawn);
    yield return new WaitForSeconds(0.2f);

    Vector2 direction = new Vector2(playerPosition.x - coin.transform.position.x, playerPosition.y - coin.transform.position.y).normalized;
    float distanceToPlayer = (direction.x * direction.x + direction.y * direction.y);
    float speed = 2f;

    float currentTime = 0;
    float time = distanceToPlayer / speed;

    Vector2 fro = coin.transform.position;

    do
    {
      coin.transform.position = Vector2.Lerp(fro, playerPosition, currentTime / time);

      currentTime += Time.deltaTime;
      yield return null;
    } while (currentTime <= time);
    PoolingSystem.instance.RemoveObject(coin, 0, 21);
  }

  IEnumerator JackpotToGun(FishMovement fish, int _money, string _userid)
  {
    UserInRoom u = BanCa.Game.Shooting.Users[_userid];
    GameObject ga = PoolingSystem.instance.SpawnCoinJackpot();
    ga.transform.GetChild(0).GetChild(0).GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "animation", false);
    ga.transform.GetChild(0).GetChild(4).gameObject.SetActive(false);
    ga.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Text>().text = u.DisplayName.Value;
    ga.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Image>().sprite = gunGemManager.UserSlots[u.Position - 1].transform.GetChild(0).GetChild(3).GetChild(0).GetComponent<Image>().sprite;

    ga.transform.GetChild(0).GetChild(4).gameObject.SetActive(true);
    const float seconds = 3f;
    float elapsedTime = 0;

    while (elapsedTime < seconds)
    {

      elapsedTime += Time.deltaTime;
      ga.transform.GetChild(0).GetChild(4).GetComponent<Text>().text = "+" + Mathf.Floor(Mathf.Lerp(0, _money, (elapsedTime / seconds))).ToString(); ;
      yield return null;
    }
    PoolingSystem.instance.RemoveObject(ga, 1f, 2);


  }



  public bool CheckHit(BulletMovement _b, FishMovement _f)
  {
    Vector3 currentPos = _f.transform.position;
    bool result = false;
    float d_x_max = currentPos.x + _f.hitRadious;
    float d_y_max = currentPos.y + _f.hitRadious;

    float d_x_min = currentPos.x - _f.hitRadious;
    float d_y_min = currentPos.y - _f.hitRadious;

    float b_X = _b.transform.position.x;
    float b_Y = _b.transform.position.y;

    if (d_x_max > b_X && b_X > d_x_min)
    {
      if (d_y_max > b_Y && b_Y > d_y_min)
      {
        result = true;

      }
    }
    return result;

  }


  // Setting Button

  public void ButtonToPosition()
  {
    AnimationFishDie.Instance.ButtonToPosition(optionButtons);
  }


  public void ButtonSoundClick()
  {
    SoundManager.Instance.ButtonSound();
  }
  #region Audio
  public void LoadAudio()
  {
    if (Immortal.instance.MusicButtonState())
    {
      musicButton.GetComponent<Image>().sprite = music[0];
    }
    else
    {
      musicButton.GetComponent<Image>().sprite = music[1];
    }

    if (Immortal.instance.SoundButtonState())
    {
      soundButton.GetComponent<Image>().sprite = sound[0];
    }
    else
    {
      soundButton.GetComponent<Image>().sprite = sound[1];
    }
  }

  public void SoundButton()
  {
    if (Immortal.instance.SoundButton())
    {
      soundButton.GetComponent<Image>().sprite = sound[0];
    }
    else
    {
      soundButton.GetComponent<Image>().sprite = sound[1];
    }
  }

  public void MusicButton()
  {
    if (Immortal.instance.MusicButton())
    {
      musicButton.GetComponent<Image>().sprite = music[0];
    }
    else
    {
      musicButton.GetComponent<Image>().sprite = music[1];
    }
  }
  public void ButtonClickSound()
  {
    SoundManager.Instance.ButtonSound();
  }
  #endregion

  #region BUTTON
  public void Button_CHAT()
  {
    MessageController.instance.OpenChatBox();
    SoundManager.Instance.ButtonSound();
  }
  public void Button_GUNVIP()
  {
    SoundManager.Instance.ButtonSound();
    gunGemManager.OpenChooseGunVip();
  }
  #endregion
  public void Set_FishSpeed(ISFSObject _obj)
  {
    string _id = _obj.GetUtfString("id");
    float _value = _obj.GetFloat("value");
    if (BanCa.Game.Shooting.Fishes.ContainsKey(_id))
    {
      BanCa.Game.Shooting.Fishes[_id].ChangeSpeed(_value);
    }
  }

  IEnumerator FishCardToGun(FishMovement fish, GameObject shooter, string _userid, string _typeCard)
  {
    UserInRoom u = BanCa.Game.Shooting.Users[_userid];
    fish.ChangeTimeScale(6f);
    yield return new WaitForSeconds(1.8f);
    fish.ChangeTimeScale(1f);
    fish.FishDie();
    Vector3 currentPos = fish.transform.position;
    Vector3 destination = shooter.transform.position;
    destination.z = 0;
    Vector3 currentScale = fish.transform.localScale;
    Vector3 _toAngle = new Vector3(0, 0, 360f);
    Vector3 _maxScale = new Vector3(2f, 2f, 2f);


    Vector3 _posCard = new Vector3(0, -2f, 0);

    float _time = 0.1f;
    float _currentTime = 0;

    PoolingSystem.instance.RemoveFish(fish.gameObject, fish.gameObject.name);
    GameObject _card = PoolingSystem.instance.SpawnObject(currentPos, _typeCard);
    //		_card.transform.SetParent (coinContainer.transform);
    do
    {
      _card.transform.localScale = Vector3.Lerp(Vector3.zero, _maxScale, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    _card.transform.localScale = _maxScale;
    //KillCaThe

    _currentTime = 0;
    _time = 0.3f;
    do
    {
      _card.transform.localPosition = Vector3.Lerp(currentPos, _posCard, _currentTime / _time);
      _card.transform.eulerAngles = Vector3.Lerp(Vector3.zero, _toAngle, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    _card.transform.localPosition = _posCard;
    _card.transform.eulerAngles = _toAngle;
    GameObject _killcathe = PoolingSystem.instance.SpawnObject(Vector3.zero, "KillCaThe");
    _killcathe.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = u.DisplayName.Value;
    _killcathe.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<Image>().sprite = gunGemManager.UserSlots[u.Position - 1].transform.GetChild(0).GetChild(3).GetChild(0).GetComponent<Image>().sprite;
    yield return new WaitForSeconds(2f);
    PoolingSystem.instance.RemoveObject(_killcathe, 0, 3);
    _currentTime = 0;
    do
    {
      _card.transform.localPosition = Vector3.Lerp(_posCard, destination, _currentTime / _time);
      _card.transform.eulerAngles = Vector3.Lerp(_toAngle, Vector3.zero, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    _card.transform.eulerAngles = Vector3.zero;
    _card.transform.localPosition = destination;
    yield return new WaitForSeconds(0.5f);
    _currentTime = 0;
    _time = 0.3f;
    do
    {
      _card.transform.localScale = Vector3.Lerp(_maxScale, Vector3.zero, _currentTime / _time);
      _currentTime += Time.deltaTime;
      yield return null;
    } while (_currentTime < _time);
    _card.transform.localScale = Vector3.zero;
    PoolingSystem.instance.RemoveObject(_card, 0, 3);
    if (UserManager.UserName == _userid)
    {
      MessageBox.instance.Show("Chúc mừng bạn trúng thưởng 1 thẻ cào điện thoại. Vui lòng vào hộp thư để nhận thưởng ");
    }
  }

  public void BuyGem2to5Minuste(int _gem)
  {
    gunGemManager.BuyCoin2to5Minus(_gem);
  }

  public void ClearFishes()
  {
    BanCa.Game.Shooting.Fishes.Values.Select(f => f.gameObject).ForEach(Destroy);
    BanCa.Game.Shooting.Fishes.Clear();
  }
}
