﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using UnityEngine.UI;
using Utils;
using System.Linq;

public class PlayCoinManager : MonoBehaviour
{
  public static PlayCoinManager Instance;

  public Dictionary<string, BulletMovement> bullets = new Dictionary<string, BulletMovement>();


  public GunCoinManager gunCoinManager;
  public GameObject resultPanel;
  [SerializeField]
  GameObject optionButtons;

  [SerializeField]
  GameObject soundButton, musicButton;
  [SerializeField]
  Sprite[] sound, music;

  void Awake()
  {
    Instance = this;
  }

  void Start()
  {
    SFSManager.instance.GetAllFish();
    SFSManager.instance.GetAllUserRoomGold();
    MusicManager.Instance.PlayInGameMusic();
    LoadAudio();
  }

  void FixedUpdate()
  {
    var bulletInRoom = new List<string>(bullets.Keys);

    foreach (var key in bulletInRoom)
    {
      BulletMovement bullet = bullets[key];
      if (Mathf.Abs(bullet.transform.position.y) > 6 || Mathf.Abs(bullet.transform.position.x) > 10)
      {
        RemoveBullet(key, bullet);
      }
      RenderBullet(key, bullet);
    }
    var fishInRomm = new List<string>(BanCa.Game.Shooting.Fishes.Keys);

    foreach (var key in fishInRomm)
    {
      FishMovement f = BanCa.Game.Shooting.Fishes[key];
      if (Mathf.Abs(f.transform.position.y) > 7 || Mathf.Abs(f.transform.position.x) > 12)
      {
        RemoveFish(key, f);
      }
      RenderFish(key, f);
      var bf = new List<string>(bullets.Keys);
      foreach (var Bkey in bf)
      {
        BulletMovement bullet = bullets[Bkey];
        bool result = f.CheckHit(bullet);
        if (result)
        {
          BulletRemove(Bkey);
          GameObject ga = PoolingSystem.instance.SpawnSnare(bullet.gameObject.name, bullet.transform.position);
          ga.SetActive(true);
          PoolingSystem.instance.RemoveObject(ga, 0.6f, 30);
          RemoveBullet(Bkey, bullet);
        }
      }
    }

  }


  void RenderFish(string id, FishMovement fish)
  {
    fish.FishMove();
  }

  void RemoveFish(string id, FishMovement fish)
  {
    PoolingSystem.instance.RemoveFish(fish.gameObject, fish.gameObject.name);
    BanCa.Game.Shooting.Fishes.Remove(id);
  }

  void RemoveBullet(string id, BulletMovement bu)
  {
    PoolingSystem.instance.RemoveBullet(bu.gameObject, bu.gameObject.name);
    bullets.Remove(id);
  }

  public void AddFish(AllFish _fish)
  {
    string _fishType = _fish.type;
    try
    {
      if (!BanCa.Game.Shooting.Fishes.ContainsKey(_fish.id))
      {
        GameObject ga = PoolingSystem.instance.SpawnFish(_fishType);
        ga.GetComponent<FishMovement>().All_Bird(_fish);
        BanCa.Game.Shooting.Fishes.Add(_fish.id, ga.GetComponent<FishMovement>());
      }
    }
    catch (KeyNotFoundException e)
    {

    }
  }

  public void AddFishSingle(ISFSObject obj)
  {
    string type = obj.GetUtfString("type");
    string color = obj.GetUtfString("color");
    string path = obj.GetUtfString("path");
    float speedbonus = obj.GetFloat("speedbonus");
    float speed = obj.GetFloat("speed");
    string[] _arrID = obj.GetUtfStringArray("id");
    string[] _init = obj.GetUtfStringArray("init");
    int _sizeArr = _arrID.Length;
    for (int i = 0; i < _sizeArr; i++)
    {
      GameObject ga = PoolingSystem.instance.SpawnFish(type);

      string _idBird = _arrID[i];
      string _initBird = _init[i];
      if (string.IsNullOrEmpty(_idBird) || string.IsNullOrEmpty(type) || string.IsNullOrEmpty(path) || string.IsNullOrEmpty(_initBird) || speed <= 0)
      {
        continue;
      }
      ga.GetComponent<FishMovement>().Add_Bird(_idBird, type, color, path, _initBird, speedbonus, speed);
      BanCa.Game.Shooting.Fishes.Replace(_idBird,
        ga.GetComponent<FishMovement>(),
        fm => Destroy(fm.gameObject));
    }
  }

  void RenderBullet(string id, BulletMovement bullet)
  {
    bullet.BulletMove();

  }

  public void AddBullet(ISFSObject obj, int clientServerLag)
  {
    string _user = obj.GetUtfString("user");
    if (BanCa.Game.Shooting.Users.ContainsKey(_user))
    {
      float _angle = obj.GetFloat("angle");
      int _coin = obj.GetInt("coin");
      string typeB = obj.GetUtfString("bullettype");
      string position = obj.GetUtfString("initposition");
      GameObject ga = PoolingSystem.instance.SpawnBullet(typeB);
      string idB = obj.GetUtfString("bulletid");
      BulletMovement _bu = ga.GetComponent<BulletMovement>();
      _bu.id = idB;
      _bu.type = typeB;
      _bu.uname = _user;
      _bu.currentAngle = _angle;
      string[] posXY = position.Split(new char[] { '_' });
      _bu.posX = float.Parse(posXY[0]);
      _bu.posY = float.Parse(posXY[1]);
      _bu.Init(clientServerLag, BanCa.Game.Shooting.Users[_user].Position);
      bullets.Add(_bu.id, _bu);
      gunCoinManager.Shoot(_user, _coin, _angle);
    }
  }


  // Tải dữ liệu cá trong màn chơi
  public void LoadFishInRoom(ISFSObject obj)
  {
    var fishInRomm = new List<string>(BanCa.Game.Shooting.Fishes.Keys);
    foreach (var key in fishInRomm)
    {
      FishMovement f = BanCa.Game.Shooting.Fishes[key];
      f.gameObject.SetActive(false);
      BanCa.Game.Shooting.Fishes.Remove(key);
    }
    BanCa.Game.Shooting.Fishes.Clear();
    JSONObject js = new JSONObject(obj.GetUtfString("fishs"));
    ISFSArray fishArr = SFSArray.NewFromJsonData(js.GetField("fishs").ToString());
    foreach (ISFSObject ob in fishArr)
    {
      AllFish f = new AllFish(ob);
      AddFish(f);
    }
  }


  public void FishRemove(ISFSObject obj)
  {
    int bCost = obj.GetInt("bulletcost");
    string fid = obj.GetUtfString("id");
    string userID = obj.GetUtfString("user");
    if (!BanCa.Game.Shooting.Users.ContainsKey(userID))
      return;
    if (BanCa.Game.Shooting.Fishes.ContainsKey(fid))
    {

      FishMovement fishD = BanCa.Game.Shooting.Fishes[fid];
      BanCa.Game.Shooting.Fishes.Remove(fid);
      if (fishD.type == "jackpot")
      {
        BanCa.Game.Shooting.Events.OnJackpot.Invoke();
        int _money = obj.GetInt("money");
        SoundManager.Instance.YaySound();
        GameObject _eff = PoolingSystem.instance.SpawnEffect("KillJackpotGold", fishD.transform.position);
        PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
        AnimationFishDie.Instance.Shaking();
        PoolingSystem.instance.INIT_COIN(fishD.transform.position, _money.ToString(), "100");

        gunCoinManager.FakeCoinFishDie(userID, _money, fishD.type);
        AnimationFishDie.Instance.DIE_SMALL(fishD, gunCoinManager.GunUserPosition(userID));
      }
      else
      {
        int _coinReveive = fishD.mutiplie * bCost;
        PoolingSystem.instance.INIT_COIN(fishD.transform.position, _coinReveive.ToString(), "100");
        if (fishD.type == "tienca")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillTienCaGold", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);

          return;
        }
        else if (fishD.type == "cakiem")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("EffectCaChep", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "bachtuoc")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("Effect8", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "casau")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("EffectCaMap", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cachinh" || fishD.type == "rua")
        {
          Effect(fishD);

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cavoi")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _killBox = PoolingSystem.instance.SpawnEffect("KillJackpot", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_killBox, 1.75f, 10);
          AnimationFishDie.Instance.Shaking();

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_SPEED(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cua")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillTom", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_ROTATE(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cahe" || fishD.type == "casutu" || fishD.type == "cadenlong")
        {
          Effect(fishD);

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_ROTATE(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "casutuaura")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillCaSuTuAura", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();

          gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
          AnimationFishDie.Instance.DIE_ROTATE(fishD, gunCoinManager.GunUserPosition(userID));
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
          return;
        }
        else if (fishD.type == "cuavang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe10k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();



          return;
        }
        else if (fishD.type == "bachtuocvang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe20k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();


          return;
        }
        else if (fishD.type == "casauvang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe50k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();


          return;
        }
        else if (fishD.type == "cavoivang")
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillThe100k", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_eff, 1f, 10);
          AnimationFishDie.Instance.Shaking();


          return;
        }
        else if (obj.ContainsKey("rewardtype"))
        {
          SoundManager.Instance.AN_Ca_To();
          GameObject _killBox = PoolingSystem.instance.SpawnEffect("KillJackpot", fishD.transform.position);
          PoolingSystem.instance.RemoveObject(_killBox, 1.75f, 10);
          AnimationFishDie.Instance.Shaking();
          PoolingSystem.instance.RemoveFish(fishD.gameObject, fishD.gameObject.name);
          GameObject _eff = PoolingSystem.instance.SpawnEffect("KillBox", Vector3.zero);
          _eff.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = BanCa.Game.Shooting.Users[userID].DisplayName.Value;

          string _ReType = obj.GetUtfString("rewardtype");
          int _reVa = obj.GetInt("rewardvalue");
          if (_ReType == "card")
          {
            _eff.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "và nhận được thẻ điện thoại " + _reVa + "k";
          }
          else if (_ReType == "addgem")
          {
            _eff.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "và nhận được " + _reVa + " kim cương";
          }
          else if (_ReType == "addgold")
          {
            _eff.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "và nhận được " + _reVa + " kim cương";
            gunCoinManager.FakeCoinFishDie(userID, _reVa, fishD.type);
          }
          else if (_ReType == "text")
          {
            _eff.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "và nhận được một lời chúc may mắn";
          }
          else if (_ReType == "subgoldp")
          {
            _eff.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "và bị trừ " + _reVa + " kim cương";
            gunCoinManager.FakeCoinFishDie(userID, _reVa, fishD.type);
          }
          else if (_ReType == "subgold")
          {
            _eff.transform.GetChild(0).GetChild(3).GetComponent<Text>().text = "và bị trừ " + _reVa + " kim cương";
            gunCoinManager.FakeCoinFishDie(userID, _reVa, fishD.type);
          }
          PoolingSystem.instance.RemoveObject(_eff, 5f, 10);
          AnimationFishDie.Instance.DIE_ROTATE(fishD, gunCoinManager.GunUserPosition(userID));
          return;
        }
        else
        {
          Effect(fishD);
          AnimationFishDie.Instance.CoinToPlayer(gunCoinManager.GunUserPosition(userID).transform.position, fishD.transform.position, fishD.mutiplie);
        }

        gunCoinManager.FakeCoinFishDie(userID, _coinReveive, fishD.type);
        AnimationFishDie.Instance.DIE_SMALL(fishD, gunCoinManager.GunUserPosition(userID));
      }
    }
  }

  public void BulletRemove(string key)
  {
    bullets.Remove(key);
  }
  // Thoats game
  public void OutRoom()
  {
    SoundManager.Instance.ButtonSound();
    MessageBox.instance.Show("Thoát", "Bạn có chắc muốn thoát?", MessageBoxType.OK_CANCEL, BackConfirm);
  }

  private void BackConfirm(MessageBoxCallBack result)
  {
    if (result.Equals(MessageBoxCallBack.OK))
    {
      SFSManager.instance.USER_OUT_ROOM();
    }
  }

  public void OpenResult(ISFSObject obj)
  {
    string _username = obj.GetUtfString("username");
    int _fishValue = obj.GetInt("fish");
    int _coinValue = obj.GetInt("coins");
    if (BanCa.Game.Shooting.Users.ContainsKey(_username))
    {
      if (_username == UserManager.UserName)
      {
        resultPanel.SetActive(true);
        Loading.instance.Hide();
        Text _fishText = resultPanel.transform.GetChild(0).GetChild(5).GetComponent<Text>();
        Text _coinText = resultPanel.transform.GetChild(0).GetChild(6).GetComponent<Text>();

        resultPanel.transform.GetChild(0).GetChild(7).GetComponent<Button>().onClick.AddListener(() => LoadLevel.Instance.ShowLoadingPanel("Menu"));
        AnimationFishDie.Instance.UPDATE_SCORE(_fishValue, _fishText, _coinValue, _coinText);
        return;
      }
      else
      {
        gunCoinManager.GunGoOut(_username);
        BanCa.Game.Shooting.Users.Remove(_username);
      }
    }

  }

  void Effect(FishMovement fish)
  {
    string _listFishBan = "canho,cavangnho,cachuon,canoc";
    if (!_listFishBan.Contains(fish.type))
    {
      string[] listEffect = { "Effect5", "Effect6", "Effect7", "Effect8" };
      GameObject ga = PoolingSystem.instance.SpawnEffect(listEffect[Random.Range(0, listEffect.Length)], fish.transform.position);
      PoolingSystem.instance.RemoveObject(ga, 1.75f, 10);
    }
    else
    {
      string[] listEffect = { "Effect1", "Effect2", "Effect3", "Effect4" };
      GameObject ga = PoolingSystem.instance.SpawnEffect(listEffect[Random.Range(0, listEffect.Length)], fish.transform.position);
      PoolingSystem.instance.RemoveObject(ga, 1.75f, 10);
    }
  }

  // Setting Button

  public void ButtonToPosition()
  {
    AnimationFishDie.Instance.ButtonToPosition(optionButtons);
  }

  #region Audio

  public void LoadAudio()
  {
    if (Immortal.instance.MusicButtonState())
    {
      musicButton.GetComponent<Image>().sprite = music[0];
    }
    else
    {
      musicButton.GetComponent<Image>().sprite = music[1];
    }

    if (Immortal.instance.SoundButtonState())
    {
      soundButton.GetComponent<Image>().sprite = sound[0];
    }
    else
    {
      soundButton.GetComponent<Image>().sprite = sound[1];
    }
  }

  public void SoundButton()
  {
    if (Immortal.instance.SoundButton())
    {
      soundButton.GetComponent<Image>().sprite = sound[0];
    }
    else
    {
      soundButton.GetComponent<Image>().sprite = sound[1];
    }
  }

  public void MusicButton()
  {
    if (Immortal.instance.MusicButton())
    {
      musicButton.GetComponent<Image>().sprite = music[0];
    }
    else
    {
      musicButton.GetComponent<Image>().sprite = music[1];
    }
  }

  #endregion

  #region BUTTON

  public void Button_CHAT()
  {
    MessageController.instance.OpenChatBox();
    SoundManager.Instance.ButtonSound();
  }

  public void Button_GUNVIP()
  {
    SoundManager.Instance.ButtonSound();
    gunCoinManager.OpenChooseGunVip();
  }

  #endregion

  public void Set_FishSpeed(ISFSObject _obj)
  {
    string _id = _obj.GetUtfString("id");
    float _value = _obj.GetFloat("value");
    if (BanCa.Game.Shooting.Fishes.ContainsKey(_id))
    {
      BanCa.Game.Shooting.Fishes[_id].ChangeSpeed(_value);
    }
  }

  public void ClearFishes()
  {
    BanCa.Game.Shooting.Fishes.Values.Select(f => f.gameObject).ForEach(Destroy);
    BanCa.Game.Shooting.Fishes.Clear();
  }
}
