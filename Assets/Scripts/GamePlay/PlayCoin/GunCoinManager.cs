﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using UnityEngine.EventSystems;
using Spine.Unity;
using UnityEngine.UI;
using System.Linq;

public class GunCoinManager : MonoBehaviour
{
  public static GunCoinManager Instance;
  public BanCa.ShootingUserPanel[] UserSlots;


  Vector3[] inPositions = new Vector3[] {
    new Vector3 (-2.8f, 4.8f, 0f),
    new Vector3 (2.8f, 4.8f, 0f),
    new Vector3 (2.8f, -4.8f, 0f),
    new Vector3 (-2.8f, -4.8f, 0f),
  };

  Vector3[] outPositions = new Vector3[] {
    new Vector3 (-2.8f, 7f, 0f),
    new Vector3 (2.8f, 7f, 0f),
    new Vector3 (2.8f, -7f, 0f),
    new Vector3 (-2.8f, -7f, 0f)
  };

  [Header("CHANGE GUN")]
  int[] gunValue = { 0, 1, 2, 3 };

  // vip
  public GameObject gunVipPanel;
  public Button[] listGunVip;
  public List<Button> listGunVipUnLock = new List<Button>();

  bool isInRoom = false;

  public Text jackpotText;
  int jackpotInGame;
  BanCa.ShootingScene Scene => BanCa.Game.Shooting;

  void Awake()
  {
    Instance = this;
    SFSManager.instance.isLobby = false;
  }
  // Use this for initialization
  void Start()
  {
    InvokeRepeating("getJackPotValue", 0, 0.5f);
    int _outPosLength = outPositions.Length;
    for (int i = 0; i < _outPosLength; i++)
    {
      Scene.Guns[i].transform.position = outPositions[i];
    }
    DisplayGunVip();
    jackpotInGame = SaveLoadData.Instance.jackpotNumberGold;
    StartCoroutine(Update_Jackpot());
  }

  // Update is called once per frame
  void Update()
  {
    if (Input.GetKeyDown(KeyCode.Escape) && isInRoom)
      PlayCoinManager.Instance.OutRoom();
  }

  public void Shoot(string username, int coin, float angle)
  {
    int _slotGun = Scene.Users[username].Position - 1;
    int _typeGun = Scene.Users[username].gunType;
    Transform gunTrans = Scene.Guns[_slotGun].transform;
    Transform _gunCurrent = gunTrans.Find("Gun").transform.GetChild(_typeGun).GetChild(0);
    if (gunTrans.position.y < 0)
    {
      _gunCurrent.localEulerAngles = new Vector3(0, 0, angle - 90);
    }
    else
    {
      _gunCurrent.localEulerAngles = new Vector3(0, 0, angle + 90);
    }
    ShootAnim(_gunCurrent.GetComponent<SkeletonGraphic>());
    Scene.Users[username].Coin.Value = coin;
    if (username == UserManager.UserName) SoundManager.Instance.ShootSound(1);
  }
  public void FakeCoinFishDie(string _id, int _coin, string _typeFish)
  {
    if (Scene.Users.ContainsKey(_id))
    {
      Scene.Users[_id].Coin.Value += _coin;
      if (_id == UserManager.UserName)
        SaveLoadData.Instance.CountOne_Mission(_typeFish);
    }

  }

  public GameObject GunUserPosition(string _id)
  {
    return Scene.Guns[Scene.Users[_id].Position - 1]
      .gameObject;
  }

  void ShootAnim(SkeletonGraphic gunSpine)
  {
    gunSpine.AnimationState.SetAnimation(0, "shot", false);
  }
  // Cập nhật thông tin của tất cả ngừoi chơi 
  public void LoadGunUser(ISFSObject obj)
  {
    JSONObject js = new JSONObject(obj.GetUtfString("users"));
    ISFSArray userArr = SFSArray.NewFromJsonData(js.GetField("list").ToString());
    foreach (ISFSObject ob in userArr)
    {
      UserInRoom u = new UserInRoom(ob);
      if (!Scene.Users.ContainsKey(u.UserName))
      {
        Scene.Users.Add(u.UserName, u);
      }
      if (u.UserName == UserManager.UserName)
      {
        UserSlots[u.Position - 1].UserGemText.gameObject
          .AddComponent<BanCa.GemText>();
        u.Coin.OnAfterChange += (oldValue, newValue) =>
          BanCa.Session.Gem.Value = newValue;
        Scene.Guns[u.Position - 1]
          .OnThisIsCurrentUserGun.Invoke();
        UserSlots[u.Position - 1].NormalAvatar.sprite = UserManager.avatarSpr;
      }
      else NetworkManager.instance.GetImage(u.AvatarLink,
        UserSlots[u.Position - 1].NormalAvatar);
      UserSlots[u.Position - 1].VIPAvatar.gameObject.SetActive(u.vip > 0);
      UserSlots[u.Position - 1].gameObject.SetActive(true);
      UserSlots[u.Position - 1].DisplayNameText.SetBoundedValue(u.DisplayName);
      UserSlots[u.Position - 1].UserGemText.FormatSpecifier = "N0";
      UserSlots[u.Position - 1].UserGemText.FormatProvider =
        BanCa.Config.MoneyFormat;
      UserSlots[u.Position - 1].UserGemText.SetBoundedValue(u.Coin);
      GunComeIn(u.UserName);
      SwitchGun(u.BulletType, u.UserName);
      Scene.Guns[Scene.Users[u.UserName].Position - 1].transform.GetChild(2).GetChild(0).GetComponent<Text>().text = Scene.CurrentRoomBulletValues
          .ElementAt(u.BulletType).ToString();

      if (ob.ContainsKey("guntype"))
      {
        int _gunType = ob.GetInt("guntype");
        switch (_gunType)
        {
          case 1:
            ChangeGun(14, u.UserName); // súng vip 1 
            break;
          case 2:
            ChangeGun(15, u.UserName); // súng vip 2
            break;
          case 3:
            ChangeGun(12, u.UserName); // súng vip 3 
            break;
          case 4:
            ChangeGun(13, u.UserName); // súng vip 4
            break;
        }
      }
    }

    isInRoom = true;
    LoadLevel.Instance.HideLoadingPanel();

  }

  // Cập nhật thông tin của người chơi mới vào phòng 
  public void AddUser(ISFSObject obj)
  {
    UserInRoom u = new UserInRoom(obj);
    if (!Scene.Users.ContainsKey(u.UserName))
    {
      Scene.Users.Add(u.UserName, u);
    }
    UserSlots[u.Position - 1].gameObject.SetActive(true);
    UserSlots[u.Position - 1].DisplayNameText.SetBoundedValue(u.DisplayName);
    UserSlots[u.Position - 1].UserGemText.SetBoundedValue(u.Coin);
    GunComeIn(u.UserName);
    NetworkManager.instance.GetImage(u.AvatarLink,
      UserSlots[u.Position - 1].NormalAvatar);
    ChangeGun(gunValue[0], u.UserName);
    Scene.Guns[u.Position - 1].transform.GetChild(2).GetChild(0).GetComponent<Text>().text = Scene.CurrentRoomBulletValues.ElementAt(0).ToString();
    UserSlots[u.Position - 1].VIPAvatar.gameObject.SetActive(u.vip > 0);
  }

  public void GunComeIn(string _id)
  {
    int slot = Scene.Users[_id].Position - 1;
    StartCoroutine(MovingGun(Scene.Guns[slot].transform, outPositions[slot], inPositions[slot]));
  }

  public void GunGoOut(string _id)
  {
    int slot = Scene.Users[_id].Position - 1;
    StartCoroutine(MovingGun(Scene.Guns[slot].transform, inPositions[slot], outPositions[slot]));
    UserSlots[slot].gameObject.SetActive(false);
  }

  IEnumerator MovingGun(Transform gunDock, Vector3 fro, Vector3 to)
  {
    float time = 0.5f;
    float currentTime = 0;

    do
    {
      gunDock.position = Vector3.Lerp(fro, to, currentTime / time);

      currentTime += Time.deltaTime;
      yield return null;
    } while (currentTime <= time);
    yield return null;

    gunDock.position = to;
  }
  // Đổi súng 
  void ChangeGun(int gunNum, string userId)
  {
    Transform gunTrans = Scene.Guns[Scene.Users[userId].Position - 1].transform.Find("Gun").transform;
    for (int i = 0; i < gunTrans.childCount; i++)
    {
      if (i == gunNum)
      {
        gunTrans.GetChild(i).gameObject.SetActive(true);
        gunTrans.GetChild(i).GetChild(0).GetComponent<SkeletonGraphic>().AnimationState.SetAnimation(0, "idle", false);

      }
      else
      {
        gunTrans.GetChild(i).gameObject.SetActive(false);
      }
    }
    Scene.Users[userId].gunType = gunNum;
  }
  public void NextGun()
  {

    Scene.CurrentGunType++;
    if (Scene.CurrentGunType > gunValue.Length - 1)
    {
      Scene.CurrentGunType = 0;
    }
    ChangeGunExtendsion(Scene.CurrentGunType);
  }
  public void PreGun()
  {
    Scene.CurrentGunType--;
    if (Scene.CurrentGunType < 0)
    {
      Scene.CurrentGunType = gunValue.Length - 1;
    }
    ChangeGunExtendsion(Scene.CurrentGunType);
  }



  public void SetGun(ISFSObject obj)
  {

    int _gunCurr = obj.GetInt("bullettype");
    string _uid = obj.GetUtfString("username");
    if (!Scene.Users.ContainsKey(_uid))
      return;
    int _slotGun = Scene.Users[_uid].Position - 1;
    Scene.Guns[_slotGun].transform.GetChild(4).gameObject.SetActive(false);
    Scene.Guns[_slotGun].transform.GetChild(4).gameObject.SetActive(true);
    Scene.Guns[_slotGun].transform.GetChild(2).GetChild(0).GetComponent<Text>().text = Scene.CurrentRoomBulletValues.ElementAt(_gunCurr).ToString();
    Scene.Users[_uid].gunVipCurrent = 0;
    Scene.Users[_uid].BulletType = _gunCurr;

    int _gunType = obj.GetInt("guntype");
    if (_gunType != 0)
    {

      Scene.Users[_uid].gunVipCurrent = _gunType;
      if (_uid == UserManager.UserName)
      {
        int _countGunVipUnlock = listGunVipUnLock.Count;
        for (int i = 0; i < _countGunVipUnlock; i++)
        {
          if (i == _gunType - 1)
          {
            listGunVipUnLock[i].transform.GetChild(0).gameObject.SetActive(true);
          }
          else
          {
            listGunVipUnLock[i].transform.GetChild(0).gameObject.SetActive(false);
          }
        }
      }
      switch (_gunType)
      {
        case 1:
          ChangeGun(14, _uid); // súng vip 1 
          break;
        case 2:
          ChangeGun(15, _uid); // súng vip 2
          break;
        case 3:
          ChangeGun(12, _uid); // súng vip 3 
          break;
        case 4:
          ChangeGun(13, _uid); // súng vip 4 
          break;
      }
      gunVipPanel.SetActive(false);
      Loading.instance.Hide();
    }
    else
    {
      if (Scene.Users[_uid].gunVipCurrent == 0)
      {
        int _gunValue = Scene.Users[_uid].BulletType;
        ChangeGun(gunValue[_gunValue], _uid);
      }
      Loading.instance.Hide();
    }
  }


  void SwitchGun(int _gunCurr, string _uid)
  {
    switch (_gunCurr)
    {
      case 0:
        ChangeGun(gunValue[0], _uid);
        break;
      case 1:
        ChangeGun(gunValue[1], _uid);

        break;
      case 2:
        ChangeGun(gunValue[2], _uid);
        break;

    }
  }

  public void ChangeGunExtendsion(int _type)
  {
    if (Scene.Users[UserManager.UserName].gunVipCurrent == 0)
      SFSManager.instance.USER_CHANGE_GUN(_type, 0);
    else SFSManager.instance.USER_CHANGE_GUN(_type,
      Scene.Users[UserManager.UserName].gunVipCurrent);
  }

  public void ChangeGunVipOFF()
  {
    Loading.instance.Show();
    listGunVipUnLock[Scene.Users[UserManager.UserName].gunVipCurrent - 1].transform.GetChild(0).gameObject.SetActive(false);
    Scene.Users[UserManager.UserName].gunVipCurrent = 0;
    ChangeGunExtendsion(Scene.CurrentGunType);
    gunVipPanel.SetActive(false);

  }
  // Vip
  public void UnLockGunVip()
  {
    if (BanCa.Session.CurrentVIPLevel.Value >= 6)
    {
      for (int i = 0; i < listGunVip.Length; i++)
      {
        listGunVipUnLock.Add(listGunVip[i]);
      }
      return;
    }
    else if (BanCa.Session.CurrentVIPLevel.Value >= 4)
    {
      for (int i = 0; i < listGunVip.Length - 1; i++)
      {
        listGunVipUnLock.Add(listGunVip[i]);
      }
      return;
    }
    else if (BanCa.Session.CurrentVIPLevel.Value >= 3)
    {
      for (int i = 0; i < listGunVip.Length - 2; i++)
      {
        listGunVipUnLock.Add(listGunVip[i]);
      }
      return;
    }
    else if (BanCa.Session.CurrentVIPLevel.Value >= 2)
    {
      listGunVipUnLock.Add(listGunVip[0]);
      return;
    }
  }
  public void DisplayGunVip()
  {
    UnLockGunVip();
    for (int i = 0; i < listGunVipUnLock.Count; i++)
    {
      listGunVipUnLock[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Images/vip/equip");
      listGunVipUnLock[i].enabled = true;
    }
  }

  public void OpenChooseGunVip()
  {
    gunVipPanel.SetActive(true);
  }

  public void BuyGoldGemComplete(int _gem)
  {
    if (Scene.Users.ContainsKey(UserManager.UserName))
      Scene.Users[UserManager.UserName].Coin.Value = _gem;
  }
  //	StartCoroutine (Update_Jackpot ());
  IEnumerator Update_Jackpot()
  {
    float elapsedTime = 0;
    float seconds = 2f;
    int _jackpotValue = SaveLoadData.Instance.jackpotNumberGold;
    do
    {
      jackpotText.text = SaveLoadData.FormatMoney((int)Mathf.Floor(Mathf.Lerp(0, _jackpotValue, (elapsedTime / seconds))));
      elapsedTime += Time.deltaTime;
      yield return null;
    } while (elapsedTime < seconds);
    jackpotText.text = SaveLoadData.FormatMoney(_jackpotValue);
  }
  public void GetJackPot(ISFSObject obj)
  {
    double NewValue = obj.GetDouble("jackpot_gold");
    int OldValue = jackpotInGame;
    StartCoroutine(updateJackPot(OldValue, Mathf.FloorToInt((float)NewValue)));
  }
  IEnumerator updateJackPot(int firstValue, int secondValue)
  {
    float elapsedTime = 0;
    float seconds = 0.4f;
    do
    {
      jackpotText.text = SaveLoadData.FormatMoney((int)Mathf.Floor(Mathf.Lerp(firstValue, secondValue, (elapsedTime / seconds))));
      elapsedTime += Time.deltaTime;
      yield return null;
    } while (elapsedTime < seconds);
    jackpotInGame = secondValue;
    jackpotText.text = SaveLoadData.FormatMoney(secondValue);
    SaveLoadData.Instance.jackpotNumberGold = secondValue;
  }
  void getJackPotValue()
  {
    SFSManager.instance.GetGemJackPot();
  }
}
