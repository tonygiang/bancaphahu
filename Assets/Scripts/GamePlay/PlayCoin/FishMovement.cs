﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using UnityEngine.UI;
using Spine.Unity;
using System.Linq;
using Utils;

public class FishMovement : MonoBehaviour
{


  public string id, type;
  public string path;
  public FishPath fishPath;
  public int frameLife;
  public int directorIndex;

  public float speed, hitRadious, posX, posY;
  public int currentAngle, nextAngle;
  public Vector3 direction, directionLag;
  public int mutiplie;
  public string color = "none";
  public float bonus = 0;

  public SkeletonAnimation ske = null;
  [SerializeField]
  string[] skinNames;

  WaitForSeconds mWait = new WaitForSeconds(0.05f);
  public static float SpeedMultiplier = 1f;

  IEnumerator TimeDelay()
  {
    int _lagAngle = ((int)SFSManager.instance.clientServerLag) / 20;
    int _lagDu = ((int)SFSManager.instance.clientServerLag) % 20;

    for (int i = 1; i <= _lagAngle; i++)
    {
      if (nextAngle > currentAngle)
      {
        currentAngle += 1;
      }
      else if (nextAngle < currentAngle)
      {
        currentAngle -= 1;
      }
      direction = new Vector3((speed + bonus) * Mathf.Cos(Mathf.Deg2Rad * currentAngle), (speed + bonus) * Mathf.Sin(Mathf.Deg2Rad * currentAngle), 0);

      transform.localPosition += direction;
      yield return Utils.Unity.Constants.WaitForFixedUpdate;
    }
    directionLag = direction * _lagDu / 20f;
    transform.localPosition += directionLag;

  }

  public bool CheckHit(BulletMovement _bullet)
  {
    Vector3 current_position = transform.position;
    int position = _bullet.gunPos;
    if (position == 1 && current_position.y > 3.3f && current_position.x > -4.3f && current_position.x < -1.3f)
    {
      return false;
    }
    else if (position == 2 && current_position.y > 3.3f && current_position.x > 1.3f && current_position.x < 4.3f)
    {
      return false;
    }
    else if (position == 3 && current_position.y < -3.3f && current_position.x > 1.3f && current_position.x < 4.3f)
    {
      return false;
    }
    else if (position == 4 && current_position.y < -3.3f && current_position.x > -4.3f && current_position.x < -1.3f)
    {
      return false;
    }

    bool result = false;
    float d_x_max = current_position.x + hitRadious;
    float d_y_max = current_position.y + hitRadious;

    float d_x_min = current_position.x - hitRadious;
    float d_y_min = current_position.y - hitRadious;

    float b_X = _bullet.transform.position.x;
    float b_Y = _bullet.transform.position.y;

    if (d_x_max > b_X && b_X > d_x_min)
      if (d_y_max > b_Y && b_Y > d_y_min) result = true;
    return result;

  }

  public void FishDie()
  {
    transform.GetChild(1).gameObject.SetActive(true);
    transform.SetParent(null);
  }



  public void ChangeTimeScale(float _timescalse)
  {
    ske.timeScale = _timescalse;
  }

  public void ChangeSpeed(float _value)
  {
    bonus = _value;
  }

  // GET ALL BIRD
  public void All_Bird(AllFish _allfish)
  {
    id = _allfish.id;
    type = _allfish.type;
    path = _allfish.path;
    int _frameCount = _allfish.frame;
    frameLife = 0;
    directorIndex = 0;
    fishPath = SaveLoadData.Instance.ListFishPath[path];
    currentAngle = fishPath.initAngel;
    nextAngle = currentAngle;
    posX = _allfish.posX;
    posY = _allfish.posY;
    bonus = _allfish.speedbonus;
    speed = _allfish.speed;
    transform.GetChild(1).gameObject.SetActive(false);
    //		speed=SaveLoadData.Instance.listFishInfo [type].Speed;
    mutiplie = SaveLoadData.Instance.listFishInfo[type].Mutiplie;
    hitRadious = SaveLoadData.Instance.listFishInfo[type].Radius;
    transform.localPosition = new Vector3(posX, posY, 0);
    transform.localEulerAngles = new Vector3(0, 0, currentAngle);
    transform.localScale = Vector3.one;
    direction = new Vector3(speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle), 0);

    ske.timeScale = 1f;
    for (int i = 0; i < _frameCount; i++)
    {
      FishMove();
    }
    StartCoroutine(TimeDelay());
    InitColor();
  }

  // ADD BIRD
  public void Add_Bird(string _id, string _type, string _color, string _path, string _init, float _speedbonus, float _speed)
  {
    id = _id;
    type = _type;
    color = _color;
    path = _path;
    bonus = _speedbonus;
    string[] posXY = _init.Split(new char[] { '_' });
    posX = float.Parse(posXY[0]);
    posY = float.Parse(posXY[1]);
    frameLife = 0;
    directorIndex = 0;
    fishPath = SaveLoadData.Instance.ListFishPath[path];
    currentAngle = fishPath.initAngel;
    nextAngle = currentAngle;
    transform.GetChild(1).gameObject.SetActive(false);
    speed = _speed;

    mutiplie = SaveLoadData.Instance.listFishInfo[type].Mutiplie;
    hitRadious = SaveLoadData.Instance.listFishInfo[type].Radius;
    transform.localPosition = new Vector3(posX, posY, 0);
    transform.localEulerAngles = new Vector3(0, 0, currentAngle);
    transform.localScale = Vector3.one;
    direction = new Vector3(speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle), 0);
    bonus = 0;
    ske.timeScale = 1f;
    StartCoroutine(TimeDelay());
    InitColor();

  }
  // MOVEMENT
  public void FishMove()
  {

    CheckRotation();
    if (currentAngle < nextAngle)
    {
      currentAngle += 1;
    }
    else if (currentAngle > nextAngle)
    {
      currentAngle -= 1;
    }

    direction = new Vector3((speed + bonus) * Mathf.Cos(Mathf.Deg2Rad * currentAngle), (speed + bonus) * Mathf.Sin(Mathf.Deg2Rad * currentAngle), 0);
    transform.localEulerAngles = new Vector3(0, 0, currentAngle);
    transform.localPosition += direction * SpeedMultiplier;

    if (bonus > 0)
    {
      bonus -= 0.001f;
      if (bonus < 0)
      {
        bonus = 0;
      }
    }

  }
  public void CheckRotation()
  {

    frameLife++;
    if (fishPath.Frame.Size() > directorIndex)
    {
      if (frameLife == fishPath.GetX(directorIndex))
      {
        nextAngle = fishPath.GetY(directorIndex);
        directorIndex++;
      }
    }

  }

  public void InitColor()
  {
    if (color == "none")
    {
      InitSpawn();
    }
    else
    {
      if (ske != null)
      {
        ske.skeleton.SetSkin(color);
      }
    }
  }

  public void InitSpawn()
  {
    StopAllCoroutines();
    if (ske != null && skinNames.Length > 1)
      ske.skeleton.SetSkin(skinNames[Random.Range(0, skinNames.Length)]);

    if (name.Equals("rua"))
    {
      if (Random.value > 0.5) ske.skeleton.SetSkin("1");
      else ske.skeleton.SetSkin("2");
    }
    if (name.Equals("jackpot"))
    {
      if (Random.value > 0.5) ske.skeleton.SetSkin("tien tim");
      else ske.skeleton.SetSkin("tien xanh la");
    }
    if (name.Equals("tienca"))
    {
      if (Random.value > 0.5) ske.skeleton.SetSkin("kim cuong");
      else ske.skeleton.SetSkin("vang");
    }
  }

  IEnumerator ChangeAnim(float waitTime)
  {
    while (gameObject.activeSelf)
    {
      yield return new WaitForSeconds(waitTime);
      ske.AnimationState.SetAnimation(0, "special", false);
      yield return new WaitForSpineAnimationComplete(ske.AnimationState.GetCurrent(0));
      ske.AnimationState.SetAnimation(0, "swim", true);
    }
  }

  IEnumerator HitAnim()
  {
    ske.skeleton.SetColor(Color.red);
    yield return mWait;
    ske.skeleton.SetColor(Color.white);
  }

  BanCa.ShootingScene Scene => BanCa.Game.Shooting;

  void OnEnable()
  {
    if (ske != null) if (ske.skeleton != null)
        ske.skeleton.SetColor(Scene.NormalFishColor);
  }

  void OnDisable()
  {
    Scene.Events.OnFishDie.Invoke(id);
  }

  void OnDestroy()
  {
    Scene.Events.OnFishDie.Invoke(id);
  }

  public void SendTarget()
  {
    SFSObject parameters = new SFSObject();
    parameters.PutUtfString("username", UserManager.UserName);
    parameters.PutUtfString("fishId", id);
    parameters.PutUtfString("bullettype",
      Scene.CurrentRoomBulletTypes
        .ElementAt(Scene.Users[UserManager.UserName].BulletType));
    parameters.PutInt("bulletCost",
      Scene.CurrentRoomBulletValues.ElementAt(Scene.CurrentGunType));
    SFSManager.sfs.Send(new ExtensionRequest("chooseTarget",
      parameters,
      NetworkManager.instance.LobbyRoom));
    Scene.Events.OnPickTargetModeEnd.Invoke(id);
  }
}
