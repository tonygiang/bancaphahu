﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOfBird : MonoBehaviour {

	public Transform bird;
	float birdAngle;
	Vector3 rotaY = new Vector3(1,-1,0);
	Vector3 rotaDefault = Vector3.one;
//	void Start(){
//
//	}

	void LateUpdate () {
		birdAngle = transform.eulerAngles.z;
		if (birdAngle < 90 || birdAngle>270) {
//			bird.localEulerAngles = new Vector3 (0, 0, -birdAngle);
			bird.localScale = rotaDefault;
		} else {
			bird.localScale = rotaY;
		}
	}
}
