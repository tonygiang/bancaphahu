﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{

  public string id, type;
  public float speed, currentAngle, posX, posY;
  public int cost;
  public Vector3 direction, directionLag;
  public int gunPos;

  public string uname;


  public void Init(int clientServerLag, int _gunPos)
  {
    gunPos = _gunPos;
    transform.position = new Vector3(posX, posY, 0);
    speed = SaveLoadData.Instance.listBulletInfo[type].Speed;
    cost = SaveLoadData.Instance.listBulletInfo[type].Cost;


    transform.localEulerAngles = new Vector3(0, 0, currentAngle);
    direction = new Vector3(speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle));

    directionLag = direction * clientServerLag / 20;
    transform.position += directionLag;

  }

  public void InitTut(string _id, string _type, float _curAngle, int _gunPos, float _posX, float _posY)
  {
    id = _id;
    type = _type;
    gunPos = _gunPos;
    currentAngle = _curAngle;
    transform.position = new Vector3(_posX, _posY, 0);
    speed = SaveLoadData.Instance.listBulletInfo[type].Speed;
    cost = SaveLoadData.Instance.listBulletInfo[type].Cost;
    transform.localEulerAngles = new Vector3(0, 0, currentAngle);
    direction = new Vector3(speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle));

  }
  public void InitArena(int clientServerLag, int _gunPos)
  {
    gunPos = _gunPos;
    transform.position = new Vector3(posX, posY, 0);
    speed = SaveLoadData.Instance.listBulletInfo[type].Speed;
    cost = SaveLoadData.Instance.listBulletInfo[type].Cost;


    transform.localEulerAngles = new Vector3(0, 0, currentAngle);
    direction = new Vector3(speed * Mathf.Cos(Mathf.Deg2Rad * currentAngle), speed * Mathf.Sin(Mathf.Deg2Rad * currentAngle));

    directionLag = direction * clientServerLag / 20;
    transform.position += directionLag;

  }

  public void BulletMove()
  {
    transform.position += direction;

  }




}
