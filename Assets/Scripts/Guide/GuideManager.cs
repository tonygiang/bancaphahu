﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuideManager : MonoBehaviour {
	public static GuideManager Instance;
	public GameObject guidePanel;
	public Image[] listTabBtn;
	public Sprite spriteTabDefault, spriteTabActive;
	public GameObject[] listContent;

	[SerializeField]
	Transform fishContainer;

	bool isLoad=false;
	void Awake(){
		Instance = this;
	}
	void LoadFish(){
		Dictionary<string,FishInfo> _newFish = SaveLoadData.Instance.listFishInfo;
		foreach (Transform _fi in fishContainer) {
			if (_newFish.ContainsKey (_fi.name)) {
				_fi.GetChild (1).GetComponent<Text> ().text = "x" + _newFish [_fi.name].Mutiplie.ToString ();// Muti
				_fi.GetChild (2).GetComponent<Text> ().text = _newFish [_fi.name].Name;// Name
			}
		}
		isLoad = true;
	}
	public void PickTab(int value){
		for (int i = 0; i < listTabBtn.Length; i++) {
			if (i == value) {
				listTabBtn [i].GetComponent<Button> ().enabled = false;
				listTabBtn [i].transform.GetChild (0).GetComponent<Text> ().color = new Color(1f,1f,1f,1f);
				listTabBtn [i].transform.GetChild (0).GetComponent<Outline> ().enabled = true;
				listTabBtn [i].sprite = spriteTabActive ;
				//listTabBtn [i].GetComponent<RectTransform> ().anchoredPosition = new Vector3 (67f, listTabBtn [i].GetComponent<RectTransform> ().anchoredPosition.y, 0);
				listContent[i].SetActive(true);
			} else {
				listTabBtn [i].GetComponent<Button> ().enabled = true;
				listTabBtn [i].transform.GetChild (0).GetComponent<Text> ().color = new Color(1f,1f,1f,0.5f);
				listTabBtn [i].transform.GetChild (0).GetComponent<Outline> ().enabled = false;
				listTabBtn [i].sprite = spriteTabDefault ;
				//listTabBtn [i].GetComponent<RectTransform> ().anchoredPosition = new Vector3 (17f, listTabBtn [i].GetComponent<RectTransform> ().anchoredPosition.y, 0);
				listContent[i].SetActive(false);
			}
		}
	}

	public void OpenPanel(){
		guidePanel.SetActive (true);
		guidePanel.transform.GetChild (1).GetComponent<Button> ().onClick.AddListener (() => {
			guidePanel.SetActive (false);
		});
		PickTab (3);
		if (!isLoad) {
			LoadFish ();
		}

	}
}
