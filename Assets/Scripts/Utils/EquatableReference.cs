﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EquatableReference<T> : IEquatable<T> where T : class, new()
{
  public bool Equals(T other) => this == other;
  static T _default = new T();
  public static T Default => _default;
}
