using System.Collections.Generic;
using System;
using System.Collections;
using Sfs2X.Entities.Data;
using System.Linq;

namespace Utils
{
  public static class Algorithm
  {
    public static R CastTo<R>(this IConvertible source) =>
      (R)Convert.ChangeType(source, typeof(R));

    public static R As<R>(this System.Object source) => (R)source;

    public static bool HasContent(this string source) =>
      !String.IsNullOrWhiteSpace(source);

    public static int ToInt(this string str) => int.Parse(str);

    public static bool InRange<T>(this T source, T minValue, T maxValue)
      where T : IComparable =>
      source.CompareTo(minValue) >= 0 && source.CompareTo(maxValue) <= 0;

    public static T ClampTo<T>(this T source, T minValue, T maxValue)
      where T : IComparable => source.CompareTo(minValue) < 0 ? minValue :
      source.CompareTo(maxValue) > 0 ? maxValue :
      source;

    public static (T1, T2) MakeTuple<T1, T2>(this T1 item1, T2 item2) =>
      (item1, item2);

    public static (T1, T2, T3) MakeTuple<T1, T2, T3>(this T1 item1,
      T2 item2,
      T3 item3) =>
      (item1, item2, item3);

    public static (T1, T2) MakeTupleAppend<T1, T2>(this T2 item2, T1 item1) =>
      (item1, item2);

    public static T PassTo<T>(this T argument, Action<T> action)
    {
      action(argument);
      return argument;
    }

    public static (T1, T2) PassTo<T1, T2>(this (T1, T2) arguments,
      Action<T1, T2> action)
    {
      action(arguments.Item1, arguments.Item2);
      return arguments;
    }

    public static (T1, T2, T3) PassTo<T1, T2, T3>(this (T1, T2, T3) arguments,
      Action<T1, T2, T3> action)
    {
      action(arguments.Item1, arguments.Item2, arguments.Item3);
      return arguments;
    }

    public static R PassTo<T, R>(this T argument, Func<T, R> function) =>
      function(argument);

    public static R PassTo<T1, T2, R>(this (T1, T2) arguments,
      Func<T1, T2, R> function) =>
      function(arguments.Item1, arguments.Item2);

    public static IEnumerable<T> ForEach<T>(this IEnumerable<T> source,
      Action<T> action)
    {
      foreach (T element in source) action(element);
      return source;
    }

    public static R ConvertBy<T, R>(this T source, Func<T, R> converter) =>
      converter(source);

    public static Nullable<T> NullIf<T>(this T source, bool condition)
      where T : struct =>
      condition ? null : new Nullable<T>(source);

    public static Nullable<T> NullIf<T>(this T source, Predicate<T> condition)
      where T : struct =>
      condition(source) ? null : new Nullable<T>(source);

    public static IDictionary<TKey, TValue> Replace<TKey, TValue>(
      this IDictionary<TKey, TValue> source,
      TKey key,
      TValue value,
      Action<TValue> actionForExistingValue = null)
    {
      if (source.ContainsKey(key)) actionForExistingValue?.Invoke(source[key]);
      source[key] = value;
      return source;
    }

    public static TValue ElementAtOrFillWith<TKey, TValue>(
      this IDictionary<TKey, TValue> source,
      TKey key,
      TValue customDefault)
    {
      if (!source.ContainsKey(key)) source[key] = customDefault;
      return source[key];
    }

    public static TSource FirstOrValue<TSource>(
      this IEnumerable<TSource> source,
      TSource value) where TSource : class
    {
      var firstFound = source.FirstOrDefault();
      if (firstFound == null) return firstFound;
      return value;
    }

    public static TSource FirstOrValue<TSource>(
      this IEnumerable<TSource> source,
      Func<TSource, bool> predicate,
      TSource value) where TSource : class
    {
      var firstFound = source.FirstOrDefault(predicate);
      if (firstFound == null) return firstFound;
      return value;
    }
  }

  public static class Algorithm2
  {
    public static T NullIf<T>(this T source, bool condition)
      where T : class =>
      condition ? null : source;

    public static T NullIf<T>(this T source, Predicate<T> condition)
      where T : class =>
      condition(source) ? null : source;
  }
}