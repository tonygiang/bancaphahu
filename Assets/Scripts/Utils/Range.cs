﻿using System;

public struct Range<T> where T : IComparable
{
  public T Min;
  public T Max;

  public Range(T min, T max) { Min = min; Max = max; }

  public override string ToString() => $"(Min:{Min}, Max:{Max})";
}
