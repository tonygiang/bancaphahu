﻿using System;

public class Observable<T> where T : IEquatable<T>
{
  protected T _value = default(T);
  public virtual T Value
  {
    get => _value;
    set
    {
      T oldValue = _value;
      bool hasChange = !oldValue.Equals(value);
      if (hasChange) OnBeforeChange(oldValue, value);
      OnBeforeSet(_value);
      _value = value;
      OnAfterSet(_value);
      if (hasChange) OnAfterChange(oldValue, value);
    }
  }

  public event Action<T, T> OnBeforeChange = delegate { };
  public event Action<T, T> OnAfterChange = delegate { };
  public event Action<T> OnBeforeSet = delegate { };
  public event Action<T> OnAfterSet = delegate { };

  public Observable(T initialValue) => _value = initialValue;
}