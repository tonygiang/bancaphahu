﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using Sfs2X.Entities.Data;

[Serializable] public class UnityEventString : UnityEvent<string> { }

[Serializable] public class UnityEventBool : UnityEvent<bool> { }

[Serializable] public class UnityEventInt : UnityEvent<int> { }

[Serializable] public class UnityEventFloat : UnityEvent<float> { }

[Serializable] public class UnityEventSFS : UnityEvent<ISFSObject> { }

[Serializable]
public class UnityEventOnlineTime : UnityEvent<BanCa.OnlineTimeMilestone> { }

[Serializable]
public class UnityEventSkillTrigger : UnityEvent<BanCa.SkillTrigger> { }