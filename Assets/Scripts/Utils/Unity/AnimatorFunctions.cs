﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorFunctions : ExtensionBehaviour<Animator>
{
  public UnityEvent OnAnimationEnd = new UnityEvent();

  public void InvokeOnAnimationEnd() => OnAnimationEnd.Invoke();
}
