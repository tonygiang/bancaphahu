﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientsideValidator<T> : ExtensionBehaviour<T>
  where T : MonoBehaviour
{
  [SerializeField] [Multiline] string _failureMessage = string.Empty;
  public string FailureMessage => _failureMessage;

  public virtual bool Validate(T component) => false;
}
