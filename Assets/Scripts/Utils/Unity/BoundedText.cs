﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;
using Utils;

public class BoundedText<T> : ExtensionBehaviour<Text>
  where T : IEquatable<T>
{
  protected Observable<T> _boundedValue = null;
  public Observable<T> BoundedValue
  {
    get => _boundedValue;
    set
    {
      if (_boundedValue == value) return;

      _boundedValue?.PassTo(v => v.OnAfterChange -= UpdateText);
      _boundedValue = value;
      _boundedValue?.PassTo(v =>
      {
        UpdateText(v.Value, v.Value);
        v.OnAfterChange += UpdateText;
      });
    }
  }

  void OnDestroy() => BoundedValue?.PassTo(v => v.OnAfterChange -= UpdateText);

  public BoundedText<T> SetBoundedValue(Observable<T> newBoundedValue)
  {
    ExecuteOnBaseComp(() => BoundedValue = newBoundedValue);
    return this;
  }

  protected virtual void UpdateText(T oldValue, T newValue) =>
    BaseComp.text = newValue.ToString();
}

public class BoundedTextFormattable<T> : BoundedText<T>
  where T : IEquatable<T>, IFormattable
{
  public string FormatSpecifier = string.Empty;
  public IFormatProvider FormatProvider = CultureInfo.InvariantCulture;

  protected override void UpdateText(T oldValue, T newValue) =>
    BaseComp.text = newValue.ToString(FormatSpecifier, FormatProvider);
}