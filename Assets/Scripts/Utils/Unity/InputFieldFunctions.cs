﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
[RequireComponent(typeof(InputField))]
public class InputFieldFunctions : ExtensionBehaviour<InputField>
{
  public void SaveContentToPlayerPrefs(string key) =>
    PlayerPrefs.SetString(key, BaseComp.text);

  public void CopyContentToInputField(InputField targetField) =>
    targetField.text = BaseComp.text;
}
