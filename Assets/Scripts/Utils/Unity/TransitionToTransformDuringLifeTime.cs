﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionToTransformDuringLifeTime : MonoBehaviour
{
  public Transform DestinationTransform = null;
  public float TransitionSeconds = 0f;
  Vector3 _startingPosition = Vector3.zero;
  void OnEnable()
  {
    _startingPosition = transform.position;
    StartCoroutine(Transition());
  }

  IEnumerator Transition()
  {
    var wait = new WaitForFixedUpdate();
    for (float elaspedSeconds = 0f;
      elaspedSeconds < TransitionSeconds;
      elaspedSeconds += Time.fixedDeltaTime)
    {
      yield return wait;
      gameObject.transform.position = Vector3.Lerp(
        gameObject.transform.position,
        DestinationTransform.position,
        elaspedSeconds / TransitionSeconds
      );
    }
    gameObject.transform.position = DestinationTransform.position;
    enabled = false;
    yield break;
  }

  public void SetDestination(Transform newDestination) =>
    DestinationTransform = newDestination;
}
