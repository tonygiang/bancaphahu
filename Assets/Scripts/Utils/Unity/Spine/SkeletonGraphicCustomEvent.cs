﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using Utils;

public class SkeletonGraphicCustomEvent : ExtensionBehaviour<SkeletonGraphic>
{
  [SerializeField] string CustomEventName = string.Empty;
  [SerializeField]
  UnityEventTrackEntry ActionsOnEvent = new UnityEventTrackEntry();
  void OnEnable() => BaseComp.AnimationState.Event += InvokeEvent;

  void OnDisable() => BaseComp.AnimationState.Event -= InvokeEvent;

  void InvokeEvent(TrackEntry entry, Spine.Event e) => ActionsOnEvent
    .NullIf(e.data.name != CustomEventName)?
    .Invoke(entry);
}
