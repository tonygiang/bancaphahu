﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using System;
using UnityEngine.Events;

public class SingleAnimationSkeletonGraphic :
  ExtensionBehaviour<SkeletonGraphic>
{
  [SerializeField] int OnEnableTrackIndex = 0;
  [SerializeField] string OnEnableAnimaton = string.Empty;
  [SerializeField] bool OnEnableLoop = false;
  public UnityEventTrackEntry OnAnimationComplete = new UnityEventTrackEntry();
  public UnityEventTrackEntry OnAnimationEnd = new UnityEventTrackEntry();

  override protected void Awake()
  {
    base.Awake();
    BaseComp.AnimationState.Complete += OnAnimationComplete.Invoke;
    BaseComp.AnimationState.End += OnAnimationEnd.Invoke;
  }

  void OnEnable() => BaseComp.AnimationState.SetAnimation(OnEnableTrackIndex,
    OnEnableAnimaton,
    OnEnableLoop);

  void DisableSelf(TrackEntry trackEntry) => gameObject.SetActive(false);
}

[Serializable] public class UnityEventTrackEntry : UnityEvent<TrackEntry> { }