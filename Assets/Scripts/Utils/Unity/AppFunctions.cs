﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppFunctions : MonoBehaviour
{
  public void SetLastSceneName(string lastSceneName) =>
    AppInfo.LastSceneName = lastSceneName;

  public void OpenURL(string url) => Application.OpenURL(url);
}
