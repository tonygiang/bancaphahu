﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ExtensionBehaviour<T> : MonoBehaviour where T : Component
{
  T _baseComp = null;
  [HideInInspector] public T BaseComp => _baseComp;
  Action OnAwake = () => { };

  protected virtual void Awake()
  {
    _baseComp = GetComponent<T>();
    OnAwake();
  }

  protected void ExecuteOnBaseComp(Action action)
  {
    if (BaseComp == null) OnAwake += action;
    else action();
  }
}
