﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Platform
{
  DEFAULT,
  ANDROID,
  IOS,
  WEBGL
}
