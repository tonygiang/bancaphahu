﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
[RequireComponent(typeof(InputField))]
public class ToLowerInputFieldOnEndEdit : ExtensionBehaviour<InputField>
{
  void OnEnable() => BaseComp.onEndEdit.AddListener(ToLower);

  void OnDisable() => BaseComp.onEndEdit.RemoveListener(ToLower);

  void ToLower(string text) => BaseComp.text = text.ToLower();
}
