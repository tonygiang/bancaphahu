﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadContentFromPlayerPrefsOnStart : ExtensionBehaviour<InputField>
{
  [SerializeField] string KeyToLoad = string.Empty;

  void Start()
  {
    BaseComp.text = PlayerPrefs.HasKey(KeyToLoad) ?
      PlayerPrefs.GetString(KeyToLoad) : BaseComp.text;
  }
}
