﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
[RequireComponent(typeof(InputField))]
public class TrimInputFieldOnEndEdit : ExtensionBehaviour<InputField>
{
  void OnEnable() => BaseComp.onEndEdit.AddListener(Trim);

  void OnDisable() => BaseComp.onEndEdit.RemoveListener(Trim);

  void Trim(string text) => BaseComp.text = text.Trim();
}
