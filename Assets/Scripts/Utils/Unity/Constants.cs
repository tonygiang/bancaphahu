﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.Unity
{
  public static class Constants
  {
    public static WaitForFixedUpdate WaitForFixedUpdate =
      new WaitForFixedUpdate();
  }
}
