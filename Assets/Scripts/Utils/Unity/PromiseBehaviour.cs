﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class PromiseBehaviour<ErrorT, ReturnT> : MonoBehaviour
{
  protected Action<ErrorT> ErrorEvent = error => { };
  protected Action<ReturnT> ThenEvent = result => { };

  void Start() => StartCoroutine(Wait());

  protected abstract IEnumerator Wait();

  public PromiseBehaviour<ErrorT, ReturnT> IfError(Action<ErrorT> errorResolver)
  {
    ErrorEvent += errorResolver;
    return this;
  }

  public PromiseBehaviour<ErrorT, ReturnT> Then(
    Action<ReturnT> responseConsumer)
  {
    ThenEvent += responseConsumer;
    return this;
  }
}
