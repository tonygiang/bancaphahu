﻿using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using System.Collections.Generic;
using Sfs2X.Entities.Data;

namespace Utils
{
  public static partial class UnityFunctions
  {
    public static GameObject SetName(this GameObject source, string name)
    {
      source.name = name;
      return source;
    }

    public static ResolveUnityWebRequest Resolve(this UnityWebRequest source,
      string nameForResolver = "WebRequest Resolver") =>
      new GameObject().SetName(nameForResolver)
        .AddComponent<ResolveUnityWebRequest>()
        .SetRequest(source);

    public static (Range<float> x, Range<float> y) GetRanges(
      this RectTransform source)
    {
      var corners = new Vector3[4];
      source.GetWorldCorners(corners);
      return (x: new Range<float>(corners[0].x, corners[2].x),
        y: new Range<float>(corners[0].y, corners[2].y));
    }

    public static (Range<float> x, Range<float> y) GetMovementRangeOf(
      this (Range<float> x, Range<float> y) source,
      RectTransform containedTransform)
    {
      var movementRanges = source;
      movementRanges.x.Min += containedTransform.rect.width / 2;
      movementRanges.x.Max -= containedTransform.rect.width / 2;
      movementRanges.y.Min += containedTransform.rect.height / 2;
      movementRanges.y.Max -= containedTransform.rect.height / 2;
      return movementRanges;
    }

    public static float ClampTo(this float source, Range<float> range) =>
      Mathf.Clamp(source, range.Min, range.Max);

    public static Vector3 To(this Vector3 source, Vector3 target) =>
      target - source;

    public static Vector2 To(this Vector2 source, Vector2 target) =>
      target - source;

    public static Vector2 ToVector2(this Vector3 source) =>
      new Vector2(source.x, source.y);

    public static float SignedAngleTo(this Vector2 from, Vector2 to) =>
      Vector2.SignedAngle(from, to);

    public static float SignedAngleTo(this Vector3 from, Vector2 to) =>
      Vector2.SignedAngle(from, to);

    public static Quaternion SetEulerZ(this Quaternion source, float z) =>
      Quaternion.Euler(source.eulerAngles.x, source.eulerAngles.y, z);
  }

  public static partial class SFSFunction
  {
    public static IDictionary<string, TValue> ToDictionary<TValue>(
      this ISFSObject source) => source.GetKeys()
      .ToDictionary(k => k, k => (TValue)source.GetClass(k));

    public static bool HasKey(this ISFSObject source, string key) =>
      source.GetKeys().Contains(key);
  }
}