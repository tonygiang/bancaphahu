﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFunctions : ExtensionBehaviour<Image>
{
  public void DecreaseFillAmountOverTime(float seconds) =>
    StartCoroutine(ChangeFillAmount(seconds, false));

  public void InreaseFillAmountOverTime(float seconds) =>
    StartCoroutine(ChangeFillAmount(seconds, true));

  IEnumerator ChangeFillAmount(float seconds, bool increaseFill)
  {
    for (float elapsedSeconds = 0f;
      elapsedSeconds < seconds;
      elapsedSeconds += Time.fixedDeltaTime)
    {
      var fraction = elapsedSeconds / seconds;
      BaseComp.fillAmount = increaseFill ? fraction : 1 - fraction;
      yield return Utils.Unity.Constants.WaitForFixedUpdate;
    }
    BaseComp.fillAmount = increaseFill ? 1 : 0;
    yield break;
  }
}
