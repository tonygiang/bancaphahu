﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogLastSceneName : MonoBehaviour
{
  void OnDestroy() => AppInfo.LastSceneName = AppInfo.ActiveScene.name;
}
