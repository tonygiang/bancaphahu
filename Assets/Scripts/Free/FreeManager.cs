﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Entities.Data;
using System.Linq;
using Newtonsoft.Json;
using Utils;
using System.Linq;

public class FreeManager : MonoBehaviour
{

  public static FreeManager Instance;

  // checkin
  public GameObject giftContainer;
  [SerializeField] BanCa.LoginRewardItem LoginRewardItem = null;
  public Text checkInValueText;
  public Button checkInBtn;
  public static ISFSObject CheckInSFSData = null;
  BanCa.MainMenuScene Scene => BanCa.Game.MainMenu;

  void Awake()
  {
    Instance = this;
  }

  #region  CHECK IN 
  public void OpenPanel(ISFSObject sfsObj)
  {
    checkInValueText.text = $"{SaveLoadData.CheckInValue}/{BanCa.Gifts.LoginRewards.Count()}";
    checkInBtn.interactable = !CheckInSFSData.GetBool("Checked");

    Scene.Gifts.LoginRewardItems = BanCa.Gifts.LoginRewards
      .Select(r => Instantiate(LoginRewardItem, giftContainer.transform)
        .Init(r))
      .ToList();
  }

  public void LoadUserCheckIn(ISFSObject _obj)
  {
    SaveLoadData.CheckInValue = _obj.GetInt("dailyreward");
    if (BanCa.Game.MainMenu.Gifts.LoginRewardItems.Count() > 0) return;
    OpenPanel(_obj);
  }

  public void RefresfCheckIn(ISFSObject _obj)
  {
    BanCa.Session.Gem.Value += _obj.GetInt("gem");
    BanCa.Session.TreasureKeys.Value += _obj.GetInt("keyKhoBauVip");
    _obj.GetSFSObject("objskill").ToDictionary<int>()
      .ForEach(p => BanCa.Session.Skills[p.Key].Charges.Value += p.Value);
    Scene.Gifts.LoginRewardItems
      .Where(i => i.BoundReward.ID == _obj.GetInt("id")).First()
      .SetReached(true);
    int _id = _obj.GetInt("id");
    checkInValueText.text = $"{_id}/{BanCa.Gifts.LoginRewards.Count()}";
    checkInBtn.interactable = false;
  }

  #endregion

  public void ResetCheckIn()
  {
    foreach (Transform tran in giftContainer.transform)
    {
      Destroy(tran.gameObject);
    }
  }

  #region SHARE FACEBOOK
  public void ShareFB()
  {
    if (UserManager.isQuickLogin)
    {
      MessageBox.instance.Show("Bạn cần đăng nhập bằng facebook để thực hiện chức năng này");
    }
    else
    {
      FBConnector.FBShareGame();
    }

  }

  #endregion
}
