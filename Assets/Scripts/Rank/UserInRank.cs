﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

[System.Serializable]
public class UserInRank {

	public string displayname;
	public int value;
	public string time;
	public string amount;
	public UserInRank(ISFSObject data){
		displayname = data.GetUtfString ("displayname");
		amount = data.GetUtfString ("amount");
		value = data.GetInt ("value");
		time = data.GetUtfString ("time");
	}
}
