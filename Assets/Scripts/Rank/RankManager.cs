﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Utils;
using UnityEngine.Networking;

public class RankManager : MonoBehaviour
{

  public static RankManager Instance;

  public GameObject rankPanel;
  public GameObject topContainer, topItem;

  public Image[] listTabBtn;
  public Sprite spriteTabDefault, spriteTabActive;

  public Image[] listTabChilBtn;
  public Sprite[] spriteTabChilDefault, spriteTabChilActive;
  public Text valueText;
  string[] listValue = { "KIM CƯƠNG", "KIM CƯƠNG", "KIM CƯƠNG", "KIM CƯƠNG" };
  Color halfWhite = new Color(1f, 1f, 1f, 0.5f);

  void Awake()
  {
    Instance = this;
    rankPanel.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() =>
    {
      rankPanel.SetActive(false);
      UIMenu.Instance.mainMenu.SetActive(true);
    });
  }


  // on
  public void OpenPanel()
  {
    UIMenu.Instance.mainMenu.SetActive(false);
    rankPanel.SetActive(true);
    SwitchTab("gem");
  }

  public void LoadDataTopRespone(ISFSObject _obj)
  {
    string _type = _obj.GetUtfString("type");
    LoadTop(_obj);
  }
  public void LoadTop(ISFSObject _obj)
  {
    List<UserInRank> _listtop = new List<UserInRank>();
    ISFSArray arrMis = _obj.GetSFSArray("list");
    foreach (ISFSObject mi in arrMis)
    {
      UserInRank _mis = new UserInRank(mi);
      _listtop.Add(_mis);
    }
    OpenTop(_listtop);
  }

  public void OpenTop(List<UserInRank> _listtop)
  {
    foreach (Transform tran in topContainer.transform)
    {
      Destroy(tran.gameObject);
    }
    for (int index = 0; index < _listtop.Count; index++)
    {
      GameObject ga = Instantiate(topItem) as GameObject;
      ga.transform.SetParent(topContainer.transform);
      ga.transform.localScale = Vector3.one;
      if (string.IsNullOrEmpty(_listtop[index].amount))
      {
        rankPanel.transform.GetChild(0).transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = "HẠNG";
        ga.transform.GetChild(0).GetComponent<Text>().text = (index + 1).ToString();
        ga.transform.GetChild(1).GetComponent<Text>().text = _listtop[index].displayname;
        ga.transform.GetChild(2).GetComponent<Text>().text = Global.CoinToString(_listtop[index].value);
      }
      else
      {
        rankPanel.transform.GetChild(0).transform.GetChild(4).transform.GetChild(0).GetComponent<Text>().text = "TIME";
        ga.transform.GetChild(0).GetComponent<Text>().text = _listtop[index].time;
        ga.transform.GetChild(0).GetComponent<Text>().fontSize = 20;
        ga.transform.GetChild(1).GetComponent<Text>().text = _listtop[index].displayname;
        if (!string.IsNullOrEmpty(_listtop[index].amount))
        {
          ga.transform.GetChild(2).GetComponent<Text>().text = Global.CoinToString(int.Parse(_listtop[index].amount));
        }

      }

    }
  }



  // off 
  public void PickTab(int _value, string _type)
  {
    for (int i = 0; i < listTabBtn.Length; i++)
    {
      if (i == _value)
      {
        listTabBtn[i].GetComponent<Button>().enabled = false;
        listTabBtn[i].sprite = spriteTabActive;
        listTabBtn[i].transform.GetChild(0).GetComponent<Text>().color = Color.white;
        listTabBtn[i].transform.GetChild(0).GetComponent<Outline>().enabled = true;
        valueText.text = listValue[i];

      }
      else
      {
        listTabBtn[i].GetComponent<Button>().enabled = true;
        listTabBtn[i].sprite = spriteTabDefault;
        listTabBtn[i].transform.GetChild(0).GetComponent<Text>().color = halfWhite;
        listTabBtn[i].transform.GetChild(0).GetComponent<Outline>().enabled = false;
      }
    }
    listTabChilBtn[0].GetComponent<Button>().onClick.RemoveAllListeners();
    listTabChilBtn[0].GetComponent<Button>().onClick.AddListener(() =>
    {
      PickTabChil(0);
      if (_value == 1)
      {
        Network.PostToServer("top",
          ("method", "top_rank_gem"),
          ("type", $"{_type}day"))
          .Then(HandleGetTopRankResponse);
      }
      else if (_value == 2)
      {
        Network.PostToServer("top",
          ("method", "jackpot_history"),
          ("type", $"{_type}day"))
          .Then(HandleGetTopRankResponse);
      }

    });
    listTabChilBtn[1].GetComponent<Button>().onClick.RemoveAllListeners();
    listTabChilBtn[1].GetComponent<Button>().onClick.AddListener(() =>
    {
      PickTabChil(1);
      if (_value == 1)
      {
        Network.PostToServer("top",
          ("method", "top_rank_gem"),
          ("type", $"{_type}day"))
          .Then(HandleGetTopRankResponse);
      }
      else if (_value == 2)
      {
        Network.PostToServer("top",
          ("method", "jackpot_history"),
          ("type", $"{_type}week"))
          .Then(HandleGetTopRankResponse);
      }
    });
    PickTabChil(0);
    if (_value == 1)
    {
      Network.PostToServer("top",
        ("method", "top_rank_gem"),
        ("type", $"{_type}day"))
        .Then(HandleGetTopRankResponse);
    }
    else if (_value == 2)
    {
      Network.PostToServer("top",
        ("method", "jackpot_history"),
        ("type", $"{_type}day"))
        .Then(HandleGetTopRankResponse);
    }
  }

  public void PickTabChil(int _value)
  {
    for (int i = 0; i < listTabChilBtn.Length; i++)
    {
      if (i == _value)
      {
        listTabChilBtn[i].GetComponent<Button>().enabled = false;
        listTabChilBtn[i].sprite = spriteTabChilActive[i];

      }
      else
      {
        listTabChilBtn[i].GetComponent<Button>().enabled = true;
        listTabChilBtn[i].sprite = spriteTabChilDefault[i];
      }
      listTabChilBtn[i].SetNativeSize();
    }
  }

  public void SwitchTab(string _topType)
  {
    switch (_topType)
    {
      case "gold":
        PickTab(0, _topType);
        break;

      case "gem":
        PickTab(1, _topType);
        break;
      case "jackpot":
        PickTab(2, _topType);
        break;
      case "arena":
        PickTab(3, _topType);
        break;

    }
  }

  void HandleGetTopRankResponse(DownloadHandler handler)
  {
    ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
      .PassTo(SFSObject.NewFromJsonData);
    if (smartfoxResponse.GetInt("message_code") != 1)
      MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
    else LoadDataTopRespone(smartfoxResponse);
  }
}
