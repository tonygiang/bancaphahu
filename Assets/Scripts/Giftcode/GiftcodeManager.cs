﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiftcodeManager : MonoBehaviour {
	public static GiftcodeManager Instance;

	[SerializeField]
	Image btnInviImg,diamondImg;

	[SerializeField]
	Sprite btnSprite,diamondSprite;



	[SerializeField]
	GameObject giftcodePanel,inputcodePanel,hideInvite;


	[SerializeField]
	Text nhapmaIviText,nhan2000gemText,usernameDisplay;


	public InputField inputGiftcode;
	public string giftcodeType="even";

	Vector2 scaleMax =new Vector2(1.1f,1.1f);
	Vector2 scaleMin =new Vector2(0.9f,0.9f);
	Vector2 scaleMax2 =new Vector2(1.4f,1.4f);
	float timeST = 0.3f;
	float currentTimeST = 0;

	bool isReceive=false;


	void Awake(){
		Instance = this;
		if (PlayerPrefs.GetInt ("invitecode") == 1) {
			HideButtonInvi ();
		}

	}





	public void GIFTCODE_SEND(){
		string _code = inputGiftcode.text;
		if (_code.Length > 0)
		{
			SFSManager.instance.SendGiftCode (_code);
			//Method.Instance.GetResponseCode ();
		}
		else if(_code==UserManager.UserName){
//			MessageBox.instance.Show(LanguageChange.Instance.languageObject.GetUtfString ("notigiftcode"));
		}else
		{
			MessageBox.instance.Show("Bạn chưa nhập mã ");
		}
	}
	public void GIFTCODE_OPEN()
	{
		SoundManager.Instance.ButtonSound ();
		usernameDisplay.text = UserManager.UserName;
//		khinguoichoiText.text = LanguageChange.Instance.languageObject.GetUtfString ("khinguoichoi") + "<size=23><color=#00a1d7>"+UserManager.UserName+"</color></size>";
		StartCoroutine(ShowAnimPopUpOpen(false,giftcodePanel));
	}
	public void GIFTCODE_CLOSE()
	{
		StartCoroutine(ShowAnimPopUpOpen(true,giftcodePanel));
	}
	public void GIFTCODE_INPUT_OPEN(string _type)
	{
		giftcodePanel.transform.GetChild(0).gameObject.SetActive (false);
		if (_type == "invitation") {
			inputcodePanel.transform.GetChild (1).GetComponent<Text> ().text = "Vui lòng nhập mã được giới thiệu ";
		} else if (_type == "event") {
			inputcodePanel.transform.GetChild (1).GetComponent<Text> ().text ="Vui lòng nhập mã sự kiện";
		}
		giftcodeType = _type;
		StartCoroutine (ShowAnimPopUpInput (false, giftcodePanel));
		SoundManager.Instance.ButtonSound ();
	}
	public void GIFTCODE_INPUT_CLOSE()
	{
		SoundManager.Instance.ButtonSound ();
		StartCoroutine(ShowAnimPopUpInput(true,giftcodePanel));
	}
	IEnumerator ShowAnimPopUpOpen(bool isClose,GameObject _panel)
	{
		
		Transform _giftcode = _panel.transform.GetChild (0);
		Transform _inputcode = _panel.transform.GetChild (1);
		_panel.SetActive(true);
		if (isClose) {
			timeST = 0.1f;
			currentTimeST = 0;
			do
			{
				_giftcode.localScale = Vector2.Lerp(Vector2.one, Vector2.zero, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			_giftcode.localScale = scaleMax;
			_panel.SetActive(false);
		} else {
			
			timeST = 0.15f;
			currentTimeST = 0;

			do
			{
				_giftcode.localScale = Vector2.Lerp(Vector2.zero, scaleMax, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			timeST = 0.05f;
			currentTimeST = 0;
			do
			{
				_giftcode.localScale = Vector2.Lerp(scaleMax,scaleMin, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			_giftcode.localScale = Vector2.one;
		}

	}

	IEnumerator ShowAnimPopUpInput(bool isClose,GameObject _panel)
	{

		Transform _giftcode = _panel.transform.GetChild (0);
		Transform _inputcode = _panel.transform.GetChild (1);
		_panel.SetActive(true);
		if (isClose) {
			timeST = 0.1f;
			currentTimeST = 0;
			do
			{
				_inputcode.localScale = Vector2.Lerp(Vector2.one, Vector2.zero, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			_inputcode.localScale = Vector2.zero;
			_inputcode.gameObject.SetActive (false);
			_giftcode.gameObject.SetActive (true);
			currentTimeST = 0;
			do
			{
				_giftcode.localScale = Vector2.Lerp(Vector2.zero, scaleMax, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			timeST = 0.05f;
			currentTimeST = 0;
			do
			{
				_giftcode.localScale = Vector2.Lerp(scaleMax,scaleMin, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			_giftcode.localScale = Vector2.one;
		} else {
			
			timeST = 0.1f;
			currentTimeST = 0;
			do
			{
				_giftcode.localScale = Vector2.Lerp(Vector2.one, Vector2.zero, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			_giftcode.localScale = scaleMax;
			_inputcode.gameObject.SetActive (true);
			_giftcode.gameObject.SetActive (false);
			currentTimeST = 0;
			do
			{
				_inputcode.localScale = Vector2.Lerp(Vector2.zero, scaleMax, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			timeST = 0.05f;
			currentTimeST = 0;
			do
			{
				_inputcode.localScale = Vector2.Lerp(scaleMax,scaleMin, currentTimeST / timeST);
				currentTimeST += Time.deltaTime;
				yield return null;
			} while(currentTimeST <= timeST);
			_inputcode.localScale = Vector2.one;
		}
	}
	public void FinishInvite(string _type){
		if (_type == "invitecode") {
			HideButtonInvi ();
			PlayerPrefs.SetInt ("invitecode", 1);
			PlayerPrefs.Save ();
		}
	}
	void HideButtonInvi(){
		nhan2000gemText.color = Color.gray;
		hideInvite.SetActive (false);
		btnInviImg.sprite = btnSprite;
		btnInviImg.SetNativeSize ();
		diamondImg.sprite = diamondSprite;
		diamondImg.SetNativeSize ();
		btnInviImg.GetComponent<Button> ().enabled = false;
		nhapmaIviText.text = "CHỈ 1 LẦN";
		nhapmaIviText.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		isReceive = true;
	}

	public void CopyCode(){
//		GUIUtility.systemCopyBuffer = UserManager.UserName;
		UniClipboard.SetText (UserManager.UserName);
	}
}
