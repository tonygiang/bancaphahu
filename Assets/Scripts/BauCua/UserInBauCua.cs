﻿using System.Collections;
using UnityEngine;
using System.Xml.Linq;
using Sfs2X.Entities.Data;
using System.Collections.Generic;

//[System.Serializable]
public class UserInBauCua {

	public string UserName;
	public string DisplayName;
	public int Gem;
	public string AvatarLink;
	public int vip;
	public int slotIndex;
	public int betValue;

	public UserInBauCua(ISFSObject obj){
		UserName = obj.GetUtfString ("uname");
		DisplayName = obj.GetUtfString ("displayname");
		Gem = int.Parse(obj.GetData ("gem").Data.ToString());
		vip = obj.GetInt ("vip");
		AvatarLink = obj.GetUtfString ("avatar");
	}
}
