﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Sfs2X.Entities.Data;
using Spine.Unity;

public class BauCuaManager : MonoBehaviour
{


	public static BauCuaManager Instance;

	public Dictionary<string,UserInBauCua> listUserInRoom = new Dictionary<string, UserInBauCua> ();

	int chipType = 100;
	[SerializeField]
	Animator hotGirl, bowlAni;
	[SerializeField]
	GameObject startBet, moBat, thangX2X3, LichSuPanel, KetQuaPanel;
	[SerializeField]
	SkeletonGraphic clockAni;
	[SerializeField]
	SkeletonAnimation startAni;
	[SerializeField]
	SpriteRenderer[] listXucXac;
	[SerializeField]
	Image[] listHistory;
	[SerializeField]
	GameObject[] TextTienCuoc;

	[SerializeField]
	GameObject bangTongKet;

	[SerializeField]
	Transform coinCanvas;

	int TienDatCuoc, TienThangCuoc;


	int[] betValueisMe = { 0, 0, 0, 0, 0, 0 };

	string[] resultArray;
	bool isResult = false;

	public Button[] listCoinValue;

	public List<GameObject> listAvatar;
	public List<GameObject> dsAva;

	//int index = 0;
	public GameObject myAvatar;

	public List<GameObject> playerSlots;
	public List<GameObject> avas;

	List<UserReward> listReward = new List<UserReward> ();

	bool isInBauCua = false;

	List<string> listTRUOT = new List<string> ();
	List<string> listTRUNG = new List<string> ();


	public GameObject[] listbtnBauCua;
	public List<GameObject> DSCUOC;
	public GameObject dealer;
	public GameObject KhongDuocDatCuoc;

	private bool isHistoryPanelOpen = false;

	void Awake ()
	{
		Instance = this;
		for (int x = 0; x < listbtnBauCua.Length; x++) {
			listTRUOT.Add (listbtnBauCua [x].name);
		}
	}

	void Start ()
	{
		ALLUSER (SaveLoadData.Instance.AllUserBauCua);
		avas = new List<GameObject> ();
		MusicManager.Instance.PlayInGameBauCua ();

        
		ChooseChipType (chipType);

		LoadLevel.Instance.HideLoadingPanel ();

	}

	IEnumerator Shake ()
	{
		#if UNITY_ANDROID || UNITY_IOS
		Handheld.Vibrate ();
		#endif

		float elapsed = 0.0f;

		float duration = 1, magnitude = 0.05f;

		Vector3 originalCamPos = Camera.main.transform.position;

		while (elapsed < duration) {

			elapsed += Time.deltaTime;          

			float percentComplete = elapsed / duration;         
			float damper = 1.0f - Mathf.Clamp (4.0f * percentComplete - 3.0f, 0.0f, 1.0f);

			// map value to [-1, 1]
			float x = Random.value * 2.0f - 1.0f;
			float y = Random.value * 2.0f - 1.0f;
			x *= magnitude * damper;
			y *= magnitude * damper;

			Camera.main.transform.position = new Vector3 (x, y, originalCamPos.z);

			yield return null;
		}

		Camera.main.transform.position = originalCamPos;
	}

	public void Shaking ()
	{
		StartCoroutine (Shake ());
	}

	// -------- BAU CUA MOI ---------------//
	public void PhaseBauCua (ISFSObject _obj)
	{
		int _index = _obj.GetInt ("index");
		int _time = _obj.GetInt ("time");
		switch (_index) {
		// Xóc đĩa 
		case 0:
			KhongDuocDatCuoc.SetActive (true);
			if (_time == 3)
				hotGirl.SetTrigger ("active");


			break;
		// Đặt cược 
		case 1:
			clockAni.transform.GetChild (0).GetComponent<Text> ().text = _time.ToString ();
			if (_time == 15) {
				startBet.SetActive (true);
				SoundManager.Instance.SapBatDau ();
				KhongDuocDatCuoc.SetActive (false);
			}

			clockAni.gameObject.SetActive (true);
			if (_time < 5) {
				clockAni.AnimationState.SetAnimation (0, "bao dong", true);
			} else {
				clockAni.AnimationState.SetAnimation (0, "chuong reo", true);
			}
			SoundManager.Instance.Clock ();
			break;

		// Mở bát 
		case 2:
			
			KhongDuocDatCuoc.SetActive (true);
			clockAni.gameObject.SetActive (false);
			if (_time == 3) {
				moBat.SetActive (true);
				bowlAni.gameObject.SetActive (true);
				bowlAni.SetTrigger ("MoBat");
			}
			break;

		// Trả thưởng 
		case 3:
			KhongDuocDatCuoc.SetActive (true);
			if (_time == 10) {
			}

			if (_time == 5) {
				TraThuong ();
			}
			break;

		// màn chơi mới
		case 4:
			thangX2X3.SetActive (false);
			for (int i = 0; i < DSCUOC.Count; i++) {
				PoolingBauCua.instance.RemoveChip (DSCUOC [i]);
			}
			listTRUOT.Clear ();
			listTRUNG.Clear ();
			int _size = listbtnBauCua.Length;
			for (int x = 0; x < _size; x++) {
				listTRUOT.Add (listbtnBauCua [x].name);
				listbtnBauCua [x].transform.GetComponent<OCuoc> ().listChip.Clear ();
			}
			DSCUOC.Clear ();

			TienDatCuoc = 0;
			TienThangCuoc = 0;

			for (int j = 0; j < listbtnBauCua.Length; j++) {
				listbtnBauCua [j].GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Images/BauCua/table/" + listbtnBauCua [j].name);
				TextTienCuoc [j].SetActive (false);
				betValueisMe [j] = 0;
			}
			KhongDuocDatCuoc.SetActive (true);
			if (_time == 3) {
				StartCoroutine (NewGame ());

			}
			break;
		}
	}

	void TraThuong ()
	{
		if (TienDatCuoc > 0) {
			int _tiensautinh = TienThangCuoc - TienDatCuoc;
			if (_tiensautinh < 0) {
				//thua
				StartCoroutine (HienBangTongKet (0, _tiensautinh));
			} else if (_tiensautinh > 0) {
				// thang
				StartCoroutine (HienBangTongKet (1, _tiensautinh));
			} else {
				//hoa
				StartCoroutine (HienBangTongKet (2, _tiensautinh));
			}
		}
		ShowHistory ();
		for (int i = 0; i < listReward.Count; i++) {
			int _slot = listUserInRoom [listReward [i].uname].slotIndex;
			if (listReward [i].uname != UserManager.UserName) {
				GameObject _gemText = PoolingBauCua.instance.SpawnCoin (SaveLoadData.FormatMoney (listReward [i].reward));
				_gemText.transform.SetParent (coinCanvas.transform);
				_gemText.transform.localScale = new Vector3 (0.3f, 0.3f, 1f);
				_gemText.transform.position = playerSlots [_slot].transform.position;
				PoolingBauCua.instance.RemoveObject (_gemText, 2f, 10);
				playerSlots [_slot].transform.GetChild (2).GetChild (1).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (listReward [i].gem);
			}
		}
		listReward.Clear ();
	}

	IEnumerator HienBangTongKet (int _type, int _money)
	{
		bangTongKet.SetActive (true);
		Color _colorA = bangTongKet.GetComponent<Image> ().color;
		_colorA.a = 0;
		float _timeStart = 0;
		float _timeEnd = 0.8f;
		do {
			_colorA.a += Time.deltaTime * 2;
			bangTongKet.GetComponent<Image> ().color = _colorA;
			_timeStart += Time.deltaTime * 2;
			yield return null;
		} while(_timeStart < _timeEnd);


		switch (_type) {
		// thua
		case 0:
			SoundManager.Instance.Thua ();
			bangTongKet.transform.GetChild (0).gameObject.SetActive (true);
			bangTongKet.transform.GetChild (0).GetComponent<Text> ().text = SaveLoadData.FormatMoney(_money);
			bangTongKet.transform.GetChild (1).gameObject.SetActive (true);
			yield return new WaitForSeconds (3f);
			bangTongKet.transform.GetChild (1).gameObject.SetActive (false);
			break;
		//thang
		case 1:
			SoundManager.Instance.Thang ();
			bangTongKet.transform.GetChild (0).gameObject.SetActive (true);
			bangTongKet.transform.GetChild (0).GetComponent<Text> ().text = "+ " + SaveLoadData.FormatMoney(_money);
			bangTongKet.transform.GetChild (2).gameObject.SetActive (true);
			yield return new WaitForSeconds (3f);
			bangTongKet.transform.GetChild (2).gameObject.SetActive (false);
			break;
		//hoa
		case 2:
			SoundManager.Instance.Thua ();
			bangTongKet.transform.GetChild (3).gameObject.SetActive (true);
			yield return new WaitForSeconds (3f);
			bangTongKet.transform.GetChild (3).gameObject.SetActive (false);
			break;
		}
		bangTongKet.SetActive (false);


	}



	void ShowHistory ()
	{
		if (isResult) {
			int _size = listXucXac.Length;
			for (int i = 0; i < _size; i++) {
				listHistory [i].sprite = Resources.Load<Sprite> ("Images/BauCua/xucxacHistory/" + listXucXac [i].sprite.name);
			}
		}
	}

	void ShowResult ()
	{
		
	}



	IEnumerator NewGame ()
	{
		SoundManager.Instance.SapBatDau ();
		startAni.gameObject.SetActive (true);
		yield return new WaitForSpineAnimationComplete (startAni.AnimationState.GetCurrent (0));
		startAni.gameObject.SetActive (false);
	}

	public void ChooseBetType (string _betType)
	{
		for (int i = 0; i < listbtnBauCua.Length; i++) {
			if (_betType == listbtnBauCua [i].name) {
				if (betValueisMe [i] + chipType <= 10000000) {
					if (BanCa.Session.Gem.Value >= chipType) {
						SFSManager.instance.BET_ON (_betType, chipType);
					} else {
						MessageBox.instance.ShowGoToShop ("THÔNG BÁO", "Bạn không đủ kim cương. Nạp thêm kim cương hoặc nhận quà hằng ngày để tiêp tục chơi game nhé!", MessageBoxType.OK_CANCEL, GoToShopGem);
					}
				} else {
					MessageBox.instance.Show ("Mỗi cửa chỉ được phép đặt tối đa 10.000.000 kim cương");
				}
			}
		}
	}

	void GoToShopGem (MessageBoxCallBack result)
	{
		if (result.Equals (MessageBoxCallBack.OK)) {
			Shop.Instance.OpenShop ();
		}
	}

	public void ChooseChipType (int _chipType)
	{

		for (int i = 0; i < listCoinValue.Length; i++) {
			int _chipValue = int.Parse (listCoinValue [i].name);
			if (_chipType == _chipValue) {
				listCoinValue [i].transform.GetChild (0).gameObject.SetActive (true);
				chipType = _chipValue;
				listCoinValue [i].enabled = false;
			} else {
				listCoinValue [i].transform.GetChild (0).gameObject.SetActive (false);
				listCoinValue [i].enabled = true;
			}
		}
	}

	public void ResBet (ISFSObject _obj)
	{
		if (isInBauCua) {
			string _uname = _obj.GetUtfString ("uname");
			int _gem = _obj.GetInt ("gem");
			string _betType = _obj.GetUtfString ("bet");
			int _money = _obj.GetInt ("money");
			GameObject _targetPos = null;
			for (int i = 0; i < listbtnBauCua.Length; i++) {
				if (listbtnBauCua [i].name == _betType) {
					_targetPos = listbtnBauCua [i];
					listUserInRoom [_uname].betValue += _money;
					listUserInRoom [_uname].Gem = _gem;

					GameObject _chip = PoolingBauCua.instance.SpawnChip (_money.ToString ());
					DSCUOC.Add (_chip);
					if (UserManager.UserName == _uname) {
						_chip.GetComponent<Chip> ().Init (myAvatar.transform.position, _targetPos.transform.position, true);
						betValueisMe [i] += _money;
						TienDatCuoc += _money;
						TextTienCuoc [i].SetActive (true);
						TextTienCuoc [i].transform.GetChild (0).GetComponent<Text> ().text = SaveLoadData.FormatMoney (betValueisMe [i]);
						BanCa.Session.Gem.Value = _gem;
					} else {
						_chip.GetComponent<Chip> ().Init (playerSlots [listUserInRoom [_uname].slotIndex].transform.position, _targetPos.transform.position, true);
						playerSlots [listUserInRoom [_uname].slotIndex].transform.GetChild (2).GetChild (1).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (_gem);

					}
					listbtnBauCua [i].transform.GetComponent<OCuoc> ().listChip.Add (_chip);
					SoundManager.Instance.ChipMove ();
				}
			}

		}

	}

	public void RESULT (ISFSObject _obj)
	{
		int _indexXX = 0;
		string[] _result = _obj.GetUtfStringArray ("result");
		resultArray = _result;
		isResult = true;
		for (int i = 0; i < _result.Length; i++) {
			ISFSObject _ob = SFSObject.NewFromJsonData (_result [i]);
			UserResult _ur = new UserResult (_ob);
			for (int j = 0; j < _ur.count; j++) {
				listXucXac [_indexXX].sprite = Resources.Load<Sprite> ("Images/BauCua/xucxac/" + _ur.item);
				_indexXX++;
			}
			listTRUNG.Add (_ur.item);
			listTRUOT.Remove (_ur.item);
		}
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			OutRoom ();
		}
	}

	public void OutRoom ()
	{
		SoundManager.Instance.ButtonSound ();
		MessageBox.instance.Show ("Thoát", "Rời phòng bạn sẽ mất số tiền đã đặt cược. Bạn có chắc chắn thoát không?", MessageBoxType.OK_CANCEL, BackConfirm);
	}

	private void BackConfirm (MessageBoxCallBack result)
	{
		if (result.Equals (MessageBoxCallBack.OK)) {
			SFSManager.instance.USER_OUT_ROOM_BAUCUA ();
		}
	}



	public void REWARD (ISFSObject _obj)
	{
		if (isInBauCua) {
			string[] _listWinner = _obj.GetUtfStringArray ("reward");
			for (int i = 0; i < _listWinner.Length; i++) {
				ISFSObject _ob = SFSObject.NewFromJsonData (_listWinner [i]);
				UserReward _ur = new UserReward (_ob);
				listReward.Add (_ur);
				if (UserManager.UserName == _ur.uname) {
					TienThangCuoc = _ur.reward;
					BanCa.Session.Gem.Value = _ur.gem;
				}
			}

			if (isResult) {
				for (int i = 0; i < resultArray.Length; i++) {
					ISFSObject _ob = SFSObject.NewFromJsonData (resultArray [i]);
					UserResult _ur = new UserResult (_ob);
					for (int j = 0; j < listbtnBauCua.Length; j++) {
						if (listbtnBauCua [j].name == _ur.item) {
							listbtnBauCua [j].GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Images/BauCua/table/" + _ur.item + "WIN");
							switch (_ur.count) {
							case 2: 
								thangX2X3.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Images/BauCua/Ratio/x2");
								thangX2X3.transform.position = listbtnBauCua [j].transform.position;
								thangX2X3.SetActive (true);
								break;
							case 3: 
								thangX2X3.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("Images/BauCua/Ratio/x3");
								thangX2X3.transform.position = listbtnBauCua [j].transform.position;
								thangX2X3.SetActive (true);
								break;
							}
						}
					}
				}
			}
			for (int i = 0; i < listTRUOT.Count; i++) {
				for (int j = 0; j < listbtnBauCua.Length; j++) {
					if (listbtnBauCua [j].name == listTRUOT [i]) {
						for (int k = 0; k < listbtnBauCua [j].transform.GetComponent<OCuoc> ().listChip.Count; k++) {
							listbtnBauCua [j].transform.GetComponent<OCuoc> ().listChip [k].GetComponent<Chip> ().Init (listbtnBauCua [j].transform.GetComponent<OCuoc> ().listChip [k].transform.position, dealer.transform.position, false);
						}

					}
				}
			}
//			for (int i = 0; i < listTRUNG.Count; i++) {
//				print ("o trung: " + listTRUNG [i]);
//				for (int j = 0; j < listbtnBauCua.Length; j++) {
//					if (listbtnBauCua [j].name == listTRUNG [i]) {
//						for (int k = 0; k < listbtnBauCua [j].transform.GetComponent<OCuoc> ().listChip.Count; k++) {
//							GameObject _ga = PoolingBauCua.instance.SpawnChip (listbtnBauCua [j].transform.GetComponent<OCuoc> ().listChip [k].name);
//							_ga.GetComponent<Chip> ().Init ( dealer.transform.position,listbtnBauCua [j].transform.GetComponent<OCuoc> ().listChip [k].transform.position, false);
//						}
//					}
//				}
//			}

//		for (int l = 0; l < listTRUNG.Count; l++) {
//			SoundManager.Instance.ChipMove ();
//			foreach (var key in _userinroom) {
//				if (listUserInRoom [key].listBet.ContainsKey (listTRUNG [l])) {
//					List<GameObject> _listChip = listUserInRoom [key].listBet [listTRUNG [l]].chip;
//					foreach (GameObject objChip in _listChip) {
//						GameObject _chip = PoolingBauCua.instance.SpawnChip (objChip.name);
//						_chip.GetComponent<Chip> ().Init (dealer.transform.position, objChip.transform.position, true);
//						listUserInRoom [key].listBet [listTRUNG [l]].chip.Add (_chip);
//					}
//					for (int i = 0; i < listReward.Count; i++) {
//						foreach (GameObject objChip in listUserInRoom[key].listBet [listTRUNG [l]].chip) {
//							objChip.GetComponent<Chip> ().Init (obj.transform.position, playerSlots [listUserInRoom [listReward [i].uname].slotIndex].transform.position, false);
//						}
//					}
//				}
//			}
//		}
		
		}
	}

	public void ADDUSER (ISFSObject _obj)
	{
		UserInBauCua u = new UserInBauCua (_obj);
		if (!listUserInRoom.ContainsKey (u.UserName)) {
			listUserInRoom.Add (u.UserName, u);
		}
		int index = Random.Range (0, dsAva.Count);
		avas.Add (dsAva [index]);
		dsAva [index].transform.GetChild (2).GetChild (0).gameObject.GetComponent<Text> ().text = u.DisplayName;
		dsAva [index].transform.GetChild (2).GetChild (1).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (u.Gem);
		dsAva [index].transform.GetChild (2).gameObject.SetActive (true);
		dsAva [index].transform.GetChild (1).gameObject.SetActive (true);
		dsAva [index].transform.GetChild (0).GetComponent<Mask> ().enabled = true;
		NetworkManager.instance.GetImage (u.AvatarLink, dsAva [index].transform.GetChild (0).GetChild (0).GetComponent<Image> ());
		u.slotIndex = dsAva [index].GetComponent<PlayerBauCua> ().slotIndex;
		dsAva.RemoveAt (index);
		listAvatar = avas;
	}

	public void ALLUSER (ISFSObject _obj)
	{
		JSONObject js = new JSONObject (_obj.GetUtfString ("users"));
		ISFSArray userArr = SFSArray.NewFromJsonData (js.GetField ("list").ToString ());
		foreach (ISFSObject ob in userArr) {
			UserInBauCua u = new UserInBauCua (ob);
			listUserInRoom.Add (u.UserName, u);
		}
		foreach (UserInBauCua ubaucua in listUserInRoom.Values) {
			if (ubaucua.UserName == UserManager.UserName) {
				myAvatar.transform.GetChild (1).gameObject.GetComponent<Text> ().text = ubaucua.DisplayName;
				ubaucua.slotIndex = myAvatar.GetComponent<PlayerBauCua> ().slotIndex;
				BauCuaManager.Instance.myAvatar.GetComponent<PlayerBauCua> ().coinStart = ubaucua.Gem;
				BauCuaManager.Instance.myAvatar.GetComponent<PlayerBauCua> ().coinWin = ubaucua.Gem;
//				if (!UserManager.isQuickLogin) {
//					myAvatar.transform.GetChild (2).GetChild (0).GetChild (0).GetComponent<Image> ().sprite = FBConnector.instance.spriteAvatar ();
//				} else {
//					myAvatar.transform.GetChild (2).GetChild (0).GetChild (0).GetComponent<Image> ().sprite = Resources.Load<Sprite> ("avatar/" + UserManager.avatarName);
//				}
				myAvatar.transform.GetChild (2).GetChild (0).GetChild (0).GetComponent<Image> ().sprite = UserManager.avatarSpr;
			} else {

				int index = Random.Range (0, dsAva.Count);
				avas.Add (dsAva [index]);

				dsAva [index].transform.GetChild (2).GetChild (0).gameObject.GetComponent<Text> ().text = ubaucua.DisplayName;
				dsAva [index].transform.GetChild (2).GetChild (1).gameObject.GetComponent<Text> ().text = SaveLoadData.FormatMoney (ubaucua.Gem);
				dsAva [index].transform.GetChild (2).gameObject.SetActive (true);
				dsAva [index].transform.GetChild (1).gameObject.SetActive (true);
				dsAva [index].transform.GetChild (0).GetComponent<Mask> ().enabled = true;
				NetworkManager.instance.GetImage (ubaucua.AvatarLink, dsAva [index].transform.GetChild (0).GetChild (0).GetComponent<Image> ());
				ubaucua.slotIndex = dsAva [index].GetComponent<PlayerBauCua> ().slotIndex;
				dsAva.RemoveAt (index);
				listAvatar = avas;

			}
		}
		isInBauCua = true;
	}

	public void OpenResult (ISFSObject obj)
	{
		string _username = obj.GetUtfString ("username");
		if (listUserInRoom.ContainsKey (_username)) {
			if (_username == UserManager.UserName) {
				Loading.instance.Hide ();
				LoadLevel.Instance.ShowLoadingPanel ("Menu", "Đang tải thông tin...");
				return;
			} else {
				REMOVE_USER (_username);			
			}
		}

	}

	public void REMOVE_USER (string _username)
	{
		int slot = listUserInRoom [_username].slotIndex;
		playerSlots [slot].transform.GetChild (2).GetChild (0).gameObject.GetComponent<Text> ().text = "";
		playerSlots [slot].transform.GetChild (2).GetChild (1).gameObject.GetComponent<Text> ().text = "";
		playerSlots [slot].transform.GetChild (2).gameObject.SetActive (false);
		playerSlots [slot].transform.GetChild (1).gameObject.SetActive (false);
		playerSlots [slot].transform.GetChild (0).GetChild (0).GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Images/BauCua/playerwait");
		playerSlots [slot].transform.GetChild (0).GetComponent<Mask> ().enabled = false;
		listAvatar.Remove (playerSlots [slot]);
		dsAva.Add (playerSlots [slot]);
		listUserInRoom.Remove (_username);
	}

	IEnumerator ChipMovement (Transform _start, Transform shooter)
	{

		float bTime = 1.8f;
		float bCurrentTime = 0;
		Vector3 _fro = new Vector3 (0.3f, 0.3f, 0.3f);
		Vector3 _to = new Vector3 (0.5f, 0.5f, 0.5f);

		do {
			_start.transform.eulerAngles = Vector3.Lerp (Vector3.zero, new Vector3 (0, 0, 360f), bCurrentTime / bTime);
			bCurrentTime += Time.deltaTime;
			yield return null;
		} while(bCurrentTime <= bTime);
//		_start.FishDie ();
		Vector3 currentPos = _start.transform.position;
		Vector3 destination = shooter.transform.position;
		destination.z = 0;
		Vector3 currentScale = _start.transform.localScale;

		//		float time = GetDistance(fish.transform.position, shooter.transform.position) / 4f;
		float time = 0.5f;
		float currentTime = 0;

		do {
			_start.transform.position = Vector3.Lerp (currentPos, destination, currentTime / time);
			_start.transform.localScale = Vector3.Lerp (currentScale, new Vector3 (0.1f, 0.1f, 1), currentTime / time);

			currentTime += Time.deltaTime;
			yield return null;

		} while(currentTime <= time);
		PoolingSystem.instance.RemoveFish (_start.gameObject, _start.gameObject.name);
	}

	public void OpenHistoryList ()
	{
		LichSuPanel.SetActive (!isHistoryPanelOpen);
		KetQuaPanel.SetActive (isHistoryPanelOpen);

		isHistoryPanelOpen = !isHistoryPanelOpen;
		if (isHistoryPanelOpen) {
			SFSManager.instance.GetListHistoryBauCua ();
		}
	}

	public void getHistoryList (ISFSObject obj)
	{
		JSONObject js = new JSONObject (obj.GetUtfString ("result"));
		ISFSArray historyArr = SFSArray.NewFromJsonData (js.ToString ());

		Transform contenPanel = LichSuPanel.transform.GetChild (0).transform.GetChild (0).transform.transform.GetChild (0);
		foreach (Transform trans in contenPanel.transform) {
			if (trans.gameObject.name.Contains ("Clone")) {
				Destroy (trans.gameObject);
			}
		}
		contenPanel.GetComponent<RectTransform> ().SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, 86f * historyArr.Count);
		Transform phienPanel = contenPanel.transform.GetChild (0);
		foreach (SFSObject data in historyArr) {
			string kq = data.GetUtfString ("ketqua");
			string[] ArrKq = kq.Split (',');
			Transform clone = Instantiate (phienPanel);
			clone.gameObject.SetActive (true);
			clone.SetParent (contenPanel);
			var index = 0;
			foreach (var item in ArrKq) {
				string[] Array = item.Split ('_');
				for (int i = 0; i < int.Parse(Array [1]); i++) {
					clone.GetChild(index).transform.GetComponent<Image>().sprite  = Resources.Load<Sprite> ("Images/BauCua/xucxacHistory/" + Array[0]);
					clone.transform.localScale = new Vector3 (1f, 1f, 1f);
					index++;
				}
			}
		}
	}
}


//

