﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;


[System.Serializable]
public class UserReward {

	public string uname;
	public int reward;
	public int gem;

	public UserReward(ISFSObject _obj){
		uname = _obj.GetUtfString ("uname");
		reward = _obj.GetInt ("reward");
		gem = _obj.GetInt ("gem");
	}
}
