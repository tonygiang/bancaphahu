﻿using UnityEngine;
using System.Collections;

public class Chip : MonoBehaviour {
	//public GameObject endPos;
	Vector2 end,start;

	public void Init(Vector2 start,Vector2 end, bool bc)
	{
		if (bc) {
			this.end = new Vector2 (Random.Range (end.x - 0.8f, end.x + 0.8f), Random.Range (end.y - 0.4f, end.y + 0.4f));
		} else {
			this.end = end;
		}
		this.start = start;
		gameObject.transform.position= start;

	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.position = Vector2.MoveTowards (gameObject.transform.position, end, 10*Time.deltaTime);
	}

	void ChipMove(){

	}
}
