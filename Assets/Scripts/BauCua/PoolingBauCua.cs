﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PoolingBauCua : MonoBehaviour {
	public static PoolingBauCua instance;

	public Transform chipContainer;


	void Awake()
	{
		instance = this;
	}

	public GameObject SpawnChip(string chipName)
	{
		
		GameObject pa = transform.Find(chipName).gameObject;
		GameObject ga = null;

		if(pa.transform.childCount > 0)
		{
			ga = pa.transform.GetChild(0).gameObject;

		}
		else
		{
			ga = Instantiate (Resources.Load<GameObject> ("Prefabs/BauCua/Chips/" + chipName));
			ga.name = ga.name.Replace("(Clone)", "");
		}
		ga.transform.SetParent (chipContainer);
		ga.transform.position = new Vector3 (100f, 100f, 0);
		ga.SetActive(true);
		return ga;
	}

	public void RemoveChip(GameObject fish, string chipName)
	{
		GameObject pa = transform.Find(chipName).gameObject;
		if(pa.transform.childCount <= 50)
		{
			fish.transform.SetParent(pa.transform);
			fish.name = pa.name;
			fish.transform.localPosition = new Vector3(0, 0, 0);

			fish.SetActive(false);
		}
		else
		{
			Destroy(fish);
		}
	}





	public GameObject SpawnEffect(string snareName, Vector3 position)
	{
		GameObject pa = transform.Find(snareName).gameObject;
		GameObject ga = null;

		if(pa.transform.childCount > 0)
		{
			ga = pa.transform.GetChild(0).gameObject;
			ga.SetActive(true);
			ga.transform.SetParent(null);
			ga.transform.position = position;
		}
		else
		{
			ga = Instantiate(Resources.Load<GameObject>("Prefabs/EffectFishDie/" + snareName), position, new Quaternion(0, 0, 0, 0)) as GameObject;
			ga.name = ga.name.Replace("(Clone)", "");
		}

		return ga;
	}

	public GameObject SpawnCoin(string _gemText)
	{
		GameObject pa = transform.Find("GemBauCua").gameObject;
		GameObject ga = null;

		if(pa.transform.childCount > 0)
		{
			ga = pa.transform.GetChild(0).gameObject;

			ga.transform.SetParent(null);
		}
		else
		{
			ga = Instantiate(Resources.Load<GameObject>("Prefabs/Others/GemBauCua")) as GameObject;
			ga.name = ga.name.Replace("(Clone)", "");
		}
		ga.GetComponent<Text> ().text = "+"+_gemText;
		ga.SetActive(true);
		return ga;
	}

	public GameObject SpawnCoinJackpot(Vector3 position)
	{
		GameObject pa = transform.Find("PointTextJackpot").gameObject;
		GameObject ga = null;

		if(pa.transform.childCount > 0)
		{
			ga = pa.transform.GetChild(0).gameObject;
			ga.SetActive(true);
			ga.transform.SetParent(null);
		}
		else
		{
			ga = Instantiate(Resources.Load<GameObject>("Prefabs/Others/" + "PointTextJackpot"), position, new Quaternion(0, 0, 0, 0)) as GameObject;
			ga.name = ga.name.Replace("(Clone)", "");
		}
		ga.transform.position = position;


		return ga;
	}

	public GameObject SpawnObject(Vector3 position, string name)
	{
		GameObject pa = transform.Find(name).gameObject;
		GameObject ga = null;

		if(pa.transform.childCount > 0)
		{
			ga = pa.transform.GetChild(0).gameObject;
			ga.SetActive(true);
			ga.transform.parent = null;
		}
		else
		{
			ga = Instantiate(Resources.Load<GameObject>("Prefabs/Others/" + name), position, new Quaternion(0, 0, 0, 0)) as GameObject;
			ga.name = ga.name.Replace("(Clone)", "");
		}
		ga.transform.position = position;

		return ga;
	}

	public void RemoveObject(GameObject ga, float waitTime, int childCount)
	{
		StartCoroutine(CouRemoveObject(ga, waitTime, childCount));
	}

	private IEnumerator CouRemoveObject(GameObject ga, float waitTime, int childCount)
	{
		yield return new WaitForSeconds(waitTime);
		if(ga != null)
		if(transform.Find(ga.name) != null)
		{
			GameObject pa = transform.Find(ga.name).gameObject;


			if(pa.transform.childCount <= childCount)
			{
				ga.transform.SetParent(pa.transform);
				ga.transform.position = Vector3.zero;
				ga.gameObject.name = pa.gameObject.name;
				ga.SetActive(false);
			}
			else
			{
				Destroy(ga);
				//				Debug.Log("Destroy " + ga.name);
			}
		}
	}

	public void RemoveChip(GameObject ga){
		if(ga != null)
		if(transform.Find(ga.name) != null)
		{
			GameObject pa = transform.Find(ga.name).gameObject;


			if(pa.transform.childCount <= 50)
			{
				ga.transform.SetParent(pa.transform);
				ga.transform.position = Vector3.zero;
				ga.gameObject.name = pa.gameObject.name;
				ga.SetActive(false);
			}
			else
			{
				Destroy(ga);
			}
		}
	}
}
