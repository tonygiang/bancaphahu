﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

[System.Serializable]
public class UserResult {

	public string item;
	public int count;

	public UserResult(ISFSObject obj){
		item = obj.GetUtfString ("item");
		count = obj.GetInt ("count");
	}
}
