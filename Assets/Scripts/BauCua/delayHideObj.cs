﻿using UnityEngine;
using System.Collections;

public class delayHideObj : MonoBehaviour {
	[SerializeField]
	float time;
	void OnEnable(){
		Invoke ("hide", time);
	}

	void hide(){
		gameObject.SetActive (false);
	}
}
