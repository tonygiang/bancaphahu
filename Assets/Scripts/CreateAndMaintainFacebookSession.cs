﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class CreateAndMaintainFacebookSession : MonoBehaviour
{
  void Awake()
  {
    DontDestroyOnLoad(gameObject);
    FB.Init(FB.ActivateApp);
  }

  void OnApplicationPause(bool pauseStatus)
  {
    if (pauseStatus == true) return;
    FB.Init(FB.ActivateApp);
  }
}
