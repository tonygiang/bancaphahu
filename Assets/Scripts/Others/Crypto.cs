﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System;
using System.Text;
using Utils;

public class Crypto
{
  static string keyStr = "1EAFECC3CDEBC4C234F24D576CE5D985";
  static SymmetricAlgorithm _algorithm = null;
  static SymmetricAlgorithm algorithm
  {
    get
    {
      if (_algorithm == null)
      {
        _algorithm = new RijndaelManaged();
        _algorithm.BlockSize = 128;
        _algorithm.KeySize = 128;
        _algorithm.Mode = CipherMode.CBC;
        _algorithm.Padding = PaddingMode.PKCS7;

        byte[] keyArr = Convert.FromBase64String(keyStr);
        byte[] KeyArrBytes32Value = new byte[24];
        Array.Copy(keyArr, KeyArrBytes32Value, 24);

        byte[] ivArr = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
        byte[] IVBytes16Value = new byte[16];
        Array.Copy(ivArr, IVBytes16Value, 16);

        _algorithm.Key = KeyArrBytes32Value;
        _algorithm.IV = IVBytes16Value;
      }
      return _algorithm;
    }
  }

  public static string Encrypt(string PlainText)
  {
    ICryptoTransform encrypto = algorithm.CreateEncryptor();

    byte[] plainTextByte = ASCIIEncoding.UTF8.GetBytes(PlainText);
    byte[] CipherText = encrypto.TransformFinalBlock(plainTextByte, 0, plainTextByte.Length);
    return Convert.ToBase64String(CipherText);

  }

  public static string Decrypt(string CipherText)
  {
    ICryptoTransform decrypto = algorithm.CreateDecryptor();

    byte[] encryptedBytes = Convert.FromBase64CharArray(CipherText.ToCharArray(), 0, CipherText.Length);
    byte[] decryptedData = decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length);
    return ASCIIEncoding.UTF8.GetString(decryptedData);
  }
}
