﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using System.Collections.Generic;

public class Loading : MonoBehaviour
{
  public static Loading instance;

  [SerializeField]
  GameObject loadingPanel, notiPanel;

  public List<GameObject> listNoti = new List<GameObject>();
  public List<GameObject> gobs = new List<GameObject>();
  [SerializeField] GameObject formNoti;
  [SerializeField] Image imgBgNoti;

  bool isRun = false;

  void Awake()
  {
    if (instance != null)
    {
      Destroy(gameObject);
      return;
    }
    instance = this;
    DontDestroyOnLoad(gameObject);
  }

  public void Show()
  {
    loadingPanel.SetActive(true);

  }

  public void Hide()
  {
    loadingPanel.SetActive(false);
  }

  public void ShowNotification(string content)
  {

    GameObject noti = Instantiate(formNoti);
    Text txt_content = noti.GetComponent<Text>();
    txt_content.text = content;

    if (noti == null) Debug.Log("noti is null");
    if (notiPanel == null) Debug.Log("notiPanel is null");

    noti.transform.SetParent(notiPanel.transform.GetChild(0).GetChild(0).transform, false);
    noti.GetComponent<RectTransform>().anchoredPosition = new Vector2(400, 0);
    listNoti.Add(noti);

    if (!isRun)
    {

      StartCoroutine(runNoti(content));
    }
  }



  IEnumerator runNoti(string _content)
  {
    isRun = true;

    //		while(imgBgNoti.color.a<1)
    //		{
    //			imgBgNoti.color += new Color(0, 0, 0, 0.02f);
    //			yield return new WaitForFixedUpdate();
    //
    //		}
    Vector3 _notiScale = imgBgNoti.transform.localScale;
    while (_notiScale.x < 1)
    {

      _notiScale.x += 0.1f;
      imgBgNoti.transform.localScale = _notiScale;
      yield return null;

    }
    imgBgNoti.transform.localScale = Vector3.one;

    List<GameObject> temp_gobs;
    if (gobs.Count == 0 && listNoti.Count > 0)
    {
      GameObject first = listNoti[0];
      gobs.Add(first);
      listNoti.RemoveAt(0);
    }

    do
    {
      temp_gobs = new List<GameObject>(gobs);
      foreach (GameObject gob in temp_gobs)
      {
        RectTransform rect = gob.GetComponent<RectTransform>();
        rect.anchoredPosition -= new Vector2(3, 0);
        if (rect.anchoredPosition.x + rect.rect.width < -400)
        {
          gobs.Remove(gob);
          Destroy(gob);
        }
        if (rect.anchoredPosition.x + rect.rect.width < -400 && listNoti.Count > 0 && gobs.Count <= 1)
        {

          GameObject next = listNoti[0];
          gobs.Add(next);
          listNoti.RemoveAt(0);
        }
      }
      temp_gobs.Clear();
      yield return new WaitForFixedUpdate();
    } while (gobs.Count > 0);

    //		while (imgBgNoti.color.a > 0)
    //		{
    //			imgBgNoti.color -= new Color(0, 0, 0, 0.02f);
    //			yield return new WaitForFixedUpdate();
    //
    //		}
    while (_notiScale.x > 0)
    {

      _notiScale.x -= 0.1f;
      imgBgNoti.transform.localScale = _notiScale;
      yield return null;

    }
    imgBgNoti.transform.localScale = new Vector3(0, 1f, 1f);
    isRun = false;
  }

}
