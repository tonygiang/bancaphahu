﻿using UnityEngine;
using System.Collections;

public class Blinking : MonoBehaviour {
	[SerializeField]
	Transform central;

	[SerializeField]
	float multiple = 1;

	float degreesPerSecond = 120;

	Vector3 v;

	void Start()
	{
		central = transform.parent;
		v = (transform.position - central.position) * multiple;
		transform.localEulerAngles = new Vector3(0, 0, Random.Range(0, 90));

	}

	void Update () {
		transform.Rotate(Vector3.forward * 2);

		v = Quaternion.AngleAxis (degreesPerSecond * Time.deltaTime, Vector3.back) * v;
		transform.position = central.position + v;
	}
}
