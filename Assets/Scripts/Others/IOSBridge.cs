﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Runtime.InteropServices;

public class IOSBridge : MonoBehaviour {
	#if UNITY_IOS
	[DllImport ("__Internal")]
	private static extern void OpenMessage(string number, string body);

	[DllImport ("__Internal")]
	private static extern void OpenURL(string url);

	[DllImport ("__Internal")]
	private static extern string GetCarrierCode();

	[DllImport ("__Internal")]
	private static extern string GetCarrierName();

	[DllImport ("__Internal")]
	private static extern void OpenPhone(string phoneNumber);

	public static void SendMess(string number, string body)
	{
		Debug.Log("number: " + number + "\nbody: " + body);
		OpenMessage(number, body);
	}

	public static void OpenLink(string url)
	{
		OpenURL(url);
	}

	public static string GetCCode()
	{
		return GetCarrierCode();
	}

	public static string GetCName()
	{
		return GetCarrierName();
	}

	public static void MakePhoneCall(string number)
	{
		OpenPhone (number);
	}
	#endif
}