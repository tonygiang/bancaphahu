﻿using UnityEngine;
using System.Collections;
using System;

public class Global : MonoBehaviour {

	public static void OpenStore()
	{
		#if UNITY_ANDROID
		Application.OpenURL("https://bancaphahu.xyz/");
		#elif UNITY_IPHONE
//        Application.OpenURL("itms-apps://itunes.apple.com/app/id1255364467");
		Application.OpenURL("https://bancaphahu.xyz/");
		#endif
	}

	public static string unEscape(string str)
	{
		return System.Text.RegularExpressions.Regex.Unescape(str);
	}

	public static string CoinToString(int coin)
	{
		char[] charArray = coin.ToString().ToCharArray();
		string str = "";
		int count = 0;
		for(int i = charArray.Length - 1; i >= 0; --i)
		{
			str += charArray[i];
			++count;
			if( count % 3 == 0 && count < charArray.Length )
			{
				str += ".";
			}
		}

		charArray = str.ToCharArray();
		Array.Reverse(charArray);
		return new string(charArray);
	}

	public static string XuToString(int xu)
	{
		char[] charArray = xu.ToString().ToCharArray();
		string str = "";
		int count = 0;
		for(int i = charArray.Length - 1; i >= 0; --i)
		{
			str += charArray[i];
			++count;
			if( count % 3 == 0 && count < charArray.Length )
			{
				str += ".";
			}
		}

		charArray = str.ToCharArray();
		Array.Reverse(charArray);
		return new string(charArray);
	}
}
