﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class SetScreenTimeout : RunFixedTimePerLaunchBehaviour
{
  [SerializeField] int SecondsUntilTimeout = SleepTimeout.NeverSleep;

  void Start()
  {
    StartEvent.AddListener(() => Screen.sleepTimeout = SecondsUntilTimeout);
  }
}
