﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sfs2X.Entities.Data;

public class Version{

	public string function;
	public string exchangerate;
	public string reason;
//	public string cpName;
//	public string smsCode;
//	public string smsNumbers;
//	public string disCountCard;
//	public string disCountSMS;
//	public string disCountIAP;

	public int versionCode;
	public int state;

	public int versionFish;
	public int versionBullet;
	public int versionCheckin;
	public int versionQuest;
	public int versionCard;
	public int versionSms;

	public Version (ISFSObject obj)
	{
		this.versionCode = obj.GetInt ("versioncode");
		this.function = obj.GetUtfString("function");
		this.state = obj.GetInt ("state");
		this.reason = obj.GetUtfString("reason");
		this.exchangerate = obj.GetUtfString ("exchangerate");

//		this.versionFish = obj.GetInt ("version_fish");
//		this.versionBullet = obj.GetInt ("version_bullet");
//		this.versionCheckin = obj.GetInt ("version_checkin");
//		this.versionQuest = obj.GetInt ("version_quest");
//		this.versionCard = obj.GetInt ("version_card");
//		this.versionSms = obj.GetInt ("version_sms");
		//{"version_fish":1,"version_bullet":1,"version_checkin":null,"version_quest":null,"version_card":null,"version_sms":null,"exchangerate":"1.2,1.2,1.2,1.2,1.2","state":1,"reason":"None"}
	}

	public void CheckVerMessBoxCallBack(MessageBoxCallBack callBack)
	{
		if(callBack.Equals(MessageBoxCallBack.OK))
		{
			Global.OpenStore();
		}
	}

	public void BaoTriServer(MessageBoxCallBack callBack)
	{
		if(callBack.Equals(MessageBoxCallBack.OK))
		{
			Application.Quit ();
		}
	}
}
