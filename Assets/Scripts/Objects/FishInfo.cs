﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

[System.Serializable]
public class FishInfo {
	public string Type;
	public float Speed;
	public int Mutiplie;
	public float Radius;
	public string Name;

	public FishInfo(ISFSObject obj){
		Type = obj.GetUtfString ("type");
		Speed = obj.GetFloat ("speed");
		Mutiplie = obj.GetInt ("mutiplie");
		Radius = obj.GetFloat ("radius");
		Name = obj.GetUtfString ("name");
	}

}
