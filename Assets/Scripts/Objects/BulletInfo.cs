﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

[System.Serializable]
public class BulletInfo  {
	public  string Type;
	public int Cost;
	public float Speed;

	public BulletInfo(ISFSObject obj){
		Type = obj.GetUtfString ("id");
		Cost = obj.GetInt ("cost");
		Speed = obj.GetFloat ("speed");
	}

}
