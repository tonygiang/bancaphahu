using UnityEngine;
using Sfs2X;
using Sfs2X.Requests;
using Sfs2X.Core;
using Sfs2X.Entities.Variables;
using System.Collections.Generic;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Util;
using Sfs2X.Requests.Buddylist;
using Sfs2X.Entities.Invitation;
using System;
using BE;
using Utils;
using System.Linq;

public class SFSManager : MonoBehaviour
{
  public static SFSManager instance;

  //pubic variale
  //============================================================================

  //	private int ServerPort = 9933;
  //    private string zoneName = "BancaV2";
  private string zoneName = "Cakiem";
  private string defaultRoom = "lobby";
  public int clientServerLag;

  public bool isStartTime = false;

  //private variable
  //============================================================================
#if UNITY_WEBGL
  public static SmartFox sfs = new SmartFox(UseWebSocket.WSS);
#else
  public static SmartFox sfs = new SmartFox();
#endif
  public int roomValue = 0;
  ConfigData cfg;


  public bool isLobby = false;
  public bool isDisconnect = false;

  public bool isLogInSfs = false;
#if UNITY_ANDROID
  string client = "android";
  string brand = "android_cakiem";

#elif UNITY_IOS
	string client = "ios";
	string brand = "ios_cakiem";

#elif UNITY_WEBGL
	string client = "webgl";
	string brand = "webgl_cakiem";

#else
  string client = "webgl";
  string brand = "webgl_cakiem";
#endif
  //	float timeLoad;
  //	bool istimeLoad=false;

  void Awake()
  {
    if (instance != null)
    {
      Destroy(this);
      return;
    }
    DontDestroyOnLoad(this);
    instance = this;
    Init();
  }

  void OnApplicationQuit()
  {
    sfs.RemoveAllEventListeners();
    sfs.Send(new LogoutRequest());
    if (sfs.IsConnected) sfs.Disconnect();
  }

  public void Init()
  {
    cfg = new ConfigData();
    cfg.Host = Network.SmartfoxProxyIP;
    cfg.Port = Network.SmartfoxProxyPort;
    cfg.Zone = "Cakiem";
    if (AppInfo.CurrentPlatform == Platform.WEBGL) sfs.ThreadSafeMode = true;
    if (Debug.isDebugBuild)
    {
      sfs.Debug = true;
    }

    sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
    sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnDisconnected);
    sfs.AddEventListener(SFSEvent.LOGIN, OnLogIn);
    sfs.AddEventListener(SFSEvent.LOGOUT, OnLogOut);
    sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoom);
    sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionRespone);
    sfs.AddEventListener(SFSEvent.PING_PONG, OnPingPong);
    sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, OnEnterRoom);
  }

  void OnLogIn(BaseEvent e)
  {
    sfs.Send(new JoinRoomRequest(defaultRoom));
    sfs.EnableLagMonitor(true);
  }

  void OnLogOut(BaseEvent e)
  {
    UIMenu.Instance.LogOutSuccess();
  }

  void OnJoinRoom(BaseEvent e)
  {
    Room room = (Room)e.Params["room"];

    if (room.Name == "lobby")
    {

      NetworkManager.instance.LobbyRoom = sfs.GetRoomByName(room.Name);
      isLobby = true;
      isLogInSfs = true;
    }

    if (room.Name == "taixiu")
    {
      NetworkManager.instance.TaixiuRoom = sfs.GetRoomByName(room.Name);
    }
    if (room.GroupId == "arena")
    {
      NetworkManager.instance.ArenaRoom = sfs.GetRoomByName(room.Name);
      isLobby = false;
    }
    if (room.GroupId == "baucua")
    {
      Debug.Log("join room baucua");
      NetworkManager.instance.LobbyRoom = sfs.GetRoomByName(room.Name);
      isLobby = false;
    }

  }

  void OnConnection(BaseEvent e)
  {
    if ((bool)e.Params["success"])
      sfs.Send(new LoginRequest(UserManager.UserName, "", zoneName));
    else if (sfs.IsConnected)
      sfs.Send(new LoginRequest(UserManager.UserName, "", zoneName));
  }

  void OnPingPong(BaseEvent e)
  {
    clientServerLag = (int)e.Params["lagValue"] / 2;
    //        if (Debug.isDebugBuild)
    //            Debug.Log("Ping " + clientServerLag);
  }

  void ResponseCode(string cmd, ISFSObject isfs)
  {

    string code = isfs.GetText("code");
    Dictionary<string, object> value = null;
    SaveLoadData.listResponseCode.TryGetValue(code, out value);
    if (value == null)
    {
      return;
    }
    object objMessage;
    value.TryGetValue("message", out objMessage);
    string message = objMessage.ToString();

    int gold = 0;
    if (value.ContainsKey("gold"))
    {
      gold = isfs.GetInt("gold");
      message = message.Replace("gold", SaveLoadData.FormatMoney(gold));
    }

    int gem = 0;
    if (value.ContainsKey("gem"))
    {
      gem = isfs.GetInt("gem");
      message = message.Replace("gem", SaveLoadData.FormatMoney(gem));

    }

    int bet = 0;
    if (value.ContainsKey("bet"))
    {
      bet = isfs.GetInt("bet");
      message = message.Replace("bet", SaveLoadData.FormatMoney(bet));
    }

    string displayname = "";
    if (value.ContainsKey("displayname"))
    {

      displayname = isfs.GetText("displayname");
      message = message.Replace("displayname", displayname);
    }

    string roomName = "";
    if (value.ContainsKey("room"))
    {
      roomName = displayname = isfs.GetText("room");
      message = message.Replace("room", roomName);

    }

    if (value.ContainsKey("price"))
    {
      int _price = isfs.GetInt("price");

      message = message.Replace("price", SaveLoadData.FormatMoney(_price));
    }
    if (value.ContainsKey("operator"))
    {
      string _operator = isfs.GetUtfString("operator");
      message = message.Replace("operator", _operator.ToString());
    }


    switch (cmd)
    {
      case "notification":
        Loading.instance.ShowNotification(message);
        break;

      case "private_popup":
        object objTypePopUp;
        value.TryGetValue("type", out objTypePopUp);
        string strTypePopUp = objTypePopUp.ToString();
        if (strTypePopUp == "normal")
        {

        }
        else if (strTypePopUp == "gift")
        {
          //UserManager.Coin += gold;
          BanCa.Session.Gem.Value += gem;
        }
        else if (strTypePopUp == "purchaser")
        {
          BanCa.Session.Gem.Value += gem;
          UserManager.LoadVipBuyGem(gem);
        }

        MessageBox.instance.Show("THÔNG BÁO", message.ToString());
        if (PlayCoinManager.Instance != null)
        {
          PlayCoinManager.Instance.gunCoinManager.BuyGoldGemComplete(BanCa.Session.Gem.Value);
        }
        else if (PlayGemManager.Instance != null)
        {
          PlayGemManager.Instance.gunGemManager.BuyGoldGemComplete(BanCa.Session.Gem.Value);
        }
        Loading.instance.Hide();
        break;

      case "res_exchange_money":
        MessageBox.instance.Show(message);

        if (isfs.ContainsKey("gold"))
        {
          if (PlayCoinManager.Instance != null)
          {
            PlayCoinManager.Instance.gunCoinManager.BuyGoldGemComplete(gem);
          }
          else if (PlayGemManager.Instance != null)
          {
            PlayGemManager.Instance.gunGemManager.BuyGoldGemComplete(gem);
          }
          BanCa.Session.Gem.Value = gem;
        }
        Loading.instance.Hide();
        break;

      case "invite_user_arena":
        MessageBox.instance.OpenInvite(message, roomName, bet);
        break;

      case "res_user_shoot":
        // MessageBox.instance.Show(message);
        break;
      case "res_user_join_room_gem":
        MessageBox.instance.Show(message);
        Loading.instance.Hide();
        break;
      case "res_user_join_room_gold":
        MessageBox.instance.Show(message);
        Loading.instance.Hide();
        break;
      case "res_set_displayname":
        MessageBox.instance.Show(message);
        if (isfs.ContainsKey("displayname"))
        {
          string _displayname = isfs.GetText("displayname");
          UserManager.SET_DISPLAY_NAME(_displayname);
        }
        Loading.instance.Hide();
        break;
      case "res_user_finish_quest":
        MessageBox.instance.Show(message);
        Loading.instance.Hide();
        break;

      case "res_user_request_arena":

        MessageBox.instance.Show(message);
        Loading.instance.Hide();
        break;

      case "res_exchangecard":
        MessageBox.instance.Show(message);
        Loading.instance.Hide();
        break;

    }


  }


  void OnExtensionRespone(BaseEvent e)
  {
    string cmd = e.Params["cmd"].ToString();
    ISFSObject obj = (ISFSObject)e.Params["params"];
    int message_code = 0;
    message_code = (obj.ContainsKey("message_code") ? obj.GetInt("message_code") : 0);

    string dontDisplay = "res_user_shoot,add_fish,bonus_speed,fish_die,update_taixiu,res_update_jackpot_value,res_slot_jackpot,phase_taixiu,update_coin";
    if (!dontDisplay.Contains(cmd))
    {
      if (Debug.isDebugBuild)
        Debug.Log(string.Format("cmd: {0}  : {1}", cmd, obj.ToJson()));
    }

    if (obj != null && obj.ContainsKey("code"))
      ResponseCode(cmd, obj);
    int _messageCode = 0;
    string _mess;

    switch (cmd)
    {

      #region Scene_Menu
      case "update_coin":
        if (obj.HasKey("gem")) BanCa.Session.Gem.Value = obj.GetInt("gem");
        if (obj.HasKey("key"))
          BanCa.Session.TreasureKeys.Value = obj.GetInt("key");
        break;
      case "gold":
        SaveLoadData.Instance.SetGoldNumber(obj);
        break;
      case "gem":
        SaveLoadData.Instance.SetGemNumber(obj);
        break;

      case "get_token":
        SendSentToken();
        break;

      case "res_sent_token":
        if (message_code != 1)
          MessageBox.instance.Show(obj.GetUtfString("message"));
        else
        {
          BanCa.Session.FirebaseID = obj.GetUtfString("FirebaseId");
          BanCa.Session.PhoneNumber = obj.GetUtfString("PhoneNumber");
          UIMenu.Instance.LogInSuccess();
        }
        Loading.instance.Hide();
        break;
      case "Public_Popup":
        string _type = obj.GetUtfString("type");
        MessageBox.instance.Show(obj.GetUtfString("content"));
        Loading.instance.Hide();
        break;

      case "res_mail":
        if (obj.GetBool("haveNewMail")) Network
          .PostToServer("BehindLogin", ("method", "getmailbox"))
          .Then(handler =>
          {
            ISFSObject sfsResponse = handler.text.PassTo(Crypto.Decrypt)
              .PassTo(SFSObject.NewFromJsonData);
            if (sfsResponse.GetInt("message_code") != 1)
              MessageBox.instance.Show(sfsResponse.GetUtfString("message"));
            else Inbox.Instance.GetMail(sfsResponse.GetSFSArray("list"));
          });
        break;

      #endregion
      // ------- SCENE CHUNG ---------//
      #region Scene_CHUNG
      case "res_get_all_fish":
        if (AppInfo.ActiveScene.name == "PlayCoin")
          PlayCoinManager.Instance.LoadFishInRoom(obj);
        else if (AppInfo.ActiveScene.name == "PlayGem")
          PlayGemManager.Instance.LoadFishInRoom(obj);
        BGManager.instance.SetMapWeather(obj);
        break;

      case "res_user_shoot":
        if (obj.GetInt("message_code") != 1) break;
        if (AppInfo.ActiveScene.name == "PlayCoin")
          PlayCoinManager.Instance.AddBullet(obj, clientServerLag);
        else if (AppInfo.ActiveScene.name == "PlayGem")
          PlayGemManager.Instance.AddBullet(obj, clientServerLag);
        break;
      case "fish_die":
        if (AppInfo.ActiveScene.name == "PlayCoin")
          PlayCoinManager.Instance.FishRemove(obj);
        else if (AppInfo.ActiveScene.name == "PlayGem")
          PlayGemManager.Instance.FishRemove(obj);
        break;
      case "add_fish":
        if (AppInfo.ActiveScene.name == "PlayCoin")
          PlayCoinManager.Instance.AddFishSingle(obj);
        if (AppInfo.ActiveScene.name == "PlayGem")
          PlayGemManager.Instance.AddFishSingle(obj);
        break;
      case "res_user_out_room":
        if (AppInfo.ActiveScene.name == "PlayCoin")
          PlayCoinManager.Instance.OpenResult(obj);
        else if (AppInfo.ActiveScene.name == "PlayGem")
          PlayGemManager.Instance.OpenResult(obj);
        else if (AppInfo.ActiveScene.name == "BauCua")
          BauCuaManager.Instance.OpenResult(obj);
        else if (obj.GetInt("message_code") != 1)
          MessageBox.instance.Show(obj.GetUtfString("message"));

        break;

      case "res_user_change_gun":
        if (AppInfo.ActiveScene.name == "PlayCoin")
          PlayCoinManager.Instance.gunCoinManager.SetGun(obj);
        else if (AppInfo.ActiveScene.name == "PlayGem")
          PlayGemManager.Instance.gunGemManager.SetGun(obj);
        break;

      case "res_public_chat":
        string _uname = obj.GetUtfString("uname");
        string _content = obj.GetUtfString("content");
        string _emo = obj.GetUtfString("displayname");
        if (TaiXiu.Instance.TaiXiuPanel.activeInHierarchy)
        {
          MessageController.instance.showLobbyMessage(_uname, _content, _emo);
        }
        break;


      case "change_wave":
        BanCa.Game.Events.OnNewWave.Invoke();
        if (BGManager.instance != null)
          BGManager.instance.ChangeBackGround(obj);
        break;
      case "weather":
        if (BGManager.instance != null)
          BGManager.instance.Weather(obj.GetUtfString("weather"));
        break;
      case "res_update_jackpot_value":
        if (AppInfo.ActiveScene.name == "PlayCoin")
          GunCoinManager.Instance.GetJackPot(obj);
        if (AppInfo.ActiveScene.name == "PlayGem")
          GunGemManager.Instance.GetJackPot(obj);
        if (AppInfo.ActiveScene.name == "Menu")
          UIMenu.Instance.GetJackPot(obj);
        break;

      case "res_slot_jackpot":
        if (AppInfo.ActiveScene.name == "SlotGame")
          SceneSlotGame.instance.GetJackPot(obj);
        else if (AppInfo.ActiveScene.name == "Menu")
          UIMenu.Instance.GetSlotJackPot(obj);
        break;
      case "bonus_speed":
        if (AppInfo.ActiveScene.name == "PlayCoin")
          PlayCoinManager.Instance.Set_FishSpeed(obj);
        else if (AppInfo.ActiveScene.name == "PlayGem")
          PlayGemManager.Instance.Set_FishSpeed(obj);
        break;
      case "res_user_finish_quest":
        if (!obj.ContainsKey("code"))
        {
          BanCa.Session.Gem.Value = obj.GetInt("gem");
          MissionManager.Instance.RefresfMission(obj);
        }
        Loading.instance.Hide();
        break;
      case "res_news_and_events":
        EventShower.Instance.GetEvent(obj);
        break;

      #endregion


      // ------- SCENE PLAY COIN ---------//
      #region Scene_Play_Coin
      case "res_user_join_room_gold":
        if (message_code == 1)
        {
          NetworkManager.instance.LobbyRoom =
            sfs.GetRoomByName(obj.GetUtfString("room_name"));

          isStartTime = true;
          LoadLevel.Instance.ShowLoadingPanel("PlayCoin", "Đang tải dữ liệu phòng chơi...");
        }
        Loading.instance.Hide();
        break;
      case "res_get_all_user_room_gold":
        PlayCoinManager.Instance.gunCoinManager.LoadGunUser(obj);
        break;

      case "add_user_room_gold":
        if (PlayCoinManager.Instance != null)
          PlayCoinManager.Instance.gunCoinManager.AddUser(obj);
        break;

      #endregion


      #region Scene_Play_Gem
      case "res_user_join_room_gem":
        Loading.instance.Hide();
        if (message_code != 1) break;
        string roomName = obj.GetUtfString("room_name");
        NetworkManager.instance.LobbyRoom = sfs.GetRoomByName(roomName);

        isStartTime = true;
        LoadLevel.Instance.ShowLoadingPanel("PlayGem", "Đang tải dữ liệu phòng chơi...");
        break;
      case "res_get_all_user_room_gem":
        PlayGemManager.Instance.gunGemManager.LoadGunUser(obj);
        break;
      case "add_user_room_gem":
        if (PlayGemManager.Instance != null)
        {
          PlayGemManager.Instance.gunGemManager.AddUser(obj);
        }
        break;


      #endregion

      #region ----- TAI XIU -----

      case "phase_taixiu":
        if (TaiXiu.Instance != null) TaiXiu.Instance.Phase(obj);
        break;
      case "res_user_bet_taixiu":
        BanCa.Session.Gem.Value = obj.GetInt("gem");
        if (TaiXiu.Instance != null) TaiXiu.Instance.ResBetUserTaiXiu(obj);
        break;
      case "back_money":
        BanCa.Session.Gem.Value = obj.GetInt("gem");
        if (TaiXiu.Instance != null) TaiXiu.Instance.BackMoney(obj);
        break;
      case "result_taixiu":
        if (TaiXiu.Instance != null) TaiXiu.Instance.ResultTaiXiu(obj);
        break;
      case "update_taixiu":
        if (TaiXiu.Instance != null) TaiXiu.Instance.UpdateTaiXiu(obj);
        break;
      case "reward_taixiu":
        BanCa.Session.Gem.Value = obj.GetInt("gem");
        if (TaiXiu.Instance != null) TaiXiu.Instance.RewardTaiXiu(obj);
        break;
      case "res_bxh_taixiu":
        if (TaiXiu.Instance != null) TaiXiu.Instance.setBxhTaixiu(obj);
        break;
      case "res_history_taixiu":
        if (TaiXiu.Instance != null) TaiXiu.Instance.setHistoryTaiXiu(obj);
        break;
      case "res_thongke_taixiu":
        if (TaiXiu.Instance != null) TaiXiu.Instance.SetThongKeTaiXiu(obj);
        break;
      #endregion

      #region UPDATE FUNCTION
      case "Update_Function":
        if (obj.ContainsKey("func"))
          BanCa.Session.Functions = obj.GetUtfString("func").Split(',');
        break;
      #endregion

      #region DROP USER
      case "Drop_User":

        SaveLoadData.Instance.DropUser(obj);
        break;
      #endregion

      #region BAUCUA
      case "res_user_request_baucua":
        _messageCode = obj.GetInt("message_code");
        _mess = obj.GetUtfString("message");
        if (_messageCode != 1)
          MessageBox.instance.Show(_mess);
        else
          LoadLevel.Instance.ShowLoadingPanel("BauCua", "Đang tải thông tin phòng chơi...");
        Loading.instance.Hide();
        break;
      case "phase_baucua":
        if (BauCuaManager.Instance != null)
          BauCuaManager.Instance.PhaseBauCua(obj);
        break;

      case "res_bet":
        if (BauCuaManager.Instance == null) break;
        _messageCode = obj.GetInt("message_code");
        _mess = obj.GetUtfString("message");
        if (_messageCode != 1)
          MessageBox.instance.Show(_mess);
        else
          BauCuaManager.Instance.ResBet(obj);
        break;
      case "result":
        if (BauCuaManager.Instance != null)
          BauCuaManager.Instance.RESULT(obj);
        break;
      case "res_baucua_result":
        if (BauCuaManager.Instance != null)
          BauCuaManager.Instance.getHistoryList(obj);
        break;
      case "reward":
        if (BauCuaManager.Instance != null)
          BauCuaManager.Instance.REWARD(obj);
        break;
      case "add_user":
        if (BauCuaManager.Instance != null)
          BauCuaManager.Instance.ADDUSER(obj);
        break;
      case "all_user_baucua":
        SaveLoadData.Instance.AllUserBauCua = obj;
        break;

      #endregion
      case "notification":
        Loading.instance.ShowNotification(obj.GetUtfString("message"));
        break;
      case "res_exchange_rate":
        EXCHANGE_CARD.Instance.LOADEXCHANGE(obj);
        Loading.instance.Hide();
        break;

      case "res_iap_purchase":
        Shop.Instance.GetIapData(obj);
        break;
      case "res_smartman":
        if (Shop.Instance.shopPanel.activeInHierarchy)
          Shop.Instance.LoadSmartUi(obj);
        else
          EXCHANGE_CARD.Instance.LoadSmartUi(obj);
        Loading.instance.Hide();
        break;
      case "res_giftcode":
        if (message_code == 0)
          MessageBox.instance.Show("THÔNG BÁO", obj.GetUtfString("message"));
        GiftcodeManager.Instance.inputGiftcode.text = "";
        Loading.instance.Hide();
        break;
      case "res_slot_roll_history":
        SceneSlotGame.instance.SetDataLichSu(obj);
        Loading.instance.Hide();
        break;
      case "res_slot_vinh_danh":
        SceneSlotGame.instance.SetDataVinhDanh(obj);
        Loading.instance.Hide();
        break;
      case "res_user_join_room_slot":
        SceneSlotGame.instance.GetGemInfo(obj);
        break;
      case "res_slot_roll":
        SceneSlotGame.instance.OnButtonSpin(obj);
        break;
      case "res_user_checkin":
        FreeManager.Instance.RefresfCheckIn(obj);
        Loading.instance.Hide();
        break;
    }


  }


  void OnDisconnected(BaseEvent e)
  {
    LoadLevel.Instance.HideLoadingPanel();
    SaveLoadData.Instance.SendUpdateMission();
    SaveLoadData.Instance.isKick = true;
    LoadLevel.Instance.ShowLoadingPanel("Menu");

  }

  void OnEnterRoom(BaseEvent e)
  {

    Room room = (Room)e.Params[("room")];
    User user = (User)e.Params[("user")];

    // Debug.Log("User " + user.Name + " just joined Room " + room.Name);
  }

  public void Login()
  {
    if (!sfs.IsConnected) sfs.Connect(cfg);
    else sfs.Send(new LoginRequest(UserManager.UserName, "", zoneName));
  }

  void Update()
  {
    if (sfs != null) sfs.ProcessEvents();
  }

  void LoadPage(MessageBoxCallBack result)
  {
    if (result.Equals(MessageBoxCallBack.OK))
    {
      Application.OpenURL("https://cakiem68.club");
    }
  }
  //    void FixedUpdate()
  //    {
  //        if (isStartTime)
  //        {
  //            timeLoadScene++;
  //        }
  //
  //    }



  public void PlayCoin(int value)
  {
    Loading.instance.Show();
    Play(value);
    roomValue = value;
  }

  void Play(int value)
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutInt("room_type", value);
    obj2.PutUtfString("token", UserManager.AccessToken);
    obj2.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_join_room_gold", obj2, NetworkManager.instance.LobbyRoom));
  }

  #region JOIN ROOM GEM

  public void PlayGem(int _value)
  {
    Loading.instance.Show();
    JoinGem(_value);
    roomValue = _value;
  }

  void JoinGem(int value)
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutInt("room_type", value);
    obj2.PutUtfString("token", UserManager.AccessToken);
    obj2.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_join_room_gem", obj2, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  public void joinRoomSlot()
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_join_room_slot", obj2, NetworkManager.instance.LobbyRoom));
  }

  public void joinSlot()
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutUtfString("token", UserManager.AccessToken);
    sfs.Send(new ExtensionRequest("slot_in_room", obj2, NetworkManager.instance.LobbyRoom));
  }

  public void OutSlot()
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutUtfString("token", UserManager.AccessToken);
    sfs.Send(new ExtensionRequest("slot_out_room", obj2, NetworkManager.instance.LobbyRoom));
  }

  public void Roll(int roomID, int BetValue, List<int> Lines)
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutUtfString("userName", UserManager.UserName);
    obj2.PutInt("roomId", roomID);
    obj2.PutInt("betValue", BetValue);
    obj2.PutIntArray("nLine", Lines.ToArray());
    sfs.Send(new ExtensionRequest("slot_roll", obj2, NetworkManager.instance.LobbyRoom));
  }

  void HideLoading()
  {
    Loading.instance.Hide();
  }





  #region ----- REQUEST TAI XIU -----

  public void SentBet(int _betValue, string _betType)
  {
    SFSObject obj = new SFSObject();
    obj.PutInt("money", _betValue); // 1000 or 2000 .....
    obj.PutUtfString("door", _betType);  // tai or xiu
    obj.PutUtfString("uid", UserManager.UserName);
    if (Debug.isDebugBuild)
      Debug.Log(obj.ToJson() + "  " + sfs);
    sfs.Send(new ExtensionRequest("user_bet_taixiu", obj, NetworkManager.instance.LobbyRoom));
  }

  public void CancelBet()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_cancel_taixiu", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GetBXHTaiXiu()
  {

    SFSObject obj = new SFSObject();
    obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("bxh_taixiu", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GetHistoryTaiXiu()
  {

    SFSObject obj = new SFSObject();
    obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("history_taixiu", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GetThongKeTaiXiu()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("thongke_taixiu", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region slot

  public void GetHistorySlot()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("slot_roll_history", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GetTopSlot()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("slot_vinh_danh", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ----- REQUEST NHIEM VU  -----

  public void SentUserFinishQuest(string _questId)
  {
    Loading.instance.Show();
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    obj.PutUtfString("questid", _questId);
    sfs.Send(new ExtensionRequest("user_finish_quest", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ----- REQUEST CHECKIN    -----

  public void SentUserCheckIn()
  {
    Loading.instance.Show();
    SFSObject obj = new SFSObject();
    obj.PutText("token", UserManager.AccessToken);
    sfs.Send(new ExtensionRequest("user_checkin_7day", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ----- REQUEST ECHANGE GEM    -----

  public void SentExchangeGem(int _gem)
  {
    if (BanCa.Session.Gem.Value >= _gem)
    {
      if (_gem > 0)
      {
        Loading.instance.Show();
        SFSObject obj = new SFSObject();
        obj.PutInt("gem", _gem);
        sfs.Send(new ExtensionRequest("exchange_money", obj, NetworkManager.instance.LobbyRoom));
      }
      else
      {
        MessageBox.instance.Show("Ban chưa nhập kim cương");
      }
    }
    else
    {
      //notdiamond
      MessageBox.instance.Show("Bạn không đủ kim cương. Nạp thêm kim cương hoặc nhận quà hằng ngày để tiêp tục chơi game nhé!");
    }
  }

  #endregion

  #region ----- REQUEST PUBLIC CHAT    -----

  public void SendPublicChat(string _content, string _emo)
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("content", _content);
    obj.PutUtfString("displayname", _emo);
    obj.PutUtfString("uname", UserManager.UserName);
    sfs.Send(new ExtensionRequest("public_chat", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ----- REQUEST SHARE FACEBOOK    -----

  public void SendShareFB()
  {
    SFSObject obj = new SFSObject();
    sfs.Send(new ExtensionRequest("share_facebook", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region --- REQUEST AIP ---

  //if android
#if UNITY_ANDROID
  public void IAP(string inAppID, string cost, string orderid, string cpname, string googleResponse)
  {
    ISFSObject obj = new SFSObject();
    obj.PutUtfString("method", "iap");
    obj.PutUtfString("client", "android");
    obj.PutUtfString("token", UserManager.AccessToken);
    obj.PutUtfString("product", inAppID);
    obj.PutUtfString("cost", cost);
    obj.PutUtfString("orderid", orderid);
    obj.PutUtfString("cpname", cpname);
    obj.PutUtfString("googleresponse", !string.IsNullOrEmpty(googleResponse) ? googleResponse : "");
    //	obj.PutUtfString ("googlesign", googleSign);

    sfs.Send(new ExtensionRequest("iap_purchase", obj, NetworkManager.instance.LobbyRoom));
    Loading.instance.Show();
  }

#elif UNITY_IOS
	public void IAP (string inAppID, string cost, string orderid, string cpname, string appleResponse)
	{
		ISFSObject obj = new SFSObject ();
		obj.PutUtfString ("method", "iap");
		obj.PutUtfString ("client", "ios");
		obj.PutUtfString ("token", UserManager.AccessToken);
		obj.PutUtfString ("product", inAppID);
		obj.PutUtfString ("cost", cost);
		obj.PutUtfString ("orderid", orderid);
		obj.PutUtfString ("cpname", cpname);
		obj.PutUtfString ("appleresponse", appleResponse);

		Loading.instance.Show ();
		sfs.Send (new ExtensionRequest ("iap_purchase", obj, NetworkManager.instance.LobbyRoom));
	}
#endif


  #endregion

  #region ----- REQUEST SET FUNCTION   -----

  public void SendSetFunction()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("uid", UserManager.UserName);
    obj.PutUtfString("func", string.Join(",", BanCa.Session.Functions));
    sfs.Send(new ExtensionRequest("set_function", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ------ REQUEST SEND TOKEN ------

  public void SendSentToken()
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutUtfString("user_name", UserManager.UserName);
    obj2.PutUtfString("token", UserManager.AccessToken);
    string _nameAva = UserManager.AvatarLink;
    //        if (UserManager.isQuickLogin)
    //        {
    //            _nameAva = UserManager.avatarName;
    //        }
    //        else
    //        {
    //            _nameAva = UserManager.AvatarLink;
    //        }
    obj2.PutUtfString("avatar", _nameAva);
    sfs.Send(new ExtensionRequest("sent_token", obj2, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ----- REQUEST GET ALL USER ROOM GOLD -----

  public void GetAllUserRoomGold()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("get_all_user_room_gold", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ----- REQUEST GET ALL USER ROOM GEM -----

  public void GetAllUserRoomGem()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("get_all_user_room_gem", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GetGemJackPot()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("update_jackpot_value", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GetSlotJackPot()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("slot_jackpot", obj, NetworkManager.instance.LobbyRoom));
  }

  public void SlotPing()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("get_ping", obj, NetworkManager.instance.LobbyRoom));
  }

  public void UpdateGem()
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("get_coin", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  #region ----- REQUEST GET ALL FISH -----

  public void GetAllFish()
  {
    sfs.Send(new ExtensionRequest("get_all_fish", new SFSObject(), NetworkManager.instance.LobbyRoom));
  }

  #endregion

  public void SendGiftCode(string code)
  {
    SFSObject obj = new SFSObject();
    obj.PutUtfString("username", UserManager.UserName);
    obj.PutUtfString("code", code);
    obj.PutUtfString("client", client);

    sfs.Send(new ExtensionRequest("giftcode", obj, NetworkManager.instance.LobbyRoom));
  }

  public void USER_OUT_ROOM()
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    sfs.Send(new ExtensionRequest("user_out_room", obj, NetworkManager.instance.LobbyRoom));
  }

  public void USER_OUT_ROOM_ARENA()
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    sfs.Send(new ExtensionRequest("user_out_room", obj, NetworkManager.instance.ArenaRoom));
    SoundManager.Instance.ButtonSound();
  }

  public void USER_REQUEST_ARENA()
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    obj.PutInt("bet", SaveLoadData.Instance._betArena);
    sfs.Send(new ExtensionRequest("user_request_arena", obj, NetworkManager.instance.LobbyRoom));
  }

  public void VALID_REQUEST_ARENA(string _roomName, int _bet)
  {
    ISFSObject _exten = new SFSObject();
    _exten.PutUtfString("Room_Name", _roomName);
    _exten.PutInt("bet", _bet);
    sfs.Send(new ExtensionRequest("valid_request_arena", _exten, NetworkManager.instance.LobbyRoom));
    Loading.instance.Show();
  }



  public void USER_CHANGE_GUN(int gunCurrent, int _gunType)
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutInt("bullettype", gunCurrent);
    obj2.PutInt("guntype", _gunType);
    obj2.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_change_gun", obj2, NetworkManager.instance.LobbyRoom));
    Loading.instance.Show();
  }

  public void USER_SHOOT(float z, string bulletCurrent)
  {
    ISFSObject obj2 = new SFSObject();
    obj2.PutFloat("angle", z);
    obj2.PutUtfString("bullettype", bulletCurrent);
    obj2.PutUtfString("username", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_shoot", obj2, NetworkManager.instance.LobbyRoom));
  }

  #region BAUCUA

  public void USER_REQUEST_BAUCUA()
  {
    ISFSObject _obj = new SFSObject();
    _obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_request_baucua", _obj, NetworkManager.instance.LobbyRoom));
    Loading.instance.Show();
  }

  public void BET_ON(string _bet, int _money)
  {
    ISFSObject _obj = new SFSObject();
    _obj.PutUtfString("uid", UserManager.UserName);
    _obj.PutUtfString("bet", _bet);
    _obj.PutInt("money", _money);
    sfs.Send(new ExtensionRequest("bet", _obj, NetworkManager.instance.LobbyRoom));
  }

  public void GetListHistoryBauCua()
  {
    ISFSObject _obj = new SFSObject();
    _obj.PutUtfString("uid", UserManager.UserName);

    sfs.Send(new ExtensionRequest("baucua_result", _obj, NetworkManager.instance.LobbyRoom));
  }

  public void USER_OUT_ROOM_BAUCUA()
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    obj.PutUtfString("uname", UserManager.UserName);
    obj.PutUtfString("uid", UserManager.UserName);
    sfs.Send(new ExtensionRequest("user_out_room", obj, NetworkManager.instance.LobbyRoom));
  }

  #endregion

  public void SET_DISPLAYNAME(string _displayname)
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    obj.PutText("displayname", _displayname);
    sfs.Send(new ExtensionRequest("set_displayname", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GET_EXCHANGE_RATE()
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    sfs.Send(new ExtensionRequest("getexchangerate", obj, NetworkManager.instance.LobbyRoom));

  }

  public void CHECK_SMART()
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    sfs.Send(new ExtensionRequest("smartman", obj, NetworkManager.instance.LobbyRoom));
  }

  public void GET_EXCHANGE_CARD(int _cardValue, string _operator)
  {
    Loading.instance.Show();
    ISFSObject obj = new SFSObject();
    obj.PutText("token", UserManager.AccessToken);
    obj.PutInt("card", _cardValue);
    obj.PutText("opera", _operator);
    print("exchangecard" + obj.ToJson());
    sfs.Send(new ExtensionRequest("exchangecard", obj, NetworkManager.instance.LobbyRoom));
  }

}
