﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
  public static LoadLevel Instance;
  public GameObject loadingPanel;
  [SerializeField] Slider progressBar;
  [SerializeField] Image slide;
  [SerializeField] Text content;

  void Awake()
  {
    if (Instance != null)
    {
      Destroy(gameObject);
      return;
    }
    Instance = this;
    DontDestroyOnLoad(this.gameObject);
  }

  public void ShowLoadingPanel(string _sceneName, string _content)
  {
    loadingPanel.SetActive(true);
    content.text = _content;
    StartCoroutine(loadLoadingScene(_sceneName));
  }

  public void ShowLoadingPanel(string _sceneName) =>
    ShowLoadingPanel(_sceneName, string.Empty);

  public void HideLoadingPanel()
  {
    loadingPanel.SetActive(false);
  }

  IEnumerator loadLoadingScene(string _scenename)
  {
    progressBar.gameObject.SetActive(true);
    progressBar.value = 0;
    slide.fillAmount = 0;
    AsyncOperation async = SceneManager.LoadSceneAsync(_scenename);
    while (!async.isDone)
    {
      progressBar.value = async.progress / 0.9f;
      slide.fillAmount = async.progress / 0.9f;
      yield return null;
    }
  }
  public void ShowTextLoading(string _content)
  {
    content.text = _content;
  }
}
