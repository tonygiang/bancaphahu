﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShakingObject : MonoBehaviour {
	float speed = 20.0f; //how fast it shakes
	float amount = 0.05f; //how much it shakes
	// Use this for initialization
	Vector3 startingPosbuttonDanThuong,startingPosbuttonDanChoi,startingPosbuttonSlot;
	public Button DanThuong,DanChoi,Slot;
	bool IsDanThuongShake = false;
	bool IsDanChoiShake = false;
	bool IsSlotShake = false;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.touchCount > 0) {
//			Touch first = Input.GetTouch (0);
//			if (first.phase == TouchPhase.Stationary) {
//				Vector3 vector = new Vector3 (startingPosbutton.x + Mathf.Sin (Time.time * speed) * amount, transform.position.y+ Mathf.Sin (Time.time * speed*2) * amount, transform.position.z);
//					transform.position = vector;
//			}
//		}
		if(IsDanThuongShake){
			Vector3 vector = new Vector3 (startingPosbuttonDanThuong.x + Mathf.Sin (Time.time * speed) * amount, DanThuong.transform.position.y+ Mathf.Sin (Time.time * speed*2) * amount, transform.position.z);
			DanThuong.transform.position = vector;
		}
		if(IsDanChoiShake){
			Vector3 vector = new Vector3 (startingPosbuttonDanChoi.x + Mathf.Sin (Time.time * speed) * amount, DanChoi.transform.position.y+ Mathf.Sin (Time.time * speed*2) * amount, transform.position.z);
			DanChoi.transform.position = vector;
		}
		if(IsSlotShake){
			Vector3 vector = new Vector3 (startingPosbuttonSlot.x + Mathf.Sin (Time.time * speed) * amount, Slot.transform.position.y+ Mathf.Sin (Time.time * speed*2) * amount, transform.position.z);
			Slot.transform.position = vector;
		}

	}
	public void Ontouch(int type){
		if (type == 0) {
			startingPosbuttonDanThuong.x = DanThuong.transform.position.x;
			startingPosbuttonDanThuong.y = DanThuong.transform.position.y;
			startingPosbuttonDanThuong.z = DanThuong.transform.position.z;
			IsDanThuongShake = true;
		} else if (type == 1) {
			startingPosbuttonDanChoi.x = DanChoi.transform.position.x;
			startingPosbuttonDanChoi.y = DanChoi.transform.position.y;
			startingPosbuttonDanChoi.z = DanChoi.transform.position.z;
			IsDanChoiShake = true;
		} else {
			IsSlotShake = true;
			startingPosbuttonSlot.x = Slot.transform.position.x;
			startingPosbuttonSlot.y = Slot.transform.position.y;
			startingPosbuttonSlot.z = Slot.transform.position.z;
		}
	}
	public void OnRelease(int type){
		if (type == 0) {
			IsDanThuongShake = false;
			DanThuong.transform.position = startingPosbuttonDanThuong;
		} else if (type == 1) {
			IsDanChoiShake = false;
			DanChoi.transform.position = startingPosbuttonDanChoi;
		} else {
			IsSlotShake = false;
			Slot.transform.position = startingPosbuttonSlot;
		}
	}
	private void Shake (Button button){
		
	}

}
