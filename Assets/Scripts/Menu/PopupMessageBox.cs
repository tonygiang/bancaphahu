﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Utils;

public class PopupMessageBox : MonoBehaviour
{
  public Button OKButton = null;
  public Button CancelButton = null;
  public Text TitleText = null;
  public Text ContentText = null;

  void Awake()
  {
    GetComponent<Animator>().Play("PopUp");
  }

  public void InstantiateWithMessage(string message) =>
    Instantiate(this).ContentText.text = message;

  public void InstantiateWithInfo(MessageBoxEvent evt)
  {
    var newMessageBox = Instantiate(this);
    if (evt.Title.HasContent()) newMessageBox.TitleText.text = evt.Title;
    if (evt.Content.HasContent()) newMessageBox.ContentText.text = evt.Content;
    evt.OKEvent?.PassTo(newMessageBox.OKButton.onClick.AddListener);
    evt.CancelEvent?.PassTo(newMessageBox.CancelButton.onClick.AddListener);
  }
}

public class MessageBoxEvent
{
  public string Title = string.Empty;
  public string Content = string.Empty;
  public UnityAction OKEvent = null;
  public UnityAction CancelEvent = null;
}
