﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Linq;
using UnityEngine.Networking;
using Utils;

[DisallowMultipleComponent]
[RequireComponent(typeof(InputField))]
public class ValidatedInputField : ExtensionBehaviour<InputField>
{
  [HideInInspector] public string ValidationFailureMessage;

  public UnityEvent OnClientValidationPass = new UnityEvent();
  public UnityEventString OnClientValidationFail = new UnityEventString();
  bool _isValidOnClient = false;
  public bool IsValidOnClient => _isValidOnClient;

  public bool HasServerValidation = false;
  public UnityEvent OnServerValidationPass = new UnityEvent();
  public UnityEventString OnServerValidationFail = new UnityEventString();
  public Func<InputField, PromiseBehaviour<string, DownloadHandler>> ServerValidationPromise;
  bool _isValidOnServer = true;
  public bool IsValidOnServer => HasServerValidation ? _isValidOnServer : true;
  ClientsideValidator<ValidatedInputField>[] _attachedValidators =>
    GetComponents<ClientsideValidator<ValidatedInputField>>();

  protected override void Awake()
  {
    base.Awake();
    BaseComp.onEndEdit.AddListener(text =>
    {
      var firstFailedValidator = _attachedValidators
        .Where(v => v.isActiveAndEnabled)
        .FirstOrDefault(v => v.Validate(this) == false);
      if (firstFailedValidator == null)
      {
        OnClientValidationPass.Invoke();
        if (HasServerValidation) ServerValidationPromise(BaseComp);
      }
      else
      {
        ValidationFailureMessage = firstFailedValidator.FailureMessage;
        OnClientValidationFail.Invoke(ValidationFailureMessage);
      }
    });

    OnClientValidationPass.AddListener(() => _isValidOnClient = true);
    OnClientValidationFail.AddListener(message => _isValidOnClient = false);
    OnServerValidationPass.AddListener(() => _isValidOnServer = true);
    OnServerValidationFail.AddListener(message => _isValidOnServer = false);
  }

  void Start()
  {
    _isValidOnClient = _attachedValidators.Where(v => v.isActiveAndEnabled)
      .All(v => v.Validate(this) == true);
  }

  public void SetValidationState(bool newState)
  {
    _isValidOnClient = newState;
    _isValidOnServer = newState;
  }

  public void Validate() => BaseComp.onEndEdit.Invoke(BaseComp.text);
}
