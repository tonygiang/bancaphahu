﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

[DisallowMultipleComponent]
[RequireComponent(typeof(ValidatedInputField))]
public class ValidIfTextLengthInRange : ClientsideValidator<ValidatedInputField>
{
  [SerializeField] int MinValidLength = 6;
  [SerializeField] int MaxValidLength = 15;

  void Start()
  {
  }

  public override bool Validate(ValidatedInputField component) =>
    component.BaseComp.text.Length.InRange(MinValidLength, MaxValidLength);
}
