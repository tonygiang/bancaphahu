﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Utils;
using Sfs2X.Entities.Data;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(ValidatedInputField))]
  public class ValidIfUsernameAvailableOnServer : MonoBehaviour
  {
    ValidatedInputField InputFieldComp;
    [SerializeField] [Multiline] string FailureMessage = string.Empty;

    void OnEnable()
    {
      InputFieldComp = GetComponent<ValidatedInputField>();
      InputFieldComp.ServerValidationPromise += CheckWithServer;
    }

    void OnDisable()
    {
      InputFieldComp.ServerValidationPromise -= CheckWithServer;
    }

    PromiseBehaviour<string, DownloadHandler> CheckWithServer(InputField field)
      => Network.PostToServer("Login",
        ("method", "checkusername"),
        ("username", field.text))
        .Then(handler =>
        {
          if (handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData)
            .GetInt("message_code") == 0)
            {
              InputFieldComp.ValidationFailureMessage = FailureMessage;
              InputFieldComp.OnServerValidationFail.Invoke(FailureMessage);
            }
          else InputFieldComp.OnServerValidationPass.Invoke();
        });
  }
}
