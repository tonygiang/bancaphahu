﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
[RequireComponent(typeof(ValidatedInputField))]
public class ValidIfMatchInputField : ClientsideValidator<ValidatedInputField>
{
  [SerializeField] InputField InputFieldToMatch = null;
  [SerializeField] bool ValidIfTextDifferentInstead = false;

  void Start()
  {
  }

  public override bool Validate(ValidatedInputField component) =>
    (InputFieldToMatch.text == component.BaseComp.text)
      ^ ValidIfTextDifferentInstead;
}
