using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Sfs2X.Entities.Data;
using Utils;
using System;

public class FBConnector : MonoBehaviour
{
  public static Texture2D textureAvatar;

  public static string displayname;
  public static string faceid;

  public static void FBShareGame()
  {
    if (!FB.IsLoggedIn) FBLoginShare();
    else Share();
  }

  public static void Share()
  {
    FB.ShareLink(contentTitle: "Bắn Cá Phá Hũ",
      contentURL: new UriBuilder(SaveLoadData.Instance.faceapplink).Uri,
      contentDescription: "Trò chơi rất hay để giải trí ",
      callback: OnShare);
  }

  static void OnShare(IShareResult result)
  {
    if (result.Error == null && !result.Cancelled)
    {
      SFSManager.instance.SendShareFB();
      Loading.instance.Show();
    }
    else MessageBox.instance.Show("Lỗi", "Bạn đã huỷ chia sẻ ");
  }

  public static void FBLoginShare()
  {
    var perms = new List<string>() { "public_profile" };
    FB.LogInWithReadPermissions(perms, LoginCallBackShare);
  }

  static void LoginCallBackShare(ILoginResult result)
  {
    if (FB.IsLoggedIn) Share();
    else MessageBox.instance.Show("Lỗi",
      "Không kết nối được đến tài khoản facebook");
  }

  public static void OPEN_FANPAGE()
  {
    string _idFB = SaveLoadData.Instance.faceappid;
    if (AppInfo.CurrentPlatform == Platform.ANDROID)
      Application.OpenURL("fb://page/" + _idFB);
    if (AppInfo.CurrentPlatform == Platform.IOS)
      Application.OpenURL("fb://profile/" + _idFB);
  }

  public static void OPEN_MESSENGER()
  {
    string _idFB = "https://www.messenger.com/t/" + SaveLoadData.Instance.faceappid;
    if (AppInfo.CurrentPlatform == Platform.WEBGL)
      Application.OpenURL(_idFB);
  }

  public static void CONNECT_FB()
  {
    var perms = new List<string>() { "public_profile", "email" };
    FB.LogInWithReadPermissions(perms, CONNECT_FB_CallBack);
  }

  static void CONNECT_FB_CallBack(ILoginResult result)
  {
    if (FB.IsLoggedIn)
    {
      string avatarLink = "https" + "://graph.facebook.com/" + AccessToken.CurrentAccessToken.UserId + "/picture?type=square&height=100&width=100";
      UserManager.AvatarLink = avatarLink;
      CONNECT_FB_NAME();
    }
    else MessageBox.instance.Show("Lỗi",
      "Không kết nối được đến tài khoản facebook");
  }
  static void CONNECT_FB_NAME() =>
    FB.API("/me?fields=id,name,first_name,last_name,email",
      HttpMethod.GET,
      CONNECT_FB_NAME_CALLBACK);

  static void CONNECT_FB_NAME_CALLBACK(IGraphResult result)
  {
    if (result.Error == null)
    {
      string _faceid = result.ResultDictionary["id"].ToString();
      string _name = result.ResultDictionary["name"].ToString();
      displayname = _name;
      faceid = _faceid;

      Network.PostToServer("Login",
        ("method", "connectfb"),
        ("faceid", _faceid),
        ("name", _name))
        .Then(handler =>
        {
          ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          if (smartfoxResponse.GetText("code") == "lobby_ok_114")
          {
            // deactive connectfb button
            UIMenu.Instance.OFF_CONNECT_FB();
            FBConnector.CONNECT_FB_AVATAR();

            UserManager.DisplayName = FBConnector.displayname;
            UserManager.faceId = FBConnector.faceid;
            UIMenu.Instance.SetUserName();
            Shop.Instance.SetInfo();
          }
          MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
        });
    }
    else Debug.Log("Failed to get user name\nError: " + result.Error);
  }

  public static void CONNECT_FB_AVATAR() =>
    FB.API("/me/picture?type=square&height=100&width=100",
      HttpMethod.GET,
      CONNECT_FB_AVATAR_CALLBACK);

  static void CONNECT_FB_AVATAR_CALLBACK(IGraphResult result)
  {
    try
    {
      if (result.Error == null && result.Texture != null)
      {
        textureAvatar = result.Texture;
        Sprite _spriteAva = Sprite.Create(textureAvatar, new Rect(0, 0, textureAvatar.width, textureAvatar.height), new Vector2(0.5f, 0.5f));
        UIMenu.Instance.SetUserAvatar(_spriteAva);
        Shop.Instance.SetUserAvatar(_spriteAva);
      }
      else Debug.Log("Failed to get user avatar\nError: " + result.Error);
    }
    catch (System.Exception ex)
    {
      Debug.Log("Lỗi get avatar");
    }
  }
}