﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Utils;

public class MoveByDragging : MonoBehaviour, IDragHandler
{
  void Start() { }

  public void OnDrag(PointerEventData eventData) =>
    transform.position += (Vector3)eventData.delta;
}
