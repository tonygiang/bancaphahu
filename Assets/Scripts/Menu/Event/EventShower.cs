﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.Linq;
public class EventShower : MonoBehaviour {

	public static EventShower Instance;

	public GameObject EventItem, EventContainer,EventPanel;
	public Text contentText;
	[SerializeField]
	Sprite clickOn,clickOff;

	void Awake(){
		Instance = this;

	}




	public void GetEvent(ISFSObject obj){
		
		contentText.text = "";
		string json = obj.GetUtfString ("events");
		//json = "\t\t[{\"title\":\"abc1\",\"content\":\"xyz1\",\"linkimage\":\"http://mmmmm\"},{\"title\":\"abc2\",\"content\":\"xyz2\",\"linkimage\":\"http://mmmmm\"},{\"title\":\"abc3\",\"content\":\"xyz3\",\"linkimage\":\"http://mmmmm\"},{\"title\":\"abc4\",\"content\":\"xyz4\",\"linkimage\":\"http://mmmmm\"}]";
		List<EventItem> lEvenItem = new List<EventItem> ();
		lEvenItem = JsonConvert.DeserializeObject<List<EventItem>> (json);
		if(lEvenItem.Count > 0)
			EventPanel.SetActive (true);
		foreach (Transform chil in EventContainer.transform) {
			if(chil.gameObject != null)
				Destroy (chil.gameObject);
		}
//		int mailNum = 0;
		foreach (var item in lEvenItem) {
			GameObject ga = Instantiate (EventItem);
			ga.SetActive (true);
			ga.transform.SetParent (EventContainer.transform);
			ga.transform.localScale = new Vector3(1f,1f,1f);
			ga.transform.GetChild (1).GetComponent<Text> ().text = item.title;
			ga.name = item.title;
			ga.GetComponent<Button> ().onClick.AddListener (() => {				
				PickEvent (item.title, item.content);
			});
		}
		PickEvent (lEvenItem.FirstOrDefault().title,lEvenItem.FirstOrDefault().content);

	}

	public void ShowContent(string _content){

		contentText.text = _content;
	}

	public void CloseEvent(){
		EventPanel.SetActive (false);
		//firstMenu.SetActive (true);
	}
		

	public void PickEvent(string _nameEvent,string _content){
		foreach (Transform tran in EventContainer.transform) {
			if (tran.name == _nameEvent) {
				tran.GetChild(0).GetComponent<Image> ().sprite = clickOn;
			} else {
				tran.GetChild(0).GetComponent<Image> ().sprite = clickOff;
			}
		}
		contentText.text = _content;
	}
	//	public void Init(ISFSObject obj)
	//	{
	//		id = obj.GetUtfString ("mailid");
	//		date = obj.GetUtfString ("datecreate");
	//		content = obj.GetUtfString ("content");
	//		state = obj.GetInt ("state");
	//		title = obj.GetUtfString ("title");
	//		if (state == 0) {
	//			mailImg.sprite = closeMail;
	//		} else {
	//			mailImg.sprite = openMail;
	//		}
	//		mailImg.SetNativeSize ();
	//		transform.GetChild (1).GetComponent<Text> ().text = title;
	//		transform.GetChild (2).GetComponent<Text> ().text = date;
	//		GetComponent<Button> ().onClick.AddListener (() => {
	//			if(state==0){
	//				Method.Instance.ReadMail (id);
	//				state=1;
	//				mailImg.sprite = openMail;
	//				mailImg.SetNativeSize ();
	//			}
	//			Inbox.Instance.PickMail(id,content);
	//		});
	//		gameObject.name = id;
	//	}


}
