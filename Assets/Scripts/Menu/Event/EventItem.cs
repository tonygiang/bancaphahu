﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using UnityEngine.UI;

public class EventItem
{

	public string content { get; set; }

	public string title{ get; set; }

	public string linkimage{ get; set; }

}
