﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using UnityEngine.EventSystems;

[DisallowMultipleComponent]
[RequireComponent(typeof(RectTransform))]
public class LimitToRectTransform : ExtensionBehaviour<RectTransform>,
  IDragHandler
{
  [SerializeField] RectTransform TransformToStayIn = null;

  void Start() { }

  public void OnDrag(PointerEventData eventData)
  {
    var movementRanges = TransformToStayIn.GetRanges()
      .GetMovementRangeOf(BaseComp);
    var thisPosition = BaseComp.position;
    thisPosition.x = thisPosition.x.ClampTo(movementRanges.x);
    thisPosition.y = thisPosition.y.ClampTo(movementRanges.y);
    BaseComp.position = thisPosition;
  }
}
