﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeAvatar : MonoBehaviour {
	public static ChangeAvatar Instance;

	public GameObject picked;
	public Image avatarImage,avatarImageMenu,avatarFB;
	public GameObject[] listAvatar;

	[SerializeField] 
	InputField inputName;

	void Awake(){
		Instance = this;
	}

	public void Dialog_Open(GameObject ga){
		ga.SetActive (true);
	}
	public void Dialog_Close(GameObject ga){
		ga.SetActive (false);
	}

	public void PickAvatar(string name){
//		foreach (GameObject ga in listAvatar) {
//			if (ga.name == name) {
//				picked.GetComponent<RectTransform> ().anchoredPosition = ga.GetComponent<RectTransform> ().anchoredPosition;
//				avatarImage.sprite = ga.GetComponent<Image> ().sprite;
//				avatarImage.SetNativeSize ();
//				avatarImageMenu.sprite = ga.GetComponent<Image> ().sprite;
//				avatarImageMenu.SetNativeSize ();
//				UserManager.Instance.avatarName = name;
//				Shop.Instance.SetUserAvatar (ga.GetComponent<Image> ().sprite);
//			}
//		}
		Sprite _ava =Resources.Load<Sprite>("avatar/"+name);
		avatarFB.sprite = _ava;
		avatarImageMenu.sprite = _ava;
		Shop.Instance.SetUserAvatar ((_ava));
	}

	public void SetAvatarFB(Sprite _spr){
		avatarFB.sprite = _spr;
		avatarImageMenu.sprite = _spr;
		avatarImage.gameObject.SetActive (false);
	}

//	public void SwitchAvatar(bool _isQuickLog){
//		if (_isQuickLog) {
//			avatarImage.gameObject.SetActive (true);
//			//avatarImageMenu.transform.parent.GetComponent<Mask> ().enabled = false;
//		} else {
//			avatarImage.gameObject.SetActive (false);
//			//avatarImageMenu.transform.parent.GetComponent<Mask> ().enabled = true;
//		}
//		Shop.Instance.ActiveMask (_isQuickLog);
//	}


	#region SET DISPLAY NAME
	public void ChangeName(){
		string _disName = inputName.text;
		if (_disName.Length >= 6) {
//			Method.Instance.SetDisplayNameQuickLogin (_disName);
			SFSManager.instance.SET_DISPLAYNAME(_disName);
			UIMenu.Instance.CHANGE_NAME_CLOSE ();
		} else {
			MessageBox.instance.Show ("Tên hiển thị tối thiểu 6 kí tự");
		}
	}
	#endregion

}
