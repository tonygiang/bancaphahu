﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Spine.Unity;
using Newtonsoft.Json;
using System.Linq;
using BE;
using Utils;

public class UIMenu : MonoBehaviour
{
  public static UIMenu Instance;

  [SerializeField]
  GameObject loginPanel, menuPanel;

  public GameObject mainMenu, settingPanel, missionPanel, setDisplayNamePanel, eventContentPanel, duatopContentPanel;

  public Text JackpotRoomGem, txtValueJackpotGold, JackPotSlotRoom1, JackPotSlotRoom2, JackPotSlotRoom3;

  [SerializeField]
  GameObject soundButton, musicButton;
  [SerializeField]
  Sprite[] sound, music;

  //[Header("--- THÔNG TIN NGƯỜI CHƠI  ---")]
  public GameObject InfoPlayerPanel;
  public Transform infoContainer;
  public Text displayName;

  public Transform playerinfo;

  [SerializeField]
  Sprite playerVip, playerNor;

  [SerializeField]
  GameObject changNameBtn, connectfbBtn;
  // setting popup
  Vector2 scaleMax = new Vector2(1.1f, 1.1f);
  Vector2 scaleMin = new Vector2(0.9f, 0.9f);
  float timeST = 0.3f;
  float currentTimeST = 0;
  public bool logined = false;

  [HideInInspector]
  public int SlotRoom1JackpotValue = 0, SlotRoom2JackpotValue = 0, SlotRoom3JackpotValue = 0;

  void Awake()
  {
    Instance = this;

  }

  void QuitConfirm(MessageBoxCallBack result)
  {
    if (result.Equals(MessageBoxCallBack.OK))
    {
      if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Menu")
      {
        Application.Quit();
      }
    }
  }
  // Use this for initialization
  void Start()
  {
    if (!AppInfo.LastSceneName.HasContent())
    {
      Network.PostToServer("CheckVersion", ("method", "checkversion"))
        .Then(handler =>
        {
          ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          if (smartfoxResponse.GetInt("message_code") != 1)
            MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
          else
          {
            LoadLevel.Instance.ShowTextLoading("Đang tải dữ liệu ...");
            Version versionFromServer = new Version(smartfoxResponse);

            if (versionFromServer.versionCode > BanCa.Config.CodeVersion)
            {
              MessageBox.instance.Show("Thông báo!", "Đã có phiên bản mới\nMời bạn cập nhật", MessageBoxType.OK, versionFromServer.CheckVerMessBoxCallBack);
              // TODO: Hide Login Panel on receiving new version
              // LoginController.instance.logInPanel.SetActive(false);
              LoadLevel.Instance.HideLoadingPanel();
              return;
            }
            else
            {
              if (versionFromServer.state == 1) LogOutSuccess();
              else
              {
                MessageBox.instance.Show(versionFromServer.reason);
                MessageBox.instance.Show("Thông báo!",
                  versionFromServer.reason,
                  MessageBoxType.OK,
                  versionFromServer.BaoTriServer);
              }
            }
            BanCa.Session.Functions = versionFromServer.function.Split(',');
            SaveLoadData.Instance.SaveCheckVersion(versionFromServer.function,
              smartfoxResponse);
            BanCa.Game.Events.OnReceiveFunctions
              .Invoke(BanCa.Session.Functions);
            LoadLevel.Instance.HideLoadingPanel();
          }
        });
    }
    else
    {
      NetworkManager.instance.LobbyRoom = SFSManager.sfs.LastJoinedRoom;
      Network.PostToServer("BehindLogin", ("method", "getmailbox"))
        .Then(handler =>
        {
          ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          if (smartfoxResponse.GetInt("message_code") != 1)
            MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
          else Inbox.Instance.GetMail(smartfoxResponse.GetSFSArray("list"));
        });
      SetUserName();
      UserManager.SET_AVATAR_BACKGAME();
      ActiveAvatarVip();
      if (SFSManager.instance.isDisconnect)
      {
        SFSManager.instance.Login();
        SFSManager.instance.isDisconnect = false;
      }

      JackpotRoomGem.text = SaveLoadData.FormatMoney(SaveLoadData.Instance.jackpotNumberGem);
      txtValueJackpotGold.text = SaveLoadData.FormatMoney(SaveLoadData.Instance.jackpotNumberGold);

      if (SaveLoadData.Instance.isKick)
      {
        MessageBox.instance.Show(SaveLoadData.Instance.reasonKick);
        SaveLoadData.Instance.isKick = false;
        UserManager.ResetUser();
        LogOutSuccess();
      }
      else
      {
        LogInSuccess();
      }
      LoadLevel.Instance.HideLoadingPanel();
      UPDATE_SCORE(SaveLoadData.Instance.jackpotNumberGold, SaveLoadData.Instance.jackpotNumberGem);
      //	SFSManager.instance.GetSlotJackPot ();
    }

    Immortal.instance.SetAudio();
    MusicManager.Instance.PlayMenuMusic();
    if (Immortal.instance.MusicButtonState())
    {
      musicButton.GetComponent<Image>().sprite = music[0];
    }
    else
    {
      musicButton.GetComponent<Image>().sprite = music[1];
    }

    if (Immortal.instance.SoundButtonState())
    {
      soundButton.GetComponent<Image>().sprite = sound[0];
    }
    else
    {
      soundButton.GetComponent<Image>().sprite = sound[1];
    }
    if (SceneSlotGame.instance != null && SceneSlotGame.instance.gemInfo != null)
    {
      LoadLevel.Instance.HideLoadingPanel();
    }

  }

  // Update is called once per frame
  public void LogInSuccess()
  {
    RegistryLogin.Instance.DangNhap_CLOSE();
    Loading.instance.Hide();
    SFSManager.instance.UpdateGem();
    loginPanel.SetActive(false);
    menuPanel.SetActive(true);
    if (UserManager.isQuickLogin)
    {
      if (UserManager.UserName == UserManager.DisplayName)
      {
        changNameBtn.SetActive(true);
      }
      else
      {
        changNameBtn.SetActive(false);
      }
    }
    else
    {
      changNameBtn.SetActive(false);
    }
    BanCa.Game.Events.OnReceiveFunctions.Invoke(BanCa.Session.Functions);
    logined = true;
    //SFSManager.instance.GetSlotJackPot ();
    //InvokeRepeating ("getJackPotValue", 0, 0.5f);
    InvokeRepeating("getJackPotValue", 0, 0.5f);
  }

  public void LogOutSuccess()
  {
    menuPanel.SetActive(false);
    loginPanel.SetActive(true);
    if (TaiXiu.Instance != null) TaiXiu.Instance.TaiXiuPanel.SetActive(false);
    LoadLevel.Instance.HideLoadingPanel();
  }

  public void PlayCoin(int _value)
  {
    if (BanCa.Session.Gem.Value >= _value)
    {
      SFSManager.instance.PlayCoin(_value);
    }
    else
    {
      MessageBox.instance.Show(string.Format("Bạn cần ít nhất {0} kim cương để vào phòng này.", _value));
    }
    SoundManager.Instance.ButtonSound();
  }
  public void PlayGem(int _value)
  {
    if (BanCa.Session.Gem.Value >= _value)
    {
      SFSManager.instance.PlayGem(_value);
    }
    else
    {
      MessageBox.instance.Show(string.Format("Bạn cần ít nhất {0} kim cương để vào phòng này.", _value));
    }
    SoundManager.Instance.ButtonSound();
  }
  public void PlaySlot()
  {
    SoundManager.Instance.ButtonSound();
    LoadLevel.Instance.ShowLoadingPanel("SlotGame", "Đang tải dữ liệu phòng chơi...");
  }

  // Setting
  public void Setting_Open(GameObject obj)
  {
    obj.SetActive(true);
  }
  public void Setting_Close(GameObject obj)
  {
    obj.SetActive(false);
  }





  // Open
  public void OpenShop()
  {
    Shop.Instance.OpenShop();
  }

  public void SetJackpotGem(int _jackpotgem)
  {
    string _money = SaveLoadData.FormatMoney(_jackpotgem);
    JackpotRoomGem.text = _money;

  }


  public void SetNumberPlayer(string roomType, int _jackpot)
  {

    if (roomType == "gem")
    {

      JackpotRoomGem.text = SaveLoadData.FormatMoney(_jackpot);
    }
    else
    {

      txtValueJackpotGold.text = SaveLoadData.FormatMoney(_jackpot);

    }
  }



  // button add gold,gem
  public void AddGold()
  {
    Shop.Instance.OpenShopAddButton(3);
  }
  public void AddGem()
  {
    Shop.Instance.OpenShop();
  }

  #region ----- ĐỔI AVATAR -----

  public void INFO_PLAYER_OPEN()
  {

    infoContainer.GetChild(3).GetComponent<Text>().text = UserManager.UserName;
    displayName.text = UserManager.DisplayName;
    TaiXiu.Instance.TaiXiuPanel.SetActive(false);
    //        InfoPlayerPanel.SetActive(true);
    ButtonClickSound();
    StartCoroutine(ShowAnimPopUp(false, InfoPlayerPanel));
  }
  public void INFO_PLAYER_CLOSE()
  {
    StartCoroutine(ShowAnimPopUp(true, InfoPlayerPanel));
  }

  #endregion

  public void FBButton()
  {
    FBConnector.OPEN_FANPAGE();
  }

  public void LogOutButton()
  {

    SaveLoadData.Instance.SendUpdateMission();
    UserManager.ResetUser();
    SFSManager.sfs.Send(new LogoutRequest());
    Loading.instance.Show();
    Invoke("HideLoading", 2f);
    InfoPlayerPanel.SetActive(false);
  }

  public void Disconnect_Menu()
  {
    SaveLoadData.Instance.SendUpdateMission();
    UserManager.ResetUser();
    Loading.instance.Show();
    Invoke("HideLoading", 2f);
    MessageBox.instance.Show("Mất kết nối tới máy chủ. Vui lòng đăng nhập lại để tiếp tục chơi.");
  }

  void HideLoading()
  {


    menuPanel.SetActive(false);
    loginPanel.SetActive(true);
    Loading.instance.Hide();
  }

  public void Disconnect()
  {
    UserManager.ResetUser();
    Loading.instance.Show();
    Invoke("HideLoading", 2f);
    Shop.Instance.CloseShop();
    if (!mainMenu.activeInHierarchy)
    {
      mainMenu.SetActive(false);
    }

    if (!missionPanel.activeInHierarchy)
    {
      missionPanel.SetActive(false);
    }
  }

  #region Setting 
  public void SETTING_OPEN()
  {
    //        settingPanel.SetActive(true);
    StartCoroutine(ShowAnimPopUp(false, settingPanel));
  }
  public void SETTING_CLOSE()
  {
    //        settingPanel.SetActive(false);
    StartCoroutine(ShowAnimPopUp(true, settingPanel));
  }
  IEnumerator ShowAnimPopUp(bool isClose, GameObject _panel)
  {
    if (isClose)
    {
      timeST = 0.15f;
      currentTimeST = 0;
      do
      {
        _panel.transform.GetChild(0).localScale = Vector2.Lerp(Vector2.one, Vector2.zero, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);
      _panel.transform.GetChild(0).localScale = scaleMax;
      _panel.SetActive(false);
    }
    else
    {
      _panel.SetActive(true);
      timeST = 0.15f;
      currentTimeST = 0;

      do
      {
        _panel.transform.GetChild(0).localScale = Vector2.Lerp(Vector2.zero, scaleMax, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);
      timeST = 0.05f;
      currentTimeST = 0;
      do
      {
        _panel.transform.GetChild(0).localScale = Vector2.Lerp(scaleMax, scaleMin, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);
      _panel.transform.GetChild(0).localScale = Vector2.one;
    }

  }

  #endregion

  #region Mission
  public void MISSION_OPEN()
  {
    mainMenu.SetActive(false);
    missionPanel.SetActive(true);
    MissionManager.Instance.OpenMission();
  }
  public void MISSION_CLOSE()
  {
    missionPanel.SetActive(false);
    mainMenu.SetActive(true);
  }
  #endregion

  #region Rank
  public void RANK_OPEN()
  {
    Network.PostToServer("top", ("method", "gettop"), ("type", "arenaday"))
      .Then(handler =>
      {
        ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
          .PassTo(SFSObject.NewFromJsonData);
        if (smartfoxResponse.GetInt("message_code") != 1)
          MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
        else RankManager.Instance.LoadDataTopRespone(smartfoxResponse);
      });
  }
  #endregion

  #region Guide
  public void GUIDE_OPEN()
  {
    settingPanel.SetActive(false);
    GuideManager.Instance.OpenPanel();
  }
  #endregion


  #region Audio
  public void SoundButton()
  {
    if (Immortal.instance.SoundButton())
    {
      soundButton.GetComponent<Image>().sprite = sound[0];
    }
    else
    {
      soundButton.GetComponent<Image>().sprite = sound[1];
    }
  }

  public void MusicButton()
  {
    if (Immortal.instance.MusicButton())
    {
      musicButton.GetComponent<Image>().sprite = music[0];
    }
    else
    {
      musicButton.GetComponent<Image>().sprite = music[1];
    }
  }
  public void ButtonClickSound()
  {
    SoundManager.Instance.ButtonSound();
  }
  #endregion

  #region Info
  public void FINISH_CHANGE_NAME(string _displayname)
  {
    playerinfo.GetChild(0).GetChild(0).GetComponent<Text>().text = _displayname;
    changNameBtn.SetActive(false);
    setDisplayNamePanel.SetActive(false);
  }
  public void SetUserName()
  {
    playerinfo.GetChild(0).GetChild(0).GetComponent<Text>().text = UserManager.DisplayName;
    changNameBtn.SetActive(false);
  }
  public void SetUserAvatar(Sprite _spr)
  {
    //playerinfo.GetChild (1).GetChild (0).GetChild (0).GetComponent<Image> ().sprite = _spr;
    ChangeAvatar.Instance.SetAvatarFB(_spr);
  }
  #endregion

  // VIP
  public void ActiveAvatarVip()
  {
    if (BanCa.Session.CurrentVIPLevel.Value > 0)
    {
      playerinfo.GetChild(1).GetChild(0).GetChild(1).gameObject.SetActive(true);
      //  playerinfo.GetChild(0).GetComponent<Image>().sprite = playerVip;
      Shop.Instance.SetDialogVip(playerVip);
    }
    else
    {
      playerinfo.GetChild(1).GetChild(0).GetChild(1).gameObject.SetActive(false);
      // playerinfo.GetChild(0).GetComponent<Image>().sprite = playerNor;
      Shop.Instance.SetDialogNor(playerNor);
    }
    //playerinfo.GetChild(0).GetComponent<Image>().SetNativeSize();
  }


  // BUTTON MAIN MENU

  public void FB_SendMes()
  {
    FBConnector.OPEN_MESSENGER();
  }


  public void ShowEventContent_OPEN()
  {
    if (BanCa.Session.Functions.Contains("card"))
    {
      if (!AppInfo.LastSceneName.HasContent())
        StartCoroutine(LoadImage("http://thanh.bancathachdau.com/eventcontentAndroid.png", eventContentPanel));
    }

  }
  public void ShowEventContent_CLOSE()
  {
    eventContentPanel.SetActive(false);
    //		StartCoroutine(ShowAnimPopUp(true,eventContentPanel));

    StartCoroutine(LoadImage("http://thanh.bancathachdau.com/eventcontentAndroid2.png", duatopContentPanel));

    ButtonClickSound();
  }

  public void CLOSE_popupDuaTOP()
  {
    StartCoroutine(ShowAnimPopUp(true, duatopContentPanel));
    ButtonClickSound();
  }
  public void CLOSE_DUA_TOP()
  {
    //		SoundManager.Instance.ButtonSound ();
    //		duatopContentPanel.SetActive (false);
    //		EventManager.Instance.OpenPanel ();
  }


  IEnumerator LoadImage(string url, GameObject _panel)
  {

    Image _image = _panel.transform.GetChild(0).GetComponent<Image>();
    Transform _dialog = _panel.transform.GetChild(0);
    WWW request = new WWW(url);

    yield return request;
    //		print (request);
    if (request.error == null)
    {
      Texture2D texture = request.texture;
      Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
      _image.sprite = sprite;
      _image.SetNativeSize();
      _panel.SetActive(true);
      timeST = 0.15f;
      currentTimeST = 0;
      SoundManager.Instance.MoBat();
      do
      {
        _dialog.localScale = Vector2.Lerp(Vector2.zero, scaleMax, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);
      timeST = 0.05f;
      currentTimeST = 0;
      do
      {
        _dialog.localScale = Vector2.Lerp(scaleMax, scaleMin, currentTimeST / timeST);
        currentTimeST += Time.deltaTime;
        yield return null;
      } while (currentTimeST <= timeST);


      _dialog.localScale = Vector2.one;
    }
    else
    {
      //			Debug.Log(request.error);
    }

  }
  public void OPEN_BAUCUA()
  {
    SoundManager.Instance.ButtonSound();
    SFSManager.instance.USER_REQUEST_BAUCUA();
  }

  public void CHANGE_NAME_OPEN()
  {
    InfoPlayerPanel.SetActive(false);
    setDisplayNamePanel.SetActive(true);
  }
  public void CHANGE_NAME_CLOSE()
  {
    setDisplayNamePanel.SetActive(false);
  }

  public void UPDATE_SCORE(int _gold, int _gem)
  {
    //StartCoroutine (Update_Jackpot (_gold,_gem));
  }

  IEnumerator Update_Jackpot(int _gold, int _gem)
  {
    float elapsedTime = 0;
    float seconds = 1.5f;
    do
    {
      txtValueJackpotGold.text = SaveLoadData.FormatMoney((int)Mathf.Floor(Mathf.Lerp(0, _gold, (elapsedTime / seconds))));
      JackpotRoomGem.text = SaveLoadData.FormatMoney((int)Mathf.Floor(Mathf.Lerp(0, _gem, (elapsedTime / seconds))));

      elapsedTime += Time.deltaTime;
      yield return null;
    } while (elapsedTime < seconds);
    txtValueJackpotGold.text = SaveLoadData.FormatMoney(_gold);
    JackpotRoomGem.text = SaveLoadData.FormatMoney(_gem);
  }


  public void CONNECT_FB()
  {
    FBConnector.CONNECT_FB();
  }

  public void OFF_CONNECT_FB()
  {
    connectfbBtn.SetActive(false);
  }
  public void ON_CONNECT_FB()
  {
    connectfbBtn.SetActive(true);
  }
  public void GetJackPot(ISFSObject obj)
  {
    double NewGoldValue = obj.GetDouble("jackpot_gold");
    int oldGoldValue = SaveLoadData.Instance.jackpotNumberGold;

    double NewGemValue = obj.GetDouble("jackpot_gem");
    int oldGemValue = SaveLoadData.Instance.jackpotNumberGem;

    StartCoroutine(updateJackPot(oldGoldValue, Mathf.FloorToInt((float)NewGoldValue), oldGemValue, Mathf.FloorToInt((float)NewGemValue)));
  }
  IEnumerator updateJackPot(int firstValue, int secondValue, int thirdValue, int ForthValue)
  {
    float elapsedTime = 0;
    float seconds = 0.4f;
    do
    {
      txtValueJackpotGold.text = SaveLoadData.FormatMoney((int)Mathf.Floor(Mathf.Lerp(firstValue, secondValue, (elapsedTime / seconds))));
      elapsedTime += Time.deltaTime;
      JackpotRoomGem.text = SaveLoadData.FormatMoney((int)Mathf.Floor(Mathf.Lerp(thirdValue, ForthValue, (elapsedTime / seconds))));
      elapsedTime += Time.deltaTime;
      yield return null;
    } while (elapsedTime < seconds);
    txtValueJackpotGold.text = SaveLoadData.FormatMoney(secondValue);
    JackpotRoomGem.text = SaveLoadData.FormatMoney(ForthValue);
    SaveLoadData.Instance.jackpotNumberGold = secondValue;
    SaveLoadData.Instance.jackpotNumberGem = ForthValue;
  }
  void getJackPotValue()
  {
    if (SFSManager.instance != null)
    {
      SFSManager.instance.GetGemJackPot();
      SFSManager.instance.GetSlotJackPot();
      SFSManager.instance.UpdateGem();
    }
  }
  public void GetSlotJackPot(ISFSObject obj)
  {
    var json = obj.GetUtfString("slotjp");
    if (string.IsNullOrEmpty(json))
    {
      return;
    }
    var lJackpotInfo = JsonConvert.DeserializeObject<List<JackPotInfo>>(json);
    if (lJackpotInfo.Count == 0)
    {
      return;
    }
    JackPotInfo Room1Jackpot = lJackpotInfo.FirstOrDefault(m => m.roomId == 1);
    JackPotInfo Room2Jackpot = lJackpotInfo.FirstOrDefault(m => m.roomId == 2);
    JackPotInfo Room3Jackpot = lJackpotInfo.FirstOrDefault(m => m.roomId == 3);
    if (Room1Jackpot != null)
    {
      StartCoroutine(updateJackPotSlot(SlotRoom1JackpotValue, Room1Jackpot.jackPot, JackPotSlotRoom1));
      SlotRoom1JackpotValue = Room1Jackpot.jackPot;
    }
    if (Room2Jackpot != null)
    {
      StartCoroutine(updateJackPotSlot(SlotRoom2JackpotValue, Room2Jackpot.jackPot, JackPotSlotRoom2));
      SlotRoom2JackpotValue = Room2Jackpot.jackPot;
    }
    if (Room3Jackpot != null)
    {
      StartCoroutine(updateJackPotSlot(SlotRoom3JackpotValue, Room3Jackpot.jackPot, JackPotSlotRoom3));
      SlotRoom3JackpotValue = Room3Jackpot.jackPot;
    }
  }
  IEnumerator updateJackPotSlot(int firstValue, int secondValue, Text text)
  {
    if (firstValue == secondValue) yield break;
    float elapsedTime = 0;
    float seconds = 0.4f;
    do
    {
      text.text = SaveLoadData.FormatMoney((int)Mathf.Floor(Mathf.Lerp(firstValue, secondValue, (elapsedTime / seconds))));
      elapsedTime += Time.deltaTime;
      elapsedTime += Time.deltaTime;
      yield return null;
    } while (elapsedTime < seconds);
    text.text = SaveLoadData.FormatMoney(secondValue);
  }
}
