﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Entities.Data;
using System.Linq;
using Newtonsoft.Json;
using Sfs2X.Requests;
public class EXCHANGE_CARD : MonoBehaviour
{
  public static EXCHANGE_CARD Instance;

  public Image[] listImgOpe;
  public Sprite[] spriteOpeDefault, spriteOpeActive;
  public Text smartAnswer;
  public List<Image> listImgCost = new List<Image>();
  private string[] listStringOpe = { "Viettel", "Vinaphone", "Mobifone" };
  private int[] listMoney = { 10000, 20000, 50000, 100000, 200000, 500000 };
  private string operatorName = "";
  private int cardValue;
  private ISFSObject data;

  public GameObject exchangeCardPanel;

  [SerializeField]
  GameObject exCardContainer, exCardItem;

  [SerializeField]
  Image[] listTabImg;

  [SerializeField]
  Sprite[] sprTabOn, sprTabOff;

  [SerializeField]
  GameObject[] panelExchange;

  bool isRate = false;

  [SerializeField]
  GameObject popupQuestion;

  [SerializeField]
  Text contentQues;

  [SerializeField]
  Text AnswerIQ;
  private string IQResult;
  void Awake()
  {
    Instance = this;
  }
  public void checkIQ()
  {
    smartAnswer.text = "";
    SFSManager.instance.CHECK_SMART();
  }

  public void LoadSmartUi(ISFSObject obj)
  {
    string json = obj.GetUtfString("who_is_smartman");
    var Data = JsonConvert.DeserializeObject<SmartIQ>(json);
    string opera = "";
    if (Data.operate)
    {
      opera = " cộng ";
    }
    else
    {
      opera = " trừ ";
    }
    IQResult = Data.okman;
    var content = Data.number1 + opera + Data.number2 + " bằng bao nhiêu?";
    MessageBox.instance.showSmartIQ(content, MessageBoxType.OK_CANCEL, checkData1);
  }
  void checkData1(MessageBoxCallBack result)
  {

    if (result.Equals(MessageBoxCallBack.OK))
    {
      if (IQResult == MessageBox.instance.SmartAnswer.text)
      {
        DOITHE_OPEN();
        AnswerIQ.text = "";
        IQResult = "";
      }
      else
      {
        MessageBox.instance.Show("THÔNG BÁO", "Sai đáp án!");
      }

    }
    AnswerIQ.text = string.Empty;
  }
  public void DOITHE_OPEN()
  {
    if (isRate)
    {
      exchangeCardPanel.SetActive(true);
      return;
    }
    SFSManager.instance.GET_EXCHANGE_RATE();
  }
  public void DOITHE_CLOSE()
  {
    exchangeCardPanel.SetActive(false);
  }


  public void ChooseTAB(int value)
  {
    for (int i = 0; i < listTabImg.Length; i++)
    {
      if (i == value)
      {
        listTabImg[i].GetComponent<Button>().enabled = false;
        listTabImg[i].sprite = sprTabOn[i];
        operatorName = listStringOpe[i];
        panelExchange[i].SetActive(true);
      }
      else
      {
        listTabImg[i].GetComponent<Button>().enabled = true;
        listTabImg[i].sprite = sprTabOff[i];
        panelExchange[i].SetActive(false);
      }
    }

    if (value == 1) Network.PostToServer("BehindLogin",
      ("method", "getcardexchange"));
  }

  public void ChooseOpera(int value)
  {

    for (int i = 0; i < listImgOpe.Length; i++)
    {
      if (i == value)
      {
        listImgOpe[i].GetComponent<Button>().enabled = false;
        listImgOpe[i].sprite = spriteOpeActive[i];
        operatorName = listStringOpe[i];
        SetOperatorType();

      }
      else
      {
        listImgOpe[i].GetComponent<Button>().enabled = true;
        listImgOpe[i].sprite = spriteOpeDefault[i];
      }
    }
  }
  List<CardFormat> GetListCard(ISFSArray data)
  {
    List<CardFormat> lCardFormat = new List<CardFormat>();
    foreach (ISFSObject _operator in data)
    {
      if (_operator.ContainsKey("Viettel"))
      {
        ISFSArray cardArr = _operator.GetSFSArray("Viettel");
        foreach (ISFSObject _card in cardArr)
        {
          CardFormat _cardFor = new CardFormat(_card);
          if (operatorName == "Viettel")
          {
            lCardFormat.Add(_cardFor);
          }
        }
      }
      if (_operator.ContainsKey("Mobifone"))
      {
        ISFSArray cardArr = _operator.GetSFSArray("Mobifone");
        foreach (ISFSObject _card in cardArr)
        {
          CardFormat _cardFor = new CardFormat(_card);
          if (operatorName == "Mobifone")
          {
            lCardFormat.Add(_cardFor);
          }
        }
      }
      if (_operator.ContainsKey("Vinaphone"))
      {
        ISFSArray cardArr = _operator.GetSFSArray("Vinaphone");
        foreach (ISFSObject _card in cardArr)
        {
          CardFormat _cardFor = new CardFormat(_card);
          if (operatorName == "Vinaphone")
          {
            lCardFormat.Add(_cardFor);
          }
        }
      }
    }
    return lCardFormat;
  }

  void SetOperatorType()
  {
    ISFSArray _listCardFormat = data.GetSFSArray("list");
    List<int> ListCardAvaiable = new List<int>();
    foreach (var item in GetListCard(_listCardFormat))
    {
      ListCardAvaiable.Add(item.price);
    }
    List<int> ListCardNotAvaiable = new List<int>();
    ListCardNotAvaiable = listMoney.Except(ListCardAvaiable).ToList();
    for (int i = 0; i < listImgCost.Count; i++)
    {
      foreach (var item in GetListCard(_listCardFormat))
      {
        int _cardValue = listMoney[i];
        if (item.price == _cardValue)
        {
          listImgCost[i].transform.GetChild(0).GetComponent<Text>().text = SaveLoadData.FormatMoney(item.gem);
          listImgCost[i].gameObject.SetActive(false);
          listImgCost[i].sprite = Resources.Load<Sprite>("Images/DoiThe/" + operatorName + "/" + (i + 1));
          listImgCost[i].GetComponent<Button>().onClick.RemoveAllListeners();
          listImgCost[i].gameObject.SetActive(true);
          listImgCost[i].GetComponent<Button>().onClick.AddListener(() =>
          {
            //				SFSManager.instance.GET_EXCHANGE_CARD (_cardValue , operatorName);
            DOITHE_OK(operatorName, item.gem);
          });
        }


      }
      foreach (var item in ListCardNotAvaiable)
      {
        if (listImgCost[i].name == item.ToString())
        {
          listImgCost[i].gameObject.SetActive(false);
          listImgCost[i].GetComponent<Button>().onClick.RemoveAllListeners();
        }
      }

    }



  }
  public void TAT_BAT_NHAMANG(ISFSObject _obj)
  {
    if (_obj.ContainsKey("vtt"))
    {
      int _acVTT = _obj.GetInt("vtt");
      if (_acVTT == 0)
      {
        listImgOpe[0].gameObject.SetActive(false);
      }
      else
      {
        listImgOpe[0].gameObject.SetActive(true);
      }
    }
    if (_obj.ContainsKey("vnp"))
    {
      int _acVTT = _obj.GetInt("vnp");
      if (_acVTT == 0)
      {
        listImgOpe[1].gameObject.SetActive(false);
      }
      else
      {
        listImgOpe[1].gameObject.SetActive(true);
      }
    }
    if (_obj.ContainsKey("vms"))
    {
      int _acVTT = _obj.GetInt("vms");
      if (_acVTT == 0)
      {
        listImgOpe[2].gameObject.SetActive(false);
      }
      else
      {
        listImgOpe[2].gameObject.SetActive(true);
      }
    }
  }

  public void LOADEXCHANGE(ISFSObject _obj)
  {
    double _rate = 1;
    string text = _obj.GetUtfString("exchange_card_format");

    data = SFSObject.NewFromJsonData(text);
    TAT_BAT_NHAMANG(data);


    //		int _countMoney = listImgCost.Count;
    //		for (int i = 0; i < _countMoney; i++) {
    //			
    //		}
    exchangeCardPanel.SetActive(true);
    //isRate = true;
    ChooseOpera(0);
  }


  public void DOITHE_OK(string _nhamang, int _menhgia)
  {
    operatorName = _nhamang;
    cardValue = _menhgia;
    SoundManager.Instance.ButtonSound();
    contentQues.text = "Bạn xác nhận dùng " + SaveLoadData.FormatMoney(_menhgia) + " để đổi thẻ " + _nhamang + " ?";
    popupQuestion.SetActive(true);
  }


  public void OK_Confirm()
  {
    popupQuestion.SetActive(false);
    SFSManager.instance.GET_EXCHANGE_CARD(cardValue, operatorName);
  }
  public void CANCLE_Confirm()
  {
    popupQuestion.SetActive(false);
  }

}
