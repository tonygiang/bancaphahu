﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(LoginButton))]
  public class SaveLoginSessionOnLoginSuccess : ExtensionBehaviour<LoginButton>
  {
    void OnEnable() => Game.Events.OnLoginSuccess.AddListener(Save);

    void OnDisable() => Game.Events.OnLoginSuccess.RemoveListener(Save);

    void Save()
    {
      Session.UserName = BaseComp.SmartfoxResponse
        .GetUtfString("username");
      Session.DisplayName = BaseComp.SmartfoxResponse
        .GetUtfString("displayname");
      Session.AccessToken = BaseComp.SmartfoxResponse
        .GetUtfString("access_token");
      // Session.VIPStatus = BaseComp.SmartfoxResponse.GetInt("vip");
    }
  }

}