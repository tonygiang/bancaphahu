﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using Sfs2X.Entities.Data;
using Utils;

namespace BanCa
{
  public class FacebookLoginButton : MonoBehaviour
  {
    List<string> _permissions = new List<string>() { "public_profile",
    "email" };
    [SerializeField] PopupMessageBox ErrorMessageBoxPrefab = null;
    [SerializeField] string FBLoginErrorTitle = string.Empty;
    [SerializeField] [TextArea] string FBLoginErrorMessage = string.Empty;

    public void OnClick() => FB.LogInWithReadPermissions(_permissions, result =>
    {
      if (FB.IsLoggedIn) FB.API("/me?fields=id,name,first_name,last_name,email",
        HttpMethod.GET,
        UserNameCallBack);
      else
      {
        var newErrorMessageBox = Instantiate(ErrorMessageBoxPrefab);
        newErrorMessageBox.TitleText.text = FBLoginErrorTitle;
        newErrorMessageBox.ContentText.text = FBLoginErrorMessage;
      };
    });

    void UserNameCallBack(IGraphResult result)
    {
      if (result.Error != null)
      {
        Debug.Log("Failed to get user name\nError: " + result.Error);
        return;
      }

      UserManager.DisplayName = result.ResultDictionary["name"].ToString();
      UserManager.faceId = result.ResultDictionary["id"].ToString(); ;
      Network.PostToServer("Login",
        ("method", "loginfb"),
        ("faceid", UserManager.faceId),
        ("name", UserManager.DisplayName))
        .Then(handler =>
        {
          ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          if (smartfoxResponse.GetInt("message_code") != 1)
            Game.Events.OnLoginFail.Invoke(
              smartfoxResponse.GetUtfString("message"));
          else
          {
            UserManager.SetInfoFB(smartfoxResponse);
            SFSManager.instance.Login();
            Game.Events.OnLoginSuccess.Invoke();
          }
        });
    }
  }

}