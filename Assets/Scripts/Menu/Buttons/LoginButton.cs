﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Linq;
using Utils;
using Sfs2X.Entities.Data;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(Button))]
  public class LoginButton : MonoBehaviour
  {
    [SerializeField] ValidatedInputField UsernameField = null;
    [SerializeField] ValidatedInputField PasswordField = null;
    [SerializeField] [TextArea] string FailureMessage = string.Empty;
    public ISFSObject SmartfoxResponse = null;

    public void OnClick()
    {
      ValidatedInputField[] validatedFields =
        new[] { UsernameField, PasswordField };

      var firstLocalInvalidField = validatedFields
        .ForEach(field => field.Validate())
        .FirstOrDefault(field => field.IsValidOnClient == false);

      if (firstLocalInvalidField != null) return;

      Network.PostToServer("Login",
        ("method", "login"),
        ("username", UsernameField.BaseComp.text),
        ("password", PasswordField.BaseComp.text))
        .Then(handler =>
        {
          SmartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          string responseCode = SmartfoxResponse.GetUtfString("code");
          if (responseCode == "lobby_failed_024")
            Game.Events.OnLoginFail.Invoke(FailureMessage);
          else
          {
            UserManager.SET_INFO_LOGIN_REGISTER(SmartfoxResponse);
            SFSManager.instance.Login();
            Game.Events.OnLoginSuccess.Invoke();
          }
        });
    }
  }
}