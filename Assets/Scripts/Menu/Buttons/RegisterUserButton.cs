﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Linq;
using Utils;
using Sfs2X.Entities.Data;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(Button))]
  public class RegisterUserButton : MonoBehaviour
  {
    [SerializeField] ValidatedInputField UsernameField = null;
    [SerializeField] ValidatedInputField PasswordField = null;
    [SerializeField] ValidatedInputField RetypedPasswordField = null;
    public UnityEvent OnRegistrationSuccess = new UnityEvent();
    public UnityEventString OnRegistrationFail = new UnityEventString();

    public void OnClick()
    {
      ValidatedInputField[] validatedFields =
        new[] { UsernameField, PasswordField, RetypedPasswordField };
      var firstLocalInvalidField = validatedFields
        .ForEach(field => field.Validate())
        .FirstOrDefault(field => field.IsValidOnClient == false);

      if (firstLocalInvalidField != null) return;

      var firstServerInvalidField = validatedFields
        .FirstOrDefault(field => field.IsValidOnServer == false);
      if (firstServerInvalidField != null) return;

      Network.PostToServer("Login",
        ("method", "register"),
        ("username", UsernameField.BaseComp.text),
        ("pass", PasswordField.BaseComp.text))
        .Then(handler =>
        {
          string responseCode = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData)
            .GetText("code");
          if (responseCode == "lobby_ok_113") OnRegistrationSuccess.Invoke();
          else SaveLoadData.listResponseCode[responseCode]["message"]
            .ToString()
            .PassTo(OnRegistrationFail.Invoke);
        });
    }
  }
}

