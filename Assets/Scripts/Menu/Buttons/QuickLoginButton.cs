﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Utils;
using UnityEngine.Events;

namespace BanCa
{
  public class QuickLoginButton : MonoBehaviour
  {
    public void OnClick() => Network.PostToServer("Login",
      ("method", "quicklogin"))
      .Then(handler =>
      {
        ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
          .PassTo(SFSObject.NewFromJsonData);
        if (smartfoxResponse.GetInt("message_code") != 1) Game.Events
          .OnLoginFail.Invoke(smartfoxResponse.GetUtfString("message"));
        else
        {
          UserManager.SetInfo(smartfoxResponse);
          SFSManager.instance.Login();
          Game.Events.OnLoginSuccess.Invoke();
        }
      });
  }
}
