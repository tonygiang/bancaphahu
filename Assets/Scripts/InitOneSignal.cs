﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class InitOneSignal : RunFixedTimePerLaunchBehaviour
{
  [SerializeField] string AppID = "cd10302e-c836-43fb-b50c-7edb84bf19e0";
  [SerializeField]
  OneSignal.OSInFocusDisplayOption DisplayOption =
    OneSignal.OSInFocusDisplayOption.Notification;
  [SerializeField] bool VibrateOnNotification = true;
  [SerializeField] bool SoundOnNotification = true;

  void Awake()
  {
    StartEvent.AddListener(() =>
    {
      OneSignal.StartInit(AppID)
      .Settings(new Dictionary<string, bool>() {
        { OneSignal.kOSSettingsAutoPrompt, true },
        { OneSignal.kOSSettingsInAppLaunchURL, true } })
      .EndInit();
      OneSignal.inFocusDisplayType = DisplayOption;
      OneSignal.RegisterForPushNotifications();
      OneSignal.EnableVibrate(VibrateOnNotification);
      OneSignal.EnableSound(SoundOnNotification);
    });
  }
}
