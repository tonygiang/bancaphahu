﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace BanCa
{
  public class Notification : MonoBehaviour
  {
    public TextMeshProUGUI ContentText = null;

    public void InstantiateWithMessage(string message) =>
      Instantiate(this).ContentText.text = message;
  }
}
