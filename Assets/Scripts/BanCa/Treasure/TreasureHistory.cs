﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Utils;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class TreasureHistory : MonoBehaviour
  {
    public IEnumerable<TreasureHistoryItem> RollHistory =
      Enumerable.Empty<TreasureHistoryItem>();
    public int PageSize = 10;
    public int MaxPage => (RollHistory.Count().CastTo<float>() / PageSize)
      .PassTo(Mathf.CeilToInt);
    public Observable<int> CurrentPage = new Observable<int>(1);
    public int CurrentPageBeginIndex =>
      RollHistory.Count() - (CurrentPage.Value * PageSize);
    public int CurrentPageEntryCount =>
      (CurrentPageBeginIndex + PageSize).ClampTo(1, PageSize);

    public void SetCurrentPage(int newCurrentPage) =>
      CurrentPage.Value = newCurrentPage.ClampTo(1, MaxPage);

    public void ChangeCurrentPage(int deltaPage) =>
      CurrentPage.Value = (CurrentPage.Value + deltaPage).ClampTo(1, MaxPage);

    public static void RequestRollHistory(int numberToRequest,
      int timeSortType = -1,
      int from = 0)
    {
      var parameters = new SFSObject();
      parameters.PutUtfString("username", UserManager.UserName);
      parameters.PutInt("number", numberToRequest);
      parameters.PutInt("timeSortType", timeSortType);
      parameters.PutInt("from", from);
      SFSManager.sfs.Send(new ExtensionRequest("kho_bau_history",
        parameters,
        NetworkManager.instance.LobbyRoom));
    }
  }
}
