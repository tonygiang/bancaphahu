﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using System;

namespace BanCa
{
  public class TreasureReward
  {
    public int ID = -1;
    public string Type = string.Empty;
    public int Value = -1;
    public bool IsJackpot = false;
    public string TextureKey
    {
      get
      {
        if (this.Type == "gem") return "gem";
        if (this.Type == "key") return "key";
        if (this.Type == "card")
        {
          if (this.Value == 10000) return "cao10k";
          if (this.Value == 20000) return "cap20k";
        }
        return string.Empty;
      }
    }

    public static TreasureReward FromSFSObject(ISFSObject source) =>
      new TreasureReward
      {
        ID = source.GetInt("id"),
        Type = source.GetUtfString("type"),
        Value = source.GetInt("value"),
        IsJackpot = source.GetBool("isJackpot")
      };

    static Dictionary<string, string> _toStringTemplate =
      new Dictionary<string, string>
    {
      {"gem", "{0} Kim Cương"},
      {"card", "Thẻ Cào {0}"},
      {"key", "{0} Chìa Khóa"}
    };

    public override string ToString() =>
      string.Format(_toStringTemplate[Type], Value);
  }
}
