﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Utils;

namespace BanCa
{
  public class TreasurePhoneCard : MonoBehaviour
  {
    [SerializeField] string OperatorName = string.Empty;
    public UnityEventString OnSuccess = new UnityEventString();
    public UnityEventString OnError = new UnityEventString();

    public void ChooseCard()
    {
      SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE,
        ProcessResponse);
      ISFSObject parameters = new SFSObject();
      parameters.PutUtfString("username", UserManager.UserName);
      parameters.PutUtfString("operator", OperatorName);
      SFSManager.sfs.Send(new ExtensionRequest("kho_bau_roll_choose_card",
        parameters,
        NetworkManager.instance.LobbyRoom));
    }

    void ProcessResponse(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_kho_bau_roll_choose_card")
        return;

      var response = evt.Params["params"].As<ISFSObject>();
      if (response.GetBool("success"))
        OnSuccess.Invoke(response.GetUtfString("message"));
      else OnError.Invoke(response.GetUtfString("message"));

      SFSManager.sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE,
        ProcessResponse);
    }
  }
}
