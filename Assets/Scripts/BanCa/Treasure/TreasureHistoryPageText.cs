﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(Text))]
  public class TreasureHistoryPageText : BoundedTextInt
  {
    protected override void Awake()
    {
      base.Awake();
      BoundedValue = Game.Treasure.History.CurrentPage;
    }
  }
}