﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using Sfs2X.Core;
using Utils;
using Spine.Unity;
using UnityEngine.Events;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class TreasureChest : MonoBehaviour
  {
    public GameObject ClosedState = null;
    public SkeletonGraphic OpenAnimation = null;
    public Observable<int> RewardID = new Observable<int>(-1);
    public AnimatorFunctions RewardText = null;
    public UnityEvent OnRollJackpot = new UnityEvent();
    public UnityEvent OnRollCard = new UnityEvent();

    public void SendRollRequest()
    {
      SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE,
        ProcessRollResult);
      ISFSObject parameters = new SFSObject();
      parameters.PutUtfString("username", UserManager.UserName);
      SFSManager.sfs.Send(new ExtensionRequest("kho_bau_roll", parameters, NetworkManager.instance.LobbyRoom));
    }

    void ProcessRollResult(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_kho_bau_roll") return;

      var result = evt.Params["params"].As<ISFSObject>();
      if (result.GetBool("success") == false)
        Game.Treasure.Events.OnRollError.Invoke(result.GetUtfString("message"));
      else
      {
        RewardID.Value = result.GetInt("id");
        Game.Treasure.OpeningChest = this;
        Game.Treasure.Events.OnRollSuccess.Invoke(new TreasureRollEvent
        {
          Message = result.GetUtfString("message"),
          CurrentKeys = result.GetInt("currentKey"),
          CurrentGem = result.GetInt("gem")
        });
      }

      SFSManager.sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE,
        ProcessRollResult);
    }
  }
}