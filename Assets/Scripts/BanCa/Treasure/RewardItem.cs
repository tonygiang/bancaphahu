﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class RewardItem : MonoBehaviour
  {
    public Image IconImage = null;
    public Text ValueText = null;
  }
}
