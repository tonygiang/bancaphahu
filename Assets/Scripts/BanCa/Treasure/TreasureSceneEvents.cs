﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.Events;
using Sfs2X.Core;
using Sfs2X.Entities.Data;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class TreasureSceneEvents : MonoBehaviour
  {
    public UnityEvent OnReady = new UnityEvent();
    public UnityEventString OnRollError = new UnityEventString();
    public UnityEventTreasureRoll OnRollSuccess = new UnityEventTreasureRoll();
    public UnityEventTreasureChest OnShowReward = new UnityEventTreasureChest();
    public UnityEvent OnNewPhase = new UnityEvent();
    public UnityEventSFS OnNewHistory = new UnityEventSFS();
    public UnityEventSFS OnAfterNewHistory = new UnityEventSFS();

    public void InvokeOnShowReward(TreasureChest chest) =>
      OnShowReward.Invoke(chest);

    public void InvokeOnNewPhaseDelayed(float seconds) => new GameObject()
      .AddComponent<DelayedAction>()
      .Call(OnNewPhase.Invoke, seconds);
  }

  public class TreasureRollEvent
  {
    public string Message = string.Empty;
    public int CurrentKeys = 0;
    public int CurrentGem = 0;
  }

  [Serializable]
  public class UnityEventTreasureRoll : UnityEvent<TreasureRollEvent> { }

  [Serializable]
  public class UnityEventTreasureChest : UnityEvent<TreasureChest> { }
}
