﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

namespace BanCa
{
  public class UpdateKeyOnRollSuccess : ExtensionBehaviour<TreasureSceneEvents>
  {
    void OnEnable() => BaseComp.OnRollSuccess.AddListener(UpdateKey);

    void OnDisable() => BaseComp.OnRollSuccess.RemoveListener(UpdateKey);

    void UpdateKey(TreasureRollEvent evt) =>
      Session.TreasureKeys.Value = evt.CurrentKeys;
  }
}
