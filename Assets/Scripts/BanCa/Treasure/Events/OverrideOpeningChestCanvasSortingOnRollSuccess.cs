﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class OverrideOpeningChestCanvasSortingOnRollSuccess :
    ExtensionBehaviour<TreasureSceneEvents>
  {
    [SerializeField] int OrderInLayer = 0;

    void OnEnable() => BaseComp.OnRollSuccess.AddListener(OverrideSorting);

    void OnDisable() => BaseComp.OnRollSuccess.RemoveListener(OverrideSorting);

    void OverrideSorting(TreasureRollEvent evt)
    {
      var canvas = Game.Treasure.OpeningChest.gameObject.AddComponent<Canvas>();
      canvas.overrideSorting = true;
      canvas.sortingOrder = OrderInLayer;
    }
  }
}
