﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class RemoveCanvasFromOpeningChestOnNewPhase : MonoBehaviour
  {
    void OnEnable() => Game.Treasure.Events.OnNewPhase
      .AddListener(RemoveCanvas);

    void OnDisable() => Game.Treasure.Events.OnNewPhase
      .RemoveListener(RemoveCanvas);

    void RemoveCanvas() => Game.Treasure.OpeningChest.GetComponent<Canvas>()
      .PassTo(Destroy);
  }
}