﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class ToggleChestStateOnRollSuccess : MonoBehaviour
  {
    [SerializeField] bool ClosedStateVisible = false;
    [SerializeField] bool OpenAnimationStateVisible = true;

    void OnEnable() => Game.Treasure.Events.OnRollSuccess
      .AddListener(ToggleChest);

    void OnDisable() => Game.Treasure.Events.OnRollSuccess
      .RemoveListener(ToggleChest);

    void ToggleChest(TreasureRollEvent evt)
    {
      Game.Treasure.OpeningChest.ClosedState.SetActive(ClosedStateVisible);
      Game.Treasure.OpeningChest.OpenAnimation.gameObject
        .SetActive(OpenAnimationStateVisible);
    }
  }
}
