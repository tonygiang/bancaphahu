﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Utils;
using System.Linq;

namespace BanCa
{
  public class RefreshHistoryItemsOnNewHistory
    : ExtensionBehaviour<TreasureHistory>
  {
    [SerializeField] Transform HistoryItemContainer = null;
    [SerializeField] TreasureHistoryItem HistoryItemPrefab = null;
    [SerializeField] int SiblingIndexOfNewestItem = 1;

    void Start() => Game.Treasure.Events.OnNewHistory
      .AddListener(CreateHistoryItem);

    void OnDestroy() => Game.Treasure.Events.OnNewHistory
      .RemoveListener(CreateHistoryItem);

    void CreateHistoryItem(ISFSObject isfso)
    {
      BaseComp.RollHistory = isfso.GetSFSArray("arr")
        .Cast<ISFSObject>()
        .Where(entry => BaseComp.RollHistory.Select(e => e.RollTime)
          .Contains(entry.GetLong("time")) == false)
        .ToList()
        .ConvertAll(entry => Instantiate(HistoryItemPrefab,
          HistoryItemContainer)
          .Init(entry.GetLong("time"), entry.GetInt("idKhoBau")))
        .Concat(BaseComp.RollHistory)
        .OrderBy(item => item.RollTime)
        .ForEach(item => item.transform
          .SetSiblingIndex(SiblingIndexOfNewestItem));
      Game.Treasure.Events.OnAfterNewHistory.Invoke(isfso);
    }
  }
}
