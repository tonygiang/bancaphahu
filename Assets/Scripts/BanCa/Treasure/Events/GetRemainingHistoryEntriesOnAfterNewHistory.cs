﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using System.Linq;
using Utils;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class GetRemainingHistoryEntriesOnAfterNewHistory :
    ExtensionBehaviour<TreasureHistory>
  {
    [SerializeField] int BatchSize = 100;

    void OnEnable() => Game.Treasure.Events.OnAfterNewHistory
      .AddListener(GetRemainingEntries);

    void OnDisable() => Game.Treasure.Events.OnAfterNewHistory
      .RemoveListener(GetRemainingEntries);

    void GetRemainingEntries(ISFSObject isfso) => BatchSize
      .NullIf(BaseComp.RollHistory.Count() >= isfso.GetInt("totalSize"))?
      .MakeTuple(-1, BaseComp.RollHistory.Count())
      .PassTo(TreasureHistory.RequestRollHistory);
  }
}
