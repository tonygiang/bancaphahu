﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

namespace BanCa
{
  public class UpdateRewardTextOnChestRewardIDChange :
    ExtensionBehaviour<TreasureChest>
  {
    [SerializeField] TextMeshProUGUI RewardText = null;
    [SerializeField] [TextArea] string Template = string.Empty;

    void OnEnable() => BaseComp.RewardID.OnAfterChange += UpdateRewardText;

    void OnDisable() => BaseComp.RewardID.OnAfterChange -= UpdateRewardText;

    void UpdateRewardText(int oldValue, int newValue)
    {
      TreasureReward rewardType = Game.Treasure.Rewards[newValue].First();
      RewardText.text = string.Format(Template,
        rewardType.Value,
        rewardType.TextureKey);
    }
  }
}
