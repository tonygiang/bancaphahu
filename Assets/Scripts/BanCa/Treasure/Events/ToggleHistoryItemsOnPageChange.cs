﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;
using System;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class ToggleHistoryItemsOnPageChange :
    ExtensionBehaviour<TreasureHistory>
  {
    void OnEnable() => BaseComp.CurrentPage.OnAfterSet +=
      ToggleHistoryItems;

    void OnDisable() => BaseComp.CurrentPage.OnAfterSet -=
      ToggleHistoryItems;

    void ToggleHistoryItems(int value)
    {
      Debug.Log($"CurrentPageBeginIndex: {BaseComp.CurrentPageBeginIndex}, CurrentPageEntryCount: {BaseComp.CurrentPageEntryCount}");
      BaseComp.RollHistory
        .ForEach(i => i.gameObject.SetActive(false))
        .Skip(BaseComp.CurrentPageBeginIndex)
        .Take(BaseComp.CurrentPageEntryCount)
        .ForEach(i => i.gameObject.SetActive(true));
    }
  }
}
