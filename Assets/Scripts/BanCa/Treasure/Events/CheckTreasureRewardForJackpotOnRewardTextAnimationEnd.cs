﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BanCa
{
  public class CheckTreasureRewardForJackpotOnRewardTextAnimationEnd :
    ExtensionBehaviour<TreasureChest>
  {
    void OnEnable() => BaseComp.RewardText.OnAnimationEnd
      .AddListener(CheckReward);

    void OnDisable() => BaseComp.RewardText.OnAnimationEnd
      .RemoveListener(CheckReward);

    void CheckReward()
    {
      if (Game.Treasure.Rewards[BaseComp.RewardID.Value].First().IsJackpot)
        BaseComp.OnRollJackpot.Invoke();
    }
  }
}
