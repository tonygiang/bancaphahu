﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BanCa
{
  public class CheckTreasureRewardForCardOnRewardTextAnimationEnd :
    ExtensionBehaviour<TreasureChest>
  {
    void OnEnable() => BaseComp.RewardText.OnAnimationEnd
      .AddListener(CheckReward);

    void OnDisable() => BaseComp.RewardText.OnAnimationEnd
      .RemoveListener(CheckReward);

    void CheckReward()
    {
      if (Game.Treasure.Rewards[BaseComp.RewardID.Value].First().Type == "card")
        BaseComp.OnRollCard.Invoke();
    }
  }
}
