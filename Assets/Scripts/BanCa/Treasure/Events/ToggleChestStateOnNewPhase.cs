﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class ToggleChestStateOnNewPhase : MonoBehaviour
  {
    [SerializeField] bool ClosedStateVisible = true;
    [SerializeField] bool OpenAnimationStateVisible = false;
    [SerializeField] bool RewardTextVisible = false;

    void OnEnable() => Game.Treasure.Events.OnNewPhase.AddListener(ToggleChest);

    void OnDisable() => Game.Treasure.Events.OnNewPhase
      .RemoveListener(ToggleChest);

    void ToggleChest()
    {
      Game.Treasure.OpeningChest.ClosedState.SetActive(ClosedStateVisible);
      Game.Treasure.OpeningChest.OpenAnimation.gameObject
        .SetActive(OpenAnimationStateVisible);
      Game.Treasure.OpeningChest.RewardText.gameObject
        .SetActive(RewardTextVisible);
    }
  }
}