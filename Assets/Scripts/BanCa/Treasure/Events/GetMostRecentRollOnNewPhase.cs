﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class GetMostRecentRollOnNewPhase : MonoBehaviour
  {
    void OnEnable() => Game.Treasure.Events.OnNewPhase
      .AddListener(GetMostRecentRoll);

    void OnDisable() => Game.Treasure.Events.OnNewPhase
      .RemoveListener(GetMostRecentRoll);

    void GetMostRecentRoll() => TreasureHistory.RequestRollHistory(1);
  }
}
