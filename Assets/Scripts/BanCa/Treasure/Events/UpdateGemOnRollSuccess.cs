﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class UpdateGemOnRollSuccess : ExtensionBehaviour<TreasureSceneEvents>
  {
    void OnEnable() => BaseComp.OnRollSuccess.AddListener(UpdateGem);

    void OnDisable() => BaseComp.OnRollSuccess.RemoveListener(UpdateGem);

    void UpdateGem(TreasureRollEvent evt) => Session.Gem.Value = evt.CurrentGem;
  }
}