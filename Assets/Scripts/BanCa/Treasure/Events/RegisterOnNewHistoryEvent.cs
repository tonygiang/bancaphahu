﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Utils;
using System.Linq;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(TreasureSceneEvents))]
  public class RegisterOnNewHistoryEvent :
    ExtensionBehaviour<TreasureSceneEvents>
  {
    void Start() => SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE,
        RefreshHistory);

    void OnDestroy() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE,
        RefreshHistory);

    void RefreshHistory(BaseEvent evt) => evt
      .NullIf(evt.Params["cmd"].ToString() != "res_kho_bau_history")?
      .Params["params"].As<ISFSObject>()
      .PassTo(BaseComp.OnNewHistory.Invoke);
  }
}

