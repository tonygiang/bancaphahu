﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

namespace BanCa
{
  public class TreasureScene : MonoBehaviour
  {
    public TreasureSceneEvents Events = null;
    public TreasureHistory History = null;
    public ILookup<int, TreasureReward> Rewards =
      Enumerable.Empty<TreasureReward>().ToLookup(r => r.ID);
    public Sprite GemSprite = null;
    public Sprite KeySprite = null;
    public Sprite Card10KSprite = null;
    public Sprite Card20KSprite = null;
    public Color JackpotColor = Color.white;
    public TreasureChest OpeningChest = null;

    void Awake() { Game.Treasure = this; Input.multiTouchEnabled = false; }

    void OnDestroy() => Input.multiTouchEnabled = true;

    public Sprite GetRewardSprite(TreasureReward reward)
    {
      if (reward.Type == "gem") return GemSprite;
      if (reward.Type == "key") return KeySprite;
      if (reward.Type == "card")
      {
        if (reward.Value == 10000) return Card10KSprite;
        if (reward.Value == 20000) return Card20KSprite;
      }
      return null;
    }
  }
}
