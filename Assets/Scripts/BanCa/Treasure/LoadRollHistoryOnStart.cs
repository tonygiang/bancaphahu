﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class LoadRollHistoryOnStart : MonoBehaviour
  {
    [SerializeField] int StartingEntryNumber = 100;

    void Start() => TreasureHistory.RequestRollHistory(StartingEntryNumber);
  }
}
