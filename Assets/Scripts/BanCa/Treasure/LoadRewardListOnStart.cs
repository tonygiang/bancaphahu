﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Utils;
using System.Linq;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(TreasureSceneEvents))]
  public class LoadRewardListOnStart : ExtensionBehaviour<TreasureSceneEvents>
  {
    [SerializeField] Transform RewardContainer = null;
    [SerializeField] RewardItem RewardItemPrefab = null;

    void Start()
    {
      SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, LoadRewards);
      SFSManager.sfs.Send(new ExtensionRequest("kho_bau_info",
        new SFSObject(),
        NetworkManager.instance.LobbyRoom));
    }

    void OnDestroy() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, LoadRewards);

    void LoadRewards(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_kho_bau_info") return;

      Game.Treasure.Rewards = evt.Params["params"].As<ISFSObject>()
        .GetSFSArray("arr").Cast<ISFSObject>()
        .Select(sfsObject => TreasureReward.FromSFSObject(sfsObject))
        .OrderBy(reward => reward.Type)
        .ThenBy(reward => reward.Value)
        .ForEach(reward =>
        {
          RewardItem newItem = Instantiate(RewardItemPrefab, RewardContainer);
          newItem.IconImage.sprite = Game.Treasure.GetRewardSprite(reward);
          newItem.ValueText.text = reward.Value
            .ToString("N0", Config.MoneyFormat);
          if (reward.IsJackpot)
            newItem.ValueText.color = Game.Treasure.JackpotColor;
        })
        .ToLookup(r => r.ID);
      BaseComp.OnReady.Invoke();
    }
  }
}