﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using Utils;
using System;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(TextMeshProUGUI))]
  public class TreasureHistoryItem : ExtensionBehaviour<TextMeshProUGUI>
  {
    public long RollTime = 0L;
    public int RewardID = -1;
    public string TimeFormat = "yyyy-MM-dd HH:mm:ss";

    public TreasureHistoryItem Init(long rollTime, int rewardID)
    {
      RollTime = rollTime;
      RewardID = rewardID;
      TreasureReward rewardType = Game.Treasure.Rewards[RewardID].First();
      BaseComp.text = string.Format(BaseComp.text,
        DateTimeOffset.FromUnixTimeMilliseconds(RollTime)
          .ToString(TimeFormat),
        rewardType.ToString(),
        rewardType.TextureKey);
      return this;
    }
  }
}