﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Utils;

namespace BanCa
{
  public class ToggleObjectsIfReceiveFunction : MonoBehaviour
  {
    [SerializeField] GameObject[] ObjectsToToggle = null;
    [SerializeField] string FunctionNameToToggle = string.Empty;

    void OnEnable() => Game.Events.OnReceiveFunctions += Toggle;

    void OnDisable() => Game.Events.OnReceiveFunctions -= Toggle;

    void Toggle(IEnumerable<string> functions) => ObjectsToToggle.ForEach(obj =>
      obj.SetActive(functions.Contains(FunctionNameToToggle)));
  }

}