﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(Text))]
  public class VIPLevelText : BoundedTextInt
  {
    protected override void Awake()
    {
      base.Awake();
      BoundedValue = Session.CurrentVIPLevel;
    }
  }
}
