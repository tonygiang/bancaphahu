﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class Game
  {
    public static BanCaEvents Events = null;
    public static MainMenuScene MainMenu = null;
    public static ShootingScene Shooting = null;
    public static TreasureScene Treasure = null;
    public static VIP VIP = null;
  }
}
