﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace BanCa
{
  public class Session
  {
    public static string UserName = string.Empty;
    public static string DisplayName = string.Empty;
    public static string AccessToken = string.Empty;
    public static string FirebaseID = string.Empty;
    public static string PhoneNumber = string.Empty;
    // public static int VIPStatus = 0;
    public static IEnumerable<string> Functions = Enumerable.Empty<string>();
    public static Observable<int> Gem = new Observable<int>(0);
    public static Observable<int> CurrentVIPGem = new Observable<int>(-1);
    public static Observable<int> CurrentVIPLevel = new Observable<int>(-1);
    public static Observable<int> TreasureKeys = new Observable<int>(0);
    public static Observable<long> OnlineMinutes = new Observable<long>(0);
    public static Dictionary<string, Skill> Skills =
      new Dictionary<string, Skill>();
    public static IEnumerable<VIPLevel> VIPLevels =
      Enumerable.Empty<VIPLevel>();
  }
}
