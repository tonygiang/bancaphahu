﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Linq;

namespace BanCa
{
  public class BanCaEvents : MonoBehaviour
  {
    public UnityEvent OnLoginSuccess = new UnityEvent();
    public UnityEventString OnLoginFail = new UnityEventString();
    public UnityEvent OnLogout = new UnityEvent();
    public UnityEvent OnNewWave = new UnityEvent();
    public UnityEventOnlineTime OnNewOnlineTimeMilestoneReached =
      new UnityEventOnlineTime();
    public UnityEventString OnNotification = new UnityEventString();
    public Action<IEnumerable<string>> OnReceiveFunctions = functions => { };

    void Awake() => Game.Events = this;

    public void InvokeLoginSuccess() => OnLoginSuccess.Invoke();
    public void InvokeLogout() => OnLogout.Invoke();
  }
}