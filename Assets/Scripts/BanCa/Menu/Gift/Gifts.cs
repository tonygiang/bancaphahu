﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BanCa
{
  public class Gifts : MonoBehaviour
  {
    public static IEnumerable<LoginReward> LoginRewards =
      Enumerable.Empty<LoginReward>();
    public List<LoginRewardItem> LoginRewardItems = new List<LoginRewardItem>();
    public static IEnumerable<OnlineTimeMilestone> OnlineTimeMilestones =
      Enumerable.Empty<OnlineTimeMilestone>();
    public List<OnlineTimeMilestoneItem> OnlineTimeMilestoneItems =
      new List<OnlineTimeMilestoneItem>();

    void Awake() => Game.MainMenu.Gifts = this;

    public void ClearOnlineTimeMilestoneItems()
    {
      OnlineTimeMilestoneItems.ForEach(m => Destroy(m.gameObject));
      OnlineTimeMilestoneItems.Clear();
    }

    public void ClearCheckInItems()
    {
      LoginRewardItems.ForEach(Destroy);
      LoginRewardItems.Clear();
    }
  }
}
