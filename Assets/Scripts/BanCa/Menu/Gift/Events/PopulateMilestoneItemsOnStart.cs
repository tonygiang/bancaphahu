﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;

namespace BanCa
{
  public class PopulateMilestoneItemsOnStart : MonoBehaviour
  {
    [SerializeField] Transform MilestoneItemContainer = null;
    [SerializeField] OnlineTimeMilestoneItem MilestoneItemPrefab = null;

    void OnEnable()
    {
      if (Game.MainMenu.Gifts.OnlineTimeMilestoneItems.Count > 0) return;

      Gifts.OnlineTimeMilestones
        .Select(m => Instantiate(MilestoneItemPrefab, MilestoneItemContainer)
          .Init(m))
        .PassTo(Game.MainMenu.Gifts.OnlineTimeMilestoneItems.AddRange);
    }
  }
}
