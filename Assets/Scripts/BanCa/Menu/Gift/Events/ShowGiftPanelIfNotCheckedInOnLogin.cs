﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Utils;
using System.Linq;

namespace BanCa
{
  public class ShowGiftPanelIfNotCheckedInOnLogin : MonoBehaviour
  {
    [SerializeField] GameObject[] GiftPanelObjects = null;

    void OnEnable() => Game.Events.OnLoginSuccess.AddListener(ShowGiftPanel);

    void OnDisable() =>
      Game.Events.OnLoginSuccess.RemoveListener(ShowGiftPanel);

    void ShowGiftPanel()
    {
      Network.PostToServer("BehindLogin",
        ("method", "getuserreward7day"),
        ("username", UserManager.UserName))
        .Then(handler =>
        {
          ISFSObject smartfoxResponse = handler.text.PassTo(Crypto.Decrypt)
            .PassTo(SFSObject.NewFromJsonData);
          if (smartfoxResponse.GetInt("message_code") != 1)
            MessageBox.instance.Show(smartfoxResponse.GetUtfString("message"));
          else
          {
            FreeManager.CheckInSFSData = smartfoxResponse;
            if (smartfoxResponse.GetBool("Checked")) return;

            GiftPanelObjects.ForEach(go => go.SetActive(true));
          }
        });
    }
  }
}
