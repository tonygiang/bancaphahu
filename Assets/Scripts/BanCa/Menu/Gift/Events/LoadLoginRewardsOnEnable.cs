﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class LoadLoginRewardsOnEnable : MonoBehaviour
  {
    void OnEnable() => FreeManager.Instance
      .LoadUserCheckIn(FreeManager.CheckInSFSData);
  }
}
