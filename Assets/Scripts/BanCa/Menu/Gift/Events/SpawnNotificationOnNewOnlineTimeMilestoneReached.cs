﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;

namespace BanCa
{
  public class SpawnNotificationOnNewOnlineTimeMilestoneReached : MonoBehaviour
  {
    [SerializeField] [TextArea] string NotificationTemplate = string.Empty;

    void OnEnable() => Game.Events.OnNewOnlineTimeMilestoneReached
      .AddListener(SpawnNotification);

    void OnDisable() => Game.Events.OnNewOnlineTimeMilestoneReached
      .RemoveListener(SpawnNotification);

    void SpawnNotification(OnlineTimeMilestone milestone)
    {
      string gemRewardString = milestone.GemReward == 0 ?
        string.Empty :
        $"{milestone.GemReward}<sprite name=\"gem\">";
      string keyRewardString = milestone.KeyReward == 0 ?
        string.Empty :
        $"{milestone.KeyReward}<sprite name=\"key\">";
      string skillRewardString = milestone.SkillRewards
        .Select(r => r.Value > 0 ?
          $"{r.Value}<sprite name=\"{r.Key}\">" :
          string.Empty)
        .MakeTupleAppend(string.Empty)
        .PassTo(string.Join);
      string.Format(NotificationTemplate,
        milestone.Minutes,
        $"{gemRewardString} {keyRewardString} {skillRewardString}")
        .PassTo(Game.Events.OnNotification.Invoke);
    }
  }
}
