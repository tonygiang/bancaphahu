﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Sfs2X.Entities;
using Utils;
using System.Linq;

namespace BanCa
{
  public class GetOnlineMilestonesOnJoinRoom : MonoBehaviour
  {
    MainMenuScene Scene => Game.MainMenu;

    void Start() => SFSManager.sfs.AddEventListener(SFSEvent.ROOM_JOIN,
      GetOnlineMilestones);

    void GetOnlineMilestones(BaseEvent e)
    {
      SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, LoadRewards);
      ISFSObject parameters = new SFSObject();
      parameters.PutUtfString("username", UserManager.UserName);
      SFSManager.sfs.Send(new ExtensionRequest("online_gift",
        parameters,
        e.Params["room"].As<Room>()));
    }

    void LoadRewards(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_online_gift") return;

      ISFSObject isfso = evt.Params["params"].As<ISFSObject>();
      Session.OnlineMinutes.Value = isfso.GetLong("currentOnlinetime");
      Gifts.OnlineTimeMilestones = isfso.GetSFSArray("listdata")
        .Cast<ISFSObject>()
        .OrderBy(m => m.GetLong("timepoint"))
        .Select(m => new OnlineTimeMilestone
        {
          Minutes = m.GetLong("timepoint"),
          GemReward = m.GetInt("gem"),
          KeyReward = m.GetInt("key"),
          SkillRewards = m.GetSFSObject("objSkill").ToDictionary<int>()
        });

      SFSManager.sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE,
        LoadRewards);
    }
  }
}
