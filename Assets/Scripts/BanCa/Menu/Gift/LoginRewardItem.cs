﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

namespace BanCa
{
  public class LoginRewardItem : MonoBehaviour
  {
    public TextMeshProUGUI DayText = null;
    public TextMeshProUGUI RewardText = null;
    public GameObject ClaimedOverlay = null;
    public GameObject UnreachedOverlay = null;
    public GameObject TickMark = null;
    public LoginReward BoundReward = null;
    static string _countAndIconTemplate =
      "{0}<size=75%><sprite name=\"{1}\"></size>";

    public LoginRewardItem Init(LoginReward reward)
    {
      BoundReward = reward;
      DayText.text = string.Format(DayText.text, reward.ID);
      string gemRewardText = string.Format(_countAndIconTemplate,
        reward.Gem,
        "gem");
      string keyRewardText = reward.Key > 0 ?
        "\n" + string.Format(_countAndIconTemplate,
          reward.Key,
          "key") :
        string.Empty;
      string skillRewardText = (reward.Skills.Count > 0 ? "\n" : string.Empty)
        + string.Join(" ",
          reward.Skills.Select(p => string.Format(_countAndIconTemplate,
            p.Value,
            p.Key)));
      RewardText.text = $"{gemRewardText}{keyRewardText}{skillRewardText}";
      UnreachedOverlay.SetActive(reward.ID > SaveLoadData.CheckInValue);
      SetReached(reward.ID < SaveLoadData.CheckInValue ||
        FreeManager.CheckInSFSData.GetBool("Checked"));
      return this;
    }

    public LoginRewardItem SetReached(bool state)
    {
      ClaimedOverlay.SetActive(state);
      TickMark.SetActive(state);
      return this;
    }
  }
}
