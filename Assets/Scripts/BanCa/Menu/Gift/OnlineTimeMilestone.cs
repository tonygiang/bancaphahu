﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace BanCa
{
  public class OnlineTimeMilestone
  {
    public long Minutes = -1;
    public int GemReward = -1;
    public int KeyReward = -1;
    public IDictionary<string, int> SkillRewards = Enumerable.Empty<int>()
      .ToDictionary(e => string.Empty);
  }
}
