﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class LoginReward
  {
    public int ID = -1;
    public int Gem = 0;
    public int Key = 0;
    public Dictionary<string, int> Skills = new Dictionary<string, int>();
  }
}
