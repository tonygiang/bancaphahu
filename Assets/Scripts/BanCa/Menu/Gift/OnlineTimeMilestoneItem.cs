﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Utils;

namespace BanCa
{
  public class OnlineTimeMilestoneItem : MonoBehaviour
  {
    public OnlineTimeMilestone Milestone = new OnlineTimeMilestone();
    public TextMeshProUGUI MilestoneTimeText = null;
    public string MilestoneTimeFormat = "hh\\:mm\\:ss";
    public TextMeshProUGUI GemRewardText = null;
    public TextMeshProUGUI KeyRewardText = null;
    public TextMeshProUGUI SkillRewardText = null;
    public RectTransform ProgressOverlay = null;
    public GameObject TickMark = null;

    void Awake() =>
      Session.OnlineMinutes.OnAfterChange += UpdateOnlineMinutes;

    void OnDestroy() =>
      Session.OnlineMinutes.OnAfterChange -= UpdateOnlineMinutes;

    void UpdateOnlineMinutes(long oldValue, long newValue)
    {
      ProgressOverlay.gameObject.SetActive(newValue < Milestone.Minutes);
      TickMark.SetActive(newValue >= Milestone.Minutes);
    }

    public OnlineTimeMilestoneItem Init(OnlineTimeMilestone newMilestone)
    {
      Milestone = newMilestone;
      MilestoneTimeText.text = TimeSpan.FromMinutes(Milestone.Minutes)
        .ToString(MilestoneTimeFormat);
      GemRewardText.gameObject.SetActive(Milestone.GemReward > 0);
      GemRewardText.text = string.Format(GemRewardText.text,
        Milestone.GemReward);
      KeyRewardText.gameObject.SetActive(Milestone.KeyReward > 0);
      KeyRewardText.text = string.Format(KeyRewardText.text,
        Milestone.KeyReward);
      newMilestone.SkillRewards.ForEach(p => 
      {
        if (p.Value == 0) return;
        var newSkillRewardText = Instantiate(SkillRewardText,
          SkillRewardText.transform.parent);
        newSkillRewardText.text = string.Format(newSkillRewardText.text,
          p.Value,
          p.Key);
        newSkillRewardText.gameObject.SetActive(true);
      });
      UpdateOnlineMinutes(Session.OnlineMinutes.Value,
        Session.OnlineMinutes.Value);
      return this;
    }
  }
}
