﻿using System.Collections;
using System.Collections.Generic;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace BanCa
{
    public class CheckOTP : MonoBehaviour
    {
        MainMenuScene Scene => Game.MainMenu;
        [SerializeField] CheckOTP NotifyOTPVerifycationPanel = null;
        [SerializeField] Text ContentText;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ShowNotifyOTPUser()
        {
            if (BanCa.Session.FirebaseID == null || BanCa.Session.FirebaseID.Equals("") ||
                BanCa.Session.PhoneNumber == null || BanCa.Session.PhoneNumber.Equals(""))
            {
                SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, ProcessCheckOTPResult);
                ISFSObject parameters = new SFSObject();

                SFSManager.sfs.Send(new ExtensionRequest("new_user_login_req_otp", parameters, NetworkManager.instance.LobbyRoom));
            }
        }

        void ProcessCheckOTPResult(BaseEvent evt)
        {
            Debug.Log(evt);
            if (evt.Params["cmd"].ToString() != "new_user_login_req_otp") return;

            var result = evt.Params["params"].As<ISFSObject>();

            //ContentText.text = result.GetUtfString("code");

            string responseCode = result.GetUtfString("code");
            ContentText.text = SaveLoadData.listResponseCode[responseCode]["message"].ToString();

            SFSManager.sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, ProcessCheckOTPResult);

            NotifyOTPVerifycationPanel.gameObject.SetActive(true);
        }
    }
}
