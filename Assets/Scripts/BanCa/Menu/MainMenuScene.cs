﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class MainMenuScene : MonoBehaviour
  {
    public MainMenuSceneEvents Events = null;
    public Gifts Gifts = null;
    public OTP OTP = null;
        public CheckOTP checkOTP = null;

        void Awake() => Game.MainMenu = this;
  }
}
