﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BanCa
{
  public class MainMenuSceneEvents : MonoBehaviour
  {
    public UnityEvent OnHaveFirebaseUID = new UnityEvent();
  }
}
