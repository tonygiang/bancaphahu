﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
using UnityEngine.Events;

namespace BanCa
{
  public class VIP : MonoBehaviour
  {
    public Sprite[] LevelImages = Array.Empty<Sprite>();
    public Image ProgressBarUntilNextLevel = null;
    public Image VIPCurrentLevelImage = null;
    public Image VIPNextLevelImage = null;
    public TextMeshProUGUI GemUntilNextLevelText = null;

    void Awake() => Game.VIP = this;
  }
}
