﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using UnityEngine.Events;

namespace BanCa
{
  public class RequireOTPButton : MonoBehaviour
  {
    [SerializeField] OTP OTPVerificationPanel = null;
    MainMenuScene Scene => Game.MainMenu;
    public UnityEvent OnHaveFirebaseUID = new UnityEvent();

    public void OnClick()
    {
      if (Session.FirebaseID.HasContent())
      { OnHaveFirebaseUID.Invoke(); return; }

      OTPVerificationPanel.gameObject.SetActive(true);
      OTPVerificationPanel.OnHaveFirebaseUID = OnHaveFirebaseUID;
    }
  }
}
