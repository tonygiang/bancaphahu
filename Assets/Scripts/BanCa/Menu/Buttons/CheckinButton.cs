﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class CheckinButton : MonoBehaviour
  {
    public void OnClick() => SFSManager.instance.SentUserCheckIn();
  }
}
