﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using Sfs2X.Core;
using Utils;
using UnityEngine.UI;
using UnityEngine.Events;

namespace BanCa
{
  public class OTP : MonoBehaviour
  {
    [SerializeField] ValidatedInputField PhoneNumberInputField = null;
    [SerializeField] InputField SMSCodeInputField = null;
    [SerializeField] string PhoneNumberPrefix = "+84";
    public UnityEvent OnReset = new UnityEvent();
    public UnityEvent OnSMSSent = new UnityEvent();
    public UnityEventString OnError = new UnityEventString();
    [SerializeField] [TextArea] string ErrorMessage = string.Empty;
    [SerializeField] uint TimeoutMilliseconds = 120 * 1000;
    MainMenuScene Scene => Game.MainMenu;
    string _SMSVerificationID = string.Empty;
    string _tempFID = string.Empty;
    string _tempPhoneNumber = string.Empty;
    [HideInInspector] public UnityEvent OnHaveFirebaseUID = null;

    void OnEnable() => OnReset.Invoke();

    public void SendSMS()
    {
      if (!PhoneNumberInputField.IsValidOnClient ||
        !PhoneNumberInputField.IsValidOnServer)
      { OnError.Invoke(ErrorMessage); return; }

      PhoneAuthProvider.GetInstance(FirebaseAuth.DefaultInstance)
        .VerifyPhoneNumber(
          PhoneNumberPrefix + PhoneNumberInputField.BaseComp.text,
          TimeoutMilliseconds,
          null,
          verificationCompleted: SendFirebaseID,
          verificationFailed: OnError.Invoke,
          codeSent: (id, token) =>
          { _SMSVerificationID = id; OnSMSSent.Invoke(); },
          codeAutoRetrievalTimeOut: id =>
          { OnError.Invoke(ErrorMessage); return; });
    }

    public void ProcessSMSCode() => PhoneAuthProvider
      .GetInstance(FirebaseAuth.DefaultInstance)
      .GetCredential(_SMSVerificationID, SMSCodeInputField.text)
      .PassTo(SendFirebaseID);

    void SendFirebaseID(Credential credential) => FirebaseAuth.DefaultInstance
      .SignInWithCredentialAsync(credential)
      .ContinueWith(task =>
      {
        if (task.IsFaulted) { OnError.Invoke(ErrorMessage); return; };

        SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE,
          ProcessOTPResult);
        ISFSObject parameters = new SFSObject();
        _tempFID = task.Result.UserId;
        _tempPhoneNumber = PhoneNumberInputField.BaseComp.text;
        parameters.PutUtfString("firebaseid", task.Result.UserId);
        parameters.PutUtfString("phone", PhoneNumberInputField.BaseComp.text);
        parameters.PutUtfString("username", UserManager.UserName);
        SFSManager.sfs.Send(new ExtensionRequest("otp",
          parameters,
          NetworkManager.instance.LobbyRoom));
      });

    void ProcessOTPResult(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_otp") return;

      var result = evt.Params["params"].As<ISFSObject>();
      if (result.GetUtfString("code") == "lobby_failed_026")
      { OnError.Invoke(ErrorMessage); return; }

      Session.FirebaseID = _tempFID;
      Session.PhoneNumber = _tempPhoneNumber;
      Scene.Events.OnHaveFirebaseUID.Invoke();
      SFSManager.sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE,
        ProcessOTPResult);
    }

    public void Reset() => OnReset.Invoke();
  }
}
