﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Core;
using Utils;
using System.Linq;

namespace BanCa
{
  public class UpdateVIPLevels : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, PopulateVIPLevels);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, PopulateVIPLevels);

    void PopulateVIPLevels(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_vip_info") return;

      Session.VIPLevels = evt.Params["params"].As<ISFSObject>()
        .GetSFSArray("lstData").Cast<ISFSObject>()
        .Select(sfso => new VIPLevel
        {
          Level = sfso.GetInt("Level"),
          Requirement = sfso.GetInt("requirement")
        })
        .OrderBy(l => l.Level);
    }
  }
}
