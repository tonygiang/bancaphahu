﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Core;
using Utils;
using System.Linq;

namespace BanCa
{
  public class UpdateCurrentVIPGem : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, UpdateVIPLevel);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, UpdateVIPLevel);

    void UpdateVIPLevel(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "update_vip") return;

      Session.CurrentVIPGem.Value = evt.Params["params"].As<ISFSObject>()
        .GetInt("vipExp");
    }
  }
}
