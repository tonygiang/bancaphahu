﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Util;
using Sfs2X.Core;
using Utils;
using System.Linq;

namespace BanCa
{
  public class UpdateSkillList : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, PopulateSkills);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, PopulateSkills);

    void PopulateSkills(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_listSkill") return;

      evt.Params["params"].As<ISFSObject>()
        .GetSFSArray("listData").Cast<ISFSObject>()
        .ForEach(sfsObj =>
        {
          string skillID = sfsObj.GetUtfString("skillId");
          Skill matchingSkill = Session.Skills
            .ElementAtOrFillWith(skillID, new Skill());
          matchingSkill.ID = skillID;
          matchingSkill.Name.Value = sfsObj.GetUtfString("skillName");
          matchingSkill.Charges.Value = sfsObj.GetInt("quantity");
          matchingSkill.Description.Value = sfsObj.GetUtfString("description");
          matchingSkill.Duration.Value = sfsObj.GetLong("timeInSeconds");
        });
    }
  }
}
