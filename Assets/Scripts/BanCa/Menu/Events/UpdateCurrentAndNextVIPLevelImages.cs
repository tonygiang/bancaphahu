﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Utils;

namespace BanCa
{
  public class UpdateCurrentAndNextVIPLevelImages : ExtensionBehaviour<VIP>
  {
    void OnEnable() => Session.CurrentVIPLevel.OnAfterChange += UpdateVIPImages;

    void OnDisable() => Session.CurrentVIPLevel.OnAfterChange -=
      UpdateVIPImages;

    void UpdateVIPImages(int oldVIPLevel, int newVIPLevel)
    {
      BaseComp.VIPCurrentLevelImage.sprite = BaseComp.LevelImages[newVIPLevel];
      BaseComp.VIPNextLevelImage.sprite = BaseComp.LevelImages
        [Session.VIPLevels.FirstOrValue(l => l.Level == newVIPLevel + 1,
          Session.VIPLevels.Last())
          .Level];
    }
  }
}
