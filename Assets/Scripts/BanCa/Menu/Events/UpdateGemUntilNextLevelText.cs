﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

namespace BanCa
{
  public class UpdateGemUntilNextLevelText : ExtensionBehaviour<VIP>
  {
    [SerializeField] [TextArea] string Template = string.Empty;

    void OnEnable() => Session.CurrentVIPGem.OnAfterChange += UpdateText;

    void OnDisable() => Session.CurrentVIPGem.OnAfterChange -= UpdateText;

    void UpdateText(int oldGemValue, int newGemValue) =>
      StartCoroutine(WaitForVIPLevelsPopulated(newGemValue));

    IEnumerator WaitForVIPLevelsPopulated(int newGemValue)
    {
      while (Session.VIPLevels.Count() == 0)
        yield return Utils.Unity.Constants.WaitForFixedUpdate;

      var nextLevel = Session.VIPLevels
        .FirstOrDefault(l => l.Level > Session.CurrentVIPLevel.Value);
      BaseComp.GemUntilNextLevelText.text = nextLevel == null ?
        string.Empty :
        string.Format(Template,
          (nextLevel.Requirement - newGemValue)
            .ToString("N0", Config.MoneyFormat));
      yield break;
    }
  }
}
