﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BanCa
{
  public class ChangeCurrentVIPLevelAccordingToVIPGem : MonoBehaviour
  {
    void OnEnable() =>
      Session.CurrentVIPGem.OnAfterChange += UpdateCurrentVIPLevel;

    void OnDisable() =>
      Session.CurrentVIPGem.OnAfterChange -= UpdateCurrentVIPLevel;

    void UpdateCurrentVIPLevel(int oldGemValue, int newGemValue) =>
      StartCoroutine(WaitForVIPLevelsPopulated(newGemValue));

    IEnumerator WaitForVIPLevelsPopulated(int newGemValue)
    {
      while (Session.VIPLevels.Count() == 0)
        yield return Utils.Unity.Constants.WaitForFixedUpdate;

      Session.CurrentVIPLevel.Value = Session.VIPLevels
        .Where(l => l.Requirement <= newGemValue)
        .Max(l => l.Level);
      yield break;
    }
  }
}
