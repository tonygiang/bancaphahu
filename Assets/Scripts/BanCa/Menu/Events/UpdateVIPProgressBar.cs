﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Utils;

namespace BanCa
{
  public class UpdateVIPProgressBar : ExtensionBehaviour<VIP>
  {
    [SerializeField] bool Incrementing = true;

    void OnEnable() =>
      Session.CurrentVIPGem.OnAfterChange += UpdateFillAmount;

    void OnDisable() =>
      Session.CurrentVIPGem.OnAfterChange -= UpdateFillAmount;

    void UpdateFillAmount(int oldGemValue, int newGemValue) =>
      StartCoroutine(WaitForVIPLevelsPopulated(newGemValue));

    IEnumerator WaitForVIPLevelsPopulated(int newGemValue)
    {
      while (Session.VIPLevels.Count() == 0)
        yield return Utils.Unity.Constants.WaitForFixedUpdate;

      float startingAmount = (float)Session.VIPLevels
        .First(l => l.Level == Session.CurrentVIPLevel.Value).Requirement;
      float endingAmount = (float)Session.VIPLevels
        .FirstOrValue(l => l.Level == Session.CurrentVIPLevel.Value + 1,
          Session.VIPLevels.Last())
        .Requirement;
      float deltaAmount = endingAmount - startingAmount;
      float fillRatio = ((float)newGemValue - startingAmount) / deltaAmount;
      BaseComp.ProgressBarUntilNextLevel.fillAmount =
        Incrementing ? fillRatio : 1 - fillRatio;
      yield break;
    }
  }
}
