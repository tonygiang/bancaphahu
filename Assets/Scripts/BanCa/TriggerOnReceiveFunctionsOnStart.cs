﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class TriggerOnReceiveFunctionsOnStart : MonoBehaviour
  {
    void Start() => Game.Events.OnReceiveFunctions.Invoke(Session.Functions);
  }
}
