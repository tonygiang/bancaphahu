﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DigitalRuby.LightningBolt;

namespace BanCa
{
  public class ShootingGun : MonoBehaviour
  {
    public Transform GunTransform = null;
    public UnityEvent OnThisIsCurrentUserGun = new UnityEvent();
    public GunScreenEdge ScreenEdge = GunScreenEdge.TOP;
    public LightningBoltScript SkillLightning = null;
    public string FollowTargetID = null;
    
    public ShootingGun SetTarget(string fishID)
    {
      FollowTargetID = fishID;
      SkillLightning.EndObject = Game.Shooting.Fishes[fishID].gameObject;
      return this;
    }
  }

  public enum GunScreenEdge { TOP, BOTTOM }
}
