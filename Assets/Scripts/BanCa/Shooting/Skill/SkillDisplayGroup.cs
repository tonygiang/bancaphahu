﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

namespace BanCa
{
  public class SkillDisplayGroup : MonoBehaviour
  {
    public Observable<Skill> BoundSkill =
      new Observable<Skill>(EquatableReference<Skill>.Default);
    public Button Button = null;
    public ImageFunctions CooldownOverlay;
    public BoundedTextInt ChargesText = null;
    public BoundedTextString NameText = null;
    public Image DescriptionIcon = null;
    public BoundedTextString DescriptionText = null;
    public BoundedTextFloat DurationText = null;
    ShootingScene Scene => Game.Shooting;

    void OnDestroy() => BoundSkill.OnAfterChange -= UpdateVisual;

    void Awake()
    {
      BoundSkill.OnAfterChange += UpdateVisual;
      if (BoundSkill.Value != EquatableReference<Skill>.Default)
        UpdateVisual(BoundSkill.Value, BoundSkill.Value);
    }

    void UpdateVisual(Skill oldSkill, Skill newSkill)
    {
      Button.image.sprite = Scene.SkillIcons[newSkill.ID];
      Button.interactable = newSkill.Charges.Value > 0;
      ChargesText.SetBoundedValue(newSkill.Charges);
      NameText.SetBoundedValue(newSkill.Name);
      DescriptionIcon.sprite = Scene.SkillIcons[newSkill.ID];
      DescriptionText.SetBoundedValue(newSkill.Description);
      DurationText.SetBoundedValue(newSkill.Duration);
    }

    public SkillDisplayGroup Bind(string skillID)
    {
      BoundSkill.Value = Session.Skills[skillID];
      return this;
    }

    public void RequestSkill()
    {
      SFSObject parameters = new SFSObject();
      parameters.PutUtfString("username", UserManager.UserName);
      parameters.PutUtfString("skillID", BoundSkill.Value.ID);
      SFSManager.sfs.Send(new ExtensionRequest("request_skill",
        parameters,
        NetworkManager.instance.LobbyRoom));
    }

    public void RequestCancelSkill()
    {
      SFSObject parameters = new SFSObject();
      parameters.PutUtfString("username", UserManager.UserName);
      parameters.PutUtfString("skillID", BoundSkill.Value.ID);
      SFSManager.sfs.Send(new ExtensionRequest("request_cancel_skill",
        parameters,
        NetworkManager.instance.LobbyRoom));
    }
  }
}
