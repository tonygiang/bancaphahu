﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class ChangeSpeedMultiplierDuringSkill : MonoBehaviour
  {
    [SerializeField] string AffectedSkillID = string.Empty;
    [SerializeField] float ValueDuringSkill = 0f;
    float _originalValue = 0f;
    ShootingSceneEvents Events => Game.Shooting.Events;

    void OnEnable()
    {
      Events.OnSkillStart.AddListener(ChangeSpeedMultiplier);
      Events.OnSkillEnd.AddListener(RevertSpeedMultiplier);
    }

    void OnDisable()
    {
      Events.OnSkillStart.RemoveListener(ChangeSpeedMultiplier);
      Events.OnSkillEnd.RemoveListener(RevertSpeedMultiplier);
    }

    void ChangeSpeedMultiplier(SkillTrigger trigger)
    {
      if (trigger.Skill.ID != AffectedSkillID) return;

      _originalValue = FishMovement.SpeedMultiplier;
      FishMovement.SpeedMultiplier = ValueDuringSkill;
    }

    void RevertSpeedMultiplier(SkillTrigger trigger)
    {
      if (trigger.Skill.ID != AffectedSkillID) return;

      FishMovement.SpeedMultiplier = _originalValue;
    }
  }
}
