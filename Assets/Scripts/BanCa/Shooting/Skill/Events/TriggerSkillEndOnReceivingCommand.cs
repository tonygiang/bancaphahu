﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Utils;

namespace BanCa
{
  public class TriggerSkillEndOnReceivingCommand : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, EndSkill);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, EndSkill);
    void EndSkill(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_EndSkill") return;

      ISFSObject sfsObject = evt.Params["params"].As<ISFSObject>();
      Game.Shooting.Events.OnSkillEnd.Invoke(new SkillTrigger
      {
        Skill = Session.Skills[sfsObject.GetUtfString("skillID")],
        TriggeringUser = sfsObject.GetUtfString("username")
      });
    }
  }
}
