﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;
using Spine.Unity;
using System.Collections.Specialized;

namespace BanCa
{
  public class DarkenAllFishDuringSkill : MonoBehaviour
  {
    [SerializeField] string AffectedSkillID = string.Empty;
    ShootingScene Scene => Game.Shooting;

    void OnEnable()
    {
      Scene.Events.OnSkillStart.AddListener(EnableChangeColor);
      Scene.Events.OnSkillEnd.AddListener(DisableChangeColor);
    }

    void OnDisable()
    {
      Scene.Events.OnSkillStart.RemoveListener(EnableChangeColor);
      Scene.Events.OnSkillEnd.RemoveListener(DisableChangeColor);
    }

    void EnableChangeColor(SkillTrigger trigger)
    {
      if (trigger.Skill.ID != AffectedSkillID) return;

      Scene.Fishes.CollectionChanged += ChangeNewFishColor;
      Scene.Fishes.Values.Select(f => f.ske.skeleton)
        .ForEach(s => s.SetColor(Scene.DarkenedFishColor));
    }

    void DisableChangeColor(SkillTrigger trigger)
    {
      if (trigger.Skill.ID != AffectedSkillID) return;

      Scene.Fishes.CollectionChanged -= ChangeNewFishColor;
      Scene.Fishes.Values.Select(f => f.ske.skeleton)
        .ForEach(s => s.SetColor(Scene.NormalFishColor));
    }

    void ChangeNewFishColor(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.Action != NotifyCollectionChangedAction.Add &&
        e.Action != NotifyCollectionChangedAction.Replace) return;

      e.NewItems.Cast<KeyValuePair<string, FishMovement>>()
        .Select(p => p.Value.ske.skeleton)
        .ForEach(s => s.SetColor(Scene.DarkenedFishColor));
    }
  }
}
