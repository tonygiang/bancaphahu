﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.LightningBolt;
using System.Linq;

namespace BanCa
{
  public class BindSkillLightningStartToGunTip :
    ExtensionBehaviour<LightningBoltScript>
  {
    [SerializeField] Transform GunContainer = null;

    void FixedUpdate() => BaseComp.StartObject = GunContainer.Cast<Transform>()
      .First(t => t.gameObject.activeInHierarchy)
      .GetComponentsInChildren<Transform>()
      .First(t => t.gameObject.name == "Gun Tip").gameObject;
  }
}
