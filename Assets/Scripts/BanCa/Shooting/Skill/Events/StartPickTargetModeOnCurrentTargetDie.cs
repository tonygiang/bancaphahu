﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Utils;

namespace BanCa
{
  public class StartPickTargetModeOnCurrentTargetDie : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, TriggerPickTargetMode);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, TriggerPickTargetMode);
    
    void TriggerPickTargetMode(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "fish_die") return;

      ISFSObject sfsObject = evt.Params["params"].As<ISFSObject>();
      if (sfsObject.GetUtfString("id") != Game.Shooting.CurrentUserTargetID)
        return;
      Game.Shooting.Events.OnPickTargetModeStart.Invoke();
    }
  }
}
