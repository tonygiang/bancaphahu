﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class ToggleButtonInteractivityOnSkillEnd :
    ExtensionBehaviour<SkillDisplayGroup>
  {
    [SerializeField] bool ToggleState = true;

    void OnEnable() => Game.Shooting.Events.OnSkillEnd.AddListener(Toggle);

    void OnDisable() => Game.Shooting.Events.OnSkillEnd.RemoveListener(Toggle);

    void Toggle(SkillTrigger trigger)
    {
      if (trigger.Skill != BaseComp.BoundSkill.Value) return;
      if (trigger.TriggeringUser != UserManager.UserName) return;

      BaseComp.Button.interactable = ToggleState;
    }
  }
}
