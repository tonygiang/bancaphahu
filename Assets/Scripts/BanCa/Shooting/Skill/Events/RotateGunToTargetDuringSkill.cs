﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;

namespace BanCa
{
  public class RotateGunToTargetDuringSkill : ExtensionBehaviour<ShootingGun>
  {
    [SerializeField] string TriggeringSkillID = string.Empty;
    [SerializeField] Transform GunContainer = null;
    [SerializeField] Transform OriginAnchor = null;
    ShootingScene Scene => Game.Shooting;

    void OnEnable()
    {
      Scene.Events.OnSkillStart.AddListener(EnableFollow);
      Scene.Events.OnSkillEnd.AddListener(DisableFollow);
    }

    void OnDisable()
    {
      Scene.Events.OnSkillStart.RemoveListener(EnableFollow);
      Scene.Events.OnSkillEnd.RemoveListener(DisableFollow);
    }

    void EnableFollow(SkillTrigger trigger)
    {
      if (trigger.Skill.ID != TriggeringSkillID) return;
      if (Scene.Guns[Scene.Users[trigger.TriggeringUser].Position - 1] !=
        BaseComp) return;

      StartCoroutine(FollowTarget());
    }

    void DisableFollow(SkillTrigger trigger)
    {
      if (trigger.Skill.ID != TriggeringSkillID) return;
      if (Scene.Guns[Scene.Users[trigger.TriggeringUser].Position - 1] !=
        BaseComp) return;

      StopAllCoroutines();
    }

    IEnumerator FollowTarget()
    {
      var wait = Utils.Unity.Constants.WaitForFixedUpdate;
      while (true)
      {
        if (!Scene.Fishes.ContainsKey(BaseComp.FollowTargetID))
        { yield return wait; continue; }

        var gun = GunContainer.Cast<Transform>()
          .First(t => t.gameObject.activeInHierarchy)
          .GetChild(0);
        gun.up = OriginAnchor.position
          .To(Scene.Fishes[BaseComp.FollowTargetID].transform.position);
        yield return wait;
      }
    }
  }
}
