﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BanCa
{
  public class SkillSpecificEvents : ExtensionBehaviour<SkillMenu>
  {
    public string SkillIDToTrigger = string.Empty;
    public UnityEventSkillTrigger OnStart = new UnityEventSkillTrigger();
    public UnityEventSkillTrigger OnEnd = new UnityEventSkillTrigger();
    public UnityEventSkillTrigger OnStartForAll = new UnityEventSkillTrigger();
    public UnityEventSkillTrigger OnEndForAll = new UnityEventSkillTrigger();
    ShootingSceneEvents Events => Game.Shooting.Events;

    void OnEnable()
    {
      Events.OnSkillStart.AddListener(StartSkill);
      Events.OnSkillEnd.AddListener(EndSkill);
    }

    void OnDisable()
    {
      Events.OnSkillStart.RemoveListener(StartSkill);
      Events.OnSkillEnd.RemoveListener(EndSkill);
    }

    void StartSkill(SkillTrigger trigger)
    {
      if (SkillIDToTrigger != trigger.Skill.ID) return;
      OnStartForAll.Invoke(trigger);
      if (UserManager.UserName != trigger.TriggeringUser) return;
      OnStart.Invoke(trigger);
    }

    void EndSkill(SkillTrigger trigger)
    {
      if (SkillIDToTrigger != trigger.Skill.ID) return;
      OnEndForAll.Invoke(trigger);
      if (UserManager.UserName != trigger.TriggeringUser) return;
      OnEnd.Invoke(trigger);
    }
  }
}
