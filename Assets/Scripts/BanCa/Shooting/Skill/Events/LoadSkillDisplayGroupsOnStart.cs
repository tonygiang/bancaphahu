﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;

namespace BanCa
{
  public class LoadSkillDisplayGroupsOnStart : ExtensionBehaviour<SkillMenu>
  {
    [SerializeField] SkillDisplayGroup SkillDisplayGroupPrefab = null;
    [SerializeField] Transform Container = null;

    void Start() => BaseComp.SkillDisplays = Session.Skills.Values.ToList()
      .ConvertAll(s => Instantiate(SkillDisplayGroupPrefab, Container)
        .Bind(s.ID));
  }
}
