﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Utils;

namespace BanCa
{
  public class TriggerSkillStartOnReceivingCommand : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, TriggerSkill);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, TriggerSkill);

    void TriggerSkill(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_StartSkill") return;

      ISFSObject sfsObject = evt.Params["params"].As<ISFSObject>();
      Game.Shooting.Events.OnSkillStart.Invoke(new SkillTrigger
      {
        Skill = Session.Skills[sfsObject.GetUtfString("skillID")],
        TriggeringUser = sfsObject.GetUtfString("username"),
        Duration = sfsObject.GetLong("timeInSeconds")
      });
    }
  }
}
