﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;
using Spine.Unity;
using System.Collections.Specialized;

namespace BanCa
{
  public class HighlightTargetFishOnPickTargetModeEnd : MonoBehaviour
  {
    ShootingScene Scene => Game.Shooting;

    void OnEnable() =>
      Scene.Events.OnPickTargetModeEnd.AddListener(HighlightTarget);

    void OnDisable() =>
      Scene.Events.OnPickTargetModeEnd.RemoveListener(HighlightTarget);

    void HighlightTarget(string targetID)
    {
      Scene.Fishes.Values.Select(f => f.ske.skeleton)
        .ForEach(s => s.SetColor(Scene.DarkenedFishColor));
      Scene.Fishes.Where(p => p.Key == targetID)
        .Select(p => p.Value.ske.skeleton)
        .ForEach(s => s.SetColor(Scene.NormalFishColor));
    }
  }
}
