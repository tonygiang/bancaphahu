﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class ToggleButtonInteractivityOnSkillStart :
    ExtensionBehaviour<SkillDisplayGroup>
  {
    [SerializeField] bool ToggleState = false;

    void OnEnable() => Game.Shooting.Events.OnSkillStart.AddListener(Toggle);

    void OnDisable() => Game.Shooting.Events.OnSkillStart
      .RemoveListener(Toggle);

    void Toggle(SkillTrigger trigger)
    {
      if (trigger.Skill != BaseComp.BoundSkill.Value) return;
      if (trigger.TriggeringUser != UserManager.UserName) return;

      BaseComp.Button.interactable = ToggleState;
    }
  }
}
