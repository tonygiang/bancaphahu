﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Utils;

namespace BanCa
{
  public class SkillMenu : MonoBehaviour
  {
    public IEnumerable<SkillDisplayGroup> SkillDisplays =
      Enumerable.Empty<SkillDisplayGroup>();

    public void EnableSkillButtonInteractable(string skillID) =>
      SkillDisplays.Where(e => e.BoundSkill.Value.ID == skillID).First()?
        .PassTo(e => e.Button.interactable = true);

    public void DisableSkillButtonInteractable(string skillID) =>
      SkillDisplays.Where(e => e.BoundSkill.Value.ID == skillID).First()?
        .PassTo(e => e.Button.interactable = false);

    public void StartCooldown(SkillTrigger trigger) => SkillDisplays
      .First(sd => sd.BoundSkill.Value == trigger.Skill)
      .CooldownOverlay
      .DecreaseFillAmountOverTime(trigger.Skill.Duration.Value);
  }
}
