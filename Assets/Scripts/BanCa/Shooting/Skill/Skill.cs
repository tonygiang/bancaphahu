﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

namespace BanCa
{
  public class Skill : EquatableReference<Skill>
  {
    public string ID = string.Empty;
    public Observable<string> Name = new Observable<string>(string.Empty);
    public Observable<int> Charges = new Observable<int>(-1);
    public Observable<string> Description =
      new Observable<string>(string.Empty);
    public Observable<float> Duration = new Observable<float>(0f);
  }

  public struct SkillTrigger
  {
    public Skill Skill;
    public string TriggeringUser;
    public float Duration;
  }
}
