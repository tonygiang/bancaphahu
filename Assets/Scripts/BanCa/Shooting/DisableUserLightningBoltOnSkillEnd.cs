﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BanCa
{
  public class DisableUserLightningBoltOnSkillEnd : MonoBehaviour
  {
    [SerializeField] string AffectedSkillID = string.Empty;
    ShootingScene Scene => Game.Shooting;

    void OnEnable() => Scene.Events.OnSkillEnd.AddListener(DisableLightning);

    void OnDisable() => Scene.Events.OnSkillEnd.RemoveListener(DisableLightning);

    void DisableLightning(SkillTrigger trigger)
    {
      if (trigger.Skill.ID != AffectedSkillID) return;

      ShootingGun gun = Scene
        .Guns[Scene.Users[trigger.TriggeringUser].Position -1];
      gun.SkillLightning.gameObject.SetActive(false);
    }
  }
}
