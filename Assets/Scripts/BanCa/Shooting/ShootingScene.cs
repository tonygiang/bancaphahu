﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace BanCa
{
  public class ShootingScene : MonoBehaviour
  {
    public ShootingSceneEvents Events = null;
    public ShootingGun[] Guns = Array.Empty<ShootingGun>();
    public UserInRoom CurrentUser => Users[UserManager.UserName];
    public ShootingGun CurrentUserGun => Guns[CurrentUser.Position - 1];
    public Dictionary<string, UserInRoom> Users =
      new Dictionary<string, UserInRoom>();
    public ObservableDictionary<string, FishMovement> Fishes =
      new ObservableDictionary<string, FishMovement>();
    public ShootingSceneType RoomType = ShootingSceneType.COIN;
    public static ILookup<ShootingSceneType, string> RoomBulletTypes = new[]
    {
      new { Key = ShootingSceneType.COIN, Value = "bullet100"},
      new { Key = ShootingSceneType.COIN, Value = "bullet200"},
      new { Key = ShootingSceneType.COIN, Value = "bullet500"},
      new { Key = ShootingSceneType.COIN, Value = "bullet800"},
      new { Key = ShootingSceneType.GEM, Value = "bullet1000"},
      new { Key = ShootingSceneType.GEM, Value = "bullet2000"},
      new { Key = ShootingSceneType.GEM, Value = "bullet3000"},
      new { Key = ShootingSceneType.GEM, Value = "bullet5000"},
      new { Key = ShootingSceneType.GEM, Value = "bullet8000"},
      new { Key = ShootingSceneType.GEM, Value = "bullet10000"}
    }.ToLookup(pair => pair.Key, pair => pair.Value);
    public IEnumerable<string> CurrentRoomBulletTypes =>
      RoomBulletTypes[RoomType];
    public static ILookup<ShootingSceneType, int> RoomBulletValues = new[]
    {
      new { Key = ShootingSceneType.COIN, Value = 100},
      new { Key = ShootingSceneType.COIN, Value = 200},
      new { Key = ShootingSceneType.COIN, Value = 500},
      new { Key = ShootingSceneType.COIN, Value = 800},
      new { Key = ShootingSceneType.GEM, Value = 1000},
      new { Key = ShootingSceneType.GEM, Value = 2000},
      new { Key = ShootingSceneType.GEM, Value = 3000},
      new { Key = ShootingSceneType.GEM, Value = 5000},
      new { Key = ShootingSceneType.GEM, Value = 8000},
      new { Key = ShootingSceneType.GEM, Value = 10000}
    }.ToLookup(pair => pair.Key, pair => pair.Value);
    public IEnumerable<int> CurrentRoomBulletValues =>
      RoomBulletValues[RoomType];
    public int CurrentGunType = 0;
    public float SecondsBetweenShots = 0.25f;
    [HideInInspector] public float SecondsTillNextShot = 0;
    public StringSpriteDictionary SkillIcons = new StringSpriteDictionary();
    public string CurrentUserTargetID = string.Empty;
    public Color DarkenedFishColor = Color.white;
    public Color NormalFishColor = Color.white;

    void Awake() => Game.Shooting = this;

    void Update() => SecondsTillNextShot -= Time.deltaTime;

    public void SetCurrentTargetID(string targetID) =>
      CurrentUserTargetID = targetID;
  }

  public enum ShootingSceneType
  {
    COIN,
    GEM
  }
}
