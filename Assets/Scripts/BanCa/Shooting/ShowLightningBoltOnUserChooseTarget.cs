﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Utils;

namespace BanCa
{
  public class ShowLightningBoltOnUserChooseTarget : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, EnableLightning);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, EnableLightning);

    ShootingScene Scene => Game.Shooting;

    void EnableLightning(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_chooseTarget") return;

      ISFSObject sfsObject = evt.Params["params"].As<ISFSObject>();
      UserInRoom triggeringUser = Scene.Users[sfsObject.GetUtfString("user")];
      ShootingGun triggeringGun = Scene.Guns[triggeringUser.Position - 1];
      triggeringGun.SetTarget(sfsObject.GetUtfString("fishId"));
      triggeringGun.SkillLightning.gameObject.SetActive(true);
    }
  }
}
