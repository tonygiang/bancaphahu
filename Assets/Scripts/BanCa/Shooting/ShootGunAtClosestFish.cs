﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;

namespace BanCa
{
  [DisallowMultipleComponent]
  public class ShootGunAtClosestFish : MonoBehaviour
  {
    void OnEnable() => StartCoroutine(Shoot());

    void OnDisable()
    {
      StopAllCoroutines();
      _currentTargetID = "null_key";
    }
    string _currentTargetID = "null_key";

    ShootingScene Scene => Game.Shooting;

    float GunAngleToFish(FishMovement fish) => Vector3.right
      .SignedAngleTo(Scene.CurrentUserGun.GunTransform.position
        .To(fish.transform.position));

    IEnumerator Shoot()
    {
      var wait = new WaitForSeconds(Game.Shooting.SecondsBetweenShots);
      while (true)
      {
        if (Scene.SecondsTillNextShot > 0f) { yield return wait; continue; }
        if (SFSManager.instance.isLobby) { yield return wait; continue; }
        if (BanCa.Session.Gem.Value < Scene.CurrentRoomBulletValues
          .ElementAt(Scene.CurrentUser.BulletType))
        {
          MessageBox.instance.ShowGoToShop("THÔNG BÁO", "Bạn không đủ kim cương. Nạp thêm kim cương hoặc nhận quà hằng ngày để tiếp tục chơi game nhé!",
            MessageBoxType.OK_CANCEL,
            result =>
            {
              if (result.Equals(MessageBoxCallBack.OK))
                Shop.Instance.OpenShop();
            });
          Scene.Events.OnEndAutoShoot.Invoke();
          yield break;
        }

        var shootableFishes = Scene.Fishes.Where(p =>
        {
          var angleToFish = GunAngleToFish(p.Value);
          return Scene.CurrentUserGun.ScreenEdge == GunScreenEdge.TOP ?
            angleToFish < 0f :
            angleToFish > 0f;
        }).ToDictionary(p => p.Key, p => p.Value);
        if (shootableFishes.Count() == 0) { yield return wait; continue; }

        if (!shootableFishes.ContainsKey(_currentTargetID))
          _currentTargetID = shootableFishes
            .OrderBy(p => GunAngleToFish(p.Value))
            .First().Key;

        var gunToTargetAngle = GunAngleToFish(Scene.Fishes[_currentTargetID]);

        Scene.SecondsTillNextShot = Scene.SecondsBetweenShots;
        SFSManager.instance.USER_SHOOT(gunToTargetAngle,
          Scene.CurrentRoomBulletTypes
            .ElementAt(Scene.Users[UserManager.UserName].BulletType));
        yield return wait;
      }
    }
  }
}
