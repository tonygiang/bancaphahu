﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Utils;

namespace BanCa
{
  public class DisableLightningBoltOnFishDie : MonoBehaviour
  {
    ShootingScene Scene => Game.Shooting;

    void OnEnable() => Scene.Events.OnFishDie.AddListener(DisableLightningBolt);

    void OnDisable() =>
      Scene.Events.OnFishDie.RemoveListener(DisableLightningBolt);

    void DisableLightningBolt(string fishID) =>
      Scene.Guns.Where(g => g.FollowTargetID == fishID)
        .ForEach(g => g.SkillLightning.gameObject.SetActive(false));
  }
}
