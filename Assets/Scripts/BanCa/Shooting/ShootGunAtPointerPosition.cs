﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Utils;
using System.Linq;

namespace BanCa
{
  public class ShootGunAtPointerPosition : MonoBehaviour,
    IPointerDownHandler,
    IPointerUpHandler,
    IDragHandler
  {
    PointerEventData _latestPointer = null;

    public void OnPointerDown(PointerEventData data)
    {
      _latestPointer = data;
      StartCoroutine(Shoot());
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
      _latestPointer = pointerEventData;
      StopAllCoroutines();
    }

    public void OnDrag(PointerEventData data) => _latestPointer = data;

    void OnDisable() => StopAllCoroutines();

    ShootingScene Scene => Game.Shooting;

    IEnumerator Shoot()
    {
      var wait = new WaitForSeconds(Scene.SecondsBetweenShots);
      while (true)
      {
        if (Scene.SecondsTillNextShot < 0f &&
          !SFSManager.instance.isLobby)
        {
          if (Session.Gem.Value < Scene.CurrentRoomBulletValues
            .ElementAt(Scene.CurrentUser.BulletType))
          {
            MessageBox.instance.ShowGoToShop("THÔNG BÁO", "Bạn không đủ kim cương. Nạp thêm kim cương hoặc nhận quà hằng ngày để tiếp tục chơi game nhé!",
              MessageBoxType.OK_CANCEL,
              result =>
              {
                if (result.Equals(MessageBoxCallBack.OK))
                  Shop.Instance.OpenShop();
              });
            yield break;
          }
          Scene.SecondsTillNextShot = Scene.SecondsBetweenShots;
          float gunToPointerAngle = Scene.CurrentUserGun.GunTransform.position
            .PassTo(Camera.main.WorldToScreenPoint)
            .ToVector2()
            .To(_latestPointer.position)
            .MakeTupleAppend((Vector2)Vector3.right)
            .PassTo(Vector2.SignedAngle);

          SFSManager.instance.USER_SHOOT(gunToPointerAngle,
            Scene.CurrentRoomBulletTypes
              .ElementAt(Scene.Users[UserManager.UserName].BulletType));
        }
        yield return wait;
      }
    }
  }
}
