﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Entities.Data;
using Sfs2X.Core;
using Utils;

namespace BanCa
{
  public class UpdateUserCoinInRoomGem : MonoBehaviour
  {
    void OnEnable() => SFSManager.sfs
      .AddEventListener(SFSEvent.EXTENSION_RESPONSE, UpdateCoin);

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, UpdateCoin);

    ShootingScene Scene => Game.Shooting;

    void UpdateCoin(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_update_coin_room_ca") return;

      ISFSObject sfsObject = evt.Params["params"].As<ISFSObject>();
      string usernameToUpdate = sfsObject.GetUtfString("username");
      int gemAmount = sfsObject.GetInt("gem");
      if (usernameToUpdate == UserManager.UserName)
        Session.Gem.Value = gemAmount;
      else Scene.Users[usernameToUpdate].Coin.Value = gemAmount;
    }
  }
}
