﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

namespace BanCa
{
  public class ShootingUserPanel : MonoBehaviour
  {
    public Image NormalAvatar = null;
    public SkeletonGraphic VIPAvatar = null;
    public BoundedTextString DisplayNameText = null;
    public BoundedTextInt UserGemText = null;
    public Image PanelImage = null;
  }
}
