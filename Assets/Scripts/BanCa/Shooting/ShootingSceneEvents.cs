﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BanCa
{
  public class ShootingSceneEvents : MonoBehaviour
  {
    public UnityEvent OnJackpot = new UnityEvent();
    public UnityEvent OnStartAutoShoot = new UnityEvent();
    public UnityEvent OnEndAutoShoot = new UnityEvent();
    public UnityEventSkillTrigger OnSkillStart = new UnityEventSkillTrigger();
    public UnityEventSkillTrigger OnSkillEnd = new UnityEventSkillTrigger();
    public UnityEvent OnPickTargetModeStart = new UnityEvent();
    public UnityEventString OnPickTargetModeEnd = new UnityEventString();
    public UnityEventString OnFishDie = new UnityEventString();

    void Awake() => Game.Shooting.Events = this;

    public void StartAutoShoot() => OnStartAutoShoot.Invoke();

    public void EndAutoShoot() => OnEndAutoShoot.Invoke();

    public void StartPickTargetMode() => OnPickTargetModeStart.Invoke();
  }
}
