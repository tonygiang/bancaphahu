﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;

namespace BanCa
{
  public class Config
  {
    static readonly Dictionary<Platform, string> _brands =
      new Dictionary<Platform, string>
      {
        {Platform.ANDROID, "android_bancaphahu"},
        {Platform.IOS, "ios_cakiem"},
        {Platform.WEBGL, "webgl_cakiem"},
        {Platform.DEFAULT, "android_bancaphahu"}
      };
    public static string Brand => _brands[AppInfo.CurrentPlatform];
    static readonly Dictionary<Platform, int> _codeVersions =
      new Dictionary<Platform, int>
      {
        {Platform.ANDROID, 3},
        {Platform.IOS, 3},
        {Platform.WEBGL, 3},
        {Platform.DEFAULT, 3}
      };
    public static int CodeVersion => _codeVersions[AppInfo.CurrentPlatform];

    public static NumberFormatInfo MoneyFormat =
      new NumberFormatInfo { NumberGroupSeparator = "." };
  }
}
