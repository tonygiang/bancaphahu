﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Globalization;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(Text))]
  public class GemText : BoundedTextInt
  {
    protected override void Awake()
    {
      base.Awake();
      FormatSpecifier = "N0";
      FormatProvider = Config.MoneyFormat;
      BoundedValue = Session.Gem;
    }
  }
}