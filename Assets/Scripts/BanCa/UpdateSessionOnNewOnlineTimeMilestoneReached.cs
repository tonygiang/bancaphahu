﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;
using Utils;
using System.Linq;

namespace BanCa
{
  public class UpdateSessionOnNewOnlineTimeMilestoneReached : MonoBehaviour
  {
    void Start() => SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE,
      UpdateSession);

    void OnDestroy() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, UpdateSession);

    void UpdateSession(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_reward_online_gift") return;

      ISFSObject isfso = evt.Params["params"].As<ISFSObject>();
      Session.Gem.Value = isfso.GetInt("gem");
      Session.TreasureKeys.Value = isfso.GetInt("key");
      Session.OnlineMinutes.Value = isfso.GetLong("minutes");
      Game.Events.OnNewOnlineTimeMilestoneReached
        .Invoke(Gifts.OnlineTimeMilestones
          .First(m => m.Minutes == isfso.GetLong("minutes")));
      isfso.GetSFSObject("objskill").ToDictionary<int>()
        .ForEach(p => Session.Skills[p.Key].Charges.Value = p.Value);
    }
  }
}
