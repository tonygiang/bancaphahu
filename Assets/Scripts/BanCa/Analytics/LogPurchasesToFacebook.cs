﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using Sfs2X.Core;
using Sfs2X.Entities.Data;
using Utils;
using System.Linq;

namespace BanCa
{
  [DisallowMultipleComponent]
  [RequireComponent(typeof(DontDestroyOnLoad))]
  public class LogPurchasesToFacebook : MonoBehaviour
  {
    [SerializeField] string CurrencyString = string.Empty;

    void OnEnable()
    {
      SFSManager.sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE,
        LogPurchase);
    }

    void OnDisable() => SFSManager.sfs
      .RemoveEventListener(SFSEvent.EXTENSION_RESPONSE,
        LogPurchase);

    void LogPurchase(BaseEvent evt)
    {
      if (evt.Params["cmd"].ToString() != "res_mail") return;
      if (!FB.IsLoggedIn) return;
      var obj = evt.Params["params"].As<ISFSObject>();
      if (!obj.ContainsKey("IsPurcharseSuccess")) return;
      if (!obj.GetBool("IsPurcharseSuccess")) return;
      FB.LogPurchase((decimal)obj.GetInt("card"), CurrencyString, null);
    }
  }
}
