//
//  SendMessPlugin.m
//  SendMessPlugin
//
//  Created by Vu Nguyen on 1/15/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

#import "SendMessPlugin.h"

@interface SendMessPlugin()

+ (SendMessPlugin*) instance;

@end

@implementation SendMessPlugin

+ (SendMessPlugin*) instance {
    static SendMessPlugin* helper = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken, ^{
        helper = [[SendMessPlugin alloc] init];
    });
    return helper;
}

- (void)SendMess:(NSString*)recipent :(NSString*)body{
    
    MFMessageComposeViewController *viewController = [[MFMessageComposeViewController alloc] init];
    if (viewController)
    {
        viewController.body = body;
        viewController.recipients = @[recipent];
        viewController.messageComposeDelegate = self;
        [UnityGetGLViewController() presentViewController:viewController animated:YES completion:nil];
    }
}

- (void)PhoneCall:(NSString*)number{
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", number]]];
}

- (void)OpenLink:(NSString*)urlString{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [UnityGetGLViewController() dismissViewControllerAnimated:YES completion:nil];
    
    switch (result) {
        case MessageComposeResultCancelled:
            
            break;
            
        case MessageComposeResultFailed:
            
            break;
            
        case MessageComposeResultSent:
            
            break;
            
        default:
            break;
    }
}

- (char*)GetCode
{
    CTTelephonyNetworkInfo *phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *phoneCarrier = [phoneInfo subscriberCellularProvider];
    return (char*)[[phoneCarrier mobileCountryCode] UTF8String];
}

- (char*)GetName
{
    CTTelephonyNetworkInfo *phoneInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *phoneCarrier = [phoneInfo subscriberCellularProvider];
    return (char*)[[phoneCarrier carrierName] UTF8String];
}

@end


static NSString* CreateNSString(const char* string)
{
    if (string != NULL)
        return [NSString stringWithUTF8String:string];
    else
        return [NSString stringWithUTF8String:""];
}

char* cStringCopy(const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    
    return res;
}

extern "C"
{
    void OpenMessage(const char* num, const char* body)
    {
        NSString* rec = CreateNSString(num);
        NSString* bod = CreateNSString(body);
        
        [[SendMessPlugin instance] SendMess:rec :bod ];
    }
    
    void OpenPhone(const char* phoneNumber)
    {
        NSString* num = CreateNSString(phoneNumber);
        
        [[SendMessPlugin instance] PhoneCall:num];
    }
    
    void OpenURL(const char* url)
    {
        NSString* stringURL = CreateNSString(url);
        [[SendMessPlugin instance] OpenLink:stringURL];
    }
    
    char* GetCarrierCode()
    {
        return cStringCopy([[SendMessPlugin instance] GetCode]);
    }
    
    char* GetCarrierName()
    {
        return cStringCopy([[SendMessPlugin instance] GetName]);
    }
}
