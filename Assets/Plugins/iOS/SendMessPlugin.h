//
//  SendMessPlugin.h
//  SendMessPlugin
//
//  Created by Vu Nguyen on 1/15/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface SendMessPlugin : NSObject <MFMessageComposeViewControllerDelegate>

- (void)SendMess:(NSString*)recipent : (NSString*)body;
- (void)OpenLink:(NSString*)urlString;
- (void)PhoneCall:(NSString*)number;

@end

extern "C"
{
    void OpenMessage(const char* number, const char* body);
    
    void OpenPhone(const char* phoneNumber);

    void OpenURL(const char* url);
    
    char* GetCarrierCode();

    char* GetCarrierName();
}
