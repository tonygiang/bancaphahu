
sung db4.png
size: 134,134
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 53
  rotate: false
  xy: 107, 45
  size: 25, 25
  orig: 25, 25
  offset: 0, 0
  index: -1
Layer 58
  rotate: false
  xy: 2, 18
  size: 32, 114
  orig: 32, 114
  offset: 0, 0
  index: -1
Layer 58 copy
  rotate: false
  xy: 36, 18
  size: 32, 114
  orig: 32, 114
  offset: 0, 0
  index: -1
Layer 59
  rotate: false
  xy: 70, 72
  size: 60, 60
  orig: 60, 60
  offset: 0, 0
  index: -1
Layer 84
  rotate: true
  xy: 70, 5
  size: 65, 35
  orig: 65, 35
  offset: 0, 0
  index: -1
