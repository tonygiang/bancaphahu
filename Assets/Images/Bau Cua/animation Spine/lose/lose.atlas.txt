
lose.png
size: 205,205
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: true
  xy: 84, 2
  size: 72, 81
  orig: 74, 83
  offset: 1, 1
  index: -1
2
  rotate: false
  xy: 2, 39
  size: 80, 81
  orig: 82, 83
  offset: 1, 1
  index: -1
3
  rotate: true
  xy: 86, 125
  size: 78, 83
  orig: 80, 85
  offset: 1, 1
  index: -1
4
  rotate: false
  xy: 2, 122
  size: 82, 81
  orig: 84, 83
  offset: 1, 1
  index: -1
5
  rotate: true
  xy: 86, 76
  size: 47, 84
  orig: 49, 86
  offset: 1, 1
  index: -1
