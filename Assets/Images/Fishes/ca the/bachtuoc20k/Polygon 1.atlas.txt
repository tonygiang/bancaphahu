
Polygon 1.png
size: 552,552
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 13 copy
  rotate: false
  xy: 2, 260
  size: 248, 290
  orig: 248, 290
  offset: 0, 0
  index: -1
Layer 13 copy - Copy
  rotate: true
  xy: 2, 10
  size: 248, 290
  orig: 248, 290
  offset: 0, 0
  index: -1
Layer 20
  rotate: true
  xy: 461, 208
  size: 123, 79
  orig: 123, 79
  offset: 0, 0
  index: -1
Layer 22 copy
  rotate: false
  xy: 406, 333
  size: 106, 92
  orig: 106, 92
  offset: 0, 0
  index: -1
Layer 22 copy - Copy
  rotate: false
  xy: 294, 176
  size: 106, 92
  orig: 106, 92
  offset: 0, 0
  index: -1
Layer 23 copy
  rotate: false
  xy: 335, 352
  size: 69, 144
  orig: 69, 144
  offset: 0, 0
  index: -1
Layer 23 copy - Copy
  rotate: true
  xy: 406, 427
  size: 69, 144
  orig: 69, 144
  offset: 0, 0
  index: -1
Layer 24 copy
  rotate: true
  xy: 252, 270
  size: 52, 207
  orig: 52, 207
  offset: 0, 0
  index: -1
Layer 24 copy - Copy
  rotate: true
  xy: 335, 498
  size: 52, 207
  orig: 52, 207
  offset: 0, 0
  index: -1
Layer 25
  rotate: true
  xy: 402, 190
  size: 78, 39
  orig: 78, 39
  offset: 0, 0
  index: -1
Layer 25 - Copy
  rotate: true
  xy: 402, 110
  size: 78, 39
  orig: 78, 39
  offset: 0, 0
  index: -1
Layer 26
  rotate: true
  xy: 443, 64
  size: 142, 68
  orig: 142, 68
  offset: 0, 0
  index: -1
Layer 26 - Copy
  rotate: false
  xy: 294, 40
  size: 142, 68
  orig: 142, 68
  offset: 0, 0
  index: -1
Layer 27
  rotate: true
  xy: 252, 324
  size: 226, 81
  orig: 226, 81
  offset: 0, 0
  index: -1
